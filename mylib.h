/*
File:   mylib.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** Mylib is a collection of files that contain code that can be used on multiple
	** projects without much work to integrate or overhead to include.
	** This file is intended to be a quick way of including all the default
	** classes in this project with one #include
	
NOTE: WINDOWS_COMPILATION, OSX_COMPILATION, or LINUX_COMPILATION must be defined before including this file
*/

#ifndef _MYLIB_H
#define _MYLIB_H

#include "my_platform.h"

#if WINDOWS_COMPILATION
	#include <windows.h> //NOTE: windows.h is included to make sure we have the required items to fill out the macros below. You can include my_platform.h first and include windows.h yourself if needed
	#define PACKED(class_to_pack) __pragma( pack(push, 1) ) class_to_pack __pragma(pack(pop))
	#define START_PACK  __pragma(pack(push, 1))
	#define END_PACK    __pragma(pack(pop))
	#define ATTR_PACKED //nothing
	#define EXPORT __declspec(dllexport)
	#define IMPORT __declspec(dllimport)
	// #if defined WIN32 //TODO: Do we need this check?
		#define __func__ __FUNCTION__
	// #endif
#elif OSX_COMPILATION
	#define PACKED(class_to_pack) class_to_pack __attribute__((__packed__))
	#define START_PACK  //nothing
	#define END_PACK    //nothing
	#define ATTR_PACKED __attribute__((__packed__))
	//TODO: Figure out how to do EXPORT and IMPORT on OSX
#else
	#define PACKED(class_to_pack) class_to_pack __attribute__((__packed__))
	#define START_PACK  //nothing
	#define END_PACK    //nothing
	#define ATTR_PACKED __attribute__((__packed__))
	//TODO: Figure out how to do EXPORT and IMPORT on OSX
#endif

#define QUOTE(string) #string

// +--------------------------------------------------------------+
// |                  Standard Library Includes                   |
// +--------------------------------------------------------------+
#include "mylib_std.h"

// +--------------------------------------------------------------+
// |                          Constants                           |
// +--------------------------------------------------------------+
#define Pi32 3.1415926535897932384626433832795f
#define Pi64 3.1415926535897932384626433832795
#define e32  2.718281828459045f
#define e64  2.718281828459045

#define Sample16_MaxValue 32767

// +--------------------------------------------------------------+
// |                           Typedefs                           |
// +--------------------------------------------------------------+
typedef uint8_t     uint8;
typedef uint16_t    uint16;
typedef uint32_t    uint32;
typedef uint64_t    uint64;

typedef int8_t 	    int8;
typedef int16_t     int16;
typedef int32_t     int32;
typedef int64_t     int64;

typedef int32_t	    bool32;

typedef float       real32;
typedef double      real64;

typedef uint8_t     flags8;
typedef uint16_t    flags16;
typedef uint32_t    flags32;

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

typedef real32      r32;
typedef real64      r64;

// +--------------------------------------------------------------+
// |                            Macros                            |
// +--------------------------------------------------------------+
#define ToRadians(degrees)		(degrees/180.0f * Pi32)
#define ToDegrees(radians)		(radians/Pi32 * 180.0f)

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

//NOTE: Assert(expression) is now defined in my_assert.h

#define Kilobytes(value) ((value) * 1024UL)
#define Megabytes(value) (Kilobytes((value)) * 1024UL)
#define Gigabytes(value) (Megabytes((value)) * 1024UL)

#define IsFlagSet(BitwiseField, Bit) (((BitwiseField) & (Bit)) != 0)
#define FlagSet(BitwiseField, Bit)   (BitwiseField) |= (Bit)
#define FlagUnset(BitwiseField, Bit) (BitwiseField) &= ~(Bit)
#define FlagToggle(BitwiseField, Bit) ((BitwiseField) ^= (Bit))
#define FlagSetTo(BitwiseField, Bit, condition) if (condition) { FlagSet((BitwiseField), (Bit)); } else { FlagUnset((BitwiseField), (Bit)); }

#define ClearArray(Array)      MyMemSet((Array), '\0', sizeof((Array)))
#define ClearStruct(Structure) MyMemSet(&(Structure), '\0', sizeof((Structure)))
#define ClearPointer(Pointer)  MyMemSet((Pointer), '\0', sizeof(*(Pointer)));

#define ReverseByteArray(array, size) do                  \
{                                                         \
	u8 _tempByte; u32 _bIndex = 0;                        \
	while (_bIndex < (u32)(size)/2)                       \
	{                                                     \
		_tempByte = (array)[_bIndex];                     \
		(array)[_bIndex] = (array)[(size) - 1 - _bIndex]; \
		(array)[(size) - 1 - _bIndex] = _tempByte;        \
		_bIndex++;                                        \
	}                                                     \
} while(0)

#define Increment(variable, max)           if ((variable) < (max)) { (variable)++; } else { (variable) = (max); }
#define IncrementU8(variable)              if ((variable) < 0xFF) { (variable)++; } else { (variable) = 0xFF; }
#define IncrementU16(variable)             if ((variable) < 0xFFFF) { (variable)++; } else { (variable) = 0xFFFF; }
#define IncrementU32(variable)             if ((variable) < 0xFFFFFFFF) { (variable)++; } else { (variable) = 0xFFFFFFFF; }
#define IncrementBy(variable, amount, max) if ((variable) + (amount) < (max) && (variable) + (amount) > (variable)) { (variable) += (amount); } else { (variable) = (max); }
#define IncrementU8By(variable, amount)    if ((variable) + (amount) < 0xFF && (variable) + (amount) > (variable)) { (variable) += (amount); } else { (variable) = 0xFF; }
#define IncrementU16By(variable, amount)   if ((variable) + (amount) < 0xFFFF && (variable) + (amount) > (variable)) { (variable) += (amount); } else { (variable) = 0xFFFF; }
#define IncrementU32By(variable, amount)   if ((variable) + (amount) < 0xFFFFFFFF && (variable) + (amount) > (variable)) { (variable) += (amount); } else { (variable) = 0xFFFFFFFF; }
#define Decrement(variable)                if ((variable) > 0) { (variable)--; } else { (variable) = 0; }
#define DecrementBy(variable, amount)      if ((variable) >= (amount)) { (variable) -= (amount); } else { (variable) = 0; }

#define IsVersionBelow(versionMajor, versionMinor, numberMajor, numberMinor) (((versionMajor) < (numberMajor)) || ((versionMajor) == (numberMajor) && (versionMinor) < (numberMinor)))
#define IsVersionAbove(versionMajor, versionMinor, numberMajor, numberMinor) (((versionMajor) > (numberMajor)) || ((versionMajor) == (numberMajor) && (versionMinor) > (numberMinor)))

#define IsEqualXor(variable1, variable2, condition1, condition2) (((variable1) == (condition1) && (variable2) == (condition2)) || ((variable1) == (condition2) && (variable2) == (condition1)))

#define FlipEndianU32(variable) variable = (((*(((const u8*)&(variable)) + 0)) & 0xFF) << 24) | (((*(((const u8*)&(variable)) + 1)) & 0xFF) << 16) | (((*(((const u8*)&(variable)) + 2)) & 0xFF) << 8) | (((*(((const u8*)&(variable)) + 3)) & 0xFF) << 0);
#define OnesComplimentU32(variable) (variable ^ 0xFFFFFFFFL)

#define STRUCT_VAR_SIZE(structureName, variableName) sizeof(((const structureName*)0)->variableName)
#define STRUCT_VAR_OFFSET(structureName, variableName) (u32)((const u8*)&((const structureName*)0)->variableName - (const u8*)((const structureName*)0))
#define STRUCT_VAR_END_OFFSET(structureName, variableName) (u32)(((const u8*)&((const structureName*)0)->variableName + sizeof(((const structureName*)0)->variableName)) - (const u8*)((const structureName*)0))
#define IS_VAR_IN_X_BYTES_OF_STRUCT(structureName, numBytes, variableName) ((numBytes) >= STRUCT_VAR_END_OFFSET((structureName), (variableName)))

// +--------------------------------------------------------------+
// |                     Forward Declarations                     |
// +--------------------------------------------------------------+
//my_stringManip.h
char* GetFileNamePart(char* filePath);
const char* GetFileNamePart(const char* filePath);

// +--------------------------------------------------------------+
// |                   Default Library Includes                   |
// +--------------------------------------------------------------+
#include "my_intrinsics.h"
#include "my_assert.h"
#include "my_debug.h"
#include "my_time.h"
#include "my_easing.h"
#include "my_memoryArena.h"
#include "my_fifo.h"
#include "my_vectors.h"
#include "my_directions.h"
#include "my_rectangles.h"
#include "my_quaternion.h"
#include "my_matrix4.h"
#include "my_transforms.h"
#include "my_aabb.h"
#include "my_random.h"
#include "my_math.h"
#include "my_colors.h"
#include "my_sorting.h"
#include "my_parsingFunctions.h"
#include "my_charClasses.h"
#include "my_stringManip.h"
#include "my_unicode.h"

#endif //_MYLIB_H