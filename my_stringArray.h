/*
File:   my_stringArray.h
Author: Taylor Robbins
Date:   03\15\2020
Description:
	** A StringArray_t is basically a DynArray_t built specifically for holding strings
	** This can be done explicitly already with the DynArray_t but it becomes ugly when you
	** want to delete the array and need to make sure you loop over all of the items and deallocate
	** each of the strings individually. Holding strings in a list is a common occurance and it would
	** be nice to make a special structure for holding the pointers and make nicer looking functions
	** that are easier to use than the functions for the DynArray_t
*/

#ifndef _MY_STRING_ARRAY_H
#define _MY_STRING_ARRAY_H

#include "my_dynamicArray.h"

typedef DynArray_t StringArray_t;

enum
{
	ArrayStringFlag_IsConstStr = 0x01,
	ArrayStringFlag_IsNullTerminated = 0x02,
};
struct ArrayString_t
{
	u8 flags;
	char* pntr;
	u32 length;
};

void DestroyStringArray(StringArray_t* array)
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* string = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(string != nullptr);
		if (string->pntr != nullptr && !IsFlagSet(string->flags, ArrayStringFlag_IsConstStr) && ArenaCanPop(array->allocArena, string->pntr))
		{
			ArenaPop(array->allocArena, string->pntr);
		}
	}
	DestroyDynamicArray(array);
}

void CreateStringArray(StringArray_t* array, MemoryArena_t* memArena, u32 allocChunkSize = 16, u32 initialSizeRequirement = 0)
{
	Assert(array != nullptr);
	Assert(memArena != nullptr);
	Assert(allocChunkSize > 0);
	CreateDynamicArray(array, memArena, sizeof(ArrayString_t), allocChunkSize, initialSizeRequirement);
}

char* StrArrayAdd(StringArray_t* array, const char* string, u32 strLength, bool makeCopy = true)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	Assert(string != nullptr || strLength == 0);
	Assert(array->itemSize == sizeof(ArrayString_t));
	
	ArrayString_t* newString = DynArrayAdd(array, ArrayString_t);
	if (newString == nullptr) { return nullptr; }
	ClearPointer(newString);
	if (makeCopy)
	{
		newString->pntr = ArenaString(array->allocArena, string, strLength);
		Assert(newString->pntr != nullptr);
		FlagSet(newString->flags, ArrayStringFlag_IsNullTerminated);
	}
	else
	{
		newString->pntr = (char*)string;
		FlagSet(newString->flags, ArrayStringFlag_IsConstStr);
	}
	newString->length = strLength;
	
	return newString->pntr;
}
char* StrArrayAddNt(StringArray_t* array, const char* string, bool makeCopy = true)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	Assert(string != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	
	ArrayString_t* newString = DynArrayAdd(array, ArrayString_t);
	if (newString == nullptr) { return nullptr; }
	ClearPointer(newString);
	if (makeCopy)
	{
		newString->pntr = ArenaNtString(array->allocArena, string);
		Assert(newString->pntr != nullptr);
	}
	else
	{
		newString->pntr = (char*)string;
		FlagSet(newString->flags, ArrayStringFlag_IsConstStr);
	}
	newString->length = MyStrLength32(string);
	FlagSet(newString->flags, ArrayStringFlag_IsNullTerminated);
	
	return newString->pntr;
}

char* StrArrayGet(StringArray_t* array, u32 index)
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	ArrayString_t* string = DynArrayGet(array, ArrayString_t, index);
	if (string == nullptr) { return nullptr; }
	return string->pntr;
}
const char* StrArrayGet(const StringArray_t* array, u32 index) //const variant
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	const ArrayString_t* string = DynArrayGet(array, ArrayString_t, index);
	if (string == nullptr) { return nullptr; }
	return (const char*)string->pntr;
}

char* StrArrayInsert(StringArray_t* array, u32 index, const char* string, u32 strLength, bool makeCopy = true)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	Assert(string != nullptr || strLength == 0);
	Assert(array->itemSize == sizeof(ArrayString_t));
	
	ArrayString_t* newString = DynArrayInsert(array, ArrayString_t, index);
	if (newString == nullptr) { return nullptr; }
	ClearPointer(newString);
	if (makeCopy)
	{
		newString->pntr = ArenaString(array->allocArena, string, strLength);
		Assert(newString->pntr != nullptr);
		FlagSet(newString->flags, ArrayStringFlag_IsNullTerminated);
	}
	else
	{
		newString->pntr = (char*)string;
		FlagSet(newString->flags, ArrayStringFlag_IsConstStr);
	}
	newString->length = strLength;
	
	return newString->pntr;
	
}
char* StrArrayInsertNt(StringArray_t* array, u32 index, const char* string, bool makeCopy = true)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	Assert(string != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	
	ArrayString_t* newString = DynArrayInsert(array, ArrayString_t, index);
	if (newString == nullptr) { return nullptr; }
	ClearPointer(newString);
	if (makeCopy)
	{
		newString->pntr = ArenaNtString(array->allocArena, string);
		Assert(newString->pntr != nullptr);
	}
	else
	{
		newString->pntr = (char*)string;
		FlagSet(newString->flags, ArrayStringFlag_IsConstStr);
	}
	newString->length = MyStrLength32(string);
	FlagSet(newString->flags, ArrayStringFlag_IsNullTerminated);
	
	return newString->pntr;
	
}

bool StrArrayRemove(StringArray_t* array, u32 index)
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	
	ArrayString_t* string = DynArrayGet(array, ArrayString_t, index);
	if (string == nullptr) { return false; }
	if (string->pntr != nullptr && !IsFlagSet(string->flags, ArrayStringFlag_IsConstStr) && ArenaCanPop(array->allocArena, string->pntr))
	{
		ArenaPop(array->allocArena, string->pntr);
	}
	
	return DynArrayRemove(array, index);
}

bool StrArrayRemoveRegion(StringArray_t* array, u32 startIndex, u32 numItems)
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	if (numItems == 0) { return true; }
	if (array->items == nullptr) { return false; }
	if (startIndex + numItems > array->length) { return false; }
	
	for (u32 sIndex = startIndex; sIndex < startIndex + numItems; sIndex++)
	{
		ArrayString_t* string = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(string != nullptr);
		if (string->pntr != nullptr && !IsFlagSet(string->flags, ArrayStringFlag_IsConstStr) && ArenaCanPop(array->allocArena, string->pntr))
		{
			ArenaPop(array->allocArena, string->pntr);
		}
	}
	
	return DynArrayRemoveRegion(array, startIndex, numItems);
}
void StrArrayClear(StringArray_t* array)
{
	Assert(array != nullptr);
	Assert(array->itemSize == sizeof(ArrayString_t));
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* string = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(string != nullptr);
		if (string->pntr != nullptr && !IsFlagSet(string->flags, ArrayStringFlag_IsConstStr) && ArenaCanPop(array->allocArena, string->pntr))
		{
			ArenaPop(array->allocArena, string->pntr);
		}
	}
	DynArrayClear(array);
}

bool IsStringInStrArray(const StringArray_t* array, const char* string, u32 strLength, bool compareAddressOnly = false)
{
	Assert(array != nullptr);
	Assert(string != nullptr || strLength == 0);
	if (string == nullptr && compareAddressOnly) { return false; }
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* arrString = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(arrString != nullptr);
		if (arrString->pntr == string && arrString->length == strLength) { return true; }
		if (!compareAddressOnly)
		{
			if (strLength > 0 && arrString->length == strLength && MyStrCompare(string, arrString->pntr, strLength) == 0) { return true; }
			if (strLength == 0 && arrString->length == strLength) { return true; }
		}
	}
	return false;
}
bool IsStringInStrArrayNt(const StringArray_t* array, const char* string, bool compareAddressOnly = false)
{
	Assert(array != nullptr);
	if (string == nullptr && compareAddressOnly) { return false; }
	u32 strLength = MyStrLength32(string);
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* arrString = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(arrString != nullptr);
		if (arrString->pntr == string && arrString->length == strLength) { return true; }
		if (!compareAddressOnly)
		{
			if (strLength > 0 && arrString->length == strLength && MyStrCompare(string, arrString->pntr, strLength) == 0) { return true; }
			if (strLength == 0 && arrString->length == strLength) { return true; }
		}
	}
	return false;
}

u32 GetStrArrayStringIndex(const StringArray_t* array, const char* string, u32 strLength, bool compareAddressOnly = false)
{
	Assert(array != nullptr);
	Assert(string != nullptr || strLength == 0);
	if (string == nullptr && compareAddressOnly) { return array->length; }
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* arrString = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(arrString != nullptr);
		if (arrString->pntr == string && arrString->length == strLength) { return sIndex; }
		if (!compareAddressOnly)
		{
			if (strLength > 0 && arrString->length == strLength && MyStrCompare(string, arrString->pntr, strLength) == 0) { return sIndex; }
			if (strLength == 0 && arrString->length == strLength) { return sIndex; }
		}
	}
	return array->length;
}
u32 GetStrArrayStringIndexNt(const StringArray_t* array, const char* string, bool compareAddressOnly = false)
{
	Assert(array != nullptr);
	if (string == nullptr && compareAddressOnly) { return array->length; }
	u32 strLength = MyStrLength32(string);
	for (u32 sIndex = 0; sIndex < array->length; sIndex++)
	{
		ArrayString_t* arrString = DynArrayGet(array, ArrayString_t, sIndex);
		Assert(arrString != nullptr);
		if (arrString->pntr == string && arrString->length == strLength) { return sIndex; }
		if (!compareAddressOnly)
		{
			if (strLength > 0 && arrString->length == strLength && MyStrCompare(string, arrString->pntr, strLength) == 0) { return sIndex; }
			if (strLength == 0 && arrString->length == strLength) { return sIndex; }
		}
	}
	return array->length;
}

#endif //  _MY_STRING_ARRAY_H
