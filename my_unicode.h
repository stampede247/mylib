/*
File:   my_unicode.h
Author: Taylor Robbins
Date:   01\01\2021
Description:
	** Holds functions that helps us convert to and from UTF-8 and other representations of Unicode characters
*/

#ifndef _MY_UNICODE_H
#define _MY_UNICODE_H

#define UTF8_MAX_CODEPOINT 0x10FFFFUL
#define UTF8_MAX_CHAR_SIZE 4 //bytes

#define UNICODE_LATIN_EXT_START 0x000000A0UL
#define UNICODE_LATIN_EXT_COUNT 96
#define UNICODE_LATIN_EXT_END   (UNICODE_LATIN_EXT_START + UNICODE_LATIN_EXT_COUNT)

#define UNICODE_CYRILLIC_START 0x00000400UL
#define UNICODE_CYRILLIC_COUNT 256
#define UNICODE_CYRILLIC_END   (UNICODE_CYRILLIC_START + UNICODE_CYRILLIC_COUNT)

#define UNICODE_HIRAGANA_START 0x000003041UL
#define UNICODE_HIRAGANA_COUNT 95
#define UNICODE_HIRAGANA_END   (UNICODE_HIRAGANA_START + UNICODE_HIRAGANA_COUNT)

#define UNICODE_KATAKANA_START 0x0000030A0UL
#define UNICODE_KATAKANA_COUNT 96
#define UNICODE_KATAKANA_END   (UNICODE_KATAKANA_START + UNICODE_KATAKANA_COUNT)

//returns number of bytes needed to store this codepoint in UTF-8 and stores the values in byteBufferOut if not nullptr
//NOTE: byteBufferOut is assumed to be 4 bytes or greater and no null-terminating character is written to the buffer
u8 GetUtf8BytesForCode(u32 codepoint, u8* byteBufferOut = nullptr, bool doAssertions = true)
{
	NotNull(byteBufferOut);
	if (codepoint <= 0x7F)
	{
		//0xxx xxxx
		if (byteBufferOut != nullptr) { byteBufferOut[0] = (u8)codepoint; }
		return 1;
	}
	else if (codepoint <= 0x7FF)
	{
		//110x xxxx 10xx xxxx
		if (byteBufferOut != nullptr)
		{
			byteBufferOut[0] = (0xC0 | ((codepoint >> 6) & 0x1F));
			byteBufferOut[1] = (0x80 | (codepoint & 0x3F));
		}
		return 2;
	}
	else if (codepoint >= 0xD800 && codepoint <= 0xDFFF)
	{
		//invalid block
		Assert(!doAssertions || (false && "Invalid codepoint for UTF8"));
		if (byteBufferOut != nullptr) { byteBufferOut[0] = '\0'; }
		return 0;
	}
	else if (codepoint <= 0xFFFF)
	{
		//1110 xxxx 10xx xxxx 10xx xxxx
		if (byteBufferOut != nullptr)
		{
			byteBufferOut[0] = (0xE0 | ((codepoint >> 12) & 0x0F));
			byteBufferOut[1] = (0x80 | ((codepoint>>6) & 0x3F));
			byteBufferOut[2] = (0x80 | (codepoint & 0x3F));
		}
		return 3;
	}
	else if (codepoint <= UTF8_MAX_CODEPOINT)
	{
		//1111 0xxx 10xx xxxx 10xx xxxx 10xx xxxx
		if (byteBufferOut != nullptr)
		{
			byteBufferOut[0] = (0xF0 | ((codepoint >> 18) & 0x07));
			byteBufferOut[1] = (0x80 | ((codepoint>>12) & 0x3F));
			byteBufferOut[2] = (0x80 | ((codepoint>>6) & 0x3F));
			byteBufferOut[3] = (0x80 | (codepoint & 0x3F));
		}
		return 4;
	}
	else
	{
		//everything above this point is also invalid
		Assert(!doAssertions || (false && "Invalid codepoint value for UTF8"));
		if (byteBufferOut != nullptr) { byteBufferOut[0] = '\0'; }
		return 0;
	}
}

//Returns the number of bytes consumed to complete the UTF-8 encoded character pointed to by strPntr
//If more bytes are needed for a full UTF-8 character than is specified by maxNumChars then we return 0
//If an invalid encoding is detected then we return 0. If maxNumChars == 0 then we return 0.
//TODO: Should we accept characters that are technically valid but encoded in a larger number of bytes than needed?
//      Like a 4-byte encoding of \0 would be dumb because it could cause non-null-terminated UTF-8 strings to become null terminated collapsed ASCII strings
u8 GetCodepointForUtf8(const char* strPntr, u32 maxNumChars, u32* codepointOut = nullptr) //somewhat tested
{
	Assert(strPntr != nullptr || maxNumChars == 0);
	if (codepointOut != nullptr) { *codepointOut = 0; }
	if (maxNumChars == 0) { return 0; }
	const u8* bytePntr = (const u8*)strPntr;
	if (bytePntr[0] <= 127)
	{
		if (codepointOut != nullptr) { *codepointOut = (u32)bytePntr[0]; }
		return 1;
	}
	else if (bytePntr[0] < 0xC0)
	{
		//An 10xx xxxx format for the first byte of a character is invalid
		return 0;
	}
	else if (bytePntr[0] < 0xE0)
	{
		if (maxNumChars < 2) { return 0; }
		if (bytePntr[1] < 0x80 || bytePntr[1] >= 0xC0) { return 0; }
		if (codepointOut != nullptr) { *codepointOut = ((u32)(bytePntr[0] & 0x1F) << 6) | ((u32)(bytePntr[1] & 0x3F) << 0); }
		return 2;
	}
	else if (bytePntr[0] < 0xF0)
	{
		if (maxNumChars < 3) { return 0; }
		if (bytePntr[1] < 0x80 || bytePntr[1] >= 0xC0) { return 0; }
		if (bytePntr[2] < 0x80 || bytePntr[2] >= 0xC0) { return 0; }
		if (codepointOut != nullptr) { *codepointOut = ((u32)(bytePntr[0] & 0x0F) << 12) | ((u32)(bytePntr[1] & 0x3F) << 6) | ((u32)(bytePntr[2] & 0x3F) << 0); }
		return 3;
	}
	else if (bytePntr[0] < 0xF8)
	{
		if (maxNumChars < 4) { return 0; }
		if (bytePntr[1] < 0x80 || bytePntr[1] >= 0xC0) { return 0; }
		if (bytePntr[2] < 0x80 || bytePntr[2] >= 0xC0) { return 0; }
		if (bytePntr[3] < 0x80 || bytePntr[3] >= 0xC0) { return 0; }
		if (codepointOut != nullptr) { *codepointOut = ((u32)(bytePntr[0] & 0x07) << 18) | ((u32)(bytePntr[1] & 0x3F) << 12) | ((u32)(bytePntr[2] & 0x3F) << 6) | ((u32)(bytePntr[3] & 0x3F) << 0); }
		return 4;
	}
	else
	{
		//Everything above this point is considered an invalid character to exist in UTF-8 encoded strings
		return 0;
	}
}

bool IsCodepointValidInUtf8(u32 codepoint)
{
	if (codepoint >= 0xD800 && codepoint <= 0xDFFF) { return false; }
	else if (codepoint > UTF8_MAX_CODEPOINT) { return false; }
	else { return true; }
}

bool IsValidUtf8(const char* strPntr, u32 strLength)
{
	Assert(strPntr != nullptr || strLength == 0);
	u32 byteIndex = 0;
	while (byteIndex < strLength)
	{
		u8 decodeResult = GetCodepointForUtf8(&strPntr[byteIndex], strLength - byteIndex, nullptr);
		if (decodeResult == 0) { return false; }
		byteIndex += decodeResult;
		Assert(byteIndex <= strLength);
	}
	return true;
}
bool IsValidUtf8Nt(const char* nullTermString)
{
	NotNull(nullTermString);
	return IsValidUtf8(nullTermString, MyStrLength32(nullTermString));
}

u32 StrLengthUtf8(const char* strPntr, u32 strLength)
{
	Assert(strPntr != nullptr || strLength == 0);
	u32 result = 0;
	u32 byteIndex = 0;
	while (byteIndex < strLength)
	{
		u8 decodeResult = GetCodepointForUtf8(&strPntr[byteIndex], strLength - byteIndex, nullptr);
		if (decodeResult == 0) { return false; }
		byteIndex += decodeResult;
		Assert(byteIndex <= strLength);
		result++;
	}
	return result;
}
u32 StrLengthUtf8Nt(const char* nullTermString)
{
	NotNull(nullTermString);
	return StrLengthUtf8(nullTermString, MyStrLength32(nullTermString));
}

//TODO: While the input may not contain null-term 1-byte chars it can contain somewhat invalid UTF-8 which makes the output potentially null-terminated earlier than intented
char* CollapseUtf8StrToAscii(MemoryArena_t* memArena, const char* utf8Str, u32 utf8StrLength, u32* asciiStrLengthOut = nullptr, char replaceChar = '?', bool* foundNonAsciiCharOut = nullptr)
{
	Assert(utf8Str != nullptr || utf8StrLength == 0);
	if (foundNonAsciiCharOut != nullptr) { *foundNonAsciiCharOut = false; }
	char* result = nullptr;
	u32 resultLength = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numAsciiChars = 0;
		u32 byteIndex = 0;
		while (byteIndex < utf8StrLength)
		{
			u32 codepointValue = 0x00000000;
			u8 decodeResult = GetCodepointForUtf8(&utf8Str[byteIndex], utf8StrLength - byteIndex, &codepointValue);
			if (decodeResult == 0)
			{
				if (asciiStrLengthOut != nullptr) { *asciiStrLengthOut = 0; }
				return nullptr;
			}
			byteIndex += decodeResult;
			Assert(byteIndex <= utf8StrLength);
			if (codepointValue <= 127)
			{
				if (result != nullptr)
				{
					Assert(numAsciiChars < resultLength);
					result[numAsciiChars] = (char)codepointValue;
				}
				numAsciiChars++;
			}
			else
			{
				if (foundNonAsciiCharOut != nullptr) { *foundNonAsciiCharOut = true; }
				if (result != nullptr)
				{
					Assert(numAsciiChars < resultLength);
					result[numAsciiChars] = replaceChar;
					// MyLibPrintLine_D("Replacing %u byte char[%u] at byte[%u] codepoint %u (%08X)", decodeResult, numAsciiChars, byteIndex, codepointValue, codepointValue);
					// if (decodeResult == 1) { result[numAsciiChars] = '1'; }
					// else if (decodeResult == 2) { result[numAsciiChars] = '2'; }
					// else if (decodeResult == 3) { result[numAsciiChars] = '3'; }
					// else if (decodeResult == 4) { result[numAsciiChars] = '4'; }
					// else { result[numAsciiChars] = replaceChar; }
				}
				numAsciiChars++;
			}
		}
		
		if (pass == 0)
		{
			if (asciiStrLengthOut != nullptr) { *asciiStrLengthOut = numAsciiChars; }
			if (memArena == nullptr) { return nullptr; }
			resultLength = numAsciiChars;
			result = PushArray(memArena, char, numAsciiChars+1);
			NotNull(result);
		}
		else
		{
			Assert(numAsciiChars == resultLength);
			result[resultLength] = '\0';
		}
	}
	return result;
}
char* CollapseUtf8StrToAsciiNt(MemoryArena_t* memArena, const char* nullTermUtf8Str, u32* asciiStrLengthOut = nullptr, char replaceChar = '?', bool* foundNonAsciiCharOut = nullptr)
{
	NotNull(nullTermUtf8Str);
	return CollapseUtf8StrToAscii(memArena, nullTermUtf8Str, MyStrLength32(nullTermUtf8Str), asciiStrLengthOut, replaceChar, foundNonAsciiCharOut);
}

bool IsUtf8StrAsciiOnly(const char* utf8Str, u32 utf8StrLength)
{
	u32 byteIndex = 0;
	while (byteIndex < utf8StrLength)
	{
		u32 codepointValue = 0x00000000;
		u8 decodeResult = GetCodepointForUtf8(&utf8Str[byteIndex], utf8StrLength - byteIndex, &codepointValue);
		if (decodeResult != 1) { return false; }
		byteIndex += decodeResult;
		Assert(byteIndex <= utf8StrLength);
		if (codepointValue > 127) { return false; }
	}
	return true;
}
bool IsUtf8StrAsciiOnlyNt(const char* nullTermUtf8Str)
{
	NotNull(nullTermUtf8Str);
	return IsUtf8StrAsciiOnly(nullTermUtf8Str, MyStrLength32(nullTermUtf8Str));
}

//returns the number of bytes you need to step from the beginning of the string to get to the nth character (returns 0 on decode failure)
u32 GetUtf8StrCharOffset(const char* utf8Str, u32 utf8StrLength, u32 charIndex)
{
	Assert(utf8Str != nullptr || utf8StrLength == 0);
	if (utf8StrLength == 0) { return 0; }
	u32 byteIndex = 0;
	u32 currentCharIndex = 0;
	while (byteIndex < utf8StrLength)
	{
		if (currentCharIndex == charIndex) { return byteIndex; }
		u8 decodeResult = GetCodepointForUtf8(&utf8Str[byteIndex], utf8StrLength - byteIndex);
		if (decodeResult == 0) { return 0; }
		byteIndex += decodeResult;
		Assert(byteIndex <= utf8StrLength);
		currentCharIndex++;
	}
	return utf8StrLength;
}
u32 GetUtf8StrCharOffsetNt(const char* nullTermUtf8Str, u32 charIndex)
{
	NotNull(nullTermUtf8Str);
	return GetUtf8StrCharOffset(nullTermUtf8Str, MyStrLength32(nullTermUtf8Str), charIndex);
}

char* GetUtf8StrSubstr(char* utf8Str, u32 utf8StrLength, u32 charIndex, u32 numChars, u32* numBytesOut = nullptr)
{
	Assert(utf8Str != nullptr || utf8StrLength == 0);
	u32 startOffset = GetUtf8StrCharOffset(utf8Str, utf8StrLength, charIndex);
	Assert(startOffset <= utf8StrLength);
	u32 numBytes = GetUtf8StrCharOffset(&utf8Str[startOffset], utf8StrLength - startOffset, numChars);
	Assert(startOffset + numBytes <= utf8StrLength);
	if (numBytesOut != nullptr) { *numBytesOut = numBytes; }
	return &utf8Str[startOffset];
}
const char* GetUtf8StrSubstr(const char* utf8Str, u32 utf8StrLength, u32 charIndex, u32 numChars, u32* numBytesOut = nullptr) //const-version
{
	return (const char*)GetUtf8StrSubstr((char*)utf8Str, utf8StrLength, charIndex, numChars, numBytesOut);
}
char* GetUtf8StrSubstrNt(char* nullTermUtf8Str, u32 charIndex, u32 numChars, u32* numBytesOut = nullptr)
{
	NotNull(nullTermUtf8Str);
	return GetUtf8StrSubstr(nullTermUtf8Str, MyStrLength32(nullTermUtf8Str), charIndex, numChars, numBytesOut);
}
const char* GetUtf8StrSubstrNt(const char* nullTermUtf8Str, u32 charIndex, u32 numChars, u32* numBytesOut = nullptr) //const-version
{
	return (const char*)GetUtf8StrSubstrNt((char*)nullTermUtf8Str, charIndex, numChars, numBytesOut);
}

//NOTE: If we reach the end of the string before getting to the requested char then we will return 0.
//      If we find invalid UTF-8 before the requested char we will return 0.
u32 GetUtf8StrCharAt(const char* utf8Str, u32 utf8StrLength, u32 charIndex)
{
	Assert(utf8Str != nullptr || utf8StrLength == 0);
	if (utf8StrLength == 0) { return 0; }
	u32 byteIndex = 0;
	u32 currentCharIndex = 0;
	while (byteIndex < utf8StrLength)
	{
		u32 codepointValue = 0x00000000;
		u8 decodeResult = GetCodepointForUtf8(&utf8Str[byteIndex], utf8StrLength - byteIndex, &codepointValue);
		if (decodeResult == 0) { return 0; }
		byteIndex += decodeResult;
		Assert(byteIndex <= utf8StrLength);
		if (currentCharIndex == charIndex) { return codepointValue; }
		currentCharIndex++;
	}
	return 0;
}
u32 GetUtf8StrCharAtNt(const char* nullTermUtf8Str, u32 charIndex)
{
	NotNull(nullTermUtf8Str);
	return GetUtf8StrCharAt(nullTermUtf8Str, MyStrLength32(nullTermUtf8Str), charIndex);
}

const char* GetUnicodeBlockNameForCodepoint(u32 codepoint)
{
	if (/*codepoint >= 0x00000000 && */codepoint <= 0x0000007F) { return "Basic Latin"; }
	if (/*codepoint >= 0x00000080 && */codepoint <= 0x000000FF) { return "Latin-1 Supplement";                             }
	if (/*codepoint >= 0x00000100 && */codepoint <= 0x0000017F) { return "Latin Extended-A";                               }
	if (/*codepoint >= 0x00000180 && */codepoint <= 0x0000024F) { return "Latin Extended-B";                               }
	if (/*codepoint >= 0x00000250 && */codepoint <= 0x000002AF) { return "IPA Extensions";                                 }
	if (/*codepoint >= 0x000002B0 && */codepoint <= 0x000002FF) { return "Spacing Modifier Letters";                       }
	if (/*codepoint >= 0x00000300 && */codepoint <= 0x0000036F) { return "Combining Diacritical Marks";                    }
	if (/*codepoint >= 0x00000370 && */codepoint <= 0x000003FF) { return "Greek and Coptic";                               }
	if (/*codepoint >= 0x00000400 && */codepoint <= 0x000004FF) { return "Cyrillic";                                       }
	if (/*codepoint >= 0x00000500 && */codepoint <= 0x0000052F) { return "Cyrillic Supplement";                            }
	if (/*codepoint >= 0x00000530 && */codepoint <= 0x0000058F) { return "Armenian";                                       }
	if (/*codepoint >= 0x00000590 && */codepoint <= 0x000005FF) { return "Hebrew";                                         }
	if (/*codepoint >= 0x00000600 && */codepoint <= 0x000006FF) { return "Arabic";                                         }
	if (/*codepoint >= 0x00000700 && */codepoint <= 0x0000074F) { return "Syriac";                                         }
	if (/*codepoint >= 0x00000750 && */codepoint <= 0x0000077F) { return "Arabic Supplement";                              }
	if (/*codepoint >= 0x00000780 && */codepoint <= 0x000007BF) { return "Thaana";                                         }
	if (/*codepoint >= 0x000007C0 && */codepoint <= 0x000007FF) { return "NKo";                                            }
	if (/*codepoint >= 0x00000800 && */codepoint <= 0x0000083F) { return "Samaritan";                                      }
	if (/*codepoint >= 0x00000840 && */codepoint <= 0x0000085F) { return "Mandaic";                                        }
	if (/*codepoint >= 0x00000860 && */codepoint <= 0x0000086F) { return "Syriac Supplement";                              }
	if (/*codepoint >= 0x000008A0 && */codepoint <= 0x000008FF) { return "Arabic Extended-A";                              }
	if (/*codepoint >= 0x00000900 && */codepoint <= 0x0000097F) { return "Devanagari";                                     }
	if (/*codepoint >= 0x00000980 && */codepoint <= 0x000009FF) { return "Bengali";                                        }
	if (/*codepoint >= 0x00000A00 && */codepoint <= 0x00000A7F) { return "Gurmukhi";                                       }
	if (/*codepoint >= 0x00000A80 && */codepoint <= 0x00000AFF) { return "Gujarati";                                       }
	if (/*codepoint >= 0x00000B00 && */codepoint <= 0x00000B7F) { return "Oriya";                                          }
	if (/*codepoint >= 0x00000B80 && */codepoint <= 0x00000BFF) { return "Tamil";                                          }
	if (/*codepoint >= 0x00000C00 && */codepoint <= 0x00000C7F) { return "Telugu";                                         }
	if (/*codepoint >= 0x00000C80 && */codepoint <= 0x00000CFF) { return "Kannada";                                        }
	if (/*codepoint >= 0x00000D00 && */codepoint <= 0x00000D7F) { return "Malayalam";                                      }
	if (/*codepoint >= 0x00000D80 && */codepoint <= 0x00000DFF) { return "Sinhala";                                        }
	if (/*codepoint >= 0x00000E00 && */codepoint <= 0x00000E7F) { return "Thai";                                           }
	if (/*codepoint >= 0x00000E80 && */codepoint <= 0x00000EFF) { return "Lao";                                            }
	if (/*codepoint >= 0x00000F00 && */codepoint <= 0x00000FFF) { return "Tibetan";                                        }
	if (/*codepoint >= 0x00001000 && */codepoint <= 0x0000109F) { return "Myanmar";                                        }
	if (/*codepoint >= 0x000010A0 && */codepoint <= 0x000010FF) { return "Georgian";                                       }
	if (/*codepoint >= 0x00001100 && */codepoint <= 0x000011FF) { return "Hangul Jamo";                                    }
	if (/*codepoint >= 0x00001200 && */codepoint <= 0x0000137F) { return "Ethiopic";                                       }
	if (/*codepoint >= 0x00001380 && */codepoint <= 0x0000139F) { return "Ethiopic Supplement";                            }
	if (/*codepoint >= 0x000013A0 && */codepoint <= 0x000013FF) { return "Cherokee";                                       }
	if (/*codepoint >= 0x00001400 && */codepoint <= 0x0000167F) { return "Unified Canadian Aboriginal Syllabics";          }
	if (/*codepoint >= 0x00001680 && */codepoint <= 0x0000169F) { return "Ogham";                                          }
	if (/*codepoint >= 0x000016A0 && */codepoint <= 0x000016FF) { return "Runic";                                          }
	if (/*codepoint >= 0x00001700 && */codepoint <= 0x0000171F) { return "Tagalog";                                        }
	if (/*codepoint >= 0x00001720 && */codepoint <= 0x0000173F) { return "Hanunoo";                                        }
	if (/*codepoint >= 0x00001740 && */codepoint <= 0x0000175F) { return "Buhid";                                          }
	if (/*codepoint >= 0x00001760 && */codepoint <= 0x0000177F) { return "Tagbanwa";                                       }
	if (/*codepoint >= 0x00001780 && */codepoint <= 0x000017FF) { return "Khmer";                                          }
	if (/*codepoint >= 0x00001800 && */codepoint <= 0x000018AF) { return "Mongolian";                                      }
	if (/*codepoint >= 0x000018B0 && */codepoint <= 0x000018FF) { return "Unified Canadian Aboriginal Syllabics Extended"; }
	if (/*codepoint >= 0x00001900 && */codepoint <= 0x0000194F) { return "Limbu";                                          }
	if (/*codepoint >= 0x00001950 && */codepoint <= 0x0000197F) { return "Tai Le";                                         }
	if (/*codepoint >= 0x00001980 && */codepoint <= 0x000019DF) { return "New Tai Lue";                                    }
	if (/*codepoint >= 0x000019E0 && */codepoint <= 0x000019FF) { return "Khmer Symbols";                                  }
	if (/*codepoint >= 0x00001A00 && */codepoint <= 0x00001A1F) { return "Buginese";                                       }
	if (/*codepoint >= 0x00001A20 && */codepoint <= 0x00001AAF) { return "Tai Tham";                                       }
	if (/*codepoint >= 0x00001AB0 && */codepoint <= 0x00001AFF) { return "Combining Diacritical Marks Extended";           }
	if (/*codepoint >= 0x00001B00 && */codepoint <= 0x00001B7F) { return "Balinese";                                       }
	if (/*codepoint >= 0x00001B80 && */codepoint <= 0x00001BBF) { return "Sundanese";                                      }
	if (/*codepoint >= 0x00001BC0 && */codepoint <= 0x00001BFF) { return "Batak";                                          }
	if (/*codepoint >= 0x00001C00 && */codepoint <= 0x00001C4F) { return "Lepcha";                                         }
	if (/*codepoint >= 0x00001C50 && */codepoint <= 0x00001C7F) { return "Ol Chiki";                                       }
	if (/*codepoint >= 0x00001C80 && */codepoint <= 0x00001C8F) { return "Cyrillic Extended-C";                            }
	if (/*codepoint >= 0x00001C90 && */codepoint <= 0x00001CBF) { return "Georgian Extended";                              }
	if (/*codepoint >= 0x00001CC0 && */codepoint <= 0x00001CCF) { return "Sundanese Supplement";                           }
	if (/*codepoint >= 0x00001CD0 && */codepoint <= 0x00001CFF) { return "Vedic Extensions";                               }
	if (/*codepoint >= 0x00001D00 && */codepoint <= 0x00001D7F) { return "Phonetic Extensions";                            }
	if (/*codepoint >= 0x00001D80 && */codepoint <= 0x00001DBF) { return "Phonetic Extensions Supplement";                 }
	if (/*codepoint >= 0x00001DC0 && */codepoint <= 0x00001DFF) { return "Combining Diacritical Marks Supplement";         }
	if (/*codepoint >= 0x00001E00 && */codepoint <= 0x00001EFF) { return "Latin Extended Additional";                      }
	if (/*codepoint >= 0x00001F00 && */codepoint <= 0x00001FFF) { return "Greek Extended";                                 }
	if (/*codepoint >= 0x00002000 && */codepoint <= 0x0000206F) { return "General Punctuation";                            }
	if (/*codepoint >= 0x00002070 && */codepoint <= 0x0000209F) { return "Superscripts and Subscripts";                    }
	if (/*codepoint >= 0x000020A0 && */codepoint <= 0x000020CF) { return "Currency Symbols";                               }
	if (/*codepoint >= 0x000020D0 && */codepoint <= 0x000020FF) { return "Combining Diacritical Marks for Symbols";        }
	if (/*codepoint >= 0x00002100 && */codepoint <= 0x0000214F) { return "Letterlike Symbols";                             }
	if (/*codepoint >= 0x00002150 && */codepoint <= 0x0000218F) { return "Number Forms";                                   }
	if (/*codepoint >= 0x00002190 && */codepoint <= 0x000021FF) { return "Arrows";                                         }
	if (/*codepoint >= 0x00002200 && */codepoint <= 0x000022FF) { return "Mathematical Operators";                         }
	if (/*codepoint >= 0x00002300 && */codepoint <= 0x000023FF) { return "Miscellaneous Technical";                        }
	if (/*codepoint >= 0x00002400 && */codepoint <= 0x0000243F) { return "Control Pictures";                               }
	if (/*codepoint >= 0x00002440 && */codepoint <= 0x0000245F) { return "Optical Character Recognition";                  }
	if (/*codepoint >= 0x00002460 && */codepoint <= 0x000024FF) { return "Enclosed Alphanumerics";                         }
	if (/*codepoint >= 0x00002500 && */codepoint <= 0x0000257F) { return "Box Drawing";                                    }
	if (/*codepoint >= 0x00002580 && */codepoint <= 0x0000259F) { return "Block Elements";                                 }
	if (/*codepoint >= 0x000025A0 && */codepoint <= 0x000025FF) { return "Geometric Shapes";                               }
	if (/*codepoint >= 0x00002600 && */codepoint <= 0x000026FF) { return "Miscellaneous Symbols";                          }
	if (/*codepoint >= 0x00002700 && */codepoint <= 0x000027BF) { return "Dingbats";                                       }
	if (/*codepoint >= 0x000027C0 && */codepoint <= 0x000027EF) { return "Miscellaneous Mathematical Symbols-A";           }
	if (/*codepoint >= 0x000027F0 && */codepoint <= 0x000027FF) { return "Supplemental Arrows-A";                          }
	if (/*codepoint >= 0x00002800 && */codepoint <= 0x000028FF) { return "Braille Patterns";                               }
	if (/*codepoint >= 0x00002900 && */codepoint <= 0x0000297F) { return "Supplemental Arrows-B";                          }
	if (/*codepoint >= 0x00002980 && */codepoint <= 0x000029FF) { return "Miscellaneous Mathematical Symbols-B";           }
	if (/*codepoint >= 0x00002A00 && */codepoint <= 0x00002AFF) { return "Supplemental Mathematical Operators";            }
	if (/*codepoint >= 0x00002B00 && */codepoint <= 0x00002BFF) { return "Miscellaneous Symbols and Arrows";               }
	if (/*codepoint >= 0x00002C00 && */codepoint <= 0x00002C5F) { return "Glagolitic";                                     }
	if (/*codepoint >= 0x00002C60 && */codepoint <= 0x00002C7F) { return "Latin Extended-C";                               }
	if (/*codepoint >= 0x00002C80 && */codepoint <= 0x00002CFF) { return "Coptic";                                         }
	if (/*codepoint >= 0x00002D00 && */codepoint <= 0x00002D2F) { return "Georgian Supplement";                            }
	if (/*codepoint >= 0x00002D30 && */codepoint <= 0x00002D7F) { return "Tifinagh";                                       }
	if (/*codepoint >= 0x00002D80 && */codepoint <= 0x00002DDF) { return "Ethiopic Extended";                              }
	if (/*codepoint >= 0x00002DE0 && */codepoint <= 0x00002DFF) { return "Cyrillic Extended-A";                            }
	if (/*codepoint >= 0x00002E00 && */codepoint <= 0x00002E7F) { return "Supplemental Punctuation";                       }
	if (/*codepoint >= 0x00002E80 && */codepoint <= 0x00002EFF) { return "CJK Radicals Supplement";                        }
	if (/*codepoint >= 0x00002F00 && */codepoint <= 0x00002FDF) { return "Kangxi Radicals";                                }
	if (/*codepoint >= 0x00002FF0 && */codepoint <= 0x00002FFF) { return "Ideographic Description Characters";             }
	if (/*codepoint >= 0x00003000 && */codepoint <= 0x0000303F) { return "CJK Symbols and Punctuation";                    }
	if (/*codepoint >= 0x00003040 && */codepoint <= 0x0000309F) { return "Hiragana";                                       }
	if (/*codepoint >= 0x000030A0 && */codepoint <= 0x000030FF) { return "Katakana";                                       }
	if (/*codepoint >= 0x00003100 && */codepoint <= 0x0000312F) { return "Bopomofo";                                       }
	if (/*codepoint >= 0x00003130 && */codepoint <= 0x0000318F) { return "Hangul Compatibility Jamo";                      }
	if (/*codepoint >= 0x00003190 && */codepoint <= 0x0000319F) { return "Kanbun";                                         }
	if (/*codepoint >= 0x000031A0 && */codepoint <= 0x000031BF) { return "Bopomofo Extended";                              }
	if (/*codepoint >= 0x000031C0 && */codepoint <= 0x000031EF) { return "CJK Strokes";                                    }
	if (/*codepoint >= 0x000031F0 && */codepoint <= 0x000031FF) { return "Katakana Phonetic Extensions";                   }
	if (/*codepoint >= 0x00003200 && */codepoint <= 0x000032FF) { return "Enclosed CJK Letters and Months";                }
	if (/*codepoint >= 0x00003300 && */codepoint <= 0x000033FF) { return "CJK Compatibility";                              }
	if (/*codepoint >= 0x00003400 && */codepoint <= 0x00004DBF) { return "CJK Unified Ideographs Extension A";             }
	if (/*codepoint >= 0x00004DC0 && */codepoint <= 0x00004DFF) { return "Yijing Hexagram Symbols";                        }
	if (/*codepoint >= 0x00004E00 && */codepoint <= 0x00009FFF) { return "CJK Unified Ideographs";                         }
	if (/*codepoint >= 0x0000A000 && */codepoint <= 0x0000A48F) { return "Yi Syllables";                                   }
	if (/*codepoint >= 0x0000A490 && */codepoint <= 0x0000A4CF) { return "Yi Radicals";                                    }
	if (/*codepoint >= 0x0000A4D0 && */codepoint <= 0x0000A4FF) { return "Lisu";                                           }
	if (/*codepoint >= 0x0000A500 && */codepoint <= 0x0000A63F) { return "Vai";                                            }
	if (/*codepoint >= 0x0000A640 && */codepoint <= 0x0000A69F) { return "Cyrillic Extended-B";                            }
	if (/*codepoint >= 0x0000A6A0 && */codepoint <= 0x0000A6FF) { return "Bamum";                                          }
	if (/*codepoint >= 0x0000A700 && */codepoint <= 0x0000A71F) { return "Modifier Tone Letters";                          }
	if (/*codepoint >= 0x0000A720 && */codepoint <= 0x0000A7FF) { return "Latin Extended-D";                               }
	if (/*codepoint >= 0x0000A800 && */codepoint <= 0x0000A82F) { return "Syloti Nagri";                                   }
	if (/*codepoint >= 0x0000A830 && */codepoint <= 0x0000A83F) { return "Common Indic Number Forms";                      }
	if (/*codepoint >= 0x0000A840 && */codepoint <= 0x0000A87F) { return "Phags-pa";                                       }
	if (/*codepoint >= 0x0000A880 && */codepoint <= 0x0000A8DF) { return "Saurashtra";                                     }
	if (/*codepoint >= 0x0000A8E0 && */codepoint <= 0x0000A8FF) { return "Devanagari Extended";                            }
	if (/*codepoint >= 0x0000A900 && */codepoint <= 0x0000A92F) { return "Kayah Li";                                       }
	if (/*codepoint >= 0x0000A930 && */codepoint <= 0x0000A95F) { return "Rejang";                                         }
	if (/*codepoint >= 0x0000A960 && */codepoint <= 0x0000A97F) { return "Hangul Jamo Extended-A";                         }
	if (/*codepoint >= 0x0000A980 && */codepoint <= 0x0000A9DF) { return "Javanese";                                       }
	if (/*codepoint >= 0x0000A9E0 && */codepoint <= 0x0000A9FF) { return "Myanmar Extended-B";                             }
	if (/*codepoint >= 0x0000AA00 && */codepoint <= 0x0000AA5F) { return "Cham";                                           }
	if (/*codepoint >= 0x0000AA60 && */codepoint <= 0x0000AA7F) { return "Myanmar Extended-A";                             }
	if (/*codepoint >= 0x0000AA80 && */codepoint <= 0x0000AADF) { return "Tai Viet";                                       }
	if (/*codepoint >= 0x0000AAE0 && */codepoint <= 0x0000AAFF) { return "Meetei Mayek Extensions";                        }
	if (/*codepoint >= 0x0000AB00 && */codepoint <= 0x0000AB2F) { return "Ethiopic Extended-A";                            }
	if (/*codepoint >= 0x0000AB30 && */codepoint <= 0x0000AB6F) { return "Latin Extended-E";                               }
	if (/*codepoint >= 0x0000AB70 && */codepoint <= 0x0000ABBF) { return "Cherokee Supplement";                            }
	if (/*codepoint >= 0x0000ABC0 && */codepoint <= 0x0000ABFF) { return "Meetei Mayek";                                   }
	if (/*codepoint >= 0x0000AC00 && */codepoint <= 0x0000D7AF) { return "Hangul Syllables";                               }
	if (/*codepoint >= 0x0000D7B0 && */codepoint <= 0x0000D7FF) { return "Hangul Jamo Extended-B";                         }
	if (/*codepoint >= 0x0000D800 && */codepoint <= 0x0000DB7F) { return "High Surrogates";                                }
	if (/*codepoint >= 0x0000DB80 && */codepoint <= 0x0000DBFF) { return "High Private Use Surrogates";                    }
	if (/*codepoint >= 0x0000DC00 && */codepoint <= 0x0000DFFF) { return "Low Surrogates";                                 }
	if (/*codepoint >= 0x0000E000 && */codepoint <= 0x0000F8FF) { return "Private Use Area";                               }
	if (/*codepoint >= 0x0000F900 && */codepoint <= 0x0000FAFF) { return "CJK Compatibility Ideographs";                   }
	if (/*codepoint >= 0x0000FB00 && */codepoint <= 0x0000FB4F) { return "Alphabetic Presentation Forms";                  }
	if (/*codepoint >= 0x0000FB50 && */codepoint <= 0x0000FDFF) { return "Arabic Presentation Forms-A";                    }
	if (/*codepoint >= 0x0000FE00 && */codepoint <= 0x0000FE0F) { return "Variation Selectors";                            }
	if (/*codepoint >= 0x0000FE10 && */codepoint <= 0x0000FE1F) { return "Vertical Forms";                                 }
	if (/*codepoint >= 0x0000FE20 && */codepoint <= 0x0000FE2F) { return "Combining Half Marks";                           }
	if (/*codepoint >= 0x0000FE30 && */codepoint <= 0x0000FE4F) { return "CJK Compatibility Forms";                        }
	if (/*codepoint >= 0x0000FE50 && */codepoint <= 0x0000FE6F) { return "Small Form Variants";                            }
	if (/*codepoint >= 0x0000FE70 && */codepoint <= 0x0000FEFF) { return "Arabic Presentation Forms-B";                    }
	if (/*codepoint >= 0x0000FF00 && */codepoint <= 0x0000FFEF) { return "Halfwidth and Fullwidth Forms";                  }
	if (/*codepoint >= 0x0000FFF0 && */codepoint <= 0x0000FFFF) { return "Specials";                                       }
	if (/*codepoint >= 0x00010000 && */codepoint <= 0x0001007F) { return "Linear B Syllabary";                             }
	if (/*codepoint >= 0x00010080 && */codepoint <= 0x000100FF) { return "Linear B Ideograms";                             }
	if (/*codepoint >= 0x00010100 && */codepoint <= 0x0001013F) { return "Aegean Numbers";                                 }
	if (/*codepoint >= 0x00010140 && */codepoint <= 0x0001018F) { return "Ancient Greek Numbers";                          }
	if (/*codepoint >= 0x00010190 && */codepoint <= 0x000101CF) { return "Ancient Symbols";                                }
	if (/*codepoint >= 0x000101D0 && */codepoint <= 0x000101FF) { return "Phaistos Disc";                                  }
	if (/*codepoint >= 0x00010280 && */codepoint <= 0x0001029F) { return "Lycian";                                         }
	if (/*codepoint >= 0x000102A0 && */codepoint <= 0x000102DF) { return "Carian";                                         }
	if (/*codepoint >= 0x000102E0 && */codepoint <= 0x000102FF) { return "Coptic Epact Numbers";                           }
	if (/*codepoint >= 0x00010300 && */codepoint <= 0x0001032F) { return "Old Italic";                                     }
	if (/*codepoint >= 0x00010330 && */codepoint <= 0x0001034F) { return "Gothic";                                         }
	if (/*codepoint >= 0x00010350 && */codepoint <= 0x0001037F) { return "Old Permic";                                     }
	if (/*codepoint >= 0x00010380 && */codepoint <= 0x0001039F) { return "Ugaritic";                                       }
	if (/*codepoint >= 0x000103A0 && */codepoint <= 0x000103DF) { return "Old Persian";                                    }
	if (/*codepoint >= 0x00010400 && */codepoint <= 0x0001044F) { return "Deseret";                                        }
	if (/*codepoint >= 0x00010450 && */codepoint <= 0x0001047F) { return "Shavian";                                        }
	if (/*codepoint >= 0x00010480 && */codepoint <= 0x000104AF) { return "Osmanya";                                        }
	if (/*codepoint >= 0x000104B0 && */codepoint <= 0x000104FF) { return "Osage";                                          }
	if (/*codepoint >= 0x00010500 && */codepoint <= 0x0001052F) { return "Elbasan";                                        }
	if (/*codepoint >= 0x00010530 && */codepoint <= 0x0001056F) { return "Caucasian Albanian";                             }
	if (/*codepoint >= 0x00010600 && */codepoint <= 0x0001077F) { return "Linear A";                                       }
	if (/*codepoint >= 0x00010800 && */codepoint <= 0x0001083F) { return "Cypriot Syllabary";                              }
	if (/*codepoint >= 0x00010840 && */codepoint <= 0x0001085F) { return "Imperial Aramaic";                               }
	if (/*codepoint >= 0x00010860 && */codepoint <= 0x0001087F) { return "Palmyrene";                                      }
	if (/*codepoint >= 0x00010880 && */codepoint <= 0x000108AF) { return "Nabataean";                                      }
	if (/*codepoint >= 0x000108E0 && */codepoint <= 0x000108FF) { return "Hatran";                                         }
	if (/*codepoint >= 0x00010900 && */codepoint <= 0x0001091F) { return "Phoenician";                                     }
	if (/*codepoint >= 0x00010920 && */codepoint <= 0x0001093F) { return "Lydian";                                         }
	if (/*codepoint >= 0x00010980 && */codepoint <= 0x0001099F) { return "Meroitic Hieroglyphs";                           }
	if (/*codepoint >= 0x000109A0 && */codepoint <= 0x000109FF) { return "Meroitic Cursive";                               }
	if (/*codepoint >= 0x00010A00 && */codepoint <= 0x00010A5F) { return "Kharoshthi";                                     }
	if (/*codepoint >= 0x00010A60 && */codepoint <= 0x00010A7F) { return "Old South Arabian";                              }
	if (/*codepoint >= 0x00010A80 && */codepoint <= 0x00010A9F) { return "Old North Arabian";                              }
	if (/*codepoint >= 0x00010AC0 && */codepoint <= 0x00010AFF) { return "Manichaean";                                     }
	if (/*codepoint >= 0x00010B00 && */codepoint <= 0x00010B3F) { return "Avestan";                                        }
	if (/*codepoint >= 0x00010B40 && */codepoint <= 0x00010B5F) { return "Inscriptional Parthian";                         }
	if (/*codepoint >= 0x00010B60 && */codepoint <= 0x00010B7F) { return "Inscriptional Pahlavi";                          }
	if (/*codepoint >= 0x00010B80 && */codepoint <= 0x00010BAF) { return "Psalter Pahlavi";                                }
	if (/*codepoint >= 0x00010C00 && */codepoint <= 0x00010C4F) { return "Old Turkic";                                     }
	if (/*codepoint >= 0x00010C80 && */codepoint <= 0x00010CFF) { return "Old Hungarian";                                  }
	if (/*codepoint >= 0x00010D00 && */codepoint <= 0x00010D3F) { return "Hanifi Rohingya";                                }
	if (/*codepoint >= 0x00010E60 && */codepoint <= 0x00010E7F) { return "Rumi Numeral Symbols";                           }
	if (/*codepoint >= 0x00010E80 && */codepoint <= 0x00010EBF) { return "Yezidi";                                         }
	if (/*codepoint >= 0x00010F00 && */codepoint <= 0x00010F2F) { return "Old Sogdian";                                    }
	if (/*codepoint >= 0x00010F30 && */codepoint <= 0x00010F6F) { return "Sogdian";                                        }
	if (/*codepoint >= 0x00010FB0 && */codepoint <= 0x00010FDF) { return "Chorasmian";                                     }
	if (/*codepoint >= 0x00010FE0 && */codepoint <= 0x00010FFF) { return "Elymaic";                                        }
	if (/*codepoint >= 0x00011000 && */codepoint <= 0x0001107F) { return "Brahmi";                                         }
	if (/*codepoint >= 0x00011080 && */codepoint <= 0x000110CF) { return "Kaithi";                                         }
	if (/*codepoint >= 0x000110D0 && */codepoint <= 0x000110FF) { return "Sora Sompeng";                                   }
	if (/*codepoint >= 0x00011100 && */codepoint <= 0x0001114F) { return "Chakma";                                         }
	if (/*codepoint >= 0x00011150 && */codepoint <= 0x0001117F) { return "Mahajani";                                       }
	if (/*codepoint >= 0x00011180 && */codepoint <= 0x000111DF) { return "Sharada";                                        }
	if (/*codepoint >= 0x000111E0 && */codepoint <= 0x000111FF) { return "Sinhala Archaic Numbers";                        }
	if (/*codepoint >= 0x00011200 && */codepoint <= 0x0001124F) { return "Khojki";                                         }
	if (/*codepoint >= 0x00011280 && */codepoint <= 0x000112AF) { return "Multani";                                        }
	if (/*codepoint >= 0x000112B0 && */codepoint <= 0x000112FF) { return "Khudawadi";                                      }
	if (/*codepoint >= 0x00011300 && */codepoint <= 0x0001137F) { return "Grantha";                                        }
	if (/*codepoint >= 0x00011400 && */codepoint <= 0x0001147F) { return "Newa";                                           }
	if (/*codepoint >= 0x00011480 && */codepoint <= 0x000114DF) { return "Tirhuta";                                        }
	if (/*codepoint >= 0x00011580 && */codepoint <= 0x000115FF) { return "Siddham";                                        }
	if (/*codepoint >= 0x00011600 && */codepoint <= 0x0001165F) { return "Modi";                                           }
	if (/*codepoint >= 0x00011660 && */codepoint <= 0x0001167F) { return "Mongolian Supplement";                           }
	if (/*codepoint >= 0x00011680 && */codepoint <= 0x000116CF) { return "Takri";                                          }
	if (/*codepoint >= 0x00011700 && */codepoint <= 0x0001173F) { return "Ahom";                                           }
	if (/*codepoint >= 0x00011800 && */codepoint <= 0x0001184F) { return "Dogra";                                          }
	if (/*codepoint >= 0x000118A0 && */codepoint <= 0x000118FF) { return "Warang Citi";                                    }
	if (/*codepoint >= 0x00011900 && */codepoint <= 0x0001195F) { return "Dives Akuru";                                    }
	if (/*codepoint >= 0x000119A0 && */codepoint <= 0x000119FF) { return "Nandinagari";                                    }
	if (/*codepoint >= 0x00011A00 && */codepoint <= 0x00011A4F) { return "Zanabazar Square";                               }
	if (/*codepoint >= 0x00011A50 && */codepoint <= 0x00011AAF) { return "Soyombo";                                        }
	if (/*codepoint >= 0x00011AC0 && */codepoint <= 0x00011AFF) { return "Pau Cin Hau";                                    }
	if (/*codepoint >= 0x00011C00 && */codepoint <= 0x00011C6F) { return "Bhaiksuki";                                      }
	if (/*codepoint >= 0x00011C70 && */codepoint <= 0x00011CBF) { return "Marchen";                                        }
	if (/*codepoint >= 0x00011D00 && */codepoint <= 0x00011D5F) { return "Masaram Gondi";                                  }
	if (/*codepoint >= 0x00011D60 && */codepoint <= 0x00011DAF) { return "Gunjala Gondi";                                  }
	if (/*codepoint >= 0x00011EE0 && */codepoint <= 0x00011EFF) { return "Makasar";                                        }
	if (/*codepoint >= 0x00011FB0 && */codepoint <= 0x00011FBF) { return "Lisu Supplement";                                }
	if (/*codepoint >= 0x00011FC0 && */codepoint <= 0x00011FFF) { return "Tamil Supplement";                               }
	if (/*codepoint >= 0x00012000 && */codepoint <= 0x000123FF) { return "Cuneiform";                                      }
	if (/*codepoint >= 0x00012400 && */codepoint <= 0x0001247F) { return "Cuneiform Numbers and Punctuation";              }
	if (/*codepoint >= 0x00012480 && */codepoint <= 0x0001254F) { return "Early Dynastic Cuneiform";                       }
	if (/*codepoint >= 0x00013000 && */codepoint <= 0x0001342F) { return "Egyptian Hieroglyphs";                           }
	if (/*codepoint >= 0x00013430 && */codepoint <= 0x0001343F) { return "Egyptian Hieroglyph Format Controls";            }
	if (/*codepoint >= 0x00014400 && */codepoint <= 0x0001467F) { return "Anatolian Hieroglyphs";                          }
	if (/*codepoint >= 0x00016800 && */codepoint <= 0x00016A3F) { return "Bamum Supplement";                               }
	if (/*codepoint >= 0x00016A40 && */codepoint <= 0x00016A6F) { return "Mro";                                            }
	if (/*codepoint >= 0x00016AD0 && */codepoint <= 0x00016AFF) { return "Bassa Vah";                                      }
	if (/*codepoint >= 0x00016B00 && */codepoint <= 0x00016B8F) { return "Pahawh Hmong";                                   }
	if (/*codepoint >= 0x00016E40 && */codepoint <= 0x00016E9F) { return "Medefaidrin";                                    }
	if (/*codepoint >= 0x00016F00 && */codepoint <= 0x00016F9F) { return "Miao";                                           }
	if (/*codepoint >= 0x00016FE0 && */codepoint <= 0x00016FFF) { return "Ideographic Symbols and Punctuation";            }
	if (/*codepoint >= 0x00017000 && */codepoint <= 0x000187FF) { return "Tangut";                                         }
	if (/*codepoint >= 0x00018800 && */codepoint <= 0x00018AFF) { return "Tangut Components";                              }
	if (/*codepoint >= 0x00018B00 && */codepoint <= 0x00018CFF) { return "Khitan Small Script";                            }
	if (/*codepoint >= 0x00018D00 && */codepoint <= 0x00018D8F) { return "Tangut Supplement";                              }
	if (/*codepoint >= 0x0001B000 && */codepoint <= 0x0001B0FF) { return "Kana Supplement";                                }
	if (/*codepoint >= 0x0001B100 && */codepoint <= 0x0001B12F) { return "Kana Extended-A";                                }
	if (/*codepoint >= 0x0001B130 && */codepoint <= 0x0001B16F) { return "Small Kana Extension";                           }
	if (/*codepoint >= 0x0001B170 && */codepoint <= 0x0001B2FF) { return "Nushu";                                          }
	if (/*codepoint >= 0x0001BC00 && */codepoint <= 0x0001BC9F) { return "Duployan";                                       }
	if (/*codepoint >= 0x0001BCA0 && */codepoint <= 0x0001BCAF) { return "Shorthand Format Controls";                      }
	if (/*codepoint >= 0x0001D000 && */codepoint <= 0x0001D0FF) { return "Byzantine Musical Symbols";                      }
	if (/*codepoint >= 0x0001D100 && */codepoint <= 0x0001D1FF) { return "Musical Symbols";                                }
	if (/*codepoint >= 0x0001D200 && */codepoint <= 0x0001D24F) { return "Ancient Greek Musical Notation";                 }
	if (/*codepoint >= 0x0001D2E0 && */codepoint <= 0x0001D2FF) { return "Mayan Numerals";                                 }
	if (/*codepoint >= 0x0001D300 && */codepoint <= 0x0001D35F) { return "Tai Xuan Jing Symbols";                          }
	if (/*codepoint >= 0x0001D360 && */codepoint <= 0x0001D37F) { return "Counting Rod Numerals";                          }
	if (/*codepoint >= 0x0001D400 && */codepoint <= 0x0001D7FF) { return "Mathematical Alphanumeric Symbols";              }
	if (/*codepoint >= 0x0001D800 && */codepoint <= 0x0001DAAF) { return "Sutton SignWriting";                             }
	if (/*codepoint >= 0x0001E000 && */codepoint <= 0x0001E02F) { return "Glagolitic Supplement";                          }
	if (/*codepoint >= 0x0001E100 && */codepoint <= 0x0001E14F) { return "Nyiakeng Puachue Hmong";                         }
	if (/*codepoint >= 0x0001E2C0 && */codepoint <= 0x0001E2FF) { return "Wancho";                                         }
	if (/*codepoint >= 0x0001E800 && */codepoint <= 0x0001E8DF) { return "Mende Kikakui";                                  }
	if (/*codepoint >= 0x0001E900 && */codepoint <= 0x0001E95F) { return "Adlam";                                          }
	if (/*codepoint >= 0x0001EC70 && */codepoint <= 0x0001ECBF) { return "Indic Siyaq Numbers";                            }
	if (/*codepoint >= 0x0001ED00 && */codepoint <= 0x0001ED4F) { return "Ottoman Siyaq Numbers";                          }
	if (/*codepoint >= 0x0001EE00 && */codepoint <= 0x0001EEFF) { return "Arabic Mathematical Alphabetic Symbols";         }
	if (/*codepoint >= 0x0001F000 && */codepoint <= 0x0001F02F) { return "Mahjong Tiles";                                  }
	if (/*codepoint >= 0x0001F030 && */codepoint <= 0x0001F09F) { return "Domino Tiles";                                   }
	if (/*codepoint >= 0x0001F0A0 && */codepoint <= 0x0001F0FF) { return "Playing Cards";                                  }
	if (/*codepoint >= 0x0001F100 && */codepoint <= 0x0001F1FF) { return "Enclosed Alphanumeric Supplement";               }
	if (/*codepoint >= 0x0001F200 && */codepoint <= 0x0001F2FF) { return "Enclosed Ideographic Supplement";                }
	if (/*codepoint >= 0x0001F300 && */codepoint <= 0x0001F5FF) { return "Miscellaneous Symbols and Pictographs";          }
	if (/*codepoint >= 0x0001F600 && */codepoint <= 0x0001F64F) { return "Emoticons";                                      }
	if (/*codepoint >= 0x0001F650 && */codepoint <= 0x0001F67F) { return "Ornamental Dingbats";                            }
	if (/*codepoint >= 0x0001F680 && */codepoint <= 0x0001F6FF) { return "Transport and Map Symbols";                      }
	if (/*codepoint >= 0x0001F700 && */codepoint <= 0x0001F77F) { return "Alchemical Symbols";                             }
	if (/*codepoint >= 0x0001F780 && */codepoint <= 0x0001F7FF) { return "Geometric Shapes Extended";                      }
	if (/*codepoint >= 0x0001F800 && */codepoint <= 0x0001F8FF) { return "Supplemental Arrows-C";                          }
	if (/*codepoint >= 0x0001F900 && */codepoint <= 0x0001F9FF) { return "Supplemental Symbols and Pictographs";           }
	if (/*codepoint >= 0x0001FA00 && */codepoint <= 0x0001FA6F) { return "Chess Symbols";                                  }
	if (/*codepoint >= 0x0001FA70 && */codepoint <= 0x0001FAFF) { return "Symbols and Pictographs Extended-A";             }
	if (/*codepoint >= 0x0001FB00 && */codepoint <= 0x0001FBFF) { return "Symbols for Legacy Computing";                   }
	if (/*codepoint >= 0x00020000 && */codepoint <= 0x0002A6DF) { return "CJK Unified Ideographs Extension B";             }
	if (/*codepoint >= 0x0002A700 && */codepoint <= 0x0002B73F) { return "CJK Unified Ideographs Extension C";             }
	if (/*codepoint >= 0x0002B740 && */codepoint <= 0x0002B81F) { return "CJK Unified Ideographs Extension D";             }
	if (/*codepoint >= 0x0002B820 && */codepoint <= 0x0002CEAF) { return "CJK Unified Ideographs Extension E";             }
	if (/*codepoint >= 0x0002CEB0 && */codepoint <= 0x0002EBEF) { return "CJK Unified Ideographs Extension F";             }
	if (/*codepoint >= 0x0002F800 && */codepoint <= 0x0002FA1F) { return "CJK Compatibility Ideographs Supplement";        }
	if (/*codepoint >= 0x00030000 && */codepoint <= 0x0003134F) { return "CJK Unified Ideographs Extension G";             }
	if (/*codepoint >= 0x000E0000 && */codepoint <= 0x000E007F) { return "Tags";                                           }
	if (/*codepoint >= 0x000E0100 && */codepoint <= 0x000E01EF) { return "Variation Selectors Supplement";                 }
	if (/*codepoint >= 0x000F0000 && */codepoint <= 0x000FFFFF) { return "Supplementary Private Use Area-A";               }
	if (/*codepoint >= 0x00100000 && */codepoint <= 0x0010FFFF) { return "Supplementary Private Use Area-B)";              }
	return "Unknown";
}

char* ConvertWideStrToUtf8(MemoryArena_t* memArena, const wchar_t* wideStrPntr, u32 wideStrLength, u32* encodedSizeOut = nullptr)
{
	Assert(wideStrPntr != nullptr || wideStrLength == 0);
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 byteIndex = 0;
		
		u8 encodeBuffer[UTF8_MAX_CHAR_SIZE];
		for (u32 cIndex = 0; cIndex < wideStrLength; cIndex++)
		{
			wchar_t wideChar = wideStrPntr[cIndex];
			u8 encodeSize = GetUtf8BytesForCode((u32)wideChar, &encodeBuffer[0], false);
			if (encodeSize == 0)
			{
				if (encodedSizeOut != nullptr) { *encodedSizeOut = 0; }
				return nullptr;
			}
			if (result != nullptr)
			{
				Assert(resultSize >= byteIndex + encodeSize);
				MyMemCopy(&result[byteIndex], &encodeBuffer[0], encodeSize);
			}
			byteIndex += encodeSize;
		}
		
		if (pass == 0)
		{
			if (encodedSizeOut != nullptr) { *encodedSizeOut = byteIndex; }
			if (memArena == nullptr) { return nullptr; }
			resultSize = byteIndex;
			result = PushArray(memArena, char, byteIndex+1);
			NotNull(result);
		}
		else
		{
			Assert(byteIndex == resultSize);
			result[resultSize] = '\0';
		}
	}
	return result;
}
char* ConvertWideStrToUtf8Nt(MemoryArena_t* memArena, const wchar_t* nullTermWideStr, u32* encodedSizeOut = nullptr)
{
	NotNull(nullTermWideStr);
	return ConvertWideStrToUtf8(memArena, nullTermWideStr, MyWideStrLength32(nullTermWideStr), encodedSizeOut);
}

#endif //  _MY_UNICODE_H
