/*
File:   my_assert.h
Author: Taylor Robbins
Date:   09\13\2017
Description:
	** This file contains the definitions for the Assert macro.
	
	If @USE_ASSERT_FAILURE_FUNCTION is defined then the file will
	declare a function AssertFailure that you must implement
	somewhere in your code.
	
	If @USE_ASSERT_FAILURE_FUNCTION is not defined then the macro
	resolves to a write to nullptr which will throw an exception
	
	In either case if @DEBUG and @CRASH_DUMP_ASSERTS are both not defined then the macro is defined
	as nothing, effectively removing whatever code was part of the Expression parameter
	
	Including this file will define @ASSERTIONS_ACTIVE to true or false depending on whether assertions
	are currently compiled in
*/

#ifndef __MY_ASSERT_H
#define __MY_ASSERT_H

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef USE_ASSERT_FAILURE_FUNCTION
#define USE_ASSERT_FAILURE_FUNCTION 0
#endif

#ifndef CRASH_DUMP_ASSERTS
#define CRASH_DUMP_ASSERTS 0
#endif

//If you define this then you should also have a MainThreadId global defined before including this file
#ifndef THREAD_SAFE_ASSERT
#define THREAD_SAFE_ASSERT 0
#endif

#if THREAD_SAFE_ASSERT
#if WINDOWS_COMPILATION
typedef DWORD ThreadId_t;
extern ThreadId_t MainThreadId;
#endif
#endif

#if (DEBUG || CRASH_DUMP_ASSERTS)
	#define ASSERTIONS_ACTIVE 1
	
	#if WINDOWS_COMPILATION
		
		#define MyDebugBreak() __debugbreak()
		
	#elif OSX_COMPILATION
		
		#define MyDebugBreak() raise(SIGINT)
		
	#elif LINUX_COMPILATION
		
		#if THREAD_SAFE_ASSERT
		typedef pthread_t ThreadId_t;
		extern ThreadId_t MainThreadId;
		#define MyDebugBreak() if (pthread_self() == MainThreadId) { raise(SIGINT); } else { pthread_exit(nullptr); }
		#else
		#define MyDebugBreak() raise(SIGINT)
		#endif
		
	#else
		
		#error You must define a compilation type for my_assert.h
		
	#endif
	
	#if USE_ASSERT_FAILURE_FUNCTION
		
		//NOTE: This function must be implemented by the user!
		extern void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr);
		
		#define Assert(Expression)  do { if(!(Expression)) { AssertFailure(__func__, __FILE__, __LINE__, #Expression); if (DEBUG) { MyDebugBreak(); } } } while(0)
		
	#else
		
		#define Assert(Expression) do { if (!(Expression)) { MyDebugBreak(); } } while(0)
		
	#endif
	
	#define NotNull(variable) Assert((variable) != nullptr)
	#define NotNull2(variable1, variable2) Assert((variable1) != nullptr && (variable2) != nullptr)
	#define NotNull3(variable1, variable2, variable3) Assert((variable1) != nullptr && (variable2) != nullptr && (variable3) != nullptr)
	#define NotNull4(variable1, variable2, variable3, variable4) Assert((variable1) != nullptr && (variable2) != nullptr && (variable3) != nullptr && (variable4) != nullptr)
	
	#define Assert_(Expression) do { if (!(Expression)) { MyDebugBreak(); } } while(0)
	#define NotNull_(variable) Assert_((variable) != nullptr)
	#define NotNull2_(variable1, variable2) Assert_((variable1) != nullptr && (variable2) != nullptr)
	#define NotNull3_(variable1, variable2, variable3) Assert_((variable1) != nullptr && (variable2) != nullptr && (variable3) != nullptr)
	#define NotNull4_(variable1, variable2, variable3, variable4) Assert_((variable1) != nullptr && (variable2) != nullptr && (variable3) != nullptr && (variable4) != nullptr)
	
	#if DEBUG
		#define DebugAssert(Expression) Assert(Expression)
		#define DebugAssert_(Expression) Assert_(Expression)
	#else
		#define DebugAssert(Expression)  //nothing
		#define DebugAssert_(Expression) //nothing
	#endif
	
#else
#define ASSERTIONS_ACTIVE 0

#define MyDebugBreak()                                        //nothing
#define Assert(Expression)                                    //nothing
#define Assert_(Expression)                                   //nothing
#define NotNull(variable)                                     //nothing
#define NotNull2(variable1, variable2)                        //nothing
#define NotNull3(variable1, variable2, variable3)             //nothing
#define NotNull4(variable1, variable2, variable3, variable4)  //nothing
#define NotNull_(variable)                                    //nothing
#define NotNull2_(variable1, variable2)                       //nothing
#define NotNull3_(variable1, variable2, variable3)            //nothing
#define NotNull4_(variable1, variable2, variable3, variable4) //nothing
#define DebugAssert(Expression)                               //nothing
#define DebugAssert_(Expression)                              //nothing

#endif

#endif // __MY_ASSERT_H
