/*
File:   my_transforms.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the Transform2_t and Transform3_t structures
	** and implements all the base functions that operate on or create them

This file is #included by default from mylib.h
*/

#ifndef _MY_TRANSFORMS_H
#define _MY_TRANSFORMS_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
struct Transform3_t
{
	v3 position;
	v3 scale;
	v3 rotation;
};

struct Transform2_t
{
	v2  position;
	v2  scale;
	v2  origin;
	r32 rotation;
};

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline Transform2_t NewTransform2(v2 position, v2 scale, v2 origin, r32 rotation)
{
	Transform2_t result = {};
	result.position = position;
	result.scale    = scale;
	result.origin   = origin;
	result.rotation = rotation;
	return result;
}

inline Transform3_t NewTransform3(v3 position, v3 scale, v3 rotation)
{
	Transform3_t result = {};
	result.position = position;
	result.scale    = scale;
	result.rotation = rotation;
	return result;
}

// +--------------------------------------------------------------+
// |                     Simple Value Defines                     |
// +--------------------------------------------------------------+
#define Transform2_Identity NewTransform2(NewVec2(0.0f, 0.0f), NewVec2(1.0f, 1.0f), NewVec2(0.0f, 0.0f), 0.0f, 0.0f)
#define Transform3_Identity NewTransform3(NewVec3(0.0f, 0.0f, 0.0f), NewVec3(1.0f, 1.0f, 1.0f), NewVec3(0.0f, 0.0f, 0.0f))

// +--------------------------------------------------------------+
// |                      Helpful Functions                       |
// +--------------------------------------------------------------+
mat4 Transform2Matrix(Transform2_t transform)
{
	mat4 result = Mat4_Identity;
	
	result = Mat4Multiply(result, Mat4Translate(NewVec3(transform.origin.x, transform.origin.y, 0.0f)));
	
	result = Mat4Multiply(result, Mat4Scale(NewVec3(transform.scale.x, transform.scale.y, 0.0f)));
	
	result = Mat4Multiply(result, Mat4RotateZ(ToRadians(transform.rotation)));
	
	result = Mat4Multiply(result, Mat4Translate(NewVec3(transform.position.x, transform.position.y, 0.0f)));
	
	return result;
}

mat4 Transform3Matrix(Transform3_t transform)
{
	mat4 result = Mat4_Identity;
	
	result = Mat4Multiply(result, Mat4Scale(transform.scale));
	
	result = Mat4Multiply(result, Mat4RotateX(ToRadians(transform.rotation.x)));
	result = Mat4Multiply(result, Mat4RotateY(ToRadians(transform.rotation.y)));
	result = Mat4Multiply(result, Mat4RotateZ(ToRadians(transform.rotation.z)));
	
	result = Mat4Multiply(result, Mat4Translate(transform.position));
	
	return result;
}

#endif //  _MY_TRANSFORMS_H
