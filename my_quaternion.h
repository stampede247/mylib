/*
File:   my_quaternion.h
Author: Taylor Robbins
Date:   09\30\2018
Description: 
	** Holds some structures and functions related to quaternion math 
*/

#ifndef _MY_QUATERNION_H
#define _MY_QUATERNION_H

// +--------------------------------------------------------------+
// |                      Struct Definition                       |
// +--------------------------------------------------------------+
union Quaternion_t
{
	r32 values[4];
	v4 vec4;
	struct
	{
		v3 axis;
		r32 angle;
	};
	struct
	{
		r32 x, y, z, w;
	};
};

// +--------------------------------------------------------------+
// |                        Alias Typedefs                        |
// +--------------------------------------------------------------+
typedef Quaternion_t quat;

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline quat QuatNormalize(const quat& q);

inline quat NewQuat(v3 axis, r32 angle, bool normalize = true)
{
	quat result = {};
	result.axis = Vec3Normalize(axis) * SinR32(angle) / 2;
	result.angle = CosR32(angle) / 2;
	if (normalize) { result = QuatNormalize(result); }
	return result;
}
inline quat NewQuat(v4 vec4, bool normalize = true) //NOTE: This is not the same as assigning the vec4 member directly!
{
	quat result = {};
	result.vec4 = vec4;
	if (normalize) { result = QuatNormalize(result); }
	return result;
}
inline quat NewQuat(r32 x, r32 y, r32 z, r32 w, bool normalize = true)
{
	quat result = {};
	result.x = x;
	result.y = y;
	result.z = z;
	result.w = w;
	if (normalize) { result = QuatNormalize(result); }
	return result;
}

// +--------------------------------------------------------------+
// |                   Basic Value Definitions                    |
// +--------------------------------------------------------------+
#define Quat_Identity NewQuat(0, 0, 0, 1, false)

// +--------------------------------------------------------------+
// |                    Basic Math Operations                     |
// +--------------------------------------------------------------+
inline quat QuatNormalize(const quat& q)
{
	//NOTE: Don't use NewQuat functions here since it might cause recursive loop
	//TODO: Is this actually what needs to happen?
	quat result = {};
	result.vec4 = Vec4Normalize(q.vec4);
	return result;
}
inline r32 QuatGetAngle(const quat& q)
{
	//TODO: Implement me better!
	return q.angle;
}
inline v3 QuatGetAxis(const quat& q)
{
	v3 result = q.axis;
	r32 resultLength = Vec3Length(result);
	if (resultLength == 0) { return Vec3_Right; }
	else { return result / resultLength; }
}

inline quat QuatLerp(const quat& start, const quat& end, r32 amount, bool linearly = true, bool normalizeResult = true)
{
	quat result;
	if (linearly)
	{
		result = NewQuat(Vec4Lerp(start.vec4, end.vec4, amount));
	}
	else
	{
		result = start; //TODO: Implement this variant!
	}
	if (normalizeResult) { result = QuatNormalize(result); }
	return result;
}

inline quat QuatMult(const quat& left, const quat& right, bool normalize = true)
{
	quat result;
	result.angle = left.angle*right.angle - Vec3Dot(left.axis, right.axis);
	result.axis = left.angle*right.axis + right.angle*left.axis + Vec3Cross(left.axis, right.axis);
	if (normalize) { result = QuatNormalize(result); }
	return result;
}

#endif //_MY_QUATERNION_H