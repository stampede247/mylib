/*
File:   my_triangulation.h
Author: Taylor Robbins
Date:   07\30\2019
Description:
	** This file holds some functions that help us triangulate polygons in various ways
*/

#ifndef _MY_TRIANGULATION_H
#define _MY_TRIANGULATION_H

#include "my_tempMemory.h"

//This function uses the ear clipping method in order to turn a convex or concave 2D polygon into collections of triangles.
//The function will return a list of indices that correspond with the traingles that need to be drawn using the given vertices.
//The indices are allocated in the given memory arena. If nullptr is give for memArena then nullptr is returned but the function still fills the numIndicesOut variable
//NOTE: The polygon should be defined in Clockwise order
u32* TraingulatePolygonEars(MemoryArena_t* memArena, u32 numVertices, const v2* vertices, u32* numIndicesOut)
{
	Assert(memArena != nullptr);
	Assert(vertices != nullptr);
	
	u32 numIndices = 0;
	u32* indices = nullptr;
	
	if (numVertices < 3)
	{
		if (numIndicesOut != nullptr) { *numIndicesOut = 0; }
		return nullptr;
	}
	else if (numVertices == 3)
	{
		numIndices = 3;
		indices = PushArray(memArena, u32, numIndices);
		Assert(indices != nullptr);
		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;
		if (numIndicesOut != nullptr) { *numIndicesOut = numIndices; }
		return indices;
	}
	
	u32 numTriangles = numVertices - 2;
	numIndices = numTriangles*3;
	if (numIndicesOut != nullptr) { *numIndicesOut = numIndices; }
	if (memArena == nullptr) { return nullptr; }
	
	indices = PushArray(memArena, u32, numIndices);
	Assert(indices != nullptr);
	
	TempPushMark();
	u32 tempNumVerts = numVertices;
	v2* tempVerts = PushArray(TempArena, v2, numVertices);
	u32* tempVertIndices = PushArray(TempArena, u32, numVertices);
	for (u32 vIndex = 0; vIndex < numVertices; vIndex++)
	{
		tempVerts[vIndex] = vertices[vIndex];
		tempVertIndices[vIndex] = vIndex;
	}
	
	u32 iIndex = 0;
	while (tempNumVerts > 3)
	{
		bool foundEar = false;
		u32 earVertIndex = 0;
		for (u32 vIndex = 0; vIndex < tempNumVerts; vIndex++)
		{
			v2 vert0 = tempVerts[(vIndex + 0) % tempNumVerts];
			v2 vert1 = tempVerts[(vIndex + 1) % tempNumVerts];
			v2 vert2 = tempVerts[(vIndex + 2) % tempNumVerts];
			if (!IsTriangleClockwise(vert0, vert1, vert2)) { continue; }
			
			bool isEar = true;
			for (u32 vIndex2 = 0; vIndex2 < tempNumVerts; vIndex2++)
			{
				if (vIndex2 < vIndex || vIndex2 > vIndex+2)
				{
					if (IsInsideTriangle(tempVerts[vIndex2], vert0, vert1, vert2))
					{
						isEar = false;
						break;
					}
				}
			}
			
			if (isEar)
			{
				foundEar = true;
				earVertIndex = vIndex;
				break;
			}
		}
		if (!foundEar)
		{
			// PrintLine_W("The polygon is invalid after %u vert(s)!", iIndex);
			TempPopMark();
			if (ArenaCanPop(memArena, indices)) { ArenaPop(memArena, indices); }
			if (numIndicesOut != nullptr) { *numIndicesOut = 0; }
			return nullptr;
		}
		Assert(foundEar);
		
		Assert(iIndex+3 <= numIndices);
		indices[iIndex] = tempVertIndices[(earVertIndex + 0) % tempNumVerts]; iIndex++;
		indices[iIndex] = tempVertIndices[(earVertIndex + 1) % tempNumVerts]; iIndex++;
		indices[iIndex] = tempVertIndices[(earVertIndex + 2) % tempNumVerts]; iIndex++;
		
		//Remove the middle vertex in the ear and shift all vertices above it down by one
		for (u32 vIndex = ((earVertIndex+1)%tempNumVerts); vIndex+1 < tempNumVerts; vIndex++)
		{
			tempVerts[vIndex] = tempVerts[vIndex+1];
			tempVertIndices[vIndex] = tempVertIndices[vIndex+1];
		}
		tempNumVerts--;
	}
	Assert(tempNumVerts == 3);
	
	Assert(iIndex == numIndices-3);
	indices[iIndex] = tempVertIndices[0]; iIndex++;
	indices[iIndex] = tempVertIndices[1]; iIndex++;
	indices[iIndex] = tempVertIndices[2]; iIndex++;
	
	TempPopMark();
	
	return indices;
}


#endif //  _MY_TRIANGULATION_H
