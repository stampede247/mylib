/*
File:   my_bucketArray.h
Author: Taylor Robbins
Date:   02\26\2020
*/

#ifndef _MY_BUCKET_ARRAY_H
#define _MY_BUCKET_ARRAY_H

struct BktArrayBucket_t
{
	BktArrayBucket_t* next;
	u32 numItems;
	u32 numItemsAlloc;
	void* items;
};

struct BktArray_t
{
	MemoryArena_t* allocArena;
	u32 itemSize;
	u32 minBucketSize;
	
	u32 length; //total numItems in the buckets
	u32 numUselessSpaces;
	
	u32 numBuckets;
	u32 lastUsedBucket;
	BktArrayBucket_t* firstBucket;
};

void DestroyBucketArray(BktArray_t* array)
{
	Assert(array != nullptr);
	BktArrayBucket_t* bucket = array->firstBucket;
	for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
	{
		Assert(bucket != nullptr);
		BktArrayBucket_t* nextBucket = bucket->next;
		ArenaPop(array->allocArena, bucket);
		bucket = nextBucket;
	}
	ClearPointer(array);
}
void CreateBucketArray(BktArray_t* array, MemoryArena_t* memArena, u32 itemSize, u32 minBucketSize = 64, u32 initialSizeRequirement = 0)
{
	Assert(array != nullptr);
	Assert(memArena != nullptr);
	Assert(minBucketSize > 0);
	ClearPointer(array);
	array->allocArena = memArena;
	array->itemSize = itemSize;
	array->minBucketSize = minBucketSize;
	array->length = 0;
	array->numUselessSpaces = 0;
	
	u32 firstBucketSize = minBucketSize;
	if (firstBucketSize < initialSizeRequirement) { firstBucketSize = initialSizeRequirement; }
	array->firstBucket = (BktArrayBucket_t*)ArenaPush(memArena, sizeof(BktArrayBucket_t) + (itemSize * firstBucketSize));
	Assert(array->firstBucket != nullptr);
	ClearPointer(array->firstBucket);
	array->firstBucket->next = nullptr;
	array->firstBucket->numItems = 0;
	array->firstBucket->numItemsAlloc = firstBucketSize;
	array->firstBucket->items = (void*)(array->firstBucket + 1);
	MyMemSet(array->firstBucket->items, 0x00, itemSize * firstBucketSize);
	array->numBuckets = 1;
	array->lastUsedBucket = 0;
}

BktArrayBucket_t* BktArrayGetLastUsedBucket(BktArray_t* array)
{
	Assert(array != nullptr);
	if (array->numBuckets == 0) { return nullptr; }
	Assert(array->lastUsedBucket < array->numBuckets);
	BktArrayBucket_t* result = array->firstBucket;
	for (u32 bIndex = 0; bIndex < array->lastUsedBucket; bIndex++)
	{
		Assert(result != nullptr);
		result = result->next;
	}
	Assert(result != nullptr);
	return result;
}
void BktArrayUpdateLastUsedBucket(BktArray_t* array)
{
	Assert(array != nullptr);
	if (array->numBuckets == 0) { return; }
	Assert(array->firstBucket != nullptr);
	Assert(array->lastUsedBucket < array->numBuckets);
	
	u32 bIndex = 0;
	//Move the lastUsedBucket index down as far as possible
	while (true)
	{
		BktArrayBucket_t* lastBucket = BktArrayGetLastUsedBucket(array);
		Assert(lastBucket != nullptr);
		if (bIndex > 0 && lastBucket->numItems < lastBucket->numItemsAlloc)
		{
			u32 numReclaimedSpaces = lastBucket->numItemsAlloc - lastBucket->numItems;
			Assert(array->numUselessSpaces >= numReclaimedSpaces);
			array->numUselessSpaces -= numReclaimedSpaces;
		}
		if (lastBucket->numItems == 0 && array->lastUsedBucket > 0)
		{
			array->lastUsedBucket--;
		}
		else { return; }
		bIndex++;
		Assert(bIndex <= array->numBuckets); //make sure this doesn't become an infinite loop on accident
	}
}
void BktArrayMoveEmptyBucketToEnd(BktArray_t* array, u32 bucketIndex)
{
	Assert(array != nullptr);
	Assert(bucketIndex < array->numBuckets);
	Assert(array->firstBucket != nullptr);
	if (bucketIndex == array->numBuckets) { return; } //already at the end
	if (bucketIndex > array->lastUsedBucket) { return; } //doesn't matter what order the last few empty buckets come in. They are all ready to be used
	Assert(bucketIndex != array->lastUsedBucket);
	BktArrayBucket_t* prevBucket = nullptr;
	BktArrayBucket_t* moveBucket = nullptr;
	BktArrayBucket_t* bucket = array->firstBucket;
	for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
	{
		Assert(bucket != nullptr);
		if (bIndex == bucketIndex)
		{
			moveBucket = bucket;
			if (prevBucket != nullptr) { prevBucket->next = bucket->next; }
			else { Assert(bIndex == 0); array->firstBucket = bucket->next; }
		}
		if (bIndex == array->numBuckets-1)
		{
			Assert(moveBucket != nullptr);
			bucket->next = moveBucket;
			moveBucket->next = nullptr;
			array->lastUsedBucket--;
			Assert(array->numUselessSpaces >= moveBucket->numItemsAlloc);
			array->numUselessSpaces -= moveBucket->numItemsAlloc;
		}
		if (bIndex < bucketIndex) { prevBucket = bucket; }
		bucket = bucket->next;
	}
}

void BktArrayExpand(BktArray_t* array, u32 numNewItemsRequired)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	if (numNewItemsRequired == 0) { return; }
	BktArrayBucket_t* lastBucket = BktArrayGetLastUsedBucket(array);
	if (lastBucket != nullptr)
	{
		u32 numSpacesAvailable = lastBucket->numItemsAlloc - lastBucket->numItems;
		while (lastBucket->next != nullptr)
		{
			lastBucket = lastBucket->next;
			Assert(lastBucket->numItems == 0);
			numSpacesAvailable += lastBucket->numItemsAlloc - lastBucket->numItems;
		}
		if (numSpacesAvailable < numNewItemsRequired)
		{
			u32 newBucketSize = array->minBucketSize;
			if (newBucketSize < numNewItemsRequired - numSpacesAvailable) { newBucketSize = (numNewItemsRequired - numSpacesAvailable); }
			BktArrayBucket_t* newBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->itemSize * newBucketSize));
			Assert(newBucket != nullptr);
			ClearPointer(newBucket);
			newBucket->next = nullptr;
			newBucket->numItems = 0;
			newBucket->numItemsAlloc = newBucketSize;
			newBucket->items = (void*)(newBucket + 1);
			MyMemSet(newBucket->items, 0x00, array->itemSize * newBucketSize);
			array->numBuckets++;
			lastBucket->next = newBucket;
		}
	}
	else
	{
		Assert(array->numBuckets == 0 && array->firstBucket == nullptr);
		Assert(array->allocArena != nullptr);
		u32 firstBucketSize = array->minBucketSize;
		if (firstBucketSize < numNewItemsRequired) { firstBucketSize = numNewItemsRequired; }
		array->firstBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->itemSize * firstBucketSize));
		Assert(array->firstBucket != nullptr);
		ClearPointer(array->firstBucket);
		array->firstBucket->next = nullptr;
		array->firstBucket->numItems = 0;
		array->firstBucket->numItemsAlloc = firstBucketSize;
		array->firstBucket->items = (void*)(array->firstBucket + 1);
		MyMemSet(array->firstBucket->items, 0x00, array->itemSize * firstBucketSize);
		array->numBuckets = 1;
		array->lastUsedBucket = 0;
	}
}

void BktArrayClear(BktArray_t* array, bool reduceToSingleBucket = false)
{
	Assert(array != nullptr);
	BktArrayBucket_t* bucket = array->firstBucket;
	while (bucket != nullptr)
	{
		bucket->numItems = 0;
		bucket = bucket->next;
	}
	array->length = 0;
	array->numUselessSpaces = 0;
	array->lastUsedBucket = 0;
	if (reduceToSingleBucket)
	{
		Assert(array->allocArena != nullptr);
		BktArrayBucket_t* prevBucket = nullptr;
		bucket = array->firstBucket;
		u32 bIndex = 0;
		while (bucket != nullptr)
		{
			BktArrayBucket_t* nextBucket = bucket->next;
			if (bucket->numItems == 0 && bIndex != 0)
			{
				Assert(bIndex > array->lastUsedBucket);
				ArenaPop(array->allocArena, bucket);
				if (prevBucket != nullptr) { prevBucket->next = nextBucket; }
				else { array->firstBucket = bucket->next; }
				Assert(array->numBuckets > 0);
				array->numBuckets--;
			}
			else { prevBucket = bucket; }
			bucket = nextBucket;
			bIndex++;
		}
		Assert(array->firstBucket != nullptr);
		Assert(array->numBuckets > 0);
		Assert(array->lastUsedBucket < array->numBuckets);
	}
}

void* BktArrayAdd_(BktArray_t* array, u32 itemSize)
{
	Assert(array != nullptr);
	Assert(array->itemSize == itemSize);
	Assert(array->allocArena != nullptr);
	
	BktArrayExpand(array, 1);
	
	BktArrayBucket_t* bucket = BktArrayGetLastUsedBucket(array);
	Assert(bucket != nullptr);
	if (bucket->numItems >= bucket->numItemsAlloc)
	{
		bucket = bucket->next;
		array->lastUsedBucket++;
	}
	Assert(bucket != nullptr);
	Assert(bucket->numItems < bucket->numItemsAlloc);
	Assert(bucket->items != nullptr);
	
	void* result = ((u8*)bucket->items) + (array->itemSize * bucket->numItems);
	bucket->numItems += 1;
	array->length += 1;
	return result;
}
#define BktArrayAdd(array, type) ((type*)BktArrayAdd_((array), sizeof(type)))

void* BktArrayGet_(BktArray_t* array, u32 itemSize, u32 index)
{
	Assert(array != nullptr);
	Assert(array->itemSize == itemSize);
	if (index >= array->length) { return nullptr; }
	u32 foundIndex = 0;
	BktArrayBucket_t* bucket = array->firstBucket;
	for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
	{
		if (index < foundIndex + bucket->numItems)
		{
			void* result = (void*)(((u8*)bucket->items) + (array->itemSize * (index - foundIndex)));
			return result;
		}
		foundIndex += bucket->numItems;
		bucket = bucket->next;
	}
	Assert(false);
	return nullptr;
}
const void* BktArrayGet_(const BktArray_t* array, u32 itemSize, u32 index) //const version
{
	return (const void*)BktArrayGet_((BktArray_t*)array, itemSize, index);
}
#define BktArrayGet(array, type, index) ((type*)BktArrayGet_((array), sizeof(type), (index)))

bool BktArrayRemoveAt(BktArray_t* array, u32 index)
{
	Assert(array != nullptr);
	if (array->numBuckets == 0 || array->firstBucket == nullptr) { return false; }
	if (index >= array->length) { return false; }
	u32 foundIndex = 0;
	BktArrayBucket_t* bucket = array->firstBucket;
	BktArrayBucket_t* prevBucket = nullptr;
	for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
	{
		if (index < foundIndex + bucket->numItems)
		{
			u32 removeIndex = (index - foundIndex);
			//copy following items down
			for (u32 iIndex = removeIndex; iIndex+1 < bucket->numItems; iIndex++)
			{
				MyMemCopy(((u8*)bucket->items) + (array->itemSize * iIndex), ((u8*)bucket->items) + (array->itemSize * (iIndex+1)), array->itemSize);
			}
			bucket->numItems--;
			array->length--;
			
			if (bIndex < array->lastUsedBucket)
			{
				//this space at the end of the bucket will become useless until the array is solidified or we become the lastUsedBucket
				array->numUselessSpaces++;
				if (bucket->numItems == 0) { BktArrayMoveEmptyBucketToEnd(array, bIndex); }
			}
			else
			{
				Assert(bIndex == array->lastUsedBucket);
				if (bucket->numItems == 0) { BktArrayUpdateLastUsedBucket(array); }
			}
			
			return true;
		}
		foundIndex += bucket->numItems;
		prevBucket = bucket;
		bucket = bucket->next;
	}
	Assert(false);
	return false;
}
bool BktArrayRemoveLast(BktArray_t* array)
{
	Assert(array != nullptr);
	if (array->numBuckets == 0 || array->firstBucket == nullptr) { return false; }
	if (array->length == 0) { return false; }
	return BktArrayRemoveAt(array, array->length-1);
}

void* BktArrayInsert_(BktArray_t* array, u32 itemSize, u32 index)
{
	//TODO: Implement me!
	return nullptr;
}
#define BktArrayInsert(array, type, index) ((type*)BktArrayInsert_((array), sizeof(type), (index)))

void* BktArrayAddBulk_(BktArray_t* array, u32 itemSize, u32 numItems, bool mustBeConsecutive)
{
	Assert(array != nullptr);
	Assert(array->itemSize == itemSize);
	Assert(array->allocArena != nullptr);
	if (numItems == 0) { return nullptr; }
	
	if (mustBeConsecutive)
	{
		BktArrayBucket_t* lastBucket = BktArrayGetLastUsedBucket(array);
		if (lastBucket != nullptr)
		{
			BktArrayBucket_t* prevBucket = nullptr;
			while (lastBucket != nullptr)
			{
				u32 numSpacesAvailable = lastBucket->numItemsAlloc - lastBucket->numItems;
				if (numSpacesAvailable >= numItems)
				{
					void* result = ((u8*)lastBucket->items) + (array->itemSize * lastBucket->numItems);
					lastBucket->numItems += numItems;
					array->length += numItems;
					return result;
				}
				else
				{
					array->lastUsedBucket++;
					array->numUselessSpaces += numSpacesAvailable;
					prevBucket = lastBucket;
					lastBucket = lastBucket->next;
				}
			}
			
			Assert(lastBucket == nullptr);
			Assert(prevBucket != nullptr);
			Assert(array->lastUsedBucket == array->numBuckets);
			Assert(array->allocArena != nullptr);
			u32 newBucketSize = array->minBucketSize;
			if (newBucketSize < numItems) { newBucketSize = numItems; }
			BktArrayBucket_t* newBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->itemSize * newBucketSize));
			Assert(newBucket != nullptr);
			ClearPointer(newBucket);
			prevBucket->next = newBucket;
			newBucket->next = nullptr;
			newBucket->numItems = numItems;
			newBucket->numItemsAlloc = newBucketSize;
			newBucket->items = (void*)(newBucket + 1);
			MyMemSet(newBucket->items, 0x00, array->itemSize * newBucketSize);
			array->numBuckets++;
			array->length += numItems;
			return newBucket->items;
		}
		else
		{
			Assert(array->numBuckets == 0 && array->firstBucket == nullptr);
			Assert(array->allocArena != nullptr);
			u32 firstBucketSize = array->minBucketSize;
			if (firstBucketSize < numItems) { firstBucketSize = numItems; }
			array->firstBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->itemSize * firstBucketSize));
			Assert(array->firstBucket != nullptr);
			ClearPointer(array->firstBucket);
			array->firstBucket->next = nullptr;
			array->firstBucket->numItems = numItems;
			array->firstBucket->numItemsAlloc = firstBucketSize;
			array->firstBucket->items = (void*)(array->firstBucket + 1);
			MyMemSet(array->firstBucket->items, 0x00, array->itemSize * firstBucketSize);
			array->numBuckets = 1;
			array->lastUsedBucket = 0;
			array->length = numItems;
			return array->firstBucket->items;
		}
	}
	else
	{
		BktArrayExpand(array, numItems);
		void* result = nullptr;
		for (u32 iIndex = 0; iIndex < numItems; iIndex++)
		{
			void* newItem = BktArrayAdd_(array, itemSize);
			Assert(newItem != nullptr);
			if (result == nullptr) { result = newItem; }
		}
		return result;
	}
}
#define BktArrayAddBulk(array, type, numItems, mustBeConsecutive) ((type*)BktArrayAddBulk_((array), sizeof(type), (numItems), (mustBeConsecutive)))

void* BktArrayInsertBulk_(BktArray_t* array, u32 itemSize, u32 index, u32 numItems, bool mustBeConsecutive)
{
	if (numItems == 0) { return nullptr; }
	if (index == array->length) { return BktArrayAddBulk_(array, itemSize, numItems, mustBeConsecutive); }
	//TODO: Implement me!
	return nullptr;
}
#define BktArrayInsertBulk(array, type, index, numItems, mustBeConsecutive) ((type*)BktArrayInsertBulk_((array), sizeof(type), (index), (numItems), (mustBeConsecutive)))

bool BktArrayRemoveRange(BktArray_t* array, u32 index, u32 numItems)
{
	//TODO: Implement me!
	return false;
}

void BktArraySolidify(BktArray_t* array, bool deallocateEmptyBuckets = false, bool singleBucket = false)
{
	Assert(array != nullptr);
	if (array->numBuckets == 0 || array->firstBucket == nullptr) { return; }
	if (singleBucket)
	{
		Assert(array->allocArena != nullptr);
		if (array->length != 0)
		{
			BktArrayBucket_t* newBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->length * array->itemSize));
			Assert(newBucket != nullptr);
			ClearPointer(newBucket);
			newBucket->items = (void*)(newBucket + 1);
			newBucket->numItemsAlloc = array->length;
			newBucket->numItems = 0;
			BktArrayBucket_t* bucket = array->firstBucket;
			for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
			{
				for (u32 iIndex = 0; iIndex < bucket->numItems; iIndex++)
				{
					Assert(newBucket->numItems < newBucket->numItemsAlloc);
					MyMemCopy(((u8*)newBucket->items) + (array->itemSize * newBucket->numItems), ((u8*)bucket->items) + (array->itemSize * iIndex), array->itemSize);
					newBucket->numItems++;
				}
				BktArrayBucket_t* nextBucket = bucket->next;
				ArenaPop(array->allocArena, bucket);
				bucket = nextBucket;
			}
			Assert(newBucket->numItems == newBucket->numItemsAlloc);
			array->numBuckets = 1;
			array->firstBucket = newBucket;
			array->lastUsedBucket = 0;
			array->numUselessSpaces = 0;
		}
		else
		{
			//We kind of need to do something special for 0 item array being solidified into a single bucket.
			//This doesn't really need any memory but we are just going to allocate a relatively small bucket
			//so that the BktArray_t doesn't need to be initialized again in order to keep using it after this
			BktArrayBucket_t* newBucket = (BktArrayBucket_t*)ArenaPush(array->allocArena, sizeof(BktArrayBucket_t) + (array->minBucketSize * array->itemSize));
			Assert(newBucket != nullptr);
			ClearPointer(newBucket);
			newBucket->items = (void*)(newBucket + 1);
			newBucket->numItemsAlloc = array->minBucketSize;
			newBucket->numItems = 0;
			BktArrayBucket_t* bucket = array->firstBucket;
			for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
			{
				BktArrayBucket_t* nextBucket = bucket->next;
				ArenaPop(array->allocArena, bucket);
				bucket = nextBucket;
			}
			array->numBuckets = 1;
			array->firstBucket = newBucket;
			array->lastUsedBucket = 0;
			array->numUselessSpaces = 0;
		}
	}
	else
	{
		u32 newLastUsedBucket = 0;
		BktArrayBucket_t* destBucket = array->firstBucket;
		u32 destBucketIndex = 0;
		BktArrayBucket_t* bucket = array->firstBucket;
		for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
		{
			for (u32 iIndex = 0; iIndex < bucket->numItems; iIndex++)
			{
				if (bucket != destBucket || iIndex != destBucketIndex)
				{
					Assert(destBucket != nullptr);
					Assert(destBucketIndex < destBucket->numItemsAlloc);
					MyMemCopy(((u8*)destBucket->items) + (array->itemSize * destBucketIndex), ((u8*)bucket->items) + (array->itemSize * iIndex), array->itemSize);
				}
				destBucketIndex++;
				if (destBucketIndex >= destBucket->numItemsAlloc)
				{
					destBucket->numItems = destBucket->numItemsAlloc;
					destBucket = destBucket->next;
					destBucketIndex = 0;
					newLastUsedBucket++;
				}
			}
			bucket = bucket->next;
		}
		if (destBucketIndex == 0 && newLastUsedBucket > 0) { newLastUsedBucket--; } //we didn't actually use that bucket so go back one
		Assert(newLastUsedBucket < array->numBuckets);
		if (destBucket != nullptr)
		{
			Assert(destBucketIndex < destBucket->numItemsAlloc);
			destBucket->numItems = destBucketIndex;
			while (destBucket->next != nullptr)
			{
				destBucket = destBucket->next;
				destBucket->numItems = 0;
			}
		}
		array->numUselessSpaces = 0;
		array->lastUsedBucket = newLastUsedBucket;
		
		if (deallocateEmptyBuckets)
		{
			BktArrayBucket_t* prevBucket = nullptr;
			bucket = array->firstBucket;
			u32 bIndex = 0;
			while (bucket != nullptr)
			{
				BktArrayBucket_t* nextBucket = bucket->next;
				if (bucket->numItems == 0 && bIndex != 0)
				{
					Assert(bIndex > array->lastUsedBucket);
					ArenaPop(array->allocArena, bucket);
					if (prevBucket != nullptr) { prevBucket->next = nextBucket; }
					else { array->firstBucket = bucket->next; }
					Assert(array->numBuckets > 0);
					array->numBuckets--;
				}
				else { prevBucket = bucket; }
				bucket = nextBucket;
				bIndex++;
			}
			Assert(array->firstBucket != nullptr);
			Assert(array->numBuckets > 0);
			Assert(array->lastUsedBucket < array->numBuckets);
		}
	}
}

//TODO: Make a function that calculates how much memory a BktArray_t is using

//NOTE: Setting lenient to true will make this function return true if the pointer is anywhere within any of the
//      allocated space for the bucket array (disregarding alignment and empty members)
bool IsPntrInBktArray(const BktArray_t* array, const void* item, u32* indexOut = nullptr, bool lenient = false)
{
	BktArrayBucket_t* bucket = array->firstBucket;
	u32 bucketBaseIndex = 0;
	for (u32 bIndex = 0; bIndex < array->numBuckets; bIndex++)
	{
		if ((const u8*)item >= (const u8*)bucket->items && (const u8*)item < ((const u8*)bucket->items) + (array->itemSize * bucket->numItems))
		{
			u32 inBucketOffset = (u32)((const u8*)item - (const u8*)bucket->items);
			if (!lenient && (inBucketOffset % array->itemSize) != 0) { return false; } //the pointer is in the bucket array but isn't pointing to the beginning of an item
			u32 inBucketIndex = (inBucketOffset / array->itemSize);
			if (!lenient && inBucketIndex >= bucket->numItems) { return false; } //The pointer is in the bucket but it's pointing to an empty item
			if (indexOut != nullptr) { *indexOut = bucketBaseIndex + inBucketIndex; }
			return true;
		}
		bucketBaseIndex += bucket->numItems;
		bucket = bucket->next;
	}
	return false;
}

#ifdef _MY_SORTING_H 
void BktArraySort(BktArray_t* array, CompareFunc_f* compareFunc, void* contextPntr)
{
	Assert(array != nullptr);
	Assert(compareFunc != nullptr);
	void* workingSpace = alloca(array->itemSize*2);
	Assert(workingSpace != nullptr);
	BktArraySolidify(array, true, true);
	Assert(array->numBuckets == 1);
	Assert(array->length == 0 || array->length == array->firstBucket->numItems);
	QuickSort(array->firstBucket->items, array->firstBucket->numItems, array->itemSize, workingSpace, compareFunc, contextPntr);
}
#endif

#endif //  _MY_BUCKET_ARRAY_H
