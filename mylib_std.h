/*
File:   mylib_std.h
Author: Taylor Robbins
Date:   05\12\2020
Description:
	** This file acts as a replacement for some standard library headers like stdbool.h, stdint.h, and stdio.h
*/

#ifndef _MYLIB_STD_H
#define _MYLIB_STD_H

#ifndef USE_MY_STD_LIB
#define USE_MY_STD_LIB 0
#endif

#ifndef USE_MIN_STD_LIB
#define USE_MIN_STD_LIB 0
#endif

#if USE_MY_STD_LIB

#ifndef _MSC_VER
#error mylib_std.h is only designed to work on Microsoft Compilers right now!
#endif

// +--------------------------------------------------------------+
// |                          stdbool.h                           |
// +--------------------------------------------------------------+
#ifndef __cplusplus
#define bool   _Bool
#define false  0
#define true   1
#endif

//#define size_t?
//#define NULL?

// +--------------------------------------------------------------+
// |                           stdint.h                           |
// +--------------------------------------------------------------+
#if (_MSC_VER < 1300)
	typedef signed char       int8_t;
	typedef signed short      int16_t;
	typedef signed int        int32_t;
	typedef unsigned char     uint8_t;
	typedef unsigned short    uint16_t;
	typedef unsigned int      uint32_t;
#else
	typedef signed __int8     int8_t;
	typedef signed __int16    int16_t;
	typedef signed __int32    int32_t;
	typedef unsigned __int8   uint8_t;
	typedef unsigned __int16  uint16_t;
	typedef unsigned __int32  uint32_t;
#endif

typedef signed __int64   int64_t;
typedef unsigned __int64 uint64_t;

// +--------------------------------------------------------------+
// |                           string.h                           |
// +--------------------------------------------------------------+
void memset(void* dest, unsigned char value, size_t length)
{
	for (size_t bIndex = 0; bIndex < length; bIndex++)
	{
		((unsigned char*)dest)[bIndex] = value;
	}
}

int memcmp(const void* ptr1, const void* ptr2, size_t length)
{
	for (size_t bIndex = 0; bIndex < length; bIndex++)
	{
		if (((unsigned char*)ptr1)[bIndex] < ((unsigned char*)ptr2)[bIndex]) { return -1; }
		if (((unsigned char*)ptr1)[bIndex] > ((unsigned char*)ptr2)[bIndex]) { return 1; }
	}
	return 0;
}

void memcpy(void* dest, const void* source, size_t length)
{
	if (dest == source) { return; }
	if ((unsigned char*)dest < (unsigned char*)source)
	{
		for (size_t bIndex = 0; bIndex < length; bIndex++)
		{
			((unsigned char*)dest)[bIndex] = ((unsigned char*)source)[bIndex];
		}
	}
	else
	{
		for (size_t bIndex = length; bIndex > 0; bIndex--)
		{
			((unsigned char*)dest)[bIndex-1] = ((unsigned char*)source)[bIndex-1];
		}
	}
}
void memmove(void* dest, const void* source, size_t length)
{
	if (dest == source) { return; }
	if ((unsigned char*)dest < (unsigned char*)source)
	{
		for (size_t bIndex = 0; bIndex < length; bIndex++)
		{
			((unsigned char*)dest)[bIndex] = ((unsigned char*)source)[bIndex];
		}
	}
	else
	{
		for (size_t bIndex = length; bIndex > 0; bIndex--)
		{
			((unsigned char*)dest)[bIndex-1] = ((unsigned char*)source)[bIndex-1];
		}
	}
}

void strcpy(char* dest, const char* source)
{
	size_t cIndex = 0;
	while (source[cIndex] != '\0')
	{
		dest[cIndex] = source[cIndex];
		cIndex++;
	}
	dest[cIndex] = '\0';
}

int strcmp(const char* str1, const char* str2)
{
	size_t cIndex = 0;
	while (true)
	{
		if (str1[cIndex] < str2[cIndex]) { return -1; }
		if (str1[cIndex] > str2[cIndex]) { return  1; }
		if (str1[cIndex] == '\0') { break; }
		cIndex++;
	}
	return 0;
}

int strncmp(const char* str1, const char* str2, size_t length)
{
	for (size_t cIndex = 0; cIndex < length; cIndex++)
	{
		if (str1[cIndex] < str2[cIndex]) { return -1; }
		if (str1[cIndex] > str2[cIndex]) { return  1; }
		if (str1[cIndex] == '\0') { break; } //TODO: Should this check happen before comparison?
	}
	return 0;
}

size_t strlen(const char* str)
{
	size_t result = 0;
	while (str[result] != '\0') { result++; }
	return result;
}

size_t wcslen(const wchar_t* str)
{
	size_t result = 0;
	while (str[result] != 0x0000) { result++; }
	return result;
}

const char* strstr(const char* str1, const char* str2)
{
	size_t cIndex = 0;
	while (str1[cIndex] != '\0')
	{
		size_t cIndex2 = 0;
		bool matches = true;
		while (str2[cIndex2] != '\0')
		{
			if (str1[cIndex+cIndex2] != str2[cIndex2]) { matches = false; break; }
			cIndex2++;
		}
		if (matches) { return &str1[cIndex]; }
		cIndex++;
	}
	return nullptr;
}

#ifndef malloc
void* malloc(size_t size)
{
	//TODO: Implement me with a global callback pointer or something?
	return nullptr;
}
#endif

#ifndef realloc
void* realloc(void* pntr, size_t newSize)
{
	//TODO: Implement me with a global callback pointer or something?
	return nullptr;
}
#endif

#ifndef free
void free(void* pointer)
{
	//TODO: Implement me with a global callback pointer or something?
}
#endif

int vsnprintf(char* s, size_t n, const char* format, va_list arg)
{
	Assert(false && "cust vsnprintf not implemented yet!");
	return -1;
}
int vsnprintf_s(char* s, size_t n, size_t count, const char* format, va_list arg)
{
	Assert(false && "cust vsnprintf_s not implemented yet!");
	return -1;
}

#define MAKE_MY_STD_WRAPPER_MACROS

#elif USE_MIN_STD_LIB

// +--------------------------------------------------------------+
// |              My Aternatives to StdLib Functions              |
// +--------------------------------------------------------------+
#include <stdint.h>
#include <stdbool.h>
#include <algorithm> //Used for min and max functions

#include <stdarg.h> //TODO: Add support for using custom printf family of functions

void MyMemSet(void* dest, unsigned char value, size_t length)
{
	for (size_t bIndex = 0; bIndex < length; bIndex++)
	{
		((unsigned char*)dest)[bIndex] = value;
	}
}

int MyMemCompare(const void* ptr1, const void* ptr2, size_t length)
{
	for (size_t bIndex = 0; bIndex < length; bIndex++)
	{
		if (((unsigned char*)ptr1)[bIndex] < ((unsigned char*)ptr2)[bIndex]) { return -1; }
		if (((unsigned char*)ptr1)[bIndex] > ((unsigned char*)ptr2)[bIndex]) { return 1; }
	}
	return 0;
}

void MyMemCopy(void* dest, const void* source, size_t length)
{
	if (dest == source) { return; }
	if ((unsigned char*)dest < (unsigned char*)source)
	{
		for (size_t bIndex = 0; bIndex < length; bIndex++)
		{
			((unsigned char*)dest)[bIndex] = ((unsigned char*)source)[bIndex];
		}
	}
	else
	{
		for (size_t bIndex = length; bIndex > 0; bIndex--)
		{
			((unsigned char*)dest)[bIndex-1] = ((unsigned char*)source)[bIndex-1];
		}
	}
}
void MyMemMove(void* dest, const void* source, size_t length)
{
	if (dest == source) { return; }
	if ((unsigned char*)dest < (unsigned char*)source)
	{
		for (size_t bIndex = 0; bIndex < length; bIndex++)
		{
			((unsigned char*)dest)[bIndex] = ((unsigned char*)source)[bIndex];
		}
	}
	else
	{
		for (size_t bIndex = length; bIndex > 0; bIndex--)
		{
			((unsigned char*)dest)[bIndex-1] = ((unsigned char*)source)[bIndex-1];
		}
	}
}

void MyStrCopyNt(char* dest, const char* source)
{
	size_t cIndex = 0;
	while (source[cIndex] != '\0')
	{
		dest[cIndex] = source[cIndex];
		cIndex++;
	}
	dest[cIndex] = '\0';
}

int MyStrCompareNt(const char* str1, const char* str2)
{
	size_t cIndex = 0;
	while (true)
	{
		if (str1[cIndex] < str2[cIndex]) { return -1; }
		if (str1[cIndex] > str2[cIndex]) { return  1; }
		if (str1[cIndex] == '\0') { break; }
		cIndex++;
	}
	return 0;
}

int MyStrCompare(const char* str1, const char* str2, size_t length)
{
	for (size_t cIndex = 0; cIndex < length; cIndex++)
	{
		if (str1[cIndex] < str2[cIndex]) { return -1; }
		if (str1[cIndex] > str2[cIndex]) { return  1; }
		if (str1[cIndex] == '\0') { break; } //TODO: Should this check happen before comparison?
	}
	return 0;
}

size_t MyStrLength(const char* str)
{
	size_t result = 0;
	while (str[result] != '\0') { result++; }
	return result;
}
uint32_t MyStrLength32(const char* str)
{
	uint32_t result = 0;
	while (str[result] != '\0') { result++; }
	return result;
}

size_t MyWideStrLength(const wchar_t* str)
{
	size_t result = 0;
	while (str[result] != 0x0000) { result++; }
	return result;
}
uint32_t MyWideStrLength32(const wchar_t* str)
{
	uint32_t result = 0;
	while (str[result] != 0x0000) { result++; }
	return result;
}

const char* MyStrStrNt(const char* str1, const char* str2)
{
	size_t cIndex = 0;
	while (str1[cIndex] != '\0')
	{
		size_t cIndex2 = 0;
		bool matches = true;
		while (str2[cIndex2] != '\0')
		{
			if (str1[cIndex+cIndex2] != str2[cIndex2]) { matches = false; break; }
			cIndex2++;
		}
		if (matches) { return &str1[cIndex]; }
		cIndex++;
	}
	return nullptr;
}

#ifndef MyMalloc
void* MyMalloc(size_t size)
{
	//TODO: Implement me with a global callback pointer or something?
	return nullptr;
}
#endif

#ifndef MyRealloc
void* MyRealloc(void* pntr, size_t newSize)
{
	//TODO: Implement me with a global callback pointer or something?
	return nullptr;
}
#endif

#ifndef MyFree
void MyFree(void* pointer)
{
	//TODO: Implement me with a global callback pointer or something?
}
#endif

#if 0//TODO: Do we want to default back to vsnprintf in some cases?
#define MyVaListPrintf(buffer, bufferSize, formatStr, vaList) vsnprintf_s(buffer, bufferSize, _TRUNCATE, formatStr, vaList)
#else
#define MyVaListPrintf(buffer, bufferSize, formatStr, vaList) vsnprintf(buffer, bufferSize, formatStr, vaList)
#endif

#else //!USE_MY_STD_LIB && !USE_MIN_STD_LIB

// +--------------------------------------------------------------+
// |                Alternate Form Reroute Macros                 |
// +--------------------------------------------------------------+
#include <stdint.h>
#include <stdbool.h>
#include <algorithm> //Used for min and max functions

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#if WINDOWS_COMPILATION
#include <intrin.h>
#elif OSX_COMPILATION
#include <signal.h>
#elif LINUX_COMPILATION
#include <signal.h>
#endif

#define MAKE_MY_STD_WRAPPER_MACROS

#endif //USE_MY_STD_LIB / USE_MIN_STD_LIB

// +--------------------------------------------------------------+
// |                    My Std Wrapper Macros                     |
// +--------------------------------------------------------------+
#ifdef MAKE_MY_STD_WRAPPER_MACROS
	#define MyMemSet(dest, value, length)     memset(dest, value, length)
	#define MyMemCompare(ptr1, ptr2, length)  memcmp(ptr1, ptr2, length)
	#define MyMemCopy(dest, source, length)   memcpy(dest, source, length)
	#define MyMemMove(dest, source, length)   memmove(dest, source, length)
	#define MyStrCopyNt(dest, source)         strcpy(dest, source)
	#define MyStrCompareNt(str1, str2)        strcmp(str1, str2)
	#define MyStrCompare(str1, str2, length)  strncmp(str1, str2, length)
	#define MyStrLength(str)                  strlen(str)
	#define MyStrLength32(str)                ((u32)strlen(str))
	#define MyWideStrLength(str)              wcslen(str)
	#define MyWideStrLength32(str)            ((u32)wcslen(str))
	#define MyStrStrNt(str1, str2)            strstr(str1, str2)
	#ifndef MyMalloc
	#define MyMalloc(size)                    malloc(size)
	#endif
	#ifndef MyRealloc
	#define MyRealloc(pntr, newSize)          realloc(pntr, size)
	#endif
	#ifndef MyFree
	#define MyFree(size)                      free(size)
	#endif
	#if 0//TODO: Do we want to default back to vsnprintf in some cases?
	#define MyVaListPrintf(buffer, bufferSize, formatStr, vaList) vsnprintf_s(buffer, bufferSize, _TRUNCATE, formatStr, vaList)
	#else
	#define MyVaListPrintf(buffer, bufferSize, formatStr, vaList) vsnprintf(buffer, bufferSize, formatStr, vaList)
	#endif
#endif //MAKE_MY_STD_WRAPPER_MACROS

#endif //  _MYLIB_STD_H
