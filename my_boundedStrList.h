/*
File:   boundedStrList.h
Author: Taylor Robbins
Date:   10\09\2017
Description:
	** This header defines a simple method of allocating an array of strings that
	** are known to be less than some specific size at maximum into a single allocation
	** chunk so they can easily be deallocated with a single ArenaPop while also
	** making the strings easy to look up by index. To accomplish this it must be
	** given a max number of items in the array and a max length for each string.
	** It then allocates a lookup table and block for holding string data in a single
	** arena allocation that will hold this max size. You can then dynamically start filling
	** this chunk up to the maximum size that was allocated at the beginning. The strings
	** can be accessed by index off of the char** strings.
	** e.g. myStrList.strings[10] will give you a char pointer to the null-terminated string at index 10
*/

#ifndef _BOUNDED_STR_LIST_H
#define _BOUNDED_STR_LIST_H

struct BoundedStrList_t
{
	u32 maxNumStrings;
	u32 maxStringLength;
	
	u32 count;
	char** values;
	
	u32 memorySize;
	u8* memoryBase;
	u8* memoryPntr;
	
	char* operator [](u32 index)
	{
		return values[index];
	}
};

void BoundedStrListCreate(BoundedStrList_t* strListPntr, u32 maxNumStrings, u32 maxStringLength, MemoryArena_t* memArena)
{
	Assert(strListPntr != nullptr);
	Assert(memArena != nullptr);
	Assert(maxNumStrings > 0 && maxStringLength > 0);
	
	BoundedStrList_t result = {};
	result.maxNumStrings = maxNumStrings;
	result.maxStringLength = maxStringLength;
	result.memorySize = (maxStringLength+1)*maxNumStrings + (sizeof(char*)*maxNumStrings);
	result.memoryBase = (u8*)ArenaPush(memArena, result.memorySize);
	MyMemSet(result.memoryBase, 0x00, result.memorySize);
	Assert(result.memoryBase != nullptr);
	
	result.count = 0;
	result.values = (char**)result.memoryBase;
	result.memoryPntr = (u8*)(&result.values[maxNumStrings]);
	
	*strListPntr = result;
}

//NOTE: You can easily do this deallocation yourself if you want. Just pop the u8* memoryBase off the arena you allocated it on
void BoundedStrListDestroy(BoundedStrList_t* strListPntr, MemoryArena_t* memArena)
{
	Assert(strListPntr != nullptr);
	Assert(memArena != nullptr);
	
	if (strListPntr->memoryBase == nullptr) { return; } //Already deallocated
	
	ArenaPop(memArena, strListPntr->memoryBase);
	ClearPointer(strListPntr);
}

char* BoundedStrListAdd(BoundedStrList_t* strListPntr, const char* newString)
{
	Assert(strListPntr != nullptr);
	Assert(newString != nullptr);
	Assert(strListPntr->memoryBase != nullptr);
	Assert(strListPntr->memoryPntr != nullptr);
	Assert(strListPntr->values != nullptr);
	Assert(strListPntr->memoryPntr >= strListPntr->memoryBase);
	Assert(strListPntr->memoryPntr < strListPntr->memoryBase + strListPntr->memorySize);
	Assert(strListPntr->count < strListPntr->maxNumStrings);
	
	u32 newStringLength = MyStrLength32(newString);
	Assert(strListPntr->memoryPntr + newStringLength+1 < strListPntr->memoryBase + strListPntr->memorySize);
	
	char* result = (char*)strListPntr->memoryPntr;
	strncpy(result, newString, newStringLength);
	result[newStringLength] = '\0';
	
	strListPntr->memoryPntr += newStringLength+1;
	strListPntr->values[strListPntr->count] = result;
	strListPntr->count++;
	
	return result;
}

void BoundedStrListSolidify(BoundedStrList_t* strListPntr, MemoryArena_t* oldArena, MemoryArena_t* newArena = nullptr, bool popOldMemory = true)
{
	Assert(strListPntr != nullptr);
	Assert(oldArena != nullptr || !popOldMemory);
	if (newArena == nullptr) { newArena = oldArena; }
	Assert(newArena != nullptr);
	Assert(strListPntr->memoryBase != nullptr);
	
	if (strListPntr->count == 0)
	{
		//NOTE: There were no strings allocated into the list. We should simply deallocate everything in this case
		if (popOldMemory)
		{
			ArenaPop(oldArena, strListPntr->memoryBase);
		}
		strListPntr->memoryBase = nullptr;
		strListPntr->values = nullptr;
		strListPntr->memoryPntr = nullptr;
		strListPntr->maxNumStrings = 0;
		return;
	}
	
	u32 newAllocationSize = strListPntr->count * sizeof(char*);
	u8* oldStringBase = (u8*)(&strListPntr->values[strListPntr->maxNumStrings]);
	newAllocationSize += (u32)(strListPntr->memoryPntr - oldStringBase);
	
	u8* newAllocation = (u8*)ArenaPush(newArena, newAllocationSize);
	char** newStringsArray = (char**)newAllocation;
	u8* newStringBase = (u8*)(&newStringsArray[strListPntr->count]);
	
	MyMemCopy(newStringsArray, strListPntr->values, sizeof(char*) * strListPntr->count);
	MyMemCopy(newStringBase, oldStringBase, (u32)(strListPntr->memoryPntr - oldStringBase));
	
	for (u32 sIndex = 0; sIndex < strListPntr->count; sIndex++)
	{
		newStringsArray[sIndex] = (char*)((u8*)newStringsArray[sIndex] - oldStringBase + newStringBase);
	}
	
	if (popOldMemory)
	{
		ArenaPop(oldArena, strListPntr->memoryBase);
	}
	
	strListPntr->memoryPntr = strListPntr->memoryPntr - oldStringBase + newStringBase;
	strListPntr->memoryBase = newAllocation;
	strListPntr->memorySize = newAllocationSize;
	Assert(strListPntr->memoryPntr == strListPntr->memoryBase + strListPntr->memorySize);
	strListPntr->values = newStringsArray;
	strListPntr->maxNumStrings = strListPntr->count;
}


#endif //  _BOUNDED_STR_LIST_H
