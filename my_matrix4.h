/*
File:   my_matrices.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the Matrix4_t structure (alias mat4)
	** and implements all the base functions that operate on or create it

This file is #included by default from mylib.h
*/

#ifndef _MY_MATRICES_H
#define _MY_MATRICES_H

// +--------------------------------------------------------------+
// |                      Struct Definition                       |
// +--------------------------------------------------------------+
union Matrix4_t
{
	//         C  R
	r32 values[4][4];
	struct
	{
		r32 col0[4];
		r32 col1[4];
		r32 col2[4];
		r32 col3[4];
	};
	struct
	{
		r32 r0c0, r1c0, r2c0, r3c0;
		r32 r0c1, r1c1, r2c1, r3c1;
		r32 r0c2, r1c2, r2c2, r3c2;
		r32 r0c3, r1c3, r2c3, r3c3;
	};
	struct
	{
		r32 row0col0, row1col0, row2col0, row3col0;
		r32 row0col1, row1col1, row2col1, row3col1;
		r32 row0col2, row1col2, row2col2, row3col2;
		r32 row0col3, row1col3, row2col3, row3col3;
	};
};

// +--------------------------------------------------------------+
// |                        Alias Typedefs                        |
// +--------------------------------------------------------------+
typedef Matrix4_t mat4;

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline mat4 NewMat4(
	r32 r0c0, r32 r0c1, r32 r0c2, r32 r0c3,
	r32 r1c0, r32 r1c1, r32 r1c2, r32 r1c3,
	r32 r2c0, r32 r2c1, r32 r2c2, r32 r2c3,
	r32 r3c0, r32 r3c1, r32 r3c2, r32 r3c3)
{
	mat4 result = {
		r0c0, r1c0, r2c0, r3c0,
		r0c1, r1c1, r2c1, r3c1,
		r0c2, r1c2, r2c2, r3c2,
		r0c3, r1c3, r2c3, r3c3
	};
	return result;
}
inline mat4 NewMat4(r32 all)
{
	mat4 result = {
		all, all, all, all,
		all, all, all, all,
		all, all, all, all,
		all, all, all, all
	};
	return result;
}

// +--------------------------------------------------------------+
// |                     Simple Value Defines                     |
// +--------------------------------------------------------------+
#define Matrix4_Identity NewMat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f)
#define Mat4_Identity Matrix4_Identity

// +--------------------------------------------------------------+
// |                Basic Math Operation Functions                |
// +--------------------------------------------------------------+
inline mat4 Mat4Multiply(const mat4& left, const mat4& right)
{
	mat4 result = {
		left.r0c0*right.r0c0 + left.r0c1*right.r1c0 + left.r0c2*right.r2c0 + left.r0c3*right.r3c0, //r0c0
		left.r1c0*right.r0c0 + left.r1c1*right.r1c0 + left.r1c2*right.r2c0 + left.r1c3*right.r3c0, //r1c0
		left.r2c0*right.r0c0 + left.r2c1*right.r1c0 + left.r2c2*right.r2c0 + left.r2c3*right.r3c0, //r2c0
		left.r3c0*right.r0c0 + left.r3c1*right.r1c0 + left.r3c2*right.r2c0 + left.r3c3*right.r3c0, //r3c0
		
		left.r0c0*right.r0c1 + left.r0c1*right.r1c1 + left.r0c2*right.r2c1 + left.r0c3*right.r3c1, //r0c1
		left.r1c0*right.r0c1 + left.r1c1*right.r1c1 + left.r1c2*right.r2c1 + left.r1c3*right.r3c1, //r1c1
		left.r2c0*right.r0c1 + left.r2c1*right.r1c1 + left.r2c2*right.r2c1 + left.r2c3*right.r3c1, //r2c1
		left.r3c0*right.r0c1 + left.r3c1*right.r1c1 + left.r3c2*right.r2c1 + left.r3c3*right.r3c1, //r3c1
		
		left.r0c0*right.r0c2 + left.r0c1*right.r1c2 + left.r0c2*right.r2c2 + left.r0c3*right.r3c2, //r0c2
		left.r1c0*right.r0c2 + left.r1c1*right.r1c2 + left.r1c2*right.r2c2 + left.r1c3*right.r3c2, //r1c2
		left.r2c0*right.r0c2 + left.r2c1*right.r1c2 + left.r2c2*right.r2c2 + left.r2c3*right.r3c2, //r2c2
		left.r3c0*right.r0c2 + left.r3c1*right.r1c2 + left.r3c2*right.r2c2 + left.r3c3*right.r3c2, //r3c2
		
		left.r0c0*right.r0c3 + left.r0c1*right.r1c3 + left.r0c2*right.r2c3 + left.r0c3*right.r3c3, //r0c3
		left.r1c0*right.r0c3 + left.r1c1*right.r1c3 + left.r1c2*right.r2c3 + left.r1c3*right.r3c3, //r1c3
		left.r2c0*right.r0c3 + left.r2c1*right.r1c3 + left.r2c2*right.r2c3 + left.r2c3*right.r3c3, //r2c3
		left.r3c0*right.r0c3 + left.r3c1*right.r1c3 + left.r3c2*right.r2c3 + left.r3c3*right.r3c3  //r3c3
	};
	return result;
}
//NOTE: This will multiply left*right FIRST and then multiply by outer
//		It's the same as outer * (left * right)
inline mat4 Mat4Multiply(const mat4& outer, const mat4& left, const mat4& right)
{
	return Mat4Multiply(outer, Mat4Multiply(left, right));
}

inline v2 Mat4MultiplyVec2(const mat4& matrix, v2 vector)
{
	v2 result = NewVec2(
		matrix.r0c0*vector.x + matrix.r0c1*vector.y + matrix.r0c2*0.0f + matrix.r0c3*1.0f,
		matrix.r1c0*vector.x + matrix.r1c1*vector.y + matrix.r1c2*0.0f + matrix.r1c3*1.0f
	);
	//TODO: Do we need to divide by w result?
	
	return  result;
}
inline v3 Mat4MultiplyVec3(const mat4& matrix, v3 vector, r32* wOut = nullptr)
{
	v3 result = NewVec3(
		matrix.r0c0*vector.x + matrix.r0c1*vector.y + matrix.r0c2*vector.z + matrix.r0c3*1.0f,
		matrix.r1c0*vector.x + matrix.r1c1*vector.y + matrix.r1c2*vector.z + matrix.r1c3*1.0f,
		matrix.r2c0*vector.x + matrix.r2c1*vector.y + matrix.r2c2*vector.z + matrix.r2c3*1.0f
	);
	
	r32 wValue = matrix.r3c0*vector.x + matrix.r3c1*vector.y + matrix.r3c2*vector.z + matrix.r3c3*1.0f;
	if (wOut != nullptr) { *wOut = wValue; }
	
	result = (1.0f / wValue) * result;
	
	return  result;
}
inline v3 Mat4MultiplyRightVec3(v3 vector, const mat4& matrix, r32* wOut = nullptr)
{
	v3 result = NewVec3(
		matrix.r0c0*vector.x + matrix.r1c0*vector.y + matrix.r2c0*vector.z + matrix.r3c0*1.0f,
		matrix.r0c1*vector.x + matrix.r1c1*vector.y + matrix.r2c1*vector.z + matrix.r3c1*1.0f,
		matrix.r0c2*vector.x + matrix.r1c2*vector.y + matrix.r2c2*vector.z + matrix.r3c2*1.0f
	);
	
	r32 wValue = matrix.r0c3*vector.x + matrix.r1c3*vector.y + matrix.r2c3*vector.z + matrix.r3c3*1.0f;
	if (wOut != nullptr) { *wOut = wValue; }
	
	result = (1.0f / wValue) * result;
	
	return  result;
}
inline v4 Mat4MultiplyVec4(const mat4& matrix, v4 vector) //TODO: Check this function?
{
	v4 result = NewVec4(
		matrix.r0c0*vector.x + matrix.r0c1*vector.y + matrix.r0c2*vector.z + matrix.r0c3*vector.w,
		matrix.r1c0*vector.x + matrix.r1c1*vector.y + matrix.r1c2*vector.z + matrix.r1c3*vector.w,
		matrix.r2c0*vector.x + matrix.r2c1*vector.y + matrix.r2c2*vector.z + matrix.r2c3*vector.w,
		matrix.r3c0*vector.x + matrix.r3c1*vector.y + matrix.r3c2*vector.z + matrix.r3c3*vector.w
	);
	//TODO: Do we need to do this for a Vec4?
	// result = (1.0f / w) * result;
	
	return  result;
}

#define Mat4Transform(matrix, transformation) (matrix) = Mat4Multiply((transformation), (matrix))

// +--------------------------------------------------------------+
// |                      Operator Overloads                      |
// +--------------------------------------------------------------+
mat4 operator * (const mat4& left, const mat4& right) { return Mat4Multiply(left, right); }

inline bool operator == (mat4 left, mat4 right)
{
	return (
		left.r0c0 == right.r0c0 && left.r0c1 == right.r0c1 && left.r0c2 == right.r0c2 && left.r0c3 == right.r0c3 &&
		left.r1c0 == right.r1c0 && left.r1c1 == right.r1c1 && left.r1c2 == right.r1c2 && left.r1c3 == right.r1c3 &&
		left.r2c0 == right.r2c0 && left.r2c1 == right.r2c1 && left.r2c2 == right.r2c2 && left.r2c3 == right.r2c3 &&
		left.r3c0 == right.r3c0 && left.r3c1 == right.r3c1 && left.r3c2 == right.r3c2 && left.r3c3 == right.r3c3
	);
}
inline bool operator != (mat4 left, mat4 right)
{
	return (
		left.r0c0 != right.r0c0 || left.r0c1 != right.r0c1 || left.r0c2 != right.r0c2 || left.r0c3 != right.r0c3 ||
		left.r1c0 != right.r1c0 || left.r1c1 != right.r1c1 || left.r1c2 != right.r1c2 || left.r1c3 != right.r1c3 ||
		left.r2c0 != right.r2c0 || left.r2c1 != right.r2c1 || left.r2c2 != right.r2c2 || left.r2c3 != right.r2c3 ||
		left.r3c0 != right.r3c0 || left.r3c1 != right.r3c1 || left.r3c2 != right.r3c2 || left.r3c3 != right.r3c3
	);
}

// +--------------------------------------------------------------+
// |            Specialized Matrix Creation Functions             |
// +--------------------------------------------------------------+
inline mat4 Mat4Translate(const v3& translation)
{
	return NewMat4( 
		1.0f, 0.0f, 0.0f, translation.x,
		0.0f, 1.0f, 0.0f, translation.y,
		0.0f, 0.0f, 1.0f, translation.z,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}
inline mat4 Mat4Translate(const v2& translation)
{
	return NewMat4( 
		1.0f, 0.0f, 0.0f, translation.x,
		0.0f, 1.0f, 0.0f, translation.y,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}
inline mat4 Mat4Translate3(r32 x, r32 y, r32 z)
{
	return NewMat4( 
		1.0f, 0.0f, 0.0f, x,
		0.0f, 1.0f, 0.0f, y,
		0.0f, 0.0f, 1.0f, z,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}
inline mat4 Mat4Translate2(r32 x, r32 y)
{
	return NewMat4( 
		1.0f, 0.0f, 0.0f, x,
		0.0f, 1.0f, 0.0f, y,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

inline mat4 Mat4Scale(const v3& scale)
{
	return NewMat4(
		scale.x,  0.0f,     0.0f,     0.0f,
		0.0f,     scale.y,  0.0f,     0.0f,
		0.0f,     0.0f,     scale.z,  0.0f,
		0.0f,     0.0f,     0.0f,     1.0f
	);
}
inline mat4 Mat4Scale3(r32 x, r32 y, r32 z)
{
	return NewMat4(
		x,    0.0f, 0.0f, 0.0f,
		0.0f, y,    0.0f, 0.0f,
		0.0f, 0.0f, z,    0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}
inline mat4 Mat4Scale2(r32 x, r32 y)
{
	return NewMat4(
		x,    0.0f, 0.0f, 0.0f,
		0.0f, y,    0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

inline mat4 Mat4RotateX(r32 angle)
{
	r32 s = sinf(angle);
	r32 t = 1.0f - cosf(angle);
	
	return NewMat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1-t,  -s,    0.0f,
		0.0f, s,    1-t,  0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

inline mat4 Mat4RotateY(r32 angle)
{
	r32 s = sinf(angle);
	r32 t = 1.0f - cosf(angle);
	
	return NewMat4(
		1-t,  0,    s,    0.0f,
		0,    1,    0,    0.0f,
		-s,   0,    1-t,  0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

inline mat4 Mat4RotateZ(r32 angle)
{
	r32 s = sinf(angle);
	r32 t = 1.0f - cosf(angle);
	
	return NewMat4(
		1-t,  -s,   0,    0.0f,
		s,    1-t,  0,    0.0f,
		0,    0,    1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

inline mat4 Mat4Rotate(const v3& axis, r32 angle)
{
	r32 c = cosf(angle);
	r32 s = sinf(angle);
	r32 t = 1.0f - c;
	
	v3 norm = Vec3Normalize(axis);
	r32 x = norm.x;
	r32 y = norm.y;
	r32 z = norm.z;
	
	return NewMat4(
		1+t*(x*x-1), -z*s+t*x*y,  y*s+t*x*z,   0.0f,
		z*s+t*x*y,   1+t*(y*y-1), -x*s+t*y*z,  0.0f,
		-y*s+t*x*z,  x*s+t*y*z,   1+t*(z*z-1), 0.0f,
		0.0f,        0.0f,        0.0f,        1.0f
	);
}

inline mat4 Mat4LookAt(const v3& position, const v3& lookAt, const v3& upVector)
{
	v3 lookNorm = Vec3Normalize(Vec3Sub(lookAt, position));
	v3 rightVector = Vec3Normalize(Vec3Cross(lookNorm, upVector));
	v3 upNorm = Vec3Normalize(Vec3Cross(rightVector, lookNorm));
	r32 rightDot = -Vec3Dot(rightVector, position);
	r32 upDot = -Vec3Dot(upNorm, position);
	r32 lookDot = -Vec3Dot(lookNorm, position);
	
	return NewMat4(
		rightVector.x, rightVector.y, rightVector.z, rightDot,
		upNorm.x     , upNorm.y     , upNorm.z     , upDot   ,
		lookNorm.x   , lookNorm.y   , lookNorm.z   , lookDot ,
		0.0f         , 0.0f         , 0.0f         , 1.0f
	);
}

inline mat4 Mat4PerspectiveFOV(r32 fovy, r32 aspectRatio, r32 zNear, r32 zFar)
{
	//NOTE: this is left-handed I guess?
	
	Assert(zFar > zNear);
	Assert(aspectRatio > 0);
	
	r32 uh = 1.0f / tanf(fovy / 2.0f);
	r32 uw = uh / aspectRatio;
	r32 depth = zFar - zNear;
	r32 z1 = zFar / depth;
	r32 z2 = (-zFar * zNear) / depth;
	
	return NewMat4(
		 uw , 0.0f, 0.0f, 0.0f,
		0.0f,  uh , 0.0f, 0.0f,
		0.0f, 0.0f,  z1 ,  z2 ,
		0.0f, 0.0f, 1.0f, 0.0f
	);
}

inline mat4 Mat4Ortho(r32 left, r32 right, r32 top, r32 bottom, r32 zNear, r32 zFar)
{
	return NewMat4(
		2.0f / (right-left)       , 0.0f                      , 0.0f                      , 0.0f,
		0.0f                      , 2.0f / (top-bottom)       , 0.0f                      , 0.0f,
		0.0f                      , 0.0f                      , -2.0f / (zFar-zNear)      , 0.0f,
		-(right+left)/(right-left), -(top+bottom)/(top-bottom), -(zFar+zNear)/(zFar-zNear), 1.0f
	);
}

//NOTE: Quaternion should be pre-normalized
inline mat4 Mat4Quaternion(quat q)
{
	return NewMat4(
		1 - 2*q.y*q.y - 2*q.z*q.z,   2*q.x*q.y - 2*q.z*q.w,     2*q.x*q.z + 2*q.y*q.w,   0,
		  2*q.x*q.y + 2*q.z*q.w,   1 - 2*q.x*q.x - 2*q.z*q.z,   2*q.y*q.z - 2*q.x*q.w,   0,
		  2*q.x*q.z - 2*q.y*q.w,     2*q.y*q.z + 2*q.x*q.w,   1 - 2*q.x*q.x - 2*q.y*q.y, 0,
		            0,                         0,                         0,             1
	);
}

inline quat QuatLocalRot(quat q, v3 axis, r32 angle)
{
	quat deltaQuat = NewQuat(axis, angle);
	return QuatMult(q, deltaQuat);
}

inline quat QuatGlobalRot(quat q, v3 axis, r32 angle)
{
	v3 localAxis = Mat4MultiplyRightVec3(axis, Mat4Quaternion(q));
	quat deltaQuat = NewQuat(localAxis, angle);
	return QuatMult(q, deltaQuat);
}

#endif //  _MY_MATRICES_H
