/*
File:   my_linkedList.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This is an implementation of a linked list which uses a LinkHeader_t that must
	** be put at the top of the structure in order to work

This file is #included by default from mylib.h
*/

#ifndef _LINKED_LIST
#define _LINKED_LIST

//NOTE: In order for the linked list to work you must include 2 void*
//		at the top of the structure in the same pattern as LinkHeader_t
//		for the linked list to use. This way the application can use the
//		pointers along with this header library

struct LinkedList_t
{
	i32 numItems;
	
	void* firstItem;
	void* lastItem;
};

struct LinkHeader_t
{
	void* lastItem;
	void* nextItem;
};

void CreateLinkedList(LinkedList_t* list)
{
	Assert(list != nullptr);
	
	ClearPointer(list);
}

void DeleteLinkedList(LinkedList_t* list, MemoryArena_t* memoryArena)
{
	LinkHeader_t* currentItem = (LinkHeader_t*)list->firstItem;
	
	while (currentItem != nullptr)
	{
		LinkHeader_t* nextItem = (LinkHeader_t*)currentItem->nextItem;
		ArenaPop(memoryArena, currentItem);
		currentItem = nextItem;
	}
	
	list->firstItem = nullptr;
	list->lastItem = nullptr;
	list->numItems = 0;
}

#define LinkedPushStruct(listPntr, arenaPntr, type) (type*)LinkedListPush((listPntr), (arenaPntr), sizeof(type))

void* LinkedListPush(LinkedList_t* list, MemoryArena_t* memoryArena, uint32 itemSize)
{
	Assert(memoryArena != nullptr);
	Assert(list != nullptr);
	Assert(itemSize > sizeof(LinkHeader_t));
	
	u8* newSpace = (u8*)ArenaPush(memoryArena, itemSize);
	MyMemSet(newSpace, 0x00, itemSize);
	LinkHeader_t* newHeader = (LinkHeader_t*)newSpace;
	
	if (list->lastItem == nullptr || list->firstItem == nullptr || list->numItems == 0)
	{
		//If one of the above conditions is true then they should all be true
		//otherwise we have some sort of error
		Assert(list->firstItem == nullptr);
		Assert(list->lastItem == nullptr);
		Assert(list->numItems == 0);
		
		list->firstItem = newSpace;
		list->lastItem = newSpace;
	}
	else
	{
		Assert(list->firstItem != nullptr);
		Assert(list->lastItem != nullptr);
		Assert(list->numItems > 0);
		
		LinkHeader_t* lastHeader = (LinkHeader_t*)list->lastItem;
		Assert(lastHeader->nextItem == nullptr);
		lastHeader->nextItem = newSpace;
		newHeader->lastItem = lastHeader;
		list->lastItem = newSpace;
	}
	
	list->numItems++;
	
	return (void*)newSpace;
}

void LinkedListPop(LinkedList_t* list, MemoryArena_t* memoryArena, int32 index)
{
	Assert(memoryArena != nullptr);
	Assert(list != nullptr);
	Assert(list->firstItem != nullptr);
	Assert(list->lastItem != nullptr);
	Assert(index >= 0);
	Assert(index < list->numItems);
	
	LinkHeader_t* currentItem = (LinkHeader_t*)list->firstItem;
	
	for (int32 currentIndex = 0; currentIndex < index; currentIndex++)
	{
		Assert(currentItem->nextItem != nullptr);
		currentItem = (LinkHeader_t*)currentItem->nextItem;
	}
	
	if (list->numItems == 1)
	{
		Assert(currentItem->nextItem == nullptr);
		Assert(list->firstItem == list->lastItem);
		
		list->firstItem = nullptr;
		list->lastItem = nullptr;
	}
	else if (index == 0)
	{
		Assert(currentItem->lastItem == nullptr);
		Assert(currentItem->nextItem != nullptr);
			
		list->firstItem = currentItem->nextItem;
		
		LinkHeader_t* nextItem = (LinkHeader_t*)currentItem->nextItem;
		Assert(nextItem->lastItem == currentItem);
		nextItem->lastItem = nullptr;
	}
	else if (index == list->numItems - 1)
	{
		Assert(currentItem->nextItem == nullptr);
		Assert(currentItem->lastItem != nullptr);
			
		list->lastItem = currentItem->lastItem;
		
		LinkHeader_t* lastItem = (LinkHeader_t*)currentItem->lastItem;
		Assert(lastItem->nextItem == currentItem);
		lastItem->nextItem = nullptr;
	}
	else
	{
		Assert(currentItem->nextItem != nullptr);
		Assert(currentItem->lastItem != nullptr);
		
		LinkHeader_t* nextItem = (LinkHeader_t*)currentItem->nextItem;
		LinkHeader_t* lastItem = (LinkHeader_t*)currentItem->lastItem;
		
		lastItem->nextItem = nextItem;
		nextItem->lastItem = lastItem;
	}
		
	ArenaPop(memoryArena, currentItem);
	list->numItems--;
}

void* LinkedListInsert(LinkedList_t* list, MemoryArena_t* memoryArena, int32 index, uint32 itemSize)
{
	Assert(memoryArena != nullptr);
	Assert(list != nullptr);
	Assert(itemSize > sizeof(LinkHeader_t));
	Assert(index >= 0);
	Assert(index <= list->numItems);
	
	void* newSpace = ArenaPush(memoryArena, itemSize);
	LinkHeader_t* newItem = (LinkHeader_t*)newSpace;
	ClearPointer(newItem);
	
	if (list->numItems == 0)
	{
		Assert(list->firstItem == nullptr);
		Assert(list->lastItem == nullptr);
		
		list->firstItem = newItem;
		list->lastItem = newItem;
	}
	else if (index == 0)
	{
		newItem->nextItem = list->firstItem;
		list->firstItem = newItem;
		
		LinkHeader_t* nextItem = (LinkHeader_t*)newItem->nextItem;
		Assert(nextItem->lastItem == nullptr);
		nextItem->lastItem = newItem;
	}
	else if (index == list->numItems-1)
	{
		newItem->lastItem = list->lastItem;
		
		LinkHeader_t* lastItem = (LinkHeader_t*)list->lastItem;
		Assert(lastItem->nextItem == nullptr);
		lastItem->nextItem = newItem;
		
		list->lastItem = newItem;
	}
	else
	{
		LinkHeader_t* lastItem = (LinkHeader_t*)list->firstItem;
		for (int32 itemIndex = 0; itemIndex < index; itemIndex++)
		{
			lastItem = (LinkHeader_t*)lastItem->nextItem;
			Assert(lastItem != nullptr);
		}
		
		LinkHeader_t* nextItem = (LinkHeader_t*)lastItem->nextItem;
		
		lastItem->nextItem = newItem;
		nextItem->lastItem = newItem;
	}
	
	list->numItems++;
	return newSpace;
}

void* LinkedListGetItem(LinkedList_t* list, int32 index)
{
	Assert(index >= 0);
	Assert(list != nullptr);
	Assert(index < list->numItems);
	
	LinkHeader_t* currentItem = (LinkHeader_t*)list->firstItem;
	for (int32 itemIndex = 0; itemIndex < index; itemIndex++)
	{
		currentItem = (LinkHeader_t*)currentItem->nextItem;
		Assert(currentItem != nullptr);
	}
	
	return currentItem;
}

void* LinkedListGetLastItem(LinkedList_t* list)
{
	Assert(list != nullptr);
	
	return list->lastItem;
}

void* FindInLinkedList(LinkedList_t* list, void* compareItem, i32 (*CompareFunctionPntr)(void* left, void* right))
{
	if (list->numItems == 0)
	{
		return nullptr;
	}
	
	LinkHeader_t* currentItem = (LinkHeader_t*)list->firstItem;
	
	while (currentItem != nullptr)
	{
		if (CompareFunctionPntr(currentItem, compareItem) == 0)
		{
			return currentItem;
		}
		
		currentItem = (LinkHeader_t*)currentItem->nextItem;
	}
	
	return nullptr;
}

#endif // _LINKED_LIST
