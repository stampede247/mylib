/*
File:   memoryArena.h
Author: Taylor Robbins
Date:   08\30\2017
Description:
	** This file defines the base functionality and basic implementations of MemoryArena_t
	** MemoryArena_t structures can be used for many things but their main purpose is to
	** provide a self implemented method of allocating (and sometimes deallocating)
	** memory in specified memory spaces.

This file is #included by default from mylib.h
*/

#ifndef _MEMORY_ARENA_H
#define _MEMORY_ARENA_H

#ifndef MEMORY_ARENA_META_INFO_ENABLED
	#ifdef DEBUG
	#define MEMORY_ARENA_META_INFO_ENABLED DEBUG
	#else
	#define MEMORY_ARENA_META_INFO_ENABLED false
	#endif
#endif

struct ArenaAllocMetaInfo_t
{
	ArenaAllocMetaInfo_t* next;
	
	u32 allocSize;
	void* allocPntr;
	
	u32 lineNumber;
	u32 fileNameLength;
	u32 funcNameLength;
};

struct ArenaWalkInfo_t
{
	u32 allocIndex;
	u32 headerSize;
	u32 size;
	u64 address;
	u32 pageOffset;
	u32 endPageOffset;
	u32 pageIndex;
	u32 pageAllocIndex;
	u32 pageSize;
	u32 spaceBefore;
	u32 numSpacesBefore;
	void* dataPntr;
	void* headerPntr;
	void* lastHeader;
	void* pageHeader;
};

typedef enum
{
	MemoryArenaType_Linear = 1,
	MemoryArenaType_Heap,
	MemoryArenaType_Temp,
	MemoryArenaType_StdHeap, //Essentially a redirect for malloc and free
	MemoryArenaType_External, //Essentially a redirect for two external functions
	MemoryArenaType_GrowingHeap,
	MemoryArenaType_Buffer,
	MemoryArenaType_BufferSingleAlloc,
	MemoryArenaType_GrowingHeapOutOfArena,
} MemoryArenaType_t;

struct MemoryArena_t
{
	MemoryArenaType_t type;
	
	void* base;
	void* otherPntr;
	
	u32 size;
	u32 used;
	
	#if MEMORY_ARENA_META_INFO_ENABLED
	MemoryArena_t* metaArena;
	ArenaAllocMetaInfo_t* firstMetaInfo;
	#endif
};

struct HeapAllocationHeader_t
{
	bool isUsed;
	u32 size;
};

struct TempArenaHeader_t
{
	u32 maxNumMarks;
	u32 numMarks;
	u32 highWaterMark;
};

typedef void* AllocationFunction_f(u32 numBytes);
typedef void FreeFunction_f(void* memPntr);
struct GrowingHeapPageHeader_t
{
	GrowingHeapPageHeader_t* next;
	u32 size;
	u32 used;
};
struct GrowingHeapHeader_t
{
	AllocationFunction_f* allocFunction;
	u32 pageSize;
	u32 maxNumPages;
	GrowingHeapPageHeader_t* firstPage;
};
struct GrowingHeapOutOfArenaHeader_t
{
	MemoryArena_t* allocArena;
	u32 pageSize;
	u32 maxNumPages;
	GrowingHeapPageHeader_t* firstPage;
};

void* ArenaPush_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arena, u32 allocSize);
void* ArenaRealloc_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arena, void* oldPntr, u32 newSize, u32 oldSize = 0, bool noMovement = false);
void ArenaPop(MemoryArena_t* arena, void* pointer);

#if MEMORY_ARENA_META_INFO_ENABLED
#define ArenaPush(arena, allocSize) ArenaPush_(__FILE__, __LINE__, __func__, (arena), (allocSize))
#define ArenaRealloc(arena, oldPntr, newSize, oldSize) ArenaRealloc_(__FILE__, __LINE__, __func__, (arena), (oldPntr), (newSize), (oldSize))
#else
#define ArenaPush(arena, allocSize) ArenaPush_(nullptr, 0, nullptr, (arena), (allocSize))
#define ArenaRealloc(arena, oldPntr, newSize, oldSize) ArenaRealloc_(nullptr, 0, nullptr, (arena), (oldPntr), (newSize), (oldSize))
#endif

#define PushStruct(arena, type) (type*)ArenaPush((arena), sizeof(type))
#define PushStruct_(filePath, lineNumber, funcName, arena, type) (type*)ArenaPush_(filePath, lineNumber, funcName, arena, sizeof(type))
#define PushArray(arena, type, number) (type*)ArenaPush((arena), sizeof(type)*(number))
#define PushArray_(filePath, lineNumber, funcName, arena, type, number) (type*)ArenaPush_(filePath, lineNumber, funcName, arena, sizeof(type) * (number))
#define GrowArray(arena, type, oldPntr, newNumber, oldNumber) (type*)ArenaRealloc((arena), (oldPntr), sizeof(type)*(newNumber), sizeof(type)*(oldNumber))

void InitializeMemoryArenaLinear(MemoryArena_t* arena, void* base , u32 size)
{
	Assert(arena != nullptr);
	Assert(base != nullptr);
	
	ClearPointer(arena);
	
	arena->type = MemoryArenaType_Linear;
	arena->base = base;
	arena->size = size;
}

void InitializeMemoryArenaHeap(MemoryArena_t* arena, void* base, u32 size)
{
	Assert(arena != nullptr);
	Assert(base != nullptr);
	
	ClearPointer(arena);
	
	arena->type = MemoryArenaType_Heap;
	arena->base = base;
	arena->size = size;
	
	HeapAllocationHeader_t* initHeader = (HeapAllocationHeader_t*)base;
	initHeader->isUsed = false;
	initHeader->size = size;
	
	arena->used = sizeof(HeapAllocationHeader_t);
}

void InitializeMemoryArenaTemp(MemoryArena_t* arena, void* base , u32 size, u32 maxNumMarks)
{
	Assert(arena != nullptr);
	Assert(base != nullptr);
	
	ClearPointer(arena);
	
	arena->type = MemoryArenaType_Temp;
	arena->base = base;
	arena->size = size;
	
	u32 headerSize = sizeof(TempArenaHeader_t) + sizeof(u32)*maxNumMarks;
	Assert(headerSize < size);
	arena->used = headerSize;
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	arenaHeader->maxNumMarks = maxNumMarks;
	arenaHeader->numMarks = 0;
	arenaHeader->highWaterMark = 0;
	
	u32* marksArray = (u32*)(arenaHeader + 1);
	MyMemSet(marksArray, 0x00, sizeof(u32)*maxNumMarks);
}

void InitializeMemoryArenaStdHeap(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	
	ClearPointer(arena);
	
	arena->type = MemoryArenaType_StdHeap;
	arena->base = nullptr;
	arena->size = 0;
	arena->used = 0;
}

void InitializeMemoryArenaExternal(MemoryArena_t* arena, AllocationFunction_f* allocFunction, FreeFunction_f* freeFunction)
{
	Assert(arena != nullptr);
	
	ClearPointer(arena);
	
	arena->type = MemoryArenaType_External;
	arena->base = (void*)(allocFunction);
	arena->otherPntr = (void*)(freeFunction);
	arena->size = 0;
	arena->used = 0;
}

void InitializeMemoryArenaGrowingHeap(MemoryArena_t* arena, AllocationFunction_f* allocFunction, u32 pageSize, u32 initialNumPages, u32 maxNumPages = 0)
{
	Assert(arena != nullptr);
	Assert(pageSize > 0);
	Assert(pageSize > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
	Assert(initialNumPages > 0);
	Assert(maxNumPages == 0 || initialNumPages <= maxNumPages);
	Assert(allocFunction != nullptr);
	
	ClearPointer(arena);
	arena->type = MemoryArenaType_GrowingHeap;
	arena->size = pageSize * initialNumPages;
	
	void* firstPage = allocFunction(pageSize);
	Assert(firstPage != nullptr);
	arena->base = firstPage;
	
	GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)firstPage;
	ClearPointer(mainHeader);
	GrowingHeapPageHeader_t* firstPageHeader = (GrowingHeapPageHeader_t*)(mainHeader + 1);
	ClearPointer(firstPageHeader);
	
	mainHeader->allocFunction = allocFunction;
	mainHeader->pageSize = pageSize;
	mainHeader->maxNumPages = maxNumPages;
	mainHeader->firstPage = firstPageHeader;
	
	firstPageHeader->next = nullptr;
	firstPageHeader->size = pageSize - sizeof(GrowingHeapHeader_t) - sizeof(GrowingHeapPageHeader_t);
	firstPageHeader->used = sizeof(HeapAllocationHeader_t);
	
	HeapAllocationHeader_t* firstAllocation = (HeapAllocationHeader_t*)(firstPageHeader+1);
	ClearPointer(firstAllocation);
	firstAllocation->isUsed = false;
	firstAllocation->size = firstPageHeader->size;
	
	GrowingHeapPageHeader_t* previousPage = firstPageHeader;
	for (u32 pIndex = 1; pIndex < initialNumPages; pIndex++)
	{
		void* newPageMem = allocFunction(pageSize);
		Assert(newPageMem != nullptr);
		
		GrowingHeapPageHeader_t* newPageHeader = (GrowingHeapPageHeader_t*)newPageMem;
		ClearPointer(newPageHeader);
		
		newPageHeader->next = nullptr;
		newPageHeader->size = pageSize - sizeof(GrowingHeapPageHeader_t);
		newPageHeader->used = sizeof(HeapAllocationHeader_t);
		
		HeapAllocationHeader_t* newAllocation = (HeapAllocationHeader_t*)(newPageHeader+1);
		ClearPointer(newAllocation);
		newAllocation->isUsed = false;
		newAllocation->size = newPageHeader->size;
		
		previousPage->next = newPageHeader;
		previousPage = newPageHeader;
	}
	
	arena->used = sizeof(GrowingHeapHeader_t) + (sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t)) * initialNumPages;
}
void InitializeMemoryArenaGrowingHeapOutOfArena(MemoryArena_t* arena, MemoryArena_t* allocArena, u32 pageSize, u32 initialNumPages, u32 maxNumPages = 0)
{
	Assert(arena != nullptr);
	Assert(pageSize > 0);
	Assert(pageSize > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
	Assert(initialNumPages > 0);
	Assert(maxNumPages == 0 || initialNumPages <= maxNumPages);
	Assert(allocArena != nullptr);
	
	ClearPointer(arena);
	arena->type = MemoryArenaType_GrowingHeapOutOfArena;
	arena->size = pageSize * initialNumPages;
	
	void* firstPage = ArenaPush(allocArena, pageSize);
	Assert(firstPage != nullptr);
	arena->base = firstPage;
	
	GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)firstPage;
	ClearPointer(mainHeader);
	GrowingHeapPageHeader_t* firstPageHeader = (GrowingHeapPageHeader_t*)(mainHeader + 1);
	ClearPointer(firstPageHeader);
	
	mainHeader->allocArena = allocArena;
	mainHeader->pageSize = pageSize;
	mainHeader->maxNumPages = maxNumPages;
	mainHeader->firstPage = firstPageHeader;
	
	firstPageHeader->next = nullptr;
	firstPageHeader->size = pageSize - sizeof(GrowingHeapOutOfArenaHeader_t) - sizeof(GrowingHeapPageHeader_t);
	firstPageHeader->used = sizeof(HeapAllocationHeader_t);
	
	HeapAllocationHeader_t* firstAllocation = (HeapAllocationHeader_t*)(firstPageHeader+1);
	ClearPointer(firstAllocation);
	firstAllocation->isUsed = false;
	firstAllocation->size = firstPageHeader->size;
	
	GrowingHeapPageHeader_t* previousPage = firstPageHeader;
	for (u32 pIndex = 1; pIndex < initialNumPages; pIndex++)
	{
		void* newPageMem = ArenaPush(allocArena, pageSize);
		Assert(newPageMem != nullptr);
		
		GrowingHeapPageHeader_t* newPageHeader = (GrowingHeapPageHeader_t*)newPageMem;
		ClearPointer(newPageHeader);
		
		newPageHeader->next = nullptr;
		newPageHeader->size = pageSize - sizeof(GrowingHeapPageHeader_t);
		newPageHeader->used = sizeof(HeapAllocationHeader_t);
		
		HeapAllocationHeader_t* newAllocation = (HeapAllocationHeader_t*)(newPageHeader+1);
		ClearPointer(newAllocation);
		newAllocation->isUsed = false;
		newAllocation->size = newPageHeader->size;
		
		previousPage->next = newPageHeader;
		previousPage = newPageHeader;
	}
	
	arena->used = sizeof(GrowingHeapHeader_t) + (sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t)) * initialNumPages;
}

void InitBufferArena(MemoryArena_t* arena, void* bufferPntr, u32 bufferSize, bool forceSingleAlloc = false)
{
	Assert(arena != nullptr);
	Assert(bufferPntr != nullptr);
	Assert(bufferSize > 0);
	ClearPointer(arena);
	arena->type = forceSingleAlloc ? MemoryArenaType_BufferSingleAlloc : MemoryArenaType_Buffer;
	arena->base = bufferPntr;
	arena->size = bufferSize;
}

void MemoryArenaAddMetaArena(MemoryArena_t* arena, MemoryArena_t* metaArena)
{
	Assert(arena != nullptr);
	Assert(arena->type != MemoryArenaType_Linear);
	Assert(arena->type != MemoryArenaType_Temp);
	#if MEMORY_ARENA_META_INFO_ENABLED
	arena->metaArena = metaArena;
	#endif
}

void* ArenaPush_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arena, u32 allocSize) //pre-declared above
{
	Assert(arena != nullptr);
	Assert(allocSize != 0);
	
	void* result = nullptr;
	switch (arena->type)
	{
		case MemoryArenaType_Linear:
		{
			if (arena->used + allocSize > arena->size)
			{
				Assert(false);
				result = nullptr;
				break;
			}
			
			result = ((u8*)arena->base) + arena->used;
			arena->used += allocSize;
		} break;
		
		case MemoryArenaType_Temp:
		{
			if (arena->used + allocSize > arena->size)
			{
				Assert(false);
				result = nullptr;
				break;
			}
			
			result = ((u8*)arena->base) + arena->used;
			arena->used += allocSize;
			
			TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
			if (arena->used > arenaHeader->highWaterMark)
			{
				arenaHeader->highWaterMark = arena->used;
			}
		} break;
		
		case MemoryArenaType_Heap:
		{
			//TODO: Look for best fit location rather than just choosing
			//		the first location that is large enough.
			
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			const u32 totalSize = headerSize + allocSize;
			
			u8* currentPntr = (u8*)arena->base;
			bool foundSpace = false;
			void* allocLocation = nullptr;
			
			while (!foundSpace)
			{
				if ((u32)(currentPntr - (u8*)arena->base) >= arena->size)
				{
					//Reached the end of the arena without finding any places to
					//store the item
					size_t offset = (size_t)(currentPntr - (u8*)arena->base);
					Assert(offset == arena->size);
					
					break;
				}
				
				HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
				Assert(header->size > 0);
				
				if (header->isUsed)
				{
					currentPntr += header->size;
				}
				else
				{
					if (header->size >= totalSize)
					{
						u32 originalSize = header->size;
						
						header->isUsed = true;
						header->size = totalSize;
						allocLocation = currentPntr + headerSize;
						arena->used += allocSize;
						
						if (originalSize - headerSize > allocSize)
						{
							u32 leftOverSize = originalSize - totalSize;
							if (leftOverSize > headerSize)
							{
								HeapAllocationHeader_t* leftOverHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
								leftOverHeader->isUsed = false;
								leftOverHeader->size = leftOverSize;
								
								arena->used += headerSize;
							}
							else
							{
								//Tack on a few extra bytes since we can't fit a new
								//header in the empty space
								header->size += leftOverSize;
								arena->used += leftOverSize;
							}
						}
						
						foundSpace = true;
					}
					else
					{
						currentPntr += header->size;
					}
				}
			}
			
			#if DEBUG
			if (allocSize != 0 && allocLocation == nullptr)
			{
				Assert(false && "Memory allocation failed!");
			}
			if (allocLocation != nullptr && allocSize > 0)
			{
				MyMemSet(allocLocation, 0xAA, allocSize);
			}
			#endif
			
			result = allocLocation;
		} break;
		
		case MemoryArenaType_StdHeap:
		{
			result = MyMalloc(allocSize);
		} break;
		
		case MemoryArenaType_External:
		{
			Assert(arena->base != nullptr);
			AllocationFunction_f* allocate = (AllocationFunction_f*)arena->base;
			result = allocate(allocSize);
		} break;
		
		case MemoryArenaType_GrowingHeap:
		case MemoryArenaType_GrowingHeapOutOfArena:
		{
			//TODO: Look for best fit location rather than just choosing
			//		the first location that is large enough.
			
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			const u32 totalSize = headerSize + allocSize;
			
			// if (allocSize == 38600)
			// {
			// 	MyDebugBreak();
			// }
			
			Assert(arena->base != nullptr);
			AllocationFunction_f* allocFunction = nullptr;
			MemoryArena_t* allocArena = nullptr;
			u32 pageSize = 0;
			u32 maxNumPages = 0;
			GrowingHeapPageHeader_t* firstPage = 0;
			if (arena->type == MemoryArenaType_GrowingHeap)
			{
				Assert(arena->size > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				Assert(mainHeader->firstPage != nullptr);
				Assert(totalSize <= mainHeader->pageSize - sizeof(GrowingHeapPageHeader_t));
				pageSize = mainHeader->pageSize;
				maxNumPages = mainHeader->maxNumPages;
				firstPage = mainHeader->firstPage;
				allocFunction = mainHeader->allocFunction;
			}
			else if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
			{
				Assert(arena->size > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				Assert(mainHeader->firstPage != nullptr);
				Assert(totalSize <= mainHeader->pageSize - sizeof(GrowingHeapPageHeader_t));
				pageSize = mainHeader->pageSize;
				maxNumPages = mainHeader->maxNumPages;
				firstPage = mainHeader->firstPage;
				allocArena = mainHeader->allocArena;
			}
			else { Assert(false); }
			
			bool foundSpace = false;
			void* allocLocation = nullptr;
			
			GrowingHeapPageHeader_t* previousPage = nullptr;
			GrowingHeapPageHeader_t* pageHeader = firstPage;
			u32 pIndex = 0;
			while (pageHeader != nullptr)
			{
				u8* pageBase = (u8*)(pageHeader+1);
				u8* currentPntr = pageBase;
				while (!foundSpace)
				{
					if (currentPntr >= pageBase + pageHeader->size)
					{
						//Reached the end of the arena without finding any places to store the item
						size_t offset = (size_t)(currentPntr - pageBase);
						Assert(offset == pageHeader->size);
						break;
					}
					
					HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
					Assert(header->size > 0);
					
					if (header->isUsed)
					{
						currentPntr += header->size;
					}
					else
					{
						if (header->size >= totalSize)
						{
							u32 originalSize = header->size;
							
							header->isUsed = true;
							header->size = totalSize;
							allocLocation = currentPntr + headerSize;
							arena->used += allocSize;
							pageHeader->used += allocSize;
							
							if (originalSize - headerSize > allocSize)
							{
								u32 leftOverSize = originalSize - totalSize;
								if (leftOverSize > headerSize)
								{
									HeapAllocationHeader_t* leftOverHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
									leftOverHeader->isUsed = false;
									leftOverHeader->size = leftOverSize;
									
									arena->used += headerSize;
									pageHeader->used += headerSize;
								}
								else
								{
									//Tack on a few extra bytes since we can't fit a new
									//header in the empty space
									header->size += leftOverSize;
									arena->used += leftOverSize;
									pageHeader->used += leftOverSize;
								}
							}
							
							foundSpace = true;
						}
						else
						{
							currentPntr += header->size;
						}
					}
				}
				
				previousPage = pageHeader;
				pageHeader = pageHeader->next;
				pIndex++;
			}
			
			if (!foundSpace && (maxNumPages == 0 || pIndex < maxNumPages))
			{
				Assert(previousPage != nullptr);
				
				void* newPageSpace = nullptr;
				if (allocFunction != nullptr) { newPageSpace = allocFunction(pageSize); }
				else if (allocArena != nullptr) { newPageSpace = ArenaPush_(filePath, lineNumber, funcName, allocArena, pageSize); }
				else { Assert(false); }
				Assert(newPageSpace != nullptr);
				
				GrowingHeapPageHeader_t* newPageHeader = (GrowingHeapPageHeader_t*)newPageSpace;
				ClearPointer(newPageHeader);
				newPageHeader->size = pageSize - sizeof(GrowingHeapPageHeader_t);
				newPageHeader->used = sizeof(HeapAllocationHeader_t);
				
				Assert(previousPage->next == nullptr);
				previousPage->next = newPageHeader;
				arena->size += sizeof(GrowingHeapPageHeader_t) + newPageHeader->size;
				arena->used += sizeof(GrowingHeapPageHeader_t) + newPageHeader->used;
				
				HeapAllocationHeader_t* firstAllocation = (HeapAllocationHeader_t*)(newPageHeader+1);
				ClearPointer(firstAllocation);
				firstAllocation->isUsed = false;
				firstAllocation->size = newPageHeader->size;
				
				//Now do the same process we did above on the new page
				u8* pageBase = (u8*)(newPageHeader+1);
				u8* currentPntr = pageBase;
				while (!foundSpace)
				{
					if (currentPntr >= pageBase + newPageHeader->size)
					{
						//Reached the end of the arena without finding any places to store the item
						size_t offset = (size_t)(currentPntr - pageBase);
						Assert(offset == newPageHeader->size);
						break;
					}
					
					HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
					Assert(header->size > 0);
					
					if (header->isUsed)
					{
						currentPntr += header->size;
					}
					else
					{
						if (header->size >= totalSize)
						{
							u32 originalSize = header->size;
							
							header->isUsed = true;
							header->size = totalSize;
							allocLocation = currentPntr + headerSize;
							arena->used += allocSize;
							newPageHeader->used += allocSize;
							
							if (originalSize - headerSize > allocSize)
							{
								u32 leftOverSize = originalSize - totalSize;
								if (leftOverSize > headerSize)
								{
									HeapAllocationHeader_t* leftOverHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
									leftOverHeader->isUsed = false;
									leftOverHeader->size = leftOverSize;
									
									arena->used += headerSize;
									newPageHeader->used += headerSize;
								}
								else
								{
									//Tack on a few extra bytes since we can't fit a new
									//header in the empty space
									header->size += leftOverSize;
									arena->used += leftOverSize;
									newPageHeader->used += leftOverSize;
								}
							}
							
							foundSpace = true;
						}
						else
						{
							currentPntr += header->size;
						}
					}
				}
			}
			
			#if DEBUG
			if (allocSize != 0 && allocLocation == nullptr)
			{
				Assert(false && "Memory allocation failed!");
			}
			if (allocLocation != nullptr && allocSize > 0)
			{
				MyMemSet(allocLocation, 0xAA, allocSize);
			}
			#endif
			
			result = allocLocation;
		} break;
		
		case MemoryArenaType_Buffer:
		case MemoryArenaType_BufferSingleAlloc:
		{
			if (arena->used + allocSize > arena->size)
			{
				Assert(false);
				result = nullptr;
				break;
			}
			if (arena->type == MemoryArenaType_BufferSingleAlloc && arena->used > 0)
			{
				Assert(false);
				result = nullptr;
				break;
			}
			
			result = ((u8*)arena->base) + arena->used;
			arena->used += allocSize;
		} break;
		
		default:
		{
			//Unknown arena type
			Assert(false);
		} break;
	}
	
	#if MEMORY_ARENA_META_INFO_ENABLED
	if (result != nullptr && arena->metaArena != nullptr && filePath != nullptr && funcName != nullptr)
	{
		ArenaAllocMetaInfo_t* lastMetaInfo = arena->firstMetaInfo;
		while (lastMetaInfo != nullptr && lastMetaInfo->next != nullptr) { lastMetaInfo = lastMetaInfo->next; }
		
		const char* fileName = GetFileNamePart(filePath);
		u32 fileNameLength = MyStrLength32(fileName);
		u32 funcNameLength = MyStrLength32(funcName);
		u32 metaAllocSize = sizeof(ArenaAllocMetaInfo_t) + fileNameLength+1 + funcNameLength+1;
		ArenaAllocMetaInfo_t* metaInfo = (ArenaAllocMetaInfo_t*)ArenaPush_(nullptr, 0, nullptr, arena->metaArena, metaAllocSize);
		if (metaInfo != nullptr)
		{
			ClearPointer(metaInfo);
			metaInfo->next = nullptr;
			metaInfo->allocSize = allocSize;
			metaInfo->allocPntr = result;
			metaInfo->fileNameLength = fileNameLength;
			metaInfo->funcNameLength = funcNameLength;
			metaInfo->lineNumber = lineNumber;
			char* fileNameSpace = (char*)(metaInfo+1);
			char* funcNameSpace = fileNameSpace + fileNameLength+1;
			MyMemCopy(fileNameSpace, fileName, fileNameLength+1);
			MyMemCopy(funcNameSpace, funcName, funcNameLength+1);
			
			if (lastMetaInfo != nullptr) { lastMetaInfo->next = metaInfo; }
			else { arena->firstMetaInfo = metaInfo; }
		}
	}
	#endif
	
	return result;
}

void* ArenaRealloc_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arena, void* oldPntr, u32 newSize, u32 oldSize, bool noMovement) //pre-declared above
{
	NotNull(arena);
	NotNull(oldPntr);
	Assert(newSize > 0);
	Assert(oldSize == 0 || newSize >= oldSize);
	switch (arena->type)
	{
		//TODO: Finish implementing this!
		#if 0
		case MemoryArenaType_Heap:
		{
			Assert(((u8*)oldPntr) >= ((u8*)arena->base) && ((u8*)oldPntr) + oldSize <= ((u8*)arena->base) + arena->size);
			if (oldSize != 0 && newSize == oldSize) { return oldPntr; }
			
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			u8* currentPntr = (u8*)arena->base;
			void* allocLocation = nullptr;
			
			while (true)
			{
				if (currentPntr - (u8*)arena->base >= arena->size)
				{
					//Reached the end of the arena without finding oldPntr allocation
					Assert(false);
					return nullptr;
				}
				
				HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
				Assert(header->size > 0);
				
				if (((u8*)oldPntr) == currentPntr + headerSize)
				{
					Assert(oldSize == 0 || headerSize + oldSize <= header->size);
					oldSize = (header->size - headerSize);
					if (newSize <= (header->size - headerSize)) { return oldPntr; }
					
					u32 numBytesNeeded = newSize - (header->size - headerSize);
					HeapAllocationHeader_t* nextHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
					
					if (((u8*)nextHeader) < (u8*)arena->base + arena->size && !nextHeader->isUsed && nextHeader->size >= numBytesNeeded)
					{
						if (nextHeader->size - numBytesNeeded >= headerSize)
						{
							
						}
						else
						{
							//There's enough space in front but there's no space for an extra header squeezed in between
							//so we will have to allocate a bit more space for this section needed to fill that little gap
							header->size += nextHeader->size;
							arena->used += nextHeader->size - headerSize;
							return oldPntr;
						}
					}
					else
					{
						
					}
				}
				else
				{
					currentPntr += header->size;
				}
			}
		} break;
		#endif
		
		case MemoryArenaType_Temp:
		{
			Assert(oldSize != 0);
			if (newSize == oldSize) { return oldPntr; }
			Assert(((u8*)oldPntr) >= (u8*)arena->base && ((u8*)oldPntr) + oldSize <= ((u8*)arena->base) + arena->used);
			if (((u8*)oldPntr) + oldSize == ((u8*)arena->base) + arena->used)
			{
				if (arena->used + (newSize - oldSize) > arena->size)
				{
					Assert(false);
					return nullptr;
				}
				arena->used += (newSize - oldSize);
				return oldPntr;
			}
			else
			{
				if (noMovement) { return nullptr; }
				return ArenaPush_(filePath, lineNumber, funcName, arena, newSize);
			}
		} break;
		
		//TODO: Finish implementing this!
		default:
		{
			Assert(false); //Unimplemented arena type
			return nullptr;
		} break;
	}
}

char* ArenaString_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arenaPntr, const char* string, u32 strLength)
{
	char* result = nullptr;
	
	result = (char*)ArenaPush_(filePath, lineNumber, funcName, arenaPntr, strLength+1);
	if (strLength > 0) { MyMemCopy(result, string, strLength); }
	result[strLength] = '\0';
	
	return result;
}

char* ArenaNtString_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arenaPntr, const char* nullTermString)
{
	u32 strLength = MyStrLength32(nullTermString);
	return ArenaString_(filePath, lineNumber, funcName, arenaPntr, nullTermString, strLength);
}

#if MEMORY_ARENA_META_INFO_ENABLED
#define ArenaString(arenaPntr, string, strLength) ArenaString_(__FILE__, __LINE__, __func__, arenaPntr, string, strLength)
#define ArenaNtString(arenaPntr, nullTermString) ArenaNtString_(__FILE__, __LINE__, __func__, arenaPntr, nullTermString)
#define ArenaPrint(arenaPntr, formatStr, ...) ArenaPrint_(__FILE__, __LINE__, __func__, arenaPntr, formatStr, __VA_ARGS__)
#else
#define ArenaString(arenaPntr, string, strLength) ArenaString_(nullptr, 0, nullptr, arenaPntr, string, strLength)
#define ArenaNtString(arenaPntr, nullTermString) ArenaNtString_(nullptr, 0, nullptr, arenaPntr, nullTermString)
#define ArenaPrint(arenaPntr, formatStr, ...) ArenaPrint_(nullptr, 0, nullptr, arenaPntr, formatStr, __VA_ARGS__)
#endif

#define ArenaPrintVa(memArena, resultName, resultLengthName, formatStr)                                              \
char* resultName = nullptr;                                                                                          \
i32 resultLengthName = 0;                                                                                            \
va_list args;                                                                                                        \
do                                                                                                                   \
{                                                                                                                    \
		va_start(args, formatStr);                                                                                   \
		resultLengthName = MyVaListPrintf(resultName, 0, formatStr, args);/*Measure first*/                          \
		va_end(args);                                                                                                \
		if (resultLengthName >= 0)                                                                                   \
		{                                                                                                            \
			resultName = PushArray((memArena), char, resultLengthName+1); /*Allocate*/                               \
			Assert_(resultName != nullptr);                                                                          \
			va_start(args, formatStr);                                                                               \
			i32 secondPrintResult = MyVaListPrintf(resultName, resultLengthName+1, formatStr, args); /*Real printf*/ \
			Assert_(secondPrintResult == resultLengthName);                                                          \
			va_end(args);                                                                                            \
			resultName[resultLengthName] = '\0';                                                                     \
		}                                                                                                            \
}                                                                                                                    \
while(0)

char* ArenaPrint_(const char* filePath, u32 lineNumber, const char* funcName, MemoryArena_t* arenaPntr, const char* formatStr, ...)
{
	char* result = nullptr;
	
	va_list args;
	
	va_start(args, formatStr);
	int length = MyVaListPrintf(result, 0, formatStr, args);//Measure first
	Assert(length >= 0);
	va_end(args);
	
	result = (char*)ArenaPush_(filePath, lineNumber, funcName, arenaPntr, sizeof(char) * (length+1)); //Allocate
	
	va_start(args, formatStr);
	MyVaListPrintf(result, (size_t)(length+1), formatStr, args); //Real printf
	va_end(args);
	
	result[length] = '\0';
	
	return result;
}

//NOTE: Passing nullptr for dataPntr asks a generic question of whether or not this kind of arena can deallocate
bool ArenaCanPop(const MemoryArena_t* arena, const void* dataPntr = nullptr)
{
	NotNull(arena);
	switch (arena->type)
	{
		case MemoryArenaType_Temp:
		{
			return false;
		} break;
		
		case MemoryArenaType_StdHeap:
		{
			//NOTE: We don't actually keep track of which pointers are valid/allocated on the standard heap
			//      so we just always return true, assuming we can deallocate any pointer
			return true;
		} break;
		
		case MemoryArenaType_External:
		{
			//NOTE: We don't actually keep track of which pointers are valid/allocated on the external source
			//      so we just always return true, assuming we can deallocate any pointer
			return true;
		} break;
		
		case MemoryArenaType_GrowingHeap:
		case MemoryArenaType_GrowingHeapOutOfArena:
		{
			NotNull(arena->base);
			if (dataPntr == nullptr) { return true; }
			
			const u8* u8Pntr = (const u8*)dataPntr;
			GrowingHeapPageHeader_t* pageHeader = nullptr;
			if (arena->type == MemoryArenaType_GrowingHeap)
			{
				GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
				pageHeader = mainHeader->firstPage;
			}
			else if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
			{
				GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
				pageHeader = mainHeader->firstPage;
			}
			else { Assert(false); }
			while (pageHeader != nullptr)
			{
				u8* pageBase = (u8*)(pageHeader+1);
				if (u8Pntr >= pageBase && u8Pntr < pageBase + pageHeader->size)
				{
					return true;
				}
				pageHeader = pageHeader->next;
			}
			return false;
		} break;
		
		case MemoryArenaType_Buffer:
		case MemoryArenaType_BufferSingleAlloc:
		{
			if (dataPntr == nullptr) { return true; }
			else if (dataPntr == arena->base) { return true; }
			else { return false; }
		} break;
		
		default:
		{
			if (dataPntr == nullptr) { return true; }
			const u8* u8Pntr = (const u8*)dataPntr;
			u8* basePntr = (u8*)arena->base;
			if (u8Pntr >= basePntr && u8Pntr < basePntr + arena->size)
			{
				return true;
			}
			else
			{
				return false;
			}
		} break;
	}
}

void ArenaPop(MemoryArena_t* arena, void* pointer) //pre-declared above
{
	Assert(arena != nullptr);
	
	if (pointer == nullptr)
	{
		Assert(false);
		return;
	}
	
	switch (arena->type)
	{
		case MemoryArenaType_Linear:
		{
			//Linear Arenas can not have items removed
			Assert(false);
		} break;
		
		case MemoryArenaType_Heap:
		{
			if ((u8*)pointer < (u8*)arena->base ||
				(u8*)pointer > (u8*)arena->base + arena->size)
			{
				//That pointer was not in this memory arena
				Assert(false);
				return;
			}
			
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			u8* currentPntr = (u8*)arena->base;
			HeapAllocationHeader_t* lastHeader = nullptr;
			bool deallocated = false;
			
			while (!deallocated)
			{
				Assert((u32)(currentPntr - (u8*)arena->base) <= arena->size);
				if ((u32)(currentPntr - (u8*)arena->base) >= arena->size)
				{
					//Reached the end of the arena without finding the item to deallocate
					Assert(false);
					break;
				}
				
				HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
				Assert(header->size > 0);
				HeapAllocationHeader_t* nextHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
				if ((u32)((u8*)nextHeader - (u8*)arena->base) >= arena->size) 
				{
					nextHeader = nullptr;
				}
				
				if ((u8*)header + headerSize == (u8*)pointer)
				{
					if (!header->isUsed)
					{
						//That item has already been deallocated
						Assert(false);
						break;
					}
					else
					{
						header->isUsed = false;
						deallocated = true;
						arena->used -= (header->size - headerSize);
						
						if (lastHeader != nullptr && !lastHeader->isUsed)
						{
							//Merge with the last free chunk
							lastHeader->size += header->size;
							header = lastHeader;
							
							arena->used -= headerSize;
						}
						
						if (nextHeader != nullptr && !nextHeader->isUsed)
						{
							header->size += nextHeader->size;
							
							arena->used -= headerSize;
						}
						
						#if DEBUG
						MyMemSet(((u8*)header) + headerSize, 0xCC, header->size - headerSize);
						#endif
					}
				}
				else
				{
					currentPntr += header->size;
				}
				
				lastHeader = header;
			}
		} break;
		
		case MemoryArenaType_GrowingHeap:
		case MemoryArenaType_GrowingHeapOutOfArena:
		{
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			Assert(arena->base != nullptr);
			GrowingHeapPageHeader_t* pageHeader = nullptr;
			if (arena->type == MemoryArenaType_GrowingHeap)
			{
				Assert(arena->size > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				pageHeader = mainHeader->firstPage;
			}
			else if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
			{
				Assert(arena->size > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				pageHeader = mainHeader->firstPage;
			}
			else { Assert(false); }
			
			bool deallocated = false;
			
			GrowingHeapPageHeader_t* previousPage = nullptr;
			Assert(pageHeader != nullptr);
			u32 pIndex = 0;
			while (pageHeader != nullptr)
			{
				u8* pageBase = (u8*)(pageHeader+1);
				u8* currentPntr = pageBase;
				HeapAllocationHeader_t* previousHeader = nullptr;
				
				while (!deallocated)
				{
					Assert(currentPntr <= pageBase + pageHeader->size);
					if (currentPntr >= pageBase + pageHeader->size)
					{
						//Reached the end of the page without finding the item to deallocate
						break;
					}
					
					HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
					Assert(header->size > 0);
					HeapAllocationHeader_t* nextHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
					if ((u8*)nextHeader >= pageBase + pageHeader->size) 
					{
						nextHeader = nullptr;
					}
					
					if ((u8*)header + headerSize == (u8*)pointer)
					{
						if (!header->isUsed)
						{
							//That item has already been deallocated
							Assert(false);
							break;
						}
						else
						{
							header->isUsed = false;
							deallocated = true;
							Assert(arena->used >= (header->size - headerSize));
							arena->used -= (header->size - headerSize);
							Assert(pageHeader->used >= (header->size - headerSize));
							pageHeader->used -= (header->size - headerSize);
							
							if (previousHeader != nullptr && !previousHeader->isUsed)
							{
								//Merge with the last free chunk
								previousHeader->size += header->size;
								header = previousHeader;
								
								Assert(arena->used >= headerSize);
								arena->used -= headerSize;
								Assert(pageHeader->used >= headerSize);
								pageHeader->used -= headerSize;
							}
							
							if (nextHeader != nullptr && !nextHeader->isUsed)
							{
								header->size += nextHeader->size;
								
								Assert(arena->used >= headerSize);
								arena->used -= headerSize;
								Assert(pageHeader->used >= headerSize);
								pageHeader->used -= headerSize;
							}
							
							#if DEBUG
							MyMemSet(((u8*)header) + headerSize, 0xCC, header->size - headerSize);
							#endif
						}
					}
					else
					{
						currentPntr += header->size;
					}
					
					previousHeader = header;
				}
				
				previousPage = pageHeader;
				pageHeader = pageHeader->next;
				pIndex++;
			}
			
			if (!deallocated)
			{
				Assert(false); //Couldn't find the allocation in this arena
			}
		} break;
		
		case MemoryArenaType_StdHeap:
		{
			MyFree(pointer);
		} break;
		
		case MemoryArenaType_External:
		{
			Assert(arena->otherPntr != nullptr);
			FreeFunction_f* deallocate = (FreeFunction_f*)(arena->otherPntr);
			deallocate(pointer);
		} break;
		
		case MemoryArenaType_Buffer:
		case MemoryArenaType_BufferSingleAlloc:
		{
			if (pointer == arena->base)
			{
				arena->used = 0;
			}
			else
			{
				//Buffer arena doesn't support popping anything but the entire buffer
				Assert(false);
			}
		} break;
		
		default:
		{
			//Unknown memory arena type
			Assert(false);
		} break;
	};
	
	#if MEMORY_ARENA_META_INFO_ENABLED
	if (pointer != nullptr && arena->metaArena != nullptr)
	{
		bool foundMatch = false;
		ArenaAllocMetaInfo_t* lastInfo = nullptr;
		ArenaAllocMetaInfo_t* currentInfo = arena->firstMetaInfo;
		while (currentInfo != nullptr)
		{
			if (currentInfo->allocPntr == pointer)
			{
				if (lastInfo != nullptr) { lastInfo->next = currentInfo->next; }
				else { arena->firstMetaInfo = currentInfo->next; }
				
				Assert(ArenaCanPop(arena->metaArena, currentInfo));
				ArenaPop(arena->metaArena, currentInfo);
				
				foundMatch = true;
				break;
			}
			
			lastInfo = currentInfo;
			currentInfo = currentInfo->next;
		}
		
		Assert(foundMatch); //couldn't find meta info for allocation
	}
	#endif
}

bool ArenaWalk(MemoryArena_t* arena, ArenaWalkInfo_t* walkInfo)
{
	NotNull(arena);
	NotNull(walkInfo);
	
	bool beginningOfWalk = (walkInfo->dataPntr == nullptr);
	switch (arena->type)
	{
		case MemoryArenaType_Heap:
		{
			if (!beginningOfWalk)
			{
				if ((u8*)walkInfo->dataPntr < (u8*)arena->base ||
					(u8*)walkInfo->dataPntr > (u8*)arena->base + arena->size)
				{
					//That previous dataPntr was not in this memory arena
					Assert(false);
					return false;
				}
			}
			
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			u8* currentPntr = nullptr;
			if (beginningOfWalk)
			{
				if (arena->base == nullptr) { return false; }
				currentPntr = (u8*)arena->base;
			}
			else
			{
				NotNull(walkInfo->headerPntr);
				HeapAllocationHeader_t* prevHeader = (HeapAllocationHeader_t*)walkInfo->headerPntr;
				currentPntr = ((u8*)prevHeader) + prevHeader->size;
			}
			HeapAllocationHeader_t* lastHeader = (HeapAllocationHeader_t*)walkInfo->lastHeader;
			bool foundFilledSection = false;
			u32 totalUnfilledSectionSize = 0;
			
			while (currentPntr != ((u8*)arena->base) + arena->size)
			{
				Assert((u32)(currentPntr - (u8*)arena->base) <= arena->size);
				
				HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
				Assert(header->size > 0);
				HeapAllocationHeader_t* nextHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
				if ((u32)((u8*)nextHeader - (u8*)arena->base) >= arena->size) 
				{
					nextHeader = nullptr;
				}
				
				if (!header->isUsed)
				{
					totalUnfilledSectionSize += header->size;
				}
				else
				{
					walkInfo->allocIndex++;
					walkInfo->headerSize = headerSize;
					walkInfo->size = (header->size - headerSize);
					walkInfo->address = (u64)(header + headerSize);
					walkInfo->pageOffset = (u32)((u8*)header - (u8*)arena->base);
					walkInfo->endPageOffset = (u32)(((u8*)header + header->size) - (u8*)arena->base);
					walkInfo->pageIndex = 0;
					walkInfo->pageAllocIndex = walkInfo->allocIndex;
					walkInfo->pageSize = arena->size;
					walkInfo->spaceBefore = totalUnfilledSectionSize;
					walkInfo->dataPntr = (void*)((u8*)header + headerSize);
					walkInfo->headerPntr = (void*)header;
					walkInfo->lastHeader = (void*)lastHeader;
					return true;
				}
				
				currentPntr += header->size;
				lastHeader = header;
			}
			
			return false;
		} break;
		
		case MemoryArenaType_GrowingHeap:
		case MemoryArenaType_GrowingHeapOutOfArena:
		{
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			NotNull(arena->base);
			GrowingHeapPageHeader_t* pageHeader = nullptr;
			if (arena->type == MemoryArenaType_GrowingHeap)
			{
				Assert(arena->size > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				pageHeader = mainHeader->firstPage;
			}
			else if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
			{
				Assert(arena->size > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
				Assert(mainHeader->pageSize > sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t));
				pageHeader = mainHeader->firstPage;
			}
			else { Assert(false); }
			
			if (!beginningOfWalk)
			{
				pageHeader = (GrowingHeapPageHeader_t*)walkInfo->pageHeader;
			}
			else
			{
				walkInfo->pageHeader = (void*)pageHeader;
			}
			
			NotNull(pageHeader);
			u32 thisWalkIndex = 0;
			while (pageHeader != nullptr)
			{
				u8* pageBase = (u8*)(pageHeader+1);
				u8* currentPntr = pageBase;
				if (!beginningOfWalk && thisWalkIndex == 0)
				{
					HeapAllocationHeader_t* prevHeader = (HeapAllocationHeader_t*)walkInfo->headerPntr;
					currentPntr = (u8*)prevHeader + prevHeader->size;
					Assert(currentPntr >= pageBase && currentPntr <= pageBase + pageHeader->size);
				}
				else
				{
					walkInfo->pageAllocIndex = 0;
				}
				walkInfo->spaceBefore = 0;
				walkInfo->numSpacesBefore = 0;
				
				while (currentPntr < pageBase + pageHeader->size)
				{
					HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
					Assert(header->size > 0);
					HeapAllocationHeader_t* nextHeader = (HeapAllocationHeader_t*)(currentPntr + header->size);
					if ((u8*)nextHeader >= pageBase + pageHeader->size) 
					{
						nextHeader = nullptr;
					}
					
					if (header->isUsed)
					{
						walkInfo->allocIndex++;
						walkInfo->pageAllocIndex++;
						walkInfo->headerSize = headerSize;
						walkInfo->size = (header->size - headerSize);
						walkInfo->address = (u64)(header + headerSize);
						walkInfo->pageOffset = (u32)((u8*)header - pageBase);
						walkInfo->endPageOffset = (u32)(((u8*)header + header->size) - pageBase);
						walkInfo->pageSize = pageHeader->size;
						walkInfo->dataPntr = (void*)((u8*)header + headerSize);
						walkInfo->headerPntr = (void*)header;
						return true;
					}
					else
					{
						walkInfo->spaceBefore += header->size;
						walkInfo->numSpacesBefore++;
						currentPntr += header->size;
					}
				}
				Assert(currentPntr == pageBase + pageHeader->size);
				
				pageHeader = pageHeader->next;
				walkInfo->pageHeader = pageHeader;
				walkInfo->pageIndex++;
				thisWalkIndex++;
			}
			
			return false;
		} break;
		
		//TODO: Implement walking functions for the other arena types
		
		default:
		{
			Assert(false); //Not implemented yet
			return false;
		} break;
	}
}

void ArenaPushMark(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	u32* marksArray = (u32*)(arenaHeader + 1);
	
	Assert(arenaHeader->numMarks < arenaHeader->maxNumMarks);
	
	u32 lastMarkPos = 0;
	for (u32 mIndex = 0; mIndex < arenaHeader->numMarks; mIndex++)
	{
		lastMarkPos += marksArray[mIndex];
	}
	
	Assert(arena->used >= lastMarkPos);
	
	u32 newMarkOffset = arena->used - lastMarkPos;
	marksArray[arenaHeader->numMarks] = newMarkOffset;
	arenaHeader->numMarks++;
}

void ArenaPopMark(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	u32* marksArray = (u32*)(arenaHeader + 1);
	
	if (arenaHeader->numMarks == 0)
	{
		// DEBUG_WriteLine("Tried to pop an TempArena mark at bottom of marks!");
		Assert(false);
		return;
	}
	
	u32 lastMarkPos = 0;
	for (u32 mIndex = 0; mIndex < arenaHeader->numMarks; mIndex++)
	{
		lastMarkPos += marksArray[mIndex];
	}
	
	Assert(arena->used >= lastMarkPos);
	
	arena->used = lastMarkPos;
	marksArray[arenaHeader->numMarks-1] = 0;
	arenaHeader->numMarks--;
}

u32 ArenaNumMarks(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	return arenaHeader->numMarks;
}

bool IsTempArenaEmpty(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	
	return (arenaHeader->numMarks == 0);
}

u32 ArenaGetHighWaterMark(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	
	return arenaHeader->highWaterMark;
}

void ArenaResetHighWaterMark(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_Temp);
	
	TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
	
	arenaHeader->highWaterMark = 0;
}

bool ArenaValidate(MemoryArena_t* arena)
{
	Assert(arena != nullptr);
	
	if (arena->used > arena->size)
	{
		// DEBUG_WriteLine("Arena used is larger than size!");
		Assert(false);
		return false;
	}
	if (arena->size == 0) { return true; }
	if (arena->base == nullptr)
	{
		// DEBUG_WriteLine("Arena base is nullptr!");
		Assert(false);
		return false;
	}
	
	switch (arena->type)
	{
		case MemoryArenaType_Linear:
		{
			//TODO: Is there any other information we can check in this arena?
			return true;
		} break;
		
		case MemoryArenaType_Temp:
		{
			TempArenaHeader_t* arenaHeader = (TempArenaHeader_t*)arena->base;
			if (arenaHeader->highWaterMark > arena->size)
			{
				// DEBUG_PrintLine("TempArena highWaterMark is larger than the arena! %u/%u", arenaHeader->highWaterMark, arena->size);
				Assert(false);
				return false;
			}
			if (arena->used > arenaHeader->highWaterMark)
			{
				// DEBUG_PrintLine("TempArena highWaterMark is smaller than used! %u/%u", arenaHeader->highWaterMark, arena->used);
				Assert(false);
				return false;
			}
			if (arenaHeader->numMarks > arenaHeader->maxNumMarks)
			{
				// DEBUG_PrintLine("TempArena numMarks is greater than maxNumMarks %u/%u", arenaHeader->numMarks, arenaHeader->maxNumMarks);
				Assert(false);
				return false;
			}
			
			u32* marksArray = (u32*)(arenaHeader + 1);
			
			u32 lastMarkPos = 0;
			for (u32 mIndex = 0; mIndex < arenaHeader->numMarks; mIndex++)
			{
				lastMarkPos += marksArray[mIndex];
				if (lastMarkPos > arena->size)
				{
					// DEBUG_PrintLine("TempArena lastMarkPos (mark[%u] %u) is larger than the arena! %u/%u", mIndex, marksArray[mIndex], lastMarkPos, arena->size);
					Assert(false);
					return false;
				}
			}
			for (u32 mIndex = arenaHeader->numMarks; mIndex < arenaHeader->maxNumMarks; mIndex++)
			{
				if (marksArray[mIndex] != 0)
				{
					// DEBUG_PrintLine("TempArena lastMarkPos (mark[%u] %u) is not 0!", mIndex, marksArray[mIndex]);
					Assert(false);
					return false;
				}
			}
			
			return true;
		} break;
		
		case MemoryArenaType_Heap:
		{
			const u32 headerSize = sizeof(HeapAllocationHeader_t);
			
			u32 headerIndex = 0;
			u32 currentOffset = 0;
			u8* currentPntr = (u8*)arena->base;
			while (currentOffset < arena->size)
			{
				HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
				u8* isUsedDataPntr = (u8*)&header->isUsed;
				if (*isUsedDataPntr != 0x00 && *isUsedDataPntr != 0x01)
				{
					// Assert(false);
					return false;
				}
				if (header->size < sizeof(HeapAllocationHeader_t))
				{
					// Assert(false);
					return false;
				}
				if (currentOffset + header->size > arena->size)
				{
					// Assert(false);
					return false;
				}
				
				currentPntr += header->size;
				currentOffset += header->size;
				headerIndex++;
			}
			
			return true;
			
		} break;
		
		case MemoryArenaType_StdHeap:
		{
			return true; //Nothing to validate
		} break;
		
		case MemoryArenaType_External:
		{
			return true; //Nothing to validate
		} break;
		
		case MemoryArenaType_GrowingHeap:
		case MemoryArenaType_GrowingHeapOutOfArena:
		{
			if (arena->base == nullptr) { return false; }
			if (arena->used < sizeof(HeapAllocationHeader_t)) { Assert(false); return false; }
			if (arena->used > arena->size) { Assert(false); return false; }
			GrowingHeapPageHeader_t* pageHeader = nullptr;
			u32 pageSize = 0;
			u32 maxNumPages = 0;
			if (arena->type == MemoryArenaType_GrowingHeap)
			{
				if (arena->size < sizeof(GrowingHeapHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t)) { Assert(false); return false; }
				GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
				pageSize = mainHeader->pageSize;
				maxNumPages = mainHeader->maxNumPages;
				pageHeader = mainHeader->firstPage;
				if (mainHeader->allocFunction == nullptr) { Assert(false); return false; }
			}
			else if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
			{
				if (arena->size < sizeof(GrowingHeapOutOfArenaHeader_t) + sizeof(GrowingHeapPageHeader_t) + sizeof(HeapAllocationHeader_t)) { Assert(false); return false; }
				GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
				pageSize = mainHeader->pageSize;
				maxNumPages = mainHeader->maxNumPages;
				pageHeader = mainHeader->firstPage;
				if (mainHeader->allocArena == nullptr) { Assert(false); return false; }
			}
			else { Assert(false); }
			if (pageHeader == nullptr) { Assert(false); return false; }
			
			GrowingHeapPageHeader_t* previousPageHeader = nullptr;
			u32 totalSize = sizeof(GrowingHeapHeader_t);
			u32 totalUsed = sizeof(GrowingHeapHeader_t);
			u32 numPages = 0;
			while (pageHeader != nullptr)
			{
				totalSize += sizeof(GrowingHeapPageHeader_t) + pageHeader->size;
				totalUsed += sizeof(GrowingHeapPageHeader_t) + pageHeader->used;
				if (previousPageHeader == nullptr)
				{
					if (pageHeader->size != pageSize - sizeof(GrowingHeapHeader_t) - sizeof(GrowingHeapPageHeader_t)) { Assert(false); return false; }
				}
				else 
				{
					if (pageHeader->size != pageSize - sizeof(GrowingHeapPageHeader_t)) { Assert(false); return false; }
				}
				
				//Basically the same check that the regular Heap arena does
				{
					const u32 headerSize = sizeof(HeapAllocationHeader_t);
					
					u32 headerIndex = 0;
					u32 currentOffset = 0;
					u8* currentPntr = (u8*)(pageHeader+1);
					while (currentOffset < pageHeader->size)
					{
						HeapAllocationHeader_t* header = (HeapAllocationHeader_t*)currentPntr;
						u8* isUsedDataPntr = (u8*)&header->isUsed;
						if (*isUsedDataPntr != 0x00 && *isUsedDataPntr != 0x01) { Assert(false); return false; }
						if (header->size < sizeof(HeapAllocationHeader_t)) { Assert(false); return false; }
						if (currentOffset + header->size > pageHeader->size) { Assert(false); return false; }
						
						currentPntr += header->size;
						currentOffset += header->size;
						headerIndex++;
					}
				}
				
				previousPageHeader = pageHeader;
				pageHeader = pageHeader->next;
				numPages++;
			}
			
			if (maxNumPages != 0 && numPages > maxNumPages) { Assert(false); return false; }
			if (totalSize != arena->size) { Assert(false); return false; }
			if (totalUsed != arena->used) { Assert(false); return false; }
			
			return true;
		} break;
		
		case MemoryArenaType_Buffer:
		case MemoryArenaType_BufferSingleAlloc:
		{
			return true; //Nothing to validate
		} break;
		
		default:
		{
			// DEBUG_PrintLine("Unknown memory arena type %02X!", arena->type);
			Assert(false);
			return false;
		} break;
	}
}

void PopGrowingHeapOutOfArenaPages(MemoryArena_t* arena, MemoryArena_t* allocArena)
{
	NotNull(arena);
	Assert(arena->type == MemoryArenaType_GrowingHeapOutOfArena);
	if (arena->base != nullptr)
	{
		Assert(allocArena != nullptr);
		bool pageIndex = 0;
		GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
		GrowingHeapPageHeader_t* pageHeader = mainHeader->firstPage;
		while (pageHeader != nullptr)
		{
			GrowingHeapPageHeader_t* nextHeader = pageHeader->next;
			if (pageIndex == 0) { ArenaPop(allocArena, mainHeader); }
			else { ArenaPop(allocArena, pageHeader); }
			pageHeader = nextHeader;
			pageIndex++;
		}
	}
}

void MemoryArenaUpdateAllocateFunction(MemoryArena_t* arena, AllocationFunction_f* allocFunction)
{
	Assert(arena != nullptr);
	Assert(allocFunction != nullptr);
	if (arena->type == MemoryArenaType_GrowingHeap)
	{
		Assert(arena->base != nullptr);
		GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
		mainHeader->allocFunction = allocFunction;
	}
	else if (arena->type == MemoryArenaType_External)
	{
		arena->base = (void*)allocFunction;
	}
	else
	{
		Assert(false); //unsupported arena type
	}
}
void MemoryArenaUpdateAllocateArena(MemoryArena_t* arena, MemoryArena_t* allocArena)
{
	Assert(arena != nullptr);
	Assert(allocArena != nullptr);
	if (arena->type == MemoryArenaType_GrowingHeapOutOfArena)
	{
		Assert(arena->base != nullptr);
		GrowingHeapOutOfArenaHeader_t* mainHeader = (GrowingHeapOutOfArenaHeader_t*)arena->base;
		mainHeader->allocArena = allocArena;
	}
	else
	{
		Assert(false); //unsupported arena type
	}
}
void MemoryArenaUpdateFreeFunction(MemoryArena_t* arena, FreeFunction_f* freeFunction)
{
	Assert(arena != nullptr);
	Assert(freeFunction != nullptr);
	if (arena->type == MemoryArenaType_External)
	{
		arena->otherPntr = (void*)freeFunction;
	}
	else
	{
		Assert(false); //unsupported arena type
	}
}

void MemoryArenaFreeMemory(MemoryArena_t* arena, FreeFunction_f* freeFunction)
{
	Assert(arena != nullptr);
	Assert(arena->type == MemoryArenaType_GrowingHeap);
	Assert(freeFunction != nullptr);
	Assert(arena->base != nullptr);
	
	GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
	Assert(mainHeader->firstPage != nullptr);
	
	GrowingHeapPageHeader_t* pageHeader = mainHeader->firstPage->next;
	while (pageHeader != nullptr)
	{
		GrowingHeapPageHeader_t* nextPageHeader = pageHeader->next;
		freeFunction(pageHeader);
		pageHeader = nextPageHeader;
	}
	
	freeFunction(arena->base);
	arena->base = nullptr;
	arena->size = 0;
	arena->used = 0;
}

//TODO: Provide some kind of analytics for various memory arenas

#endif // _MEMORY_ARENA_H