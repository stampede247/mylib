/*
File:   my_tempMemory.h
Author: Taylor Robbins
Date:   08\30\2017
Description:
	** Useful macros and functions for working with a public TempArena pointer
	** Make sure you also include tempMemory.cpp in your build
	** and set TempArena to something before calling any of these functions
*/

#ifndef _MY_TEMP_MEMORY_H
#define _MY_TEMP_MEMORY_H

#ifndef USE_DYNAMIC_TEMP_ARENA
#define USE_DYNAMIC_TEMP_ARENA false
#endif

#if USE_DYNAMIC_TEMP_ARENA
#ifndef TempArena
//NOTE: The application should define a TempArena macro that will return a pointer
//      to the TempArena in whatever context the Temp function is being called from
#error The application must define a TempArena macro for this dynamic mode to work!
#endif
#else
extern MemoryArena_t* TempArena;
#endif

#endif // _MY_TEMP_MEMORY_H
