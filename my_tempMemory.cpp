/*
File:   my_tempMemory.cpp
Author: Taylor Robbins
Date:   08\30\2017
Description: 
	** Holds the declaration of TempArena 
*/

#include "my_tempMemory.h"

#if !USE_DYNAMIC_TEMP_ARENA
//NOTE: Make sure this gets set to something before calling
//		anything that relies on TempArena
MemoryArena_t* TempArena = nullptr;
#endif

#define TempAlloc(size) ArenaPush(TempArena, size)
#define TempStruct(StructureTypeName) (StructureTypeName*)ArenaPush(TempArena, sizeof(StructureTypeName))
#define TempArray(StructureTypeName, numItems) (StructureTypeName*)ArenaPush(TempArena, sizeof(StructureTypeName) * (numItems))
#define TempString(numChars) (char*)ArenaPush(TempArena, numChars)

#define TempPushMark() ArenaPushMark(TempArena)
#define TempPopMark() ArenaPopMark(TempArena)

char* TempPrint(const char* formatStr, ...)
{
	char* result = nullptr;
	
	va_list args;
	
	//TODO: Make this use _vscprintf instead of vsnprintf for initial sizing pass
	va_start(args, formatStr);
	u32 length = (u32)MyVaListPrintf(result, 0, formatStr, args);//Measure first
	va_end(args);
	
	result = TempString(length+1); //Allocate
	if (result == nullptr) { return nullptr; }
	
	va_start(args, formatStr);
	MyVaListPrintf(result, length+1, formatStr, args); //Real printf
	va_end(args);
	
	result[length] = '\0';
	
	return result;
}

//TODO: Make this handle negative error numbers coming from vsnprintf
#define TempPrintVa(resultName, resultLengthName, formatStr)                               \
	char* resultName = nullptr;                                                            \
	u32 resultLengthName = 0;                                                              \
	va_list args;                                                                          \
	do                                                                                     \
	{                                                                                      \
		va_start(args, formatStr);                                                         \
		int printResult = MyVaListPrintf(resultName, 0, formatStr, args);/*Measure first*/ \
		Assert_(printResult >= 0 && "TempPrintVa format failed!");                         \
		resultLengthName = (u32)printResult;                                               \
		va_end(args);                                                                      \
		resultName = TempString(resultLengthName+1); /*Allocate*/                          \
		if (resultName == nullptr) { break; }                                              \
		va_start(args, formatStr);                                                         \
		MyVaListPrintf(resultName, resultLengthName+1, formatStr, args); /*Real printf*/   \
		va_end(args);                                                                      \
		resultName[resultLengthName] = '\0';                                               \
	}                                                                                      \
	while(0)

char* TempCombine(const char* string1, const char* string2)
{
	u32 length1 = MyStrLength32(string1);
	u32 length2 = MyStrLength32(string2);
	
	char* result = TempString(length1 + length2 + 1);
	MyMemCopy(&result[0], string1, length1);
	MyMemCopy(&result[length1], string2, length2);
	result[length1+length2] = '\0';
	
	return result;
}

char* FormattedSizeStrArena(MemoryArena_t* memArena, u64 numBytes)
{
	NotNull(memArena);
	if (numBytes >= Kilobytes(1))
	{
		if (numBytes >= Megabytes(1))
		{
			if (numBytes >= Gigabytes(1))
			{
				return ArenaPrint(memArena, "%luGB %luMB %lukB %lub", numBytes/Gigabytes(1), (numBytes/Megabytes(1))%1024, (numBytes/Kilobytes(1))%1024, numBytes%1024);
			}
			else
			{
				return ArenaPrint(memArena, "%luMB %lukB %lub", numBytes/Megabytes(1), (numBytes/Kilobytes(1))%1024, numBytes%1024);
			}
		}
		else
		{
			return ArenaPrint(memArena, "%lukB %lub", numBytes/Kilobytes(1), numBytes%1024);
		}
	}
	else
	{
		return ArenaPrint(memArena, "%lub", numBytes);
	}
}
char* FormattedSizeStr(u64 numBytes)
{
	return FormattedSizeStrArena(TempArena, numBytes);
}

char* FormattedMilliseconds(u64 numMilliseconds)
{
	u64 numSeconds = numMilliseconds / 1000;
	u64 numDays = (numSeconds / NUM_SEC_PER_DAY); numSeconds -= numDays * NUM_SEC_PER_DAY;
	u64 numHours = (numSeconds / NUM_SEC_PER_HOUR); numSeconds -= numHours * NUM_SEC_PER_HOUR;
	u64 numMinutes = (numSeconds / NUM_SEC_PER_MINUTE); numSeconds -= numMinutes * NUM_SEC_PER_MINUTE;
	u64 numMs = (numMilliseconds % 1000);
	
	if (numDays == 0)
	{
		if (numHours == 0)
		{
			if (numMinutes == 0)
			{
				return TempPrint("%lus %lums", numSeconds, numMs);
			}
			else
			{
				return TempPrint("%lum %lus %lums", numMinutes, numSeconds, numMs);
			}
		}
		else
		{
			return TempPrint("%luh %lum %lus %lums", numHours, numMinutes, numSeconds, numMs);
		}
	}
	else
	{
		return TempPrint("%lud %luh %lum %lus %lums", numDays, numHours, numMinutes, numSeconds, numMs);
	}
}
char* FormattedMillisecondsR64(r64 numMilliseconds)
{
	if (numMilliseconds < 0) { numMilliseconds = 0; } //we don't support negative values in this format function
	u64 numMicroseconds = (u64)FloorR64(DecimalPartR64(numMilliseconds) * 1000.0);
	u64 numSeconds = (u64)FloorR64(numMilliseconds) / 1000;
	u64 numDays = (numSeconds / NUM_SEC_PER_DAY); numSeconds -= numDays * NUM_SEC_PER_DAY;
	u64 numHours = (numSeconds / NUM_SEC_PER_HOUR); numSeconds -= numHours * NUM_SEC_PER_HOUR;
	u64 numMinutes = (numSeconds / NUM_SEC_PER_MINUTE); numSeconds -= numMinutes * NUM_SEC_PER_MINUTE;
	u64 numMs = ((u64)FloorR64(numMilliseconds) % 1000);
	
	if (numDays == 0)
	{
		if (numHours == 0)
		{
			if (numMinutes == 0)
			{
				if (numSeconds == 0)
				{
					return TempPrint("%lu.%03lums", numMs, numMicroseconds);
				}
				else
				{
					return TempPrint("%lus %lu.%03lums", numSeconds, numMs, numMicroseconds);
				}
			}
			else
			{
				return TempPrint("%lum %lus %lu.%03lums", numMinutes, numSeconds, numMs, numMicroseconds);
			}
		}
		else
		{
			return TempPrint("%luh %lum %lus %lu.%03lums", numHours, numMinutes, numSeconds, numMs, numMicroseconds);
		}
	}
	else
	{
		return TempPrint("%lud %luh %lum %lus %lu.%03lums", numDays, numHours, numMinutes, numSeconds, numMs, numMicroseconds);
	}
}

char* FormattedMillisecondsGame(u64 numMilliseconds)
{
	u64 numSeconds = numMilliseconds / 1000;
	u64 numDays = (numSeconds / NUM_SEC_PER_DAY); numSeconds -= numDays * NUM_SEC_PER_DAY;
	u64 numHours = (numSeconds / NUM_SEC_PER_HOUR); numSeconds -= numHours * NUM_SEC_PER_HOUR;
	u64 numMinutes = (numSeconds / NUM_SEC_PER_MINUTE); numSeconds -= numMinutes * NUM_SEC_PER_MINUTE;
	u64 numMs = (numMilliseconds % 1000);
	
	if (numDays == 0)
	{
		if (numHours == 0)
		{
			if (numMinutes == 0)
			{
				return TempPrint("%lu.%03lu", numSeconds, numMs);
			}
			else
			{
				return TempPrint("%lu:%02lu.%03lu", numMinutes, numSeconds, numMs);
			}
		}
		else
		{
			return TempPrint("%lu:%02lu:%02lu.%03lu", numHours, numMinutes, numSeconds, numMs);
		}
	}
	else
	{
		return TempPrint("%lu days %lu:%02lu:%02lu.%03lu", numDays, numHours, numMinutes, numSeconds, numMs);
	}
}

char* FormattedSeconds(u64 numSeconds)
{
	u64 numDays = (numSeconds / NUM_SEC_PER_DAY); numSeconds -= numDays * NUM_SEC_PER_DAY;
	u64 numHours = (numSeconds / NUM_SEC_PER_HOUR); numSeconds -= numHours * NUM_SEC_PER_HOUR;
	u64 numMinutes = (numSeconds / NUM_SEC_PER_MINUTE); numSeconds -= numMinutes * NUM_SEC_PER_MINUTE;
	
	if (numDays == 0)
	{
		if (numHours == 0)
		{
			if (numMinutes == 0)
			{
				return TempPrint("%lus", numSeconds);
			}
			else
			{
				return TempPrint("%lum %lus", numMinutes, numSeconds);
			}
		}
		else
		{
			return TempPrint("%luh %lum %lus", numHours, numMinutes, numSeconds);
		}
	}
	else
	{
		return TempPrint("%lud %luh %lum %lus", numDays, numHours, numMinutes, numSeconds);
	}
}

char* GetBinaryStrU8(u8 value, bool seperateNibbles = false)
{
	u32 allocSize = 8+1 + (seperateNibbles ? 1 : 0);
	char* result = TempString(allocSize);
	u8 bIndex = 0;
	u8 cIndex = 0;
	for (u8 mask = 0x80; mask > 0; mask = (mask>>1))
	{
		Assert(cIndex < allocSize);
		if (seperateNibbles && (bIndex%4) == 0 && bIndex != 0) { result[cIndex++] = ' '; }
		result[cIndex++] = (value & mask) ? '1' : '0';
		bIndex++;
	}
	Assert(cIndex < allocSize);
	result[cIndex] = '\0';
	return result;
}

char* GetBinaryStrU16(u16 value, bool seperateNibbles = false)
{
	u32 allocSize = 16+1 + (seperateNibbles ? 3 : 0);
	char* result = TempString(allocSize);
	u8 bIndex = 0;
	u8 cIndex = 0;
	for (u16 mask = 0x8000; mask > 0; mask = (mask>>1))
	{
		Assert(cIndex < allocSize);
		if (seperateNibbles && (bIndex%4) == 0 && bIndex != 0) { result[cIndex++] = ' '; }
		result[cIndex++] = (value & mask) ? '1' : '0';
		bIndex++;
	}
	Assert(cIndex < allocSize);
	result[cIndex] = '\0';
	return result;
}

char* GetBinaryStrU32(u32 value, bool seperateNibbles = false)
{
	u32 allocSize = 32+1 + (seperateNibbles ? 7 : 0);
	char* result = TempString(allocSize);
	u8 bIndex = 0;
	u8 cIndex = 0;
	for (u32 mask = 0x80000000; mask > 0; mask = (mask>>1))
	{
		Assert(cIndex < allocSize);
		if (seperateNibbles && (bIndex%4) == 0 && bIndex != 0) { result[cIndex++] = ' '; }
		result[cIndex++] = (value & mask) ? '1' : '0';
		bIndex++;
	}
	Assert(cIndex < allocSize);
	result[cIndex] = '\0';
	return result;
}

char* GetBinaryStrU64(u64 value, bool seperateNibbles = false)
{
	u32 allocSize = 64+1 + (seperateNibbles ? 15 : 0);
	char* result = TempString(allocSize);
	u8 bIndex = 0;
	u8 cIndex = 0;
	for (u64 mask = 0x8000000000000000; mask > 0; mask = (mask>>1))
	{
		Assert(cIndex < allocSize);
		if (seperateNibbles && (bIndex%4) == 0 && bIndex != 0) { result[cIndex++] = ' '; }
		result[cIndex++] = (value & mask) ? '1' : '0';
		bIndex++;
	}
	Assert(cIndex < allocSize);
	result[cIndex] = '\0';
	return result;
}

#ifdef _MY_TIME_H
char* FormattedRealTime(MemoryArena_t* memArena, const RealTime_t* realTime, bool compactFormat = true, bool showDate = true, bool showTime = true)
{
	Assert(memArena != nullptr);
	Assert(realTime != nullptr);
	
	if (compactFormat)
	{
		if (showDate && showTime)
		{
			return ArenaPrint(memArena, "%u/%02u/%04u %u:%02u:%02u%s", realTime->month+1, realTime->day+1, realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		}
		else if (showDate)
		{
			return ArenaPrint(memArena, "%u/%02u/%04u", realTime->month+1, realTime->day+1, realTime->year);
		}
		else if (showTime)
		{
			return ArenaPrint(memArena, "%u:%02u:%02u%s", Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		}
		else
		{
			return ArenaNtString(memArena, "");
		}
	}
	else 
	{
		if (showDate && showTime)
		{
			return ArenaPrint(memArena, "%s %s %s %04u %u:%02u:%02u%s", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthString((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		}
		else if (showDate)
		{
			return ArenaPrint(memArena, "%s %s %s %04u", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthString((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year);
		}
		else if (showTime)
		{
			return ArenaPrint(memArena, "%u:%02u:%02u%s", Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		}
		else
		{
			return ArenaNtString(memArena, "");
		}
	}
}
#endif
