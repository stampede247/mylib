/*
File:   my_tempList.h
Author: Taylor Robbins
Date:   09\03\2017
Description:
	** The TempList type takes advantage of the TempArena defined in tempMemory.h
	** You can easily initialize and push on items to the list as well as easily access them
//NOTE: There are no popping or freeing functions in the TempList
*/

#ifndef _MY_TEMP_LIST_H
#define _MY_TEMP_LIST_H

#ifndef _MY_TEMP_MEMORY_H
	#error my_tempMemory.h must be included before including tempList.h
#endif

struct TempListItem_t
{
	TempListItem_t* nextItem;
	u8 dataStart;
};

struct TempList_t
{
	u32 itemSize;
	u32 numItems;
	TempListItem_t* firstItem;
	TempListItem_t* lastItem;
};

void TempListInit(TempList_t* listPntr, u32 itemSize)
{
	Assert(listPntr != nullptr);
	Assert(itemSize != 0);
	ClearPointer(listPntr);
	
	listPntr->itemSize = itemSize;
	listPntr->numItems = 0;
	listPntr->firstItem = nullptr;
	listPntr->lastItem = nullptr;
}

#define TempListGet(listPntr, ItemType, index) (ItemType*)TempListGet_(listPntr, index); Assert(sizeof(ItemType) == (listPntr)->itemSize)

TempListItem_t* TempListGetItem_(TempList_t* listPntr, u32 index)
{
	Assert(listPntr != nullptr);
	
	if (index >= listPntr->numItems)
	{
		return nullptr;
	}
	
	TempListItem_t* currentItem = listPntr->firstItem;
	for (u32 iIndex = 0; iIndex < index; iIndex++)
	{
		Assert(currentItem != nullptr);
		currentItem = currentItem->nextItem;
	}
	Assert(currentItem != nullptr);
	
	return currentItem;
}
void* TempListGet_(TempList_t* listPntr, u32 index)
{
	Assert(listPntr != nullptr);
	
	if (index >= listPntr->numItems)
	{
		return nullptr;
	}
	
	TempListItem_t* currentItem = listPntr->firstItem;
	for (u32 iIndex = 0; iIndex < index; iIndex++)
	{
		Assert(currentItem != nullptr);
		currentItem = currentItem->nextItem;
	}
	Assert(currentItem != nullptr);
	
	return (void*)&currentItem->dataStart;
}

#define TempListPush(listPntr, ItemType)             (ItemType*)TempListPush_(listPntr, nullptr);     Assert(sizeof(ItemType) <= (listPntr)->itemSize)
#define TempListAdd(listPntr, ItemType, newItemPntr) (ItemType*)TempListPush_(listPntr, newItemPntr); Assert(sizeof(ItemType) <= (listPntr)->itemSize)

void* TempListPush_(TempList_t* listPntr, const void* newItemData = nullptr)
{
	Assert(listPntr != nullptr);
	
	TempListItem_t* newItem = (TempListItem_t*)TempAlloc(sizeof(void*) + listPntr->itemSize);
	newItem->nextItem = nullptr;
	void* itemDataPntr = (void*)&newItem->dataStart;
	
	if (newItemData != nullptr)
	{
		MyMemCopy(itemDataPntr, newItemData, listPntr->itemSize);
	}
	
	TempListItem_t* lastItem = listPntr->lastItem;
	if (lastItem != nullptr)
	{
		lastItem->nextItem = newItem;
	}
	else
	{
		Assert(listPntr->firstItem == nullptr);
		listPntr->firstItem = newItem;
	}
	
	listPntr->lastItem = newItem;
	listPntr->numItems++;
	
	return itemDataPntr;
}

#define TempListPushAt(listPntr, ItemType, index)             (ItemType*)TempListPushAt_(listPntr, index, nullptr);     Assert(sizeof(ItemType) <= (listPntr)->itemSize)
#define TempListAddAt(listPntr, ItemType, index, newItemPntr) (ItemType*)TempListPushAt_(listPntr, index, newItemPntr); Assert(sizeof(ItemType) <= (listPntr)->itemSize)

void* TempListPushAt_(TempList_t* listPntr, u32 index, const void* newItemData = nullptr)
{
	Assert(listPntr != nullptr);
	Assert(index <= listPntr->numItems);
	
	TempListItem_t* newItem = (TempListItem_t*)TempAlloc(sizeof(void*) + listPntr->itemSize);
	newItem->nextItem = nullptr;
	void* itemDataPntr = (void*)&newItem->dataStart;
	
	if (newItemData != nullptr)
	{
		MyMemCopy(itemDataPntr, newItemData, listPntr->itemSize);
	}
	
	if (index == 0)
	{
		newItem->nextItem = listPntr->firstItem;
		listPntr->firstItem = newItem;
	}
	else
	{
		TempListItem_t* itemBefore = TempListGetItem_(listPntr, index-1);
		Assert(itemBefore != nullptr);
		
		newItem->nextItem = itemBefore->nextItem;
		itemBefore->nextItem = newItem;
	}
	
	if (index == listPntr->numItems) { listPntr->lastItem = newItem; }
	listPntr->numItems++;
	
	return itemDataPntr;
}

#define TempListToArray(listPntr, ItemType, arenaPntr) (ItemType*)TempListToArray_(listPntr, arenaPntr); Assert(sizeof(ItemType) <= (listPntr)->itemSize)

void* TempListToArray_(TempList_t* listPntr, MemoryArena_t* arenaPntr)
{
	Assert(listPntr != nullptr);
	Assert(arenaPntr != nullptr);
	
	if (listPntr->numItems == 0) return nullptr;
	
	void* result = ArenaPush(arenaPntr, listPntr->numItems * listPntr->itemSize);
	
	u8* bytePntr = (u8*)result;
	TempListItem_t* itemPntr = listPntr->firstItem;
	for (u32 iIndex = 0; iIndex < listPntr->numItems; iIndex++)
	{
		void* itemDataPntr = (void*)&itemPntr->dataStart;
		MyMemCopy(bytePntr, itemDataPntr, listPntr->itemSize);
		
		bytePntr += listPntr->itemSize;
		itemPntr = itemPntr->nextItem;
	}
	
	return result;
}

#endif // _MY_TEMP_LIST_H
