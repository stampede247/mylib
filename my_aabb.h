/*
File:   my_aabb.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the AABB2_t and AABB3_t structures
	** and implements all the base functions that operate on or create them

This file is #included by default from mylib.h
*/

#ifndef _MY_AABB_H
#define _MY_AABB_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
struct AABB2_t
{
	union
	{
		v2 center;
		v2 position;
		struct { r32 cX; r32 cY; };
	};
	union
	{
		v2 size;
		v2 dimensions;
		struct { r32 width; r32 height; };
	};
};

struct AABB3_t
{
	union
	{
		v3 center;
		v3 position;
		struct { r32 cX; r32 cY; r32 cZ; };
	};
	union
	{
		v3 size;
		v3 dimensions;
		struct { r32 width; r32 height; r32 depth; };
	};
};

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline AABB3_t NewAABB3(Vec3_t center, Vec3_t size)
{
	AABB3_t result = {};
	result.center = center;
	result.size = size;
	return result;
}

inline AABB2_t NewAABB2(Vec2_t center, Vec2_t size)
{
	AABB2_t result = {};
	result.center = center;
	result.size = size;
	return result;
}

#endif //  _MY_AABB_H
