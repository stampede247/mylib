/*
File:   my_sorting.h
Author: Taylor Robbins
Date:   08\24\2019
Description:
	** This file holds all of the sorting algorithms that we have implemented. Some algorithms are implemented in different ways to provide different ways of using them
*/

#ifndef _MY_SORTING_H
#define _MY_SORTING_H

//-1: <    0: ==   1: >
#define COMPARE_FUNC_DEFINITION(functionName) i32 functionName(const void* left, const void* right, void* contextPntr)
typedef COMPARE_FUNC_DEFINITION(CompareFunc_f);

// +--------------------------------------------------------------+
// |                          Quick Sort                          |
// +--------------------------------------------------------------+
i32 QuickSortPartition(void* arrayPntr, u32 numElements, u32 elementSize, void* workingSpace, CompareFunc_f* compareFunc, void* contextPntr = nullptr)
{
	Assert(arrayPntr != nullptr);
	Assert(elementSize > 0);
	Assert(workingSpace != nullptr);
	Assert(compareFunc != nullptr);
	u8* bytePntr = (u8*)arrayPntr;
	
	u8* tempSpace = ((u8*)workingSpace) + 0;
	u8* pivotCopy = ((u8*)workingSpace) + elementSize;
	u8* pivotPntr = bytePntr + elementSize*(numElements-1);
	MyMemCopy(pivotCopy, pivotPntr, elementSize);
	
	i32 smallIndex = -1;
	u8* smallPntr = bytePntr - elementSize;
	for (u32 eIndex = 0; eIndex < numElements-1; eIndex++)
	{
		u8* elementPntr = bytePntr + elementSize*(eIndex);
		if (compareFunc((const void*)elementPntr, (const void*)pivotCopy, contextPntr) < 0)
		{
			smallIndex++;
			smallPntr += elementSize;
			
			//swap smallPntr and elementPntr
			MyMemCopy(tempSpace, smallPntr, elementSize);
			MyMemCopy(smallPntr, elementPntr, elementSize);
			MyMemCopy(elementPntr, tempSpace, elementSize);
		}
	}
	
	smallIndex++;
	smallPntr += elementSize;
	Assert((u32)smallIndex < numElements);
	MyMemCopy(pivotPntr, smallPntr, elementSize);
	MyMemCopy(smallPntr, pivotCopy, elementSize);
	
	return smallIndex;
}

//NOTE: workingSpace must be a space large enough to hold two elements. This space is used to perform swaps and to hold the pivot element
void QuickSort(void* arrayPntr, u32 numElements, u32 elementSize, void* workingSpace, CompareFunc_f* compareFunc, void* contextPntr)
{
	Assert(arrayPntr != nullptr);
	Assert(elementSize > 0);
	Assert(workingSpace != nullptr);
	Assert(compareFunc != nullptr);
	if (numElements <= 1) { return; } //nothing to sort
	u8* bytePntr = (u8*)arrayPntr;
	
	#if DEBUG
	//make sure the workingSpace is large enough by writing it to zero to make sure we don't have any memory access exceptions happen
	MyMemSet(workingSpace, 0x00, elementSize*2);
	#endif
	
	//parition the array
	i32 pi = QuickSortPartition(arrayPntr, numElements, elementSize, workingSpace, compareFunc, contextPntr);
	
	//sort lower partition
	QuickSort(arrayPntr, (u32)pi, elementSize, workingSpace, compareFunc, contextPntr);
	
	//sort upper partition
	QuickSort((void*)(bytePntr + elementSize*(pi+1)), (u32)(numElements - (pi+1)), elementSize, workingSpace, compareFunc, contextPntr);
}

void QuickSortAlloc(void* arrayPntr, u32 numElements, u32 elementSize, MemoryArena_t* workingAllocArena, CompareFunc_f* compareFunc, void* contextPntr = nullptr)
{
	Assert(elementSize > 0);
	Assert(workingAllocArena != nullptr);
	u8* workingSpace = PushArray(workingAllocArena, u8, elementSize*2);
	Assert(workingSpace != nullptr);
	QuickSort(arrayPntr, numElements, elementSize, workingSpace, compareFunc, contextPntr);
}

#endif //  _MY_SORTING_H
