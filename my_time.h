/*
File:   my_time.h
Author: Taylor Robbins
Date:   03\02\2018
Description:
	** This file holds some useful function for dealing with human readable time
*/

#ifndef _MY_TIME_H
#define _MY_TIME_H

#define NUM_MS_PER_SECOND 1000
#define NUM_MS_PER_MINUTE (60*NUM_MS_PER_SECOND)
#define NUM_MS_PER_HOUR   (60*NUM_MS_PER_MINUTE)
#define NUM_MS_PER_DAY    (24*NUM_MS_PER_HOUR)
#define NUM_MS_PER_YEAR   (365*NUM_MS_PER_DAY)

#define NUM_SEC_PER_MINUTE 60
#define NUM_SEC_PER_HOUR   (60*NUM_SEC_PER_MINUTE)
#define NUM_SEC_PER_DAY    (24*NUM_SEC_PER_HOUR)
#define NUM_SEC_PER_YEAR   (365*NUM_SEC_PER_DAY)

#define NUM_MIN_PER_HOUR    60
#define NUM_HOUR_PER_DAY    24
#define NUM_DAYS_PER_WEEK   7
#define NUM_DAYS_PER_YEAR   365
#define NUM_DAYS_PER_4YEARS (365*4 + 1)
#define NUM_MIN_PER_DAY     (NUM_HOUR_PER_DAY*NUM_MIN_PER_HOUR)
#define NUM_SEC_PER_WEEK    (NUM_SEC_PER_DAY*NUM_DAYS_PER_WEEK)
#define NUM_MIN_PER_WEEK    (NUM_MIN_PER_DAY*NUM_DAYS_PER_WEEK)

typedef enum 
{
	Month_January = 0,
	Month_February,
	Month_March,
	Month_April,
	Month_May,
	Month_June,
	Month_July,
	Month_August,
	Month_September,
	Month_October,
	Month_November,
	Month_December,
} Month_t;

typedef enum 
{
	DayOfWeek_Sunday = 0,
	DayOfWeek_Monday,
	DayOfWeek_Tuesday,
	DayOfWeek_Wednesday,
	DayOfWeek_Thursday,
	DayOfWeek_Friday,
	DayOfWeek_Saturday,
} DayOfWeek_t;

typedef enum
{
	TimeUnit_Milliseconds,
	TimeUnit_Seconds,
	TimeUnit_Minutes,
	TimeUnit_Hours,
	TimeUnit_Days,
	TimeUnit_Months,
	TimeUnit_Years,
} TimeUnit_t;

const char* GetMonthString(Month_t month)
{
	switch (month)
	{
		case Month_January:   return "January";
		case Month_February:  return "February";
		case Month_March:     return "March";
		case Month_April:     return "April";
		case Month_May:       return "May";
		case Month_June:      return "June";
		case Month_July:      return "July";
		case Month_August:    return "August";
		case Month_September: return "September";
		case Month_October:   return "October";
		case Month_November:  return "November";
		case Month_December:  return "December";
		default:              return "Unknown";
	};
}
const char* GetMonthStr(Month_t month)
{
	switch (month)
	{
		case Month_January:   return "Jan";
		case Month_February:  return "Feb";
		case Month_March:     return "Mar";
		case Month_April:     return "Apr";
		case Month_May:       return "May";
		case Month_June:      return "Jun";
		case Month_July:      return "Jul";
		case Month_August:    return "Aug";
		case Month_September: return "Sep";
		case Month_October:   return "Oct";
		case Month_November:  return "Nov";
		case Month_December:  return "Dec";
		default:              return "Unk";
	};
}

const char* GetDayOfWeekString(DayOfWeek_t dayOfWeek)
{
	switch (dayOfWeek)
	{
		case DayOfWeek_Sunday:    return "Sunday";
		case DayOfWeek_Monday:    return "Monday";
		case DayOfWeek_Tuesday:   return "Tuesday";
		case DayOfWeek_Wednesday: return "Wednesday";
		case DayOfWeek_Thursday:  return "Thursday";
		case DayOfWeek_Friday:    return "Friday";
		case DayOfWeek_Saturday:  return "Saturday";
		default:                  return "Unknown";
	};
}
const char* GetDayOfWeekStr(DayOfWeek_t dayOfWeek)
{
	switch (dayOfWeek)
	{
		case DayOfWeek_Sunday:    return "Sun";
		case DayOfWeek_Monday:    return "Mon";
		case DayOfWeek_Tuesday:   return "Tue";
		case DayOfWeek_Wednesday: return "Wed";
		case DayOfWeek_Thursday:  return "Thu";
		case DayOfWeek_Friday:    return "Fri";
		case DayOfWeek_Saturday:  return "Sat";
		default:                  return "Unk";
	};
}

const char* GetDayOfMonthString(u16 dayOfMonth)
{
	switch (dayOfMonth)
	{
		case 0:  return "1st";
		case 1:  return "2nd";
		case 2:  return "3rd";
		case 3:  return "4th";
		case 4:  return "5th";
		case 5:  return "6th";
		case 6:  return "7th";
		case 7:  return "8th";
		case 8:  return "9th";
		case 9:  return "10th";
		case 10: return "11th";
		case 11: return "12th";
		case 12: return "13th";
		case 13: return "14th";
		case 14: return "15th";
		case 15: return "16th";
		case 16: return "17th";
		case 17: return "18th";
		case 18: return "19th";
		case 19: return "20th";
		case 20: return "21st";
		case 21: return "22nd";
		case 22: return "23rd";
		case 23: return "24th";
		case 24: return "25th";
		case 25: return "26th";
		case 26: return "27th";
		case 27: return "28th";
		case 28: return "29th";
		case 29: return "30th";
		case 30: return "31st";
		default: return "Unk";
	};
}

inline u16 Convert24HourTo12Hour(u16 twentyFourHourValue)
{
	switch (twentyFourHourValue)
	{
		case 0:  return 12;
		case 1:  return 1;
		case 2:  return 2;
		case 3:  return 3;
		case 4:  return 4;
		case 5:  return 5;
		case 6:  return 6;
		case 7:  return 7;
		case 8:  return 8;
		case 9:  return 9;
		case 10: return 10;
		case 11: return 11;
		case 12: return 12;
		case 13: return 1;
		case 14: return 2;
		case 15: return 3;
		case 16: return 4;
		case 17: return 5;
		case 18: return 6;
		case 19: return 7;
		case 20: return 8;
		case 21: return 9;
		case 22: return 10;
		case 23: return 11;
		default: return 0;
	};
}

inline bool IsPostMeridian(u16 twentyFourHourValue)
{
	if (twentyFourHourValue >= 0 && twentyFourHourValue <= 11)
		return false;
	else
		return true;
}

const char* GetTimeUnitString(TimeUnit_t timeUnit)
{
	switch (timeUnit)
	{
		case TimeUnit_Milliseconds: return "milliseconds";
		case TimeUnit_Seconds:      return "seconds";
		case TimeUnit_Minutes:      return "minutes";
		case TimeUnit_Hours:        return "hours";
		case TimeUnit_Days:         return "days";
		case TimeUnit_Months:       return "months";
		case TimeUnit_Years:        return "years";
		default:                    return "unknown";
	};
}
const char* GetTimeUnitStr(TimeUnit_t timeUnit)
{
	switch (timeUnit)
	{
		case TimeUnit_Milliseconds: return "ms";
		case TimeUnit_Seconds:      return "sec";
		case TimeUnit_Minutes:      return "min";
		case TimeUnit_Hours:        return "hrs";
		case TimeUnit_Days:         return "dys";
		case TimeUnit_Months:       return "mts";
		case TimeUnit_Years:        return "yrs";
		default:                    return "unk";
	};
}

struct RealTime_t
{
	u64 timestamp;
	
	u16 year;
	u8 month;
	u8 day;
	u8 hour;
	u8 minute;
	u8 second;
	
	bool isLeapYear;
	bool isDstActive;
	u8 numMonthDays;
	u8 dayOfWeek;
	u16 dayOfYear;
	u16 minuteOfDay;
};

u8 NumDaysInMonth(u8 month, bool isLeapYear)
{
	switch (month)
	{
		case Month_January:   return 31;
		case Month_February:  return (u8)(isLeapYear ? 29 : 28);
		case Month_March:     return 31;
		case Month_April:     return 30;
		case Month_May:       return 31;
		case Month_June:      return 30;
		case Month_July:      return 31;
		case Month_August:    return 31;
		case Month_September: return 30;
		case Month_October:   return 31;
		case Month_November:  return 30;
		case Month_December:  return 31;
		default: return 30; //shouldn't happen
	}
}

bool IsDstActive(u8 month, u8 day, u8 hour, u8 dayOfWeek)
{
	//NOTE: This function is only useful for the United States or countries that start and stop DST at the same time as the US.
	bool result = false;
	
	//DST starts on the second Sunday in March at 2:00am (so jump from 2:00am to 3:00am)
	if (month == Month_March)
	{
		u8 numSundays = 0;
		u8 dow = dayOfWeek;
		i32 dIndex = day;
		while (dIndex >= 0)
		{
			if (dow == DayOfWeek_Sunday)
			{
				numSundays++;
			}
			if (dow == DayOfWeek_Sunday) { dow = DayOfWeek_Saturday; }
			else { dow--; }
			dIndex--;
		}
		
		if (numSundays >= 2)
		{
			if (numSundays == 2 && dayOfWeek == DayOfWeek_Sunday)
			{
				result = (hour >= 2); //Starts at 2:00am on this Sunday
			}
			else
			{
				result = true;
			}
		}
	}
	//DST ends on the first Sunday in November at 1:00am original time (so jump from 2:00am back to 1:00am)
	else if (month == Month_November)
	{
		result = true;
		
		u8 numSundays = 0;
		u8 dow = dayOfWeek;
		i32 dIndex = day;
		while (dIndex >= 0)
		{
			if (dow == DayOfWeek_Sunday)
			{
				numSundays++;
			}
			if (dow == DayOfWeek_Sunday) { dow = DayOfWeek_Saturday; }
			else { dow--; }
			dIndex--;
		}
		
		if (numSundays >= 1)
		{
			if (numSundays == 1 && dayOfWeek == DayOfWeek_Sunday)
			{
				result = (hour < 1); //Ends at 1:00am on this Sunday
			}
			else
			{
				result = false;
			}
		}
	}
	//All the months in between March and November are DST active months
	else if (month > Month_March && month < Month_November)
	{
		result = true;
	}
	
	//All other months are normal time
	
	#if 0
	if (RESET_BUTTON_VALUE == LOW) { result = !result; }
	#endif
	
	return result;
}

void GetRealTime(u64 timestamp, bool doDst, RealTime_t* realTimeOut)
{
	Assert(realTimeOut != nullptr);
	ClearPointer(realTimeOut);
	realTimeOut->timestamp = timestamp;
	u64 numSecondsLeft = timestamp;
	
	// if (TimezoneDoesDst && IsDstActive(timestamp, false))
	// {
	// 	numSecondsLeft += NUM_SEC_PER_HOUR;
	// }
	
	//TODO: This doesn't properly take into account the full logic for which years
	//      are and aren't leap years (every 100? years is not a leap year I think)
	//TODO: This u16 overflows in 216,000 years after 1970. Maybe we should fix that?
	u16 numFourYears = (u16)(numSecondsLeft / (NUM_SEC_PER_DAY*NUM_DAYS_PER_4YEARS));
	realTimeOut->year = (u16)(1970 + 4*numFourYears);
	numSecondsLeft -= numFourYears * (NUM_SEC_PER_DAY*NUM_DAYS_PER_4YEARS);
	
	u16 numDaysLeft = (u16)(numSecondsLeft / NUM_SEC_PER_DAY);
	numSecondsLeft -= numDaysLeft * NUM_SEC_PER_DAY;
	
	if (numDaysLeft < 365)              { realTimeOut->year += 0; numDaysLeft -= 0;           realTimeOut->isLeapYear = false; }
	else if (numDaysLeft < 365+365)     { realTimeOut->year += 1; numDaysLeft -= 365;         realTimeOut->isLeapYear = false; }
	else if (numDaysLeft < 365+365+366) { realTimeOut->year += 2; numDaysLeft -= 365+365;     realTimeOut->isLeapYear = true;  }
	else                                { realTimeOut->year += 3; numDaysLeft -= 365+365+366; realTimeOut->isLeapYear = false; }
	
	Assert(numDaysLeft <= 365);
	realTimeOut->dayOfYear = (u16)numDaysLeft;
	
	for (realTimeOut->month = 0; true; realTimeOut->month++)
	{
		Assert(realTimeOut->month < 12);
		u8 numMonthDays = NumDaysInMonth(realTimeOut->month, realTimeOut->isLeapYear);
		if (numDaysLeft < numMonthDays)
		{
			realTimeOut->numMonthDays = numMonthDays;
			break;
		}
		else
		{
			numDaysLeft -= numMonthDays;
		}
	}
	
	Assert(numDaysLeft < 31);
	realTimeOut->day = (u8)numDaysLeft;
	
	realTimeOut->hour = (u8)(numSecondsLeft / NUM_SEC_PER_HOUR);
	numSecondsLeft -= realTimeOut->hour * NUM_SEC_PER_HOUR;
	
	realTimeOut->minute = (u8)(numSecondsLeft / NUM_SEC_PER_MINUTE);
	numSecondsLeft -= realTimeOut->minute * NUM_SEC_PER_MINUTE;
	
	realTimeOut->second = (u8)numSecondsLeft;
	
	realTimeOut->minuteOfDay = (u16)((realTimeOut->hour*NUM_MIN_PER_HOUR) + realTimeOut->minute);
	Assert(realTimeOut->minuteOfDay < NUM_MIN_PER_DAY);
	
	//The dayOfWeek calculation operates indipendantly on the original
	//timestamp. Jan 1st 1970 was a Thursday so we have to offset the
	//timestamp by 4 days before taking the modulas
	realTimeOut->dayOfWeek = (u8)(((timestamp + NUM_SEC_PER_DAY*DayOfWeek_Thursday) % NUM_SEC_PER_WEEK) / NUM_SEC_PER_DAY);
	
	// +==============================+
	// |          Apply DST           |
	// +==============================+
	realTimeOut->isDstActive = false;
	if (doDst)
	{
		realTimeOut->isDstActive = IsDstActive(realTimeOut->month, realTimeOut->day, realTimeOut->hour, realTimeOut->dayOfWeek);
		if (realTimeOut->isDstActive)
		{
			realTimeOut->hour++;
			realTimeOut->minuteOfDay += NUM_MIN_PER_HOUR;
			if (realTimeOut->hour >= NUM_HOUR_PER_DAY)
			{
				realTimeOut->hour -= NUM_HOUR_PER_DAY;
				realTimeOut->day++;
				
				u8 numDaysInMonth = NumDaysInMonth(realTimeOut->month, realTimeOut->isLeapYear);
				if (realTimeOut->day >= numDaysInMonth)
				{
					realTimeOut->day -= numDaysInMonth;
					realTimeOut->month++;
					if (realTimeOut->month >= 12) //this shouldn't really happen
					{
						realTimeOut->month -= 12;
						realTimeOut->year++;
						//TODO: realTimeOut->isLeapYear might be wrong now
					}
				}
				
				realTimeOut->dayOfWeek = (u8)((realTimeOut->dayOfWeek + 1) % NUM_DAYS_PER_WEEK);
				
				realTimeOut->dayOfYear++;
				u16 numDaysInYear = (u16)(realTimeOut->isLeapYear ? NUM_DAYS_PER_YEAR+1 : NUM_DAYS_PER_YEAR);
				if (realTimeOut->dayOfYear >= numDaysInYear)
				{
					realTimeOut->dayOfYear -= (u16)numDaysInYear;
				}
			}
		}
	}
}

i64 SubtractTimes(const RealTime_t& left, const RealTime_t& right, TimeUnit_t returnUnit)
{
	Assert(returnUnit != TimeUnit_Milliseconds);
	
	i64 secDiff = (i32)left.timestamp - (i32)right.timestamp; //TODO: Is this signed cast before subtraction actually safe?
	
	switch (returnUnit)
	{
		case TimeUnit_Milliseconds:
		{
			Assert(false);
			return 0;
		} break;
		
		case TimeUnit_Seconds:
		{
			return secDiff;
		} break;
		
		case TimeUnit_Minutes:
		{
			return secDiff / NUM_SEC_PER_MINUTE;
		} break;
		
		case TimeUnit_Hours:
		{
			return secDiff / NUM_SEC_PER_HOUR;
		} break;
		
		case TimeUnit_Days:
		{
			return secDiff / NUM_SEC_PER_DAY;
		} break;
		
		case TimeUnit_Months:
		{
			Assert(false);
			return 0;
		} break;
		
		case TimeUnit_Years:
		{
			Assert(false);
			return 0;
		} break;
		
		default:
		{
			Assert(false);
			return 0;
		} break;
	};
}

#endif //  _MY_TIME_H
