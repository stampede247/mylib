/*
File:   my_vectors.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the Vec4_t, Vec3_t and Vec2_t structures (alias v4, v3, and v2)
	** and implements all the base functions that operate on or create them

This file is #included by default from mylib.h
*/

#ifndef _MY_VECTORS_H
#define _MY_VECTORS_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
union Vec2_t
{
	r32 values[2];
	struct { r32 x,     y;      };
	struct { r32 v1,    v2;     };
	struct { r32 w,     h;      };
	struct { r32 width, height; };
	
	inline Vec2_t& operator += (const Vec2_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		return *this;
	}
	inline Vec2_t& operator -= (const Vec2_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		return *this;
	}
	inline r32 operator [](u32 index)
	{
		return this->values[index];
	}
};

union Vec2i_t
{
	i32 values[2];
	struct { i32 x,     y;      };
	struct { i32 v1,    v2;     };
	struct { i32 w,     h;      };
	struct { i32 width, height; };
	
	inline Vec2i_t& operator += (const Vec2i_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		return *this;
	}
	inline Vec2i_t& operator -= (const Vec2i_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		return *this;
	}
	inline i32 operator [](u32 index)
	{
		return this->values[index];
	}
};

union Vec3_t
{
	r32 values[3];
	struct { r32 x,     y,      z;     };
	struct { r32 v1,    v2,     v3;    };
	struct { r32 r,     g,      b;     };
	struct { r32 red,   green,  blue;  };
	struct { r32 w,     h,      d;     };
	struct { r32 width, height, depth; };
	
	inline Vec3_t& operator += (const Vec3_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		this->z += rightSide.z;
		return *this;
	}
	inline Vec3_t& operator -= (const Vec3_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		this->z -= rightSide.z;
		return *this;
	}
	inline r32 operator [](u32 index)
	{
		return this->values[index];
	}
};

union Vec3i_t
{
	i32 values[3];
	struct { i32 x,     y,      z;     };
	struct { i32 v1,    v2,     v3;    };
	struct { i32 r,     g,      b;     };
	struct { i32 red,   green,  blue;  };
	struct { i32 w,     h,      d;     };
	struct { i32 width, height, depth; };
	
	inline Vec3i_t& operator += (const Vec3i_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		this->z += rightSide.z;
		return *this;
	}
	inline Vec3i_t& operator -= (const Vec3i_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		this->z -= rightSide.z;
		return *this;
	}
	inline i32 operator [](u32 index)
	{
		return this->values[index];
	}
};

union Vec4_t
{
	r32 values[4];
	struct { r32 x,     y,      z,     w;     };
	struct { r32 v1,    v2,     v3,    v4;    };
	struct { r32 r,     g,      b,     a;     };
	struct { r32 red,   green,  blue,  alpha; };
	// struct { r32 w,     h,      d,     t;     };
	struct { r32 width, height, depth, time;  };
	
	inline Vec4_t& operator += (const Vec4_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		this->z += rightSide.z;
		this->w += rightSide.w;
		return *this;
	}
	inline Vec4_t& operator -= (const Vec4_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		this->z -= rightSide.z;
		this->w -= rightSide.w;
		return *this;
	}
	inline r32 operator [](u32 index)
	{
		return this->values[index];
	}
};

union Vec4i_t
{
	i32 values[4];
	struct { i32 x,     y,      z,     w;     };
	struct { i32 v1,    v2,     v3,    v4;    };
	struct { i32 r,     g,      b,     a;     };
	struct { i32 red,   green,  blue,  alpha; };
	// struct { i32 w,     h,      d,     t;     };
	struct { i32 width, height, depth, time;  };
	
	inline Vec4i_t& operator += (const Vec4i_t& rightSide)
	{
		this->x += rightSide.x;
		this->y += rightSide.y;
		this->z += rightSide.z;
		this->w += rightSide.w;
		return *this;
	}
	inline Vec4i_t& operator -= (const Vec4i_t& rightSide)
	{
		this->x -= rightSide.x;
		this->y -= rightSide.y;
		this->z -= rightSide.z;
		this->w -= rightSide.w;
		return *this;
	}
	inline i32 operator [](u32 index)
	{
		return this->values[index];
	}
};

// +--------------------------------------------------------------+
// |                        Alias Typedefs                        |
// +--------------------------------------------------------------+
typedef Vec2_t      v2;
typedef Vec2i_t     v2i;
typedef Vec3_t      v3;
typedef Vec3i_t     v3i;
typedef Vec4_t      v4;
typedef Vec4i_t     v4i;

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline v2 NewVec2(r32 x, r32 y)
{
	v2 result = { x, y };
	return result;
}
inline v2 NewVec2(v2i vec)
{
	v2 result = { (r32)vec.x, (r32)vec.y };
	return result;
}
inline v2 NewVec2(r32 all)
{
	v2 result = { all, all };
	return result;
}

inline v2i NewVec2i(i32 x, i32 y)
{
	v2i result = { x, y };
	return result;
}
inline v2i NewVec2i(v2 vec)
{
	v2i result = { (i32)vec.x, (i32)vec.y };
	return result;
}
inline v2i NewVec2i(i32 all)
{
	v2i result = { all, all };
	return result;
}

inline v3 NewVec3(r32 x, r32 y, r32 z)
{
	v3 result = { x, y, z };
	return result;
}
inline v3 NewVec3(v3i vec)
{
	v3 result = { (r32)vec.x, (r32)vec.y, (r32)vec.z };
	return result;
}
inline v3 NewVec3(r32 all)
{
	v3 result = { all, all, all };
	return result;
}
inline v3 NewVec3(v2 xy, r32 z)
{
	v3 result = { xy.x, xy.y, z };
	return result;
}

inline v3i NewVec3i(i32 x, i32 y, i32 z)
{
	v3i result = { x, y, z };
	return result;
}
inline v3i NewVec3i(v3 vec)
{
	v3i result = { (i32)vec.x, (i32)vec.y, (i32)vec.z };
	return result;
}
inline v3i NewVec3i(i32 all)
{
	v3i result = { all, all, all };
	return result;
}
inline v3i NewVec3i(v2i xy, i32 z)
{
	v3i result = { xy.x, xy.y, z };
	return result;
}


inline v4 NewVec4(r32 x, r32 y, r32 z, r32 w)
{
	v4 result = { x, y, z , w };
	return result;
}
inline v4 NewVec4(v4i vec)
{
	v4 result = { (r32)vec.x, (r32)vec.y, (r32)vec.z, (r32)vec.w };
	return result;
}
inline v4 NewVec4(r32 all)
{
	v4 result = { all, all, all, all };
	return result;
}
inline v4 NewVec4(v3 xyz, r32 w)
{
	v4 result = { xyz.x, xyz.y, xyz.z, w };
	return result;
}

inline v4i NewVec4i(i32 x, i32 y, i32 z, i32 w)
{
	v4i result = { x, y, z, w };
	return result;
}
inline v4i NewVec4i(v4 vec)
{
	v4i result = { (i32)vec.x, (i32)vec.y, (i32)vec.z, (i32)vec.w };
	return result;
}
inline v4i NewVec4i(i32 all)
{
	v4i result = { all, all, all, all };
	return result;
}
inline v4i NewVec4i(v3i xyz, i32 w)
{
	v4i result = { xyz.x, xyz.y, xyz.z, w };
	return result;
}

// +--------------------------------------------------------------+
// |                     Simple Value Defines                     |
// +--------------------------------------------------------------+
#define Vec2_Zero      NewVec2( 0.0f,  0.0f)
#define Vec2_One       NewVec2( 1.0f,  1.0f)
#define Vec2_Half      NewVec2( 0.5f,  0.5f)
#define Vec2_Left      NewVec2(-1.0f,  0.0f)
#define Vec2_Right     NewVec2( 1.0f,  0.0f)
#define Vec2_Up        NewVec2( 0.0f, -1.0f)
#define Vec2_Down      NewVec2( 0.0f,  1.0f)

#define Vec2i_Zero     NewVec2i( 0,  0)
#define Vec2i_One      NewVec2i( 1,  1)
#define Vec2i_Left     NewVec2i(-1,  0)
#define Vec2i_Right    NewVec2i( 1,  0)
#define Vec2i_Up       NewVec2i( 0, -1)
#define Vec2i_Down     NewVec2i( 0,  1)

#define Vec3_Zero      NewVec3( 0.0f,  0.0f,  0.0f)
#define Vec3_One       NewVec3( 1.0f,  1.0f,  1.0f)
#define Vec3_Half      NewVec3( 0.5f,  0.5f,  0.5f)
#define Vec3_Left      NewVec3(-1.0f,  0.0f,  0.0f)
#define Vec3_Right     NewVec3( 1.0f,  0.0f,  0.0f)
#define Vec3_Bottom    NewVec3( 0.0f, -1.0f,  0.0f)
#define Vec3_Top       NewVec3( 0.0f,  1.0f,  0.0f)
#define Vec3_Back      NewVec3( 0.0f,  0.0f, -1.0f)
#define Vec3_Front     NewVec3( 0.0f,  0.0f,  1.0f)
#define Vec3_Down      NewVec3( 0.0f, -1.0f,  0.0f)
#define Vec3_Up        NewVec3( 0.0f,  1.0f,  0.0f)
#define Vec3_Backward  NewVec3( 0.0f,  0.0f, -1.0f)
#define Vec3_Forward   NewVec3( 0.0f,  0.0f,  1.0f)

#define Vec3i_Zero     NewVec3i( 0,  0,  0)
#define Vec3i_One      NewVec3i( 1,  1,  1)
#define Vec3i_Left     NewVec3i(-1,  0,  0)
#define Vec3i_Right    NewVec3i( 1,  0,  0)
#define Vec3i_Bottom   NewVec3i( 0, -1,  0)
#define Vec3i_Top      NewVec3i( 0,  1,  0)
#define Vec3i_Back     NewVec3i( 0,  0, -1)
#define Vec3i_Front    NewVec3i( 0,  0,  1)
#define Vec3i_Down     NewVec3i( 0, -1,  0)
#define Vec3i_Up       NewVec3i( 0,  1,  0)
#define Vec3i_Backward NewVec3i( 0,  0, -1)
#define Vec3i_Forward  NewVec3i( 0,  0,  1)

#define Vec4_Zero      NewVec4( 0.0f,  0.0f,  0.0f, 0.0f)
#define Vec4_One       NewVec4( 1.0f,  1.0f,  1.0f, 1.0f)
#define Vec4_Half      NewVec4( 0.5f,  0.5f,  0.5f, 0.5f)
#define Vec4_Left      NewVec4(-1.0f,  0.0f,  0.0f, 0.0f)
#define Vec4_Right     NewVec4( 1.0f,  0.0f,  0.0f, 0.0f)
#define Vec4_Bottom    NewVec4( 0.0f, -1.0f,  0.0f, 0.0f)
#define Vec4_Top       NewVec4( 0.0f,  1.0f,  0.0f, 0.0f)
#define Vec4_Back      NewVec4( 0.0f,  0.0f, -1.0f, 0.0f)
#define Vec4_Front     NewVec4( 0.0f,  0.0f,  1.0f, 0.0f)
#define Vec4_Down      NewVec4( 0.0f, -1.0f,  0.0f, 0.0f)
#define Vec4_Up        NewVec4( 0.0f,  1.0f,  0.0f, 0.0f)
#define Vec4_Backward  NewVec4( 0.0f,  0.0f, -1.0f, 0.0f)
#define Vec4_Forward   NewVec4( 0.0f,  0.0f,  1.0f, 0.0f)

#define Vec4i_Zero     NewVec4( 0,  0,  0, 0)
#define Vec4i_One      NewVec4( 1,  1,  1, 0)
#define Vec4i_Left     NewVec4(-1,  0,  0, 0)
#define Vec4i_Right    NewVec4( 1,  0,  0, 0)
#define Vec4i_Bottom   NewVec4( 0, -1,  0, 0)
#define Vec4i_Top      NewVec4( 0,  1,  0, 0)
#define Vec4i_Back     NewVec4( 0,  0, -1, 0)
#define Vec4i_Front    NewVec4( 0,  0,  1, 0)
#define Vec4i_Down     NewVec4( 0, -1,  0, 0)
#define Vec4i_Up       NewVec4( 0,  1,  0, 0)
#define Vec4i_Backward NewVec4( 0,  0, -1, 0)
#define Vec4i_Forward  NewVec4( 0,  0,  1, 0)

// +--------------------------------------------------------------+
// |                Basic Math Operation Functions                |
// +--------------------------------------------------------------+
inline v2 Vec2Add(const v2& left, const v2& right)
{
	v2 result = { left.x + right.x, left.y + right.y };
	return result;
}
inline v2 Vec2Sub(const v2& left, const v2& right)
{
	v2 result = { left.x - right.x, left.y - right.y };
	return result;
}
inline r32 Vec2Dot(const v2& left, const v2& right)
{
	return left.x*right.x + left.y*right.y;
}
inline v2 Vec2Scale(const v2& vector, r32 scalar)
{
	v2 result = { vector.x*scalar, vector.y*scalar };
	return result;
}
inline v2 Vec2Shrink(const v2& vector, r32 divisor)
{
	v2 result = { vector.x/divisor, vector.y/divisor };
	return result;
}
inline v2 Vec2Multiply(const v2& left, const v2& right)
{
	v2 result = { left.x * right.x, left.y * right.y };
	return result;
}
inline v2 Vec2Divide(const v2& left, const v2& right)
{
	v2 result = { left.x / right.x, left.y / right.y };
	return result;
}
inline r32 Vec2Length(const v2& vector)
{
	return SqrtR32(vector.x*vector.x + vector.y*vector.y);
}
inline v2 Vec2Normalize(const v2& vector)
{
	r32 length = Vec2Length(vector);
	if (length == 0) { return Vec2_Zero; }
	return Vec2Scale(vector, 1.0f/length);
}
inline v2 Vec2PerpRight(const v2& vector)
{
	v2 result = { -vector.y, vector.x };
	return result;
}
inline v2 Vec2PerpLeft(const v2& vector)
{
	v2 result = { vector.y, -vector.x };
	return result;
}
inline r32 Vec2Determinant(const v2& left, const v2& right)
{
	return left.x*right.y + left.y*right.x;
}
inline r32 Vec2Inner(const v2& left, const v2& right)
{
	r32 dot = Vec2Dot(left, right);
	r32 det = Vec2Determinant(left, right);
	r32 result = AtanR32(det, dot);
	return result;
}
inline v2 Vec2Round(const v2& vector)
{
	v2 result = { RoundR32(vector.x), RoundR32(vector.y) };
	return result;
}
inline v2i Vec2Roundi(const v2& vector)
{
	v2i result = { RoundR32i(vector.x), RoundR32i(vector.y) };
	return result;
}
inline v2 Vec2Floor(const v2& vector)
{
	v2 result = { FloorR32(vector.x), FloorR32(vector.y) };
	return result;
}
inline v2i Vec2Floori(const v2& vector)
{
	v2i result = { FloorR32i(vector.x), FloorR32i(vector.y) };
	return result;
}
inline v2 Vec2Ceil(const v2& vector)
{
	v2 result = { CeilR32(vector.x), CeilR32(vector.y) };
	return result;
}
inline v2i Vec2Ceili(const v2& vector)
{
	v2i result = { CeilR32i(vector.x), CeilR32i(vector.y) };
	return result;
}
inline v2 Vec2Min(const v2& vector1, const v2& vector2)
{
	v2 result = { MinR32(vector1.x, vector2.x), MinR32(vector1.y, vector2.y) };
	return result;
}
inline v2 Vec2Max(const v2& vector1, const v2& vector2)
{
	v2 result = { MaxR32(vector1.x, vector2.x), MaxR32(vector1.y, vector2.y) };
	return result;
}
inline v2 Vec2Clamp(const v2& value, const v2& min, const v2& max)
{
	v2 result = { ClampR32(value.x, min.x, max.x), ClampR32(value.y, min.y, max.y) };
	return result;
}
inline v2 Vec2Lerp(const v2& start, const v2& end, r32 amount)
{
	v2 result = { start.x + (end.x-start.x)*amount, start.y + (end.y-start.y)*amount };
	return result;
}
inline v2 Vec2Rotate(v2 vector, r32 angle)
{
	v2 result;
	result.x = CosR32(angle) * vector.x - SinR32(angle) * vector.y;
	result.y = SinR32(angle) * vector.x + CosR32(angle) * vector.y;
	return result;
}
inline v2 Vec2FromAngle(r32 angle, r32 radius = 1.0f)
{
	return NewVec2(CosR32(angle) * radius, SinR32(angle) * radius);
}
inline bool BasicallyEqualVec2(v2 left, v2 right, r32 epsilon = 0.001f)
{
	return (BasicallyEqualR32(left.x, right.x, epsilon) && BasicallyEqualR32(left.y, right.y, epsilon));
}


inline v2i Vec2iAdd(const v2i& left, const v2i& right)
{
	v2i result = { left.x + right.x, left.y + right.y };
	return result;
}
inline v2i Vec2iSub(const v2i& left, const v2i& right)
{
	v2i result = { left.x - right.x, left.y - right.y };
	return result;
}
inline i32 Vec2iDot(const v2i& left, const v2i& right)
{
	return left.x*right.x + left.y*right.y;
}
inline v2i Vec2iScale(const v2i& vector, i32 scalar)
{
	v2i result = { vector.x*scalar, vector.y*scalar };
	return result;
}
inline v2i Vec2iShrink(const v2i& vector, i32 divisor)
{
	v2i result = { vector.x/divisor, vector.y/divisor };
	return result;
}
inline v2i Vec2iMultiply(const v2i& left, const v2i& right)
{
	v2i result = { left.x * right.x, left.y * right.y };
	return result;
}
inline v2i Vec2iDivide(const v2i& left, const v2i& right)
{
	v2i result = { left.x / right.x, left.y / right.y };
	return result;
}
inline r32 Vec2iLength(const v2i& vector)
{
	return SqrtR32((r32)(vector.x*vector.x + vector.y*vector.y));
}
inline v2i Vec2iMin(const v2i& vector1, const v2i& vector2)
{
	v2i result = { MinI32(vector1.x, vector2.x), MinI32(vector1.y, vector2.y) };
	return result;
}
inline v2i Vec2iMax(const v2i& vector1, const v2i& vector2)
{
	v2i result = { MaxI32(vector1.x, vector2.x), MaxI32(vector1.y, vector2.y) };
	return result;
}
inline v2i Vec2iClamp(const v2i& value, const v2i& min, const v2i& max)
{
	v2i result = { ClampI32(value.x, min.x, max.x), ClampI32(value.y, min.y, max.y) };
	return result;
}

inline v3 Vec3Add(const v3& left, const v3& right)
{
	v3 result = { left.x + right.x, left.y + right.y, left.z + right.z };
	return result;
}
inline v3 Vec3Sub(const v3& left, const v3& right)
{
	v3 result = { left.x - right.x, left.y - right.y, left.z - right.z };
	return result;
}
inline r32 Vec3Dot(const v3& left, const v3& right)
{
	return left.x*right.x + left.y*right.y + left.z*right.z;
}
inline v3 Vec3Scale(const v3& vector, r32 scalar)
{
	v3 result = { vector.x*scalar, vector.y*scalar, vector.z*scalar };
	return result;
}
inline v3 Vec3Shrink(const v3& vector, r32 divisor)
{
	v3 result = { vector.x/divisor, vector.y/divisor, vector.z/divisor };
	return result;
}
inline v3 Vec3Multiply(const v3& left, const v3& right)
{
	v3 result = { left.x * right.x, left.y * right.y, left.z * right.z };
	return result;
}
inline v3 Vec3Divide(const v3& left, const v3& right)
{
	v3 result = { left.x / right.x, left.y / right.y, left.z / right.z };
	return result;
}
inline r32 Vec3Length(const v3& vector)
{
	return SqrtR32(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z);
}
inline v3 Vec3Normalize(const v3& vector)
{
	r32 length = Vec3Length(vector);
	if (length == 0) { return Vec3_Zero; }
	return Vec3Scale(vector, 1.0f/length);
}
inline v2i Vec2iPerpRight(const v2i& vector)
{
	v2i result = { -vector.y, vector.x };
	return result;
}
inline v2i Vec2iPerpLeft(const v2i& vector)
{
	v2i result = { vector.y, -vector.x };
	return result;
}
inline v3 Vec3Cross(const v3& left, const v3& right)
{
	v3 result = { left.y*right.z - left.z*right.y, left.z*right.x - left.x*right.z, left.x*right.y - left.y*right.x };
	return result;
}
inline v3 Vec3Min(const v3& vector1, const v3& vector2)
{
	v3 result = { MinR32(vector1.x, vector2.x), MinR32(vector1.y, vector2.y), MinR32(vector1.z, vector2.z) };
	return result;
}
inline v3 Vec3Max(const v3& vector1, const v3& vector2)
{
	v3 result = { MaxR32(vector1.x, vector2.x), MaxR32(vector1.y, vector2.y), MaxR32(vector1.z, vector2.z) };
	return result;
}
inline v3 Vec3Round(const v3& vector)
{
	v3 result = { RoundR32(vector.x), RoundR32(vector.y), RoundR32(vector.z) };
	return result;
}
inline v3i Vec3Roundi(const v3& vector)
{
	v3i result = { RoundR32i(vector.x), RoundR32i(vector.y), RoundR32i(vector.z) };
	return result;
}
inline v3 Vec3Floor(const v3& vector)
{
	v3 result = { FloorR32(vector.x), FloorR32(vector.y), FloorR32(vector.z) };
	return result;
}
inline v3i Vec3Floori(const v3& vector)
{
	v3i result = { FloorR32i(vector.x), FloorR32i(vector.y), FloorR32i(vector.z) };
	return result;
}
inline v3 Vec3Ceil(const v3& vector)
{
	v3 result = { CeilR32(vector.x), CeilR32(vector.y), CeilR32(vector.z) };
	return result;
}
inline v3i Vec3Ceili(const v3& vector)
{
	v3i result = { CeilR32i(vector.x), CeilR32i(vector.y), CeilR32i(vector.z) };
	return result;
}
inline v3 Vec3Clamp(const v3& value, const v3& min, const v3& max)
{
	v3 result = { ClampR32(value.x, min.x, max.x), ClampR32(value.y, min.y, max.y), ClampR32(value.z, min.z, max.z) };
	return result;
}
inline v3 Vec3Lerp(const v3& start, const v3& end, r32 amount)
{
	v3 result = { start.x + (end.x-start.x)*amount, start.y + (end.y-start.y)*amount, start.z + (end.z-start.z)*amount };
	return result;
}
inline bool BasicallyEqualVec3(v3 left, v3 right, r32 epsilon = 0.001f)
{
	return (BasicallyEqualR32(left.x, right.x, epsilon) && BasicallyEqualR32(left.y, right.y, epsilon) && BasicallyEqualR32(left.z, right.z, epsilon));
}

inline v3i Vec3iAdd(const v3i& left, const v3i& right)
{
	v3i result = { left.x + right.x, left.y + right.y, left.z + right.z };
	return result;
}
inline v3i Vec3iSub(const v3i& left, const v3i& right)
{
	v3i result = { left.x - right.x, left.y - right.y, left.z - right.z };
	return result;
}
inline i32 Vec3iDot(const v3i& left, const v3i& right)
{
	return left.x*right.x + left.y*right.y + left.z*right.z;
}
inline v3i Vec3iScale(const v3i& vector, i32 scalar)
{
	v3i result = { vector.x*scalar, vector.y*scalar, vector.z*scalar };
	return result;
}
inline v3i Vec3iShrink(const v3i& vector, i32 divisor)
{
	v3i result = { vector.x/divisor, vector.y/divisor, vector.z/divisor };
	return result;
}
inline v3i Vec3iMultiply(const v3i& left, const v3i& right)
{
	v3i result = { left.x * right.x, left.y * right.y, left.z * right.z };
	return result;
}
inline v3i Vec3iDivide(const v3i& left, const v3i& right)
{
	v3i result = { left.x / right.x, left.y / right.y, left.z / right.z };
	return result;
}
inline r32 Vec3iLength(const v3i& vector)
{
	return SqrtR32((r32)(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z));
}
inline v3i Vec3iCross(const v3i& left, const v3i& right)
{
	v3i result = { left.y*right.z - left.z*right.y, left.z*right.x - left.x*right.z, left.x*right.y - left.y*right.x };
	return result;
}
inline v3i Vec3iMin(const v3i& vector1, const v3i& vector2)
{
	v3i result = { MinI32(vector1.x, vector2.x), MinI32(vector1.y, vector2.y), MinI32(vector1.z, vector2.z) };
	return result;
}
inline v3i Vec3iMax(const v3i& vector1, const v3i& vector2)
{
	v3i result = { MaxI32(vector1.x, vector2.x), MaxI32(vector1.y, vector2.y), MaxI32(vector1.z, vector2.z) };
	return result;
}
inline v3i Vec3iClamp(const v3i& value, const v3i& min, const v3i& max)
{
	v3i result = { ClampI32(value.x, min.x, max.x), ClampI32(value.y, min.y, max.y), ClampI32(value.z, min.z, max.z) };
	return result;
}

inline v4 Vec4Add(const v4& left, const v4& right)
{
	return NewVec4(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
}
inline v4 Vec4Sub(const v4& left, const v4& right)
{
	return NewVec4(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
}
inline r32 Vec4Dot(const v4& left, const v4& right)
{
	return left.x*right.x + left.y*right.y + left.z*right.z + left.w*right.w;
}
inline v4 Vec4Scale(const v4& vector, r32 scalar)
{
	return NewVec4(vector.x*scalar, vector.y*scalar, vector.z*scalar, vector.w*scalar);
}
inline v4 Vec4Shrink(const v4& vector, r32 divisor)
{
	return NewVec4(vector.x/divisor, vector.y/divisor, vector.z/divisor, vector.w/divisor);
}
inline v4 Vec4Multiply(const v4& left, const v4& right)
{
	return NewVec4(left.x*right.x, left.y*right.y, left.z*right.z, left.w*right.w);
}
inline v4 Vec4Divide(const v4& left, const v4& right)
{
	return NewVec4(left.x/right.x, left.y/right.y, left.z/right.z, left.w/right.w);
}
inline r32 Vec4Length(const v4& vector)
{
	return SqrtR32(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z + vector.w*vector.w);
}
inline v4 Vec4Normalize(const v4& vector)
{
	r32 length = Vec4Length(vector);
	if (length == 0) { return Vec4_Zero; }
	return Vec4Scale(vector, 1.0f/length);
}
inline v4 Vec4Min(const v4& vector1, const v4& vector2)
{
	return NewVec4(MinR32(vector1.x, vector2.x), MinR32(vector1.y, vector2.y), MinR32(vector1.z, vector2.z), MinR32(vector1.w, vector2.w));
}
inline v4 Vec4Max(const v4& vector1, const v4& vector2)
{
	return NewVec4(MaxR32(vector1.x, vector2.x), MaxR32(vector1.y, vector2.y), MaxR32(vector1.z, vector2.z), MaxR32(vector1.w, vector2.w));
}
inline v4 Vec4Round(const v4& vector)
{
	return NewVec4(RoundR32(vector.x), RoundR32(vector.y), RoundR32(vector.z), RoundR32(vector.w));
}
inline v4i Vec4Roundi(const v4& vector)
{
	return NewVec4i(RoundR32i(vector.x), RoundR32i(vector.y), RoundR32i(vector.z), RoundR32i(vector.w));
}
inline v4 Vec4Floor(const v4& vector)
{
	v4 result = { FloorR32(vector.x), FloorR32(vector.y), FloorR32(vector.z), FloorR32(vector.w) };
	return result;
}
inline v4i Vec4Floori(const v4& vector)
{
	v4i result = { FloorR32i(vector.x), FloorR32i(vector.y), FloorR32i(vector.z), FloorR32i(vector.w) };
	return result;
}
inline v4 Vec4Ceil(const v4& vector)
{
	v4 result = { CeilR32(vector.x), CeilR32(vector.y), CeilR32(vector.z), CeilR32(vector.w) };
	return result;
}
inline v4i Vec4Ceili(const v4& vector)
{
	v4i result = { CeilR32i(vector.x), CeilR32i(vector.y), CeilR32i(vector.z), CeilR32i(vector.w) };
	return result;
}
inline v4 Vec4Clamp(const v4& value, const v4& min, const v4& max)
{
	return NewVec4(ClampR32(value.x, min.x, max.x), ClampR32(value.y, min.y, max.y), ClampR32(value.z, min.z, max.z), ClampR32(value.w, min.w, max.w));
}
inline v4 Vec4Lerp(const v4& start, const v4& end, r32 amount)
{
	return NewVec4(start.x + (end.x-start.x)*amount, start.y + (end.y-start.y)*amount, start.z + (end.z-start.z)*amount, start.w + (end.w-start.w)*amount);
}
inline bool BasicallyEqualVec4(v4 left, v4 right, r32 epsilon = 0.001f)
{
	return (BasicallyEqualR32(left.x, right.x, epsilon) && BasicallyEqualR32(left.y, right.y, epsilon) && BasicallyEqualR32(left.z, right.z, epsilon) && BasicallyEqualR32(left.w, right.w, epsilon));
}

inline v4i Vec4iAdd(const v4i& left, const v4i& right)
{
	return NewVec4i(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
}
inline v4i Vec4iSub(const v4i& left, const v4i& right)
{
	return NewVec4i(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
}
inline i32 Vec4iDot(const v4i& left, const v4i& right)
{
	return left.x*right.x + left.y*right.y + left.z*right.z + left.w*right.w;
}
inline v4i Vec4iScale(const v4i& vector, i32 scalar)
{
	return NewVec4i(vector.x*scalar, vector.y*scalar, vector.z*scalar, vector.w*scalar);
}
inline v4i Vec4iShrink(const v4i& vector, i32 divisor)
{
	return NewVec4i(vector.x/divisor, vector.y/divisor, vector.z/divisor, vector.w/divisor);
}
inline v4i Vec4iMultiply(const v4i& left, const v4i& right)
{
	return NewVec4i(left.x*right.x, left.y*right.y, left.z*right.z, left.w*right.w);
}
inline v4i Vec4iDivide(const v4i& left, const v4i& right)
{
	return NewVec4i(left.x/right.x, left.y/right.y, left.z/right.z, left.w/right.w);
}
inline r32 Vec4iLength(const v4i& vector)
{
	return SqrtR32((r32)(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z + vector.w*vector.w));
}
inline v4i Vec4iMin(const v4i& vector1, const v4i& vector2)
{
	return NewVec4i(MinI32(vector1.x, vector2.x), MinI32(vector1.y, vector2.y), MinI32(vector1.z, vector2.z), MinI32(vector1.w, vector2.w));
}
inline v4i Vec4iMax(const v4i& vector1, const v4i& vector2)
{
	return NewVec4i(MaxI32(vector1.x, vector2.x), MaxI32(vector1.y, vector2.y), MaxI32(vector1.z, vector2.z), MaxI32(vector1.w, vector2.w));
}
inline v4i Vec4iClamp(const v4i& value, const v4i& min, const v4i& max)
{
	return NewVec4i(ClampI32(value.x, min.x, max.x), ClampI32(value.y, min.y, max.y), ClampI32(value.z, min.z, max.z), ClampI32(value.w, min.w, max.w));
}

// +--------------------------------------------------------------+
// |                      Operator Overloads                      |
// +--------------------------------------------------------------+
inline v2 operator - (v2 vector)               { return NewVec2(-vector.x, -vector.y); }
inline v2 operator + (v2 left, v2 right)       { return Vec2Add(left, right); }
inline v2 operator - (v2 left, v2 right)       { return Vec2Sub(left, right); }
inline v2 operator * (v2 vector, r32 scalar)   { return Vec2Scale(vector, scalar); }
inline v2 operator * (r32 scalar, v2 vector)   { return Vec2Scale(vector, scalar); }
inline v2 operator / (v2 vector, r32 scalar)   { return Vec2Shrink(vector, scalar); }
inline bool operator == (v2 left, v2 right)    { return (left.x == right.x && left.y == right.y); }
inline bool operator != (v2 left, v2 right)    { return (left.x != right.x || left.y != right.y); }

inline v2i operator - (v2i vector)             { return NewVec2i(-vector.x, -vector.y); }
inline v2i operator + (v2i left, v2i right)    { return Vec2iAdd(left, right); }
inline v2i operator - (v2i left, v2i right)    { return Vec2iSub(left, right); }
inline v2i operator * (v2i vector, i32 scalar) { return Vec2iScale(vector, scalar); }
inline v2i operator * (i32 scalar, v2i vector) { return Vec2iScale(vector, scalar); }
inline v2i operator / (v2i vector, i32 scalar) { return Vec2iShrink(vector, scalar); }
inline bool operator == (v2i left, v2i right)  { return (left.x == right.x && left.y == right.y); }
inline bool operator != (v2i left, v2i right)  { return (left.x != right.x || left.y != right.y); }

inline v3 operator - (v3 vector)               { return NewVec3(-vector.x, -vector.y, -vector.z); }
inline v3 operator + (v3 left, v3 right)       { return Vec3Add(left, right); }
inline v3 operator - (v3 left, v3 right)       { return Vec3Sub(left, right); }
inline v3 operator * (v3 vector, r32 scalar)   { return Vec3Scale(vector, scalar); }
inline v3 operator * (r32 scalar, v3 vector)   { return Vec3Scale(vector, scalar); }
inline v3 operator / (v3 vector, r32 scalar)   { return Vec3Shrink(vector, scalar); }
inline bool operator == (v3 left, v3 right)    { return (left.x == right.x && left.y == right.y && left.z == right.z); }
inline bool operator != (v3 left, v3 right)    { return (left.x != right.x || left.y != right.y || left.z != right.z); }

inline v3i operator - (v3i vector)             { return NewVec3i(-vector.x, -vector.y, -vector.z); }
inline v3i operator + (v3i left, v3i right)    { return Vec3iAdd(left, right); }
inline v3i operator - (v3i left, v3i right)    { return Vec3iSub(left, right); }
inline v3i operator * (v3i vector, i32 scalar) { return Vec3iScale(vector, scalar); }
inline v3i operator * (i32 scalar, v3i vector) { return Vec3iScale(vector, scalar); }
inline v3i operator / (v3i vector, i32 scalar) { return Vec3iShrink(vector, scalar); }
inline bool operator == (v3i left, v3i right)  { return (left.x == right.x && left.y == right.y && left.z == right.z); }
inline bool operator != (v3i left, v3i right)  { return (left.x != right.x || left.y != right.y || left.z != right.z); }

inline v4 operator - (v4 vector)               { return NewVec4(-vector.x, -vector.y, -vector.z, -vector.w); }
inline v4 operator + (v4 left, v4 right)       { return Vec4Add(left, right); }
inline v4 operator - (v4 left, v4 right)       { return Vec4Sub(left, right); }
inline v4 operator * (v4 vector, r32 scalar)   { return Vec4Scale(vector, scalar); }
inline v4 operator * (r32 scalar, v4 vector)   { return Vec4Scale(vector, scalar); }
inline v4 operator / (v4 vector, r32 scalar)   { return Vec4Shrink(vector, scalar); }
inline bool operator == (v4 left, v4 right)    { return (left.x == right.x && left.y == right.y && left.z == right.z && left.w == right.w); }
inline bool operator != (v4 left, v4 right)    { return (left.x != right.x || left.y != right.y || left.z != right.z || left.w != right.w); }

inline v4i operator - (v4i vector)             { return NewVec4i(-vector.x, -vector.y, -vector.z, -vector.w); }
inline v4i operator + (v4i left, v4i right)    { return Vec4iAdd(left, right); }
inline v4i operator - (v4i left, v4i right)    { return Vec4iSub(left, right); }
inline v4i operator * (v4i vector, i32 scalar) { return Vec4iScale(vector, scalar); }
inline v4i operator * (i32 scalar, v4i vector) { return Vec4iScale(vector, scalar); }
inline v4i operator / (v4i vector, i32 scalar) { return Vec4iShrink(vector, scalar); }
inline bool operator == (v4i left, v4i right)  { return (left.x == right.x && left.y == right.y && left.z == right.z && left.w == right.w); }
inline bool operator != (v4i left, v4i right)  { return (left.x != right.x || left.y != right.y || left.z != right.z || left.w != right.w); }

#endif //  _MY_VECTORS_H
