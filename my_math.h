/*
File:   my_math.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines all other various functions and structures that
	** don't warrant their own file yet. It also houses any math implementation
	** functions that I've implemented (like RayVsAABB)

This file is #included by default from mylib.h
*/

#ifndef _MY_MATH_H
#define _MY_MATH_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
struct Ray2_t
{
	union
	{
		v2 origin;
		struct { r32 x, y, z; };
	};
	union
	{
		v2 direction;
		struct { r32 dX, dY, dZ; };
	};
};
struct Ray3_t
{
	union
	{
		v3 origin;
		struct { r32 x, y, z; };
	};
	union
	{
		v3 direction;
		struct { r32 dX, dY, dZ; };
	};
};

struct Frustum_t
{
	union
	{
		v3 origin;
		struct { r32 x, y, z; };
	};
	union
	{
		v3 direction;
		struct { r32 dX, dY, dZ; };
	};
	union
	{
		v3 upVector;
		struct { r32 upX, upY, upZ; };
	};
	union
	{
		v2 fov;
		struct { r32 fovX, fovY; };
	};
	r32 zNear;
	r32 zFar;
};

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline Ray2_t NewRay2(v2 origin, v2 direction)
{
	Ray2_t result = {};
	result.origin    = origin;
	result.direction = direction;
	return result;
}
inline Ray3_t NewRay3(v3 origin, v3 direction)
{
	Ray3_t result = {};
	result.origin    = origin;
	result.direction = direction;
	return result;
}

inline Frustum_t NewFrustum(v3 origin, v3 direction, v3 upVector, v2 fov, r32 zNear, r32 zFar)
{
	Frustum_t result = {};
	result.origin    = origin;
	result.direction = Vec3Normalize(direction);
	result.upVector  = Vec3Normalize(upVector);
	result.fov       = fov;
	result.zNear     = zNear;
	result.zFar      = zFar;
	return result;
}

// +--------------------------------------------------------------+
// |                      Helpful Functions                       |
// +--------------------------------------------------------------+
bool IsInfinite(r32 value)
{
	return (isinf(value) || isnan(value));
}

bool MinNoInfinities(r32 value1, r32 value2, r32 value3, r32* minValue, u8* whichIsMax = nullptr)
{
	if (IsInfinite(value1) && IsInfinite(value2) && IsInfinite(value3))
	{
		return false;
	}
	
	if (!IsInfinite(value1) && (IsInfinite(value2) || value1 <= value2) && (IsInfinite(value3) || value1 <= value3))
	{
		*minValue = value1;
		if (whichIsMax != nullptr) *whichIsMax = 0;
		return true;
	}
	else if (!IsInfinite(value2) && (IsInfinite(value1) || value2 <= value1) && (IsInfinite(value3) || value2 <= value3))
	{
		*minValue = value2;
		if (whichIsMax != nullptr) *whichIsMax = 1;
		return true;
	}
	else if (!IsInfinite(value3) && (IsInfinite(value1) || value3 <= value1) && (IsInfinite(value2) || value3 <= value2))
	{
		*minValue = value3;
		if (whichIsMax != nullptr) *whichIsMax = 2;
		return true;
	}
	else
	{
		Assert(false);
		return false;
	}
}

bool MaxNoInfinities(r32 value1, r32 value2, r32 value3, r32* maxValue, u8* whichIsMax = nullptr)
{
	if (IsInfinite(value1) && IsInfinite(value2) && IsInfinite(value3))
	{
		return false;
	}
	
	if (!IsInfinite(value1) && (IsInfinite(value2) || value1 >= value2) && (IsInfinite(value3) || value1 >= value3))
	{
		*maxValue = value1;
		if (whichIsMax != nullptr) *whichIsMax = 0;
		return true;
	}
	else if (!IsInfinite(value2) && (IsInfinite(value1) || value2 >= value1) && (IsInfinite(value3) || value2 >= value3))
	{
		*maxValue = value2;
		if (whichIsMax != nullptr) *whichIsMax = 1;
		return true;
	}
	else if (!IsInfinite(value3) && (IsInfinite(value1) || value3 >= value1) && (IsInfinite(value2) || value3 >= value2))
	{
		*maxValue = value3;
		if (whichIsMax != nullptr) *whichIsMax = 2;
		return true;
	}
	else
	{
		Assert(false);
		return false;
	}
}

u8 NumDigitsDEC(u32 number)
{
	if (number <         10) return 1;
	if (number <        100) return 2;
	if (number <       1000) return 3;
	if (number <      10000) return 4;
	if (number <     100000) return 5;
	if (number <    1000000) return 6;
	if (number <   10000000) return 7;
	if (number <  100000000) return 8;
	if (number < 1000000000) return 9;
	return 10;
}

void FindRatio(u32 num1, u32 num2, u32* out1, u32* out2)
{
	Assert(out1 != nullptr && out2 != nullptr);
	
	u32 result1 = num1;
	u32 result2 = num2;
	while (result1 > 1 && result2 > 1)
	{
		//TODO: We could do this with just prime numbers if we had a way to iterate through primes
		bool foundDivisor = false;
		for (u32 divisor = 2; divisor < result1 && divisor < result2; divisor++)
		{
			if ((result1 % divisor) == 0 && (result2 % divisor) == 0)
			{
				result1 /= divisor;
				result2 /= divisor;
				foundDivisor = true;
				break;
			}
		}
		
		if (!foundDivisor) { break; }
	}
	
	*out1 = result1;
	*out2 = result2;
}

v2 LineClosestPoint(v2 l1, v2 l2, v2 target)
{
	r32 lineLength = Vec2Length(l2 - l1);
	v2 lineNorm = (l2 - l1) / lineLength;
	r32 dot = Vec2Dot(lineNorm, target - l1);
	if (dot <= 0) { return l1; }
	if (dot >= lineLength) { return l2; }
	return l1 + (lineNorm * dot);
}

r32 AngleNormalize(r32 angle)
{
	if (IsInfinite(angle)) { return angle; }
	r32 result = angle;
	while (result < 0) { result += 2*Pi32; }
	while (result >= 2*Pi32) { result -= 2*Pi32; }
	return result;
}

r32 AngleFlipVertical(r32 angle, bool normalize = true)
{
	r32 result = Pi32*2 - angle;
	if (normalize) { result = AngleNormalize(result); }
	return result;
}
r32 AngleFlipHorizontal(r32 angle, bool normalize = true)
{
	r32 result = Pi32 - angle;
	if (normalize) { result = AngleNormalize(result); }
	return result;
}

r32 AngleLerp(r32 angleFrom, r32 angleTo, r32 amount)
{
	r32 from = AngleNormalize(angleFrom);
	r32 to = AngleNormalize(angleTo);
	if (to - from > Pi32) { to -= Pi32*2; }
	if (to - from < -Pi32) { to += Pi32*2; }
	return from + (to - from) * amount;
}

// +--------------------------------------------------------------+
// |                  Complicted Math Functions                   |
// +--------------------------------------------------------------+
// +==============================+
// |          LineVsLine          |
// +==============================+
//Returns 0 on no intersections, 1 on regular intersection, 2 on infinit
//tIntersectOut is time across l2 (0 at l2p1 and 1 at l2p2)
u8 LineVsLine(v2 l1p1, v2 l1p2, v2 l2p1, v2 l2p2, v2* intersectionOut = nullptr, r32* tIntersectOut = nullptr)
{
	r32 l1Length = Vec2Length(l1p2 - l1p1);
	Assert(l1Length > 0);
	v2 l1Direction = (l1p2 - l1p1) / l1Length;
	v2 l1Tangent = NewVec2(-l1Direction.y, l1Direction.x);
	
	r32 l2Length = Vec2Length(l2p2 - l2p1);
	Assert(l2Length > 0);
	Vec2_t l2Direction = (l2p2 - l2p1) / l2Length;
	
	r32 rdY =  Vec2Dot(l2Direction, l1Tangent);
	r32 rY  = -Vec2Dot(l2p1 - l1p1, l1Tangent);
	r32 rdX =  Vec2Dot(l2Direction, l1Direction);
	r32 rX  =  Vec2Dot(l2p1 - l1p1, l1Direction);
	
	if (rY == 0) //Starts on l1
	{
		if (rdY == 0) //Parallel
		{
			if (rX == 0 && rdX < 0)
			{
				if (intersectionOut != nullptr) { *intersectionOut = l1p1; }
				if (tIntersectOut != nullptr)   { *tIntersectOut   = 0; }
				return 1; //Starts on l1p2 and on same plane
			}
			else if (rX == l1Length && rdX > 0)
			{
				if (intersectionOut != nullptr) { *intersectionOut = l1p2; }
				if (tIntersectOut != nullptr)   { *tIntersectOut   = 0; }
				return 1; //Starts on l1p2 and on same plane
			}
			else if (rX + rdX*l2Length < 0 || rX > l1Length)
			{
				return 0; // Different segments on same plane
			}
			else
			{
				return 2; //Parallel and overlapping
			}
		}
		else
		{
			if (rX >= 0 && rX <= l1Length)
			{
				if (intersectionOut != nullptr) { *intersectionOut = l2p1; }
				if (tIntersectOut != nullptr)   { *tIntersectOut   = 0; }
				return 1; //Starts on l1
			}
			else
			{
				return 0; //Starts at point not on line segment but on line ray
			}
		}
	}
	else if (rdY == 0)
	{
		return 0; //Parallel to l1
	}
	else if (SignOfR32(rdY) != SignOfR32(rY))
	{
		return 0; //Heading away from l1
	}
	
	r32 tIntersection = rY / rdY;
	
	if (tIntersection > l2Length)
	{
		return 0; //l2 ends before reaching l1 plane
	}
	
	r32 endX = rX + tIntersection*rdX;
	
	if (endX < 0 || endX > l1Length)
	{
		return 0; //Intersects l1 plane before/after segment
	}
	
	if (intersectionOut != nullptr) { *intersectionOut = l1p1 + l1Direction * endX; }
	if (tIntersectOut != nullptr)   { *tIntersectOut   = tIntersection/l2Length; }
	return 1;
}

// +==============================+
// |       LineVsRectangle        |
// +==============================+
bool LineVsRectangle(v2 lineStart, v2 lineEnd, rec rectangle, v2* intersectStartOut = nullptr, v2* intersectEndOut = nullptr, r32* timeStartOut = nullptr, r32* timeEndOut = nullptr, Dir2_t* enterSideOut = nullptr, Dir2_t* exitSideOut = nullptr)
{
	//NOTE: Let's handle all our degenerate cases first
	if (lineStart == lineEnd) //not a line, just a point
	{
		if (IsInsideRec(rectangle, lineStart))
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart; }
			if (intersectEndOut != nullptr) { *intersectEndOut = lineEnd; }
			if (timeStartOut != nullptr) { *timeStartOut = 0.0f; }
			if (timeEndOut != nullptr) { *timeEndOut = INFINITY; }
			if (enterSideOut != nullptr) { *enterSideOut = Dir2_None; }
			if (exitSideOut != nullptr) { *exitSideOut = Dir2_None; }
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (lineStart.y == lineEnd.y) //horizontal line
	{
		r32 deltaX = lineEnd.x - lineStart.x;
		r32 intersectTime1 = (rectangle.x - lineStart.x) / deltaX;
		r32 intersectTime2 = ((rectangle.x + rectangle.width) - lineStart.x) / deltaX;
		r32 intersectStart = MinR32(intersectTime1, intersectTime2);
		r32 intersectEnd = MaxR32(intersectTime1, intersectTime2);
		if (intersectStart >= 0.0f && intersectStart <= 1.0f)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart + (lineEnd - lineStart)*intersectStart; }
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*((intersectEnd < 1.0f) ? intersectEnd : 1.0f); }
			if (timeStartOut != nullptr) { *timeStartOut = intersectStart; }
			if (timeEndOut != nullptr) { *timeEndOut = (intersectEnd < 1.0f) ? intersectEnd : 1.0f; }
			if (enterSideOut != nullptr) { *enterSideOut = (deltaX > 0) ? Dir2_Left : Dir2_Right; }
			if (exitSideOut != nullptr) { *exitSideOut = (deltaX > 0) ? Dir2_Right : Dir2_Left; }
			return true;
		}
		else if (intersectStart < 0.0f && intersectEnd >= 0.0f && intersectEnd <= 1.0f)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart; }
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*intersectEnd; }
			if (timeStartOut != nullptr) { *timeStartOut = 0.0f; }
			if (timeEndOut != nullptr) { *timeEndOut = intersectEnd; }
			if (enterSideOut != nullptr) { *enterSideOut = Dir2_None; }
			if (exitSideOut != nullptr) { *exitSideOut = (deltaX > 0) ? Dir2_Right : Dir2_Left; }
			return true;
		}
		else { return false; }
	}
	else if (lineStart.x == lineEnd.x) //vertical line
	{
		r32 deltaY = lineEnd.y - lineStart.y;
		r32 intersectTime1 = (rectangle.y - lineStart.y) / deltaY;
		r32 intersectTime2 = ((rectangle.y + rectangle.height) - lineStart.y) / deltaY;
		r32 intersectStart = MinR32(intersectTime1, intersectTime2);
		r32 intersectEnd = MaxR32(intersectTime1, intersectTime2);
		if (intersectStart >= 0.0f && intersectStart <= 1.0f)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart + (lineEnd - lineStart)*intersectStart; }
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*((intersectEnd < 1.0f) ? intersectEnd : 1.0f); }
			if (timeStartOut != nullptr) { *timeStartOut = intersectStart; }
			if (timeEndOut != nullptr) { *timeEndOut = (intersectEnd < 1.0f) ? intersectEnd : 1.0f; }
			if (enterSideOut != nullptr) { *enterSideOut = (deltaY > 0) ? Dir2_Up : Dir2_Down; }
			if (exitSideOut != nullptr) { *exitSideOut = (deltaY > 0) ? Dir2_Down : Dir2_Up; }
			return true;
		}
		else if (intersectStart < 0.0f && intersectEnd >= 0.0f && intersectEnd <= 1.0f)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart; }
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*intersectEnd; }
			if (timeStartOut != nullptr) { *timeStartOut = 0.0f; }
			if (timeEndOut != nullptr) { *timeEndOut = intersectEnd; }
			if (enterSideOut != nullptr) { *enterSideOut = Dir2_None; }
			if (exitSideOut != nullptr) { *exitSideOut = (deltaY > 0) ? Dir2_Down : Dir2_Up; }
			return true;
		}
		else { return false; }
	}
	else //This is the regular case, box dX and dY are non-zero
	{
		r32 xIntersectTime1 = (rectangle.x - lineStart.x) / (lineEnd.x - lineStart.x);
		r32 xIntersectTime2 = ((rectangle.x + rectangle.width) - lineStart.x) / (lineEnd.x - lineStart.x);
		r32 yIntersectTime1 = (rectangle.y - lineStart.y) / (lineEnd.y - lineStart.y);
		r32 yIntersectTime2 = ((rectangle.y + rectangle.height) - lineStart.y) / (lineEnd.y - lineStart.y);
		r32 xIntersectStart = MinR32(xIntersectTime1, xIntersectTime2);
		r32 xIntersectEnd = MaxR32(xIntersectTime1, xIntersectTime2);
		r32 yIntersectStart = MinR32(yIntersectTime1, yIntersectTime2);
		r32 yIntersectEnd = MaxR32(yIntersectTime1, yIntersectTime2);
		
		if (xIntersectEnd < yIntersectStart || yIntersectEnd < xIntersectStart)
		{
			//If we enter on the second axis after we have already left on the first then that means we missed the rectangle
			return false;
		}
		else if (MaxR32(xIntersectStart, yIntersectStart) > 1.0f)
		{
			//The rectangle is in the direction and would intersect if the line continued but the line is too short
			return false;
		}
		else if (MinR32(xIntersectEnd, yIntersectEnd) < 0.0f)
		{
			//The rectangle is behind us. We would reach it if the line went in the opposite direction
			return false;
		}
		
		bool enteredHorizontal = (xIntersectStart >= yIntersectStart);
		if (MaxR32(xIntersectStart, yIntersectStart) < 0.0f)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart; }
			if (timeStartOut != nullptr) { *timeStartOut = 0; }
			if (enterSideOut != nullptr) { *enterSideOut = Dir2_None; }
		}
		else if (enteredHorizontal)
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart + (lineEnd - lineStart)*xIntersectStart; }
			if (timeStartOut != nullptr) { *timeStartOut = xIntersectStart; }
			if (enterSideOut != nullptr) { *enterSideOut = ((lineEnd.x - lineStart.x) > 0) ? Dir2_Left : Dir2_Right; }
		}
		else
		{
			if (intersectStartOut != nullptr) { *intersectStartOut = lineStart + (lineEnd - lineStart)*yIntersectStart; }
			if (timeStartOut != nullptr) { *timeStartOut = yIntersectStart; }
			if (enterSideOut != nullptr) { *enterSideOut = ((lineEnd.y - lineStart.y) > 0) ? Dir2_Up : Dir2_Down; }
		}
		
		bool exitedHorizontal = (xIntersectEnd <= yIntersectEnd);
		if (MinR32(xIntersectEnd, yIntersectEnd) > 1.0f)
		{
			if (intersectEndOut != nullptr) { *intersectEndOut = lineEnd; }
			if (timeEndOut != nullptr) { *timeEndOut = 1.0f; }
			if (exitSideOut != nullptr) { *exitSideOut = Dir2_None; }
		}
		else if (exitedHorizontal)
		{
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*xIntersectEnd; }
			if (timeEndOut != nullptr) { *timeEndOut = xIntersectEnd; }
			if (exitSideOut != nullptr) { *exitSideOut = ((lineEnd.x - lineStart.x) > 0) ? Dir2_Right : Dir2_Left; }
		}
		else
		{
			if (intersectEndOut != nullptr) { *intersectEndOut = lineStart + (lineEnd - lineStart)*yIntersectEnd; }
			if (timeEndOut != nullptr) { *timeEndOut = yIntersectEnd; }
			if (exitSideOut != nullptr) { *exitSideOut = ((lineEnd.y - lineStart.y) > 0) ? Dir2_Down : Dir2_Up; }
		}
		
		return true;
	}
	
}

// +==============================+
// |          RayVsAABB           |
// +==============================+
struct RayvAABBResult_t
{
	bool intersects;
	
	AABB3_t originalBox;
	Ray3_t originalRay;
	
	r32 enterTime;
	r32 exitTime;
	
	v3 intersection;
	
	Dir3_t faceDirection;
	// v3 faceNormal;
};
RayvAABBResult_t RayVsAABB(AABB3_t box, Ray3_t ray)
{
	RayvAABBResult_t result = {};
	result.originalBox = box;
	result.originalRay = ray;
	
	if (ray.dX == 0.0f && 
		(ray.x >= box.cX + box.width/2.0f || ray.x <= box.cX - box.width/2.0f))
	{
		return result;
	}
	if (ray.dY == 0.0f && 
		(ray.y >= box.cY + box.height/2.0f || ray.y <= box.cY - box.height/2.0f))
	{
		return result;
	}
	if (ray.dZ == 0.0f && 
		(ray.z >= box.cZ + box.depth/2.0f || ray.z <= box.cZ - box.depth/2.0f))
	{
		return result;
	}
	
	if (ray.dZ == 0.0f && ray.dY == 0.0f && ray.dZ == 0.0f)
	{
		result.intersects = true;
		result.enterTime = 0.0f;
		result.exitTime = HUGE_VALF;//+infinity
		result.intersection = ray.origin;
		result.faceDirection = Dir3_None;
		return result;
	}
	
	r32 boxLeft =   (box.cX-box.width/2.0f);
	r32 boxRight =  (box.cX+box.width/2.0f);
	r32 boxBottom = (box.cY-box.height/2.0f);
	r32 boxTop =    (box.cY+box.height/2.0f);
	r32 boxBack =   (box.cZ-box.depth/2.0f);
	r32 boxFront =  (box.cZ+box.depth/2.0f);
	
	//TODO: what if ray.dX == 0
	r32 tXLeft =   (boxLeft - ray.x) / ray.dX;
	r32 tXRight =  (boxRight - ray.x) / ray.dX;
	r32 tYBottom = (boxBottom - ray.y) / ray.dY;
	r32 tYTop =    (boxTop - ray.y) / ray.dY;
	r32 tZBack =   (boxBack - ray.z) / ray.dZ;
	r32 tZFront =  (boxFront - ray.z) / ray.dZ;
	
	r32 minXt, maxXt;
	r32 minYt, maxYt;
	r32 minZt, maxZt;
	Dir3_t minXFace, maxXFace;
	Dir3_t minYFace, maxYFace;
	Dir3_t minZFace, maxZFace;
	
	if (tXLeft <= tXRight)
	{
		minXt = tXLeft;
		maxXt = tXRight;
		minXFace = Dir3_Left;
		maxXFace = Dir3_Right;
	}
	else
	{
		minXt = tXRight;
		maxXt = tXLeft;
		minXFace = Dir3_Right;
		maxXFace = Dir3_Left;
	}
	
	if (tYBottom <= tYTop)
	{
		minYt = tYBottom;
		maxYt = tYTop;
		minYFace = Dir3_Down;
		maxYFace = Dir3_Up;
	}
	else
	{
		minYt = tYTop;
		maxYt = tYBottom;
		minYFace = Dir3_Up;
		maxYFace = Dir3_Down;
	}
	
	if (tZBack <= tZFront)
	{
		minZt = tZBack;
		maxZt = tZFront;
		minZFace = Dir3_Backward;
		maxZFace = Dir3_Forward;
	}
	else
	{
		minZt = tZFront;
		maxZt = tZBack;
		minZFace = Dir3_Forward;
		maxZFace = Dir3_Backward;
	}
	
	r32 tLastAxisEnter = 0;
	Axis_t lastAxisEnter = Axis_None;
	r32 tFirstAxisExit = 0;
	Axis_t firstAxisExit = Axis_None;
	
	u8 whichAxis = 0;
	if (MaxNoInfinities(minXt, minYt, minZt, &tLastAxisEnter, &whichAxis))
	{
		switch(whichAxis)
		{
			case 0: lastAxisEnter = Axis_X; break;
			case 1: lastAxisEnter = Axis_Y; break;
			case 2: lastAxisEnter = Axis_Z; break;
			default: { Assert(false); } break;
		};
	}
	else
	{
		//NOTE: We should have handled this case above with the if
		//		statements about ray.direction being (0,0,0)
		Assert(false);
		return result;
	}
	
	if (MinNoInfinities(maxXt, maxYt, maxZt, &tFirstAxisExit, &whichAxis))
	{
		switch(whichAxis)
		{
			case 0: firstAxisExit = Axis_X; break;
			case 1: firstAxisExit = Axis_Y; break;
			case 2: firstAxisExit = Axis_Z; break;
			default: { Assert(false); } break;
		};
	}
	else
	{
		//NOTE: We should have handled this case above with the if
		//		statements about ray.direction being (0,0,0)
		Assert(false);
		return result;
	}
	
	result.enterTime = tLastAxisEnter;
	result.exitTime = tFirstAxisExit;
	if (lastAxisEnter == Axis_X) result.faceDirection = minXFace;
	else if (lastAxisEnter == Axis_Y) result.faceDirection = minYFace;
	else if (lastAxisEnter == Axis_Z) result.faceDirection = minZFace;
	else { Assert(false); }
	result.intersection = ray.origin + ray.direction*tLastAxisEnter;
	
	if (tLastAxisEnter >= tFirstAxisExit)
	{
		//Never an intersection
		return result;
	}
	
	//TODO: Is 0.01f a good number to curtail rounding errors?
	if (tLastAxisEnter >= -0.01f)
	{
		result.intersects = true;
		return result;
	}
	else
	{
		//The intersection happened in the opposite direction of of the ray
		return result;
	}
}

// +==============================+
// |     ResolveIntersection      |
// +==============================+
struct ResolveIntersectResult_t
{
	bool intersects;
	
	r32 depth;
	flags8 direction;
	v3 normal;
};
ResolveIntersectResult_t ResolveIntersection(AABB3_t staticAABB, AABB3_t movingAABB, flags8 openSides, bool canStepUp = false)
{
	ResolveIntersectResult_t result = {};
	
	if (openSides == Dir3_None)
	{
		//No sides to push out to
		return result;
	}
	
	r32 depthLeft =     (movingAABB.cX + movingAABB.width/2.0f) -  (staticAABB.cX - staticAABB.width/2.0f);
	r32 depthRight =    (staticAABB.cX + staticAABB.width/2.0f) -  (movingAABB.cX - movingAABB.width/2.0f);
	r32 depthDown =     (movingAABB.cY + movingAABB.height/2.0f) - (staticAABB.cY - staticAABB.height/2.0f);
	r32 depthUp =       (staticAABB.cY + staticAABB.height/2.0f) - (movingAABB.cY - movingAABB.height/2.0f);
	r32 depthBackward = (movingAABB.cZ + movingAABB.depth/2.0f) -  (staticAABB.cZ - staticAABB.depth/2.0f);
	r32 depthForward =  (staticAABB.cZ + staticAABB.depth/2.0f) -  (movingAABB.cZ - movingAABB.depth/2.0f);
	
	const r32 smallNum = 0.001f;
	
	if (depthLeft + smallNum >= staticAABB.width + movingAABB.width ||
		depthRight + smallNum >= staticAABB.width + movingAABB.width)
	{
		return result;
	}
	if (depthDown + smallNum >= staticAABB.height + movingAABB.height ||
		depthUp + smallNum >= staticAABB.height + movingAABB.height)
	{
		return result;
	}
	if (depthBackward + smallNum >= staticAABB.depth + movingAABB.depth ||
		depthForward + smallNum >= staticAABB.depth + movingAABB.depth)
	{
		return result;
	}
	
	result.intersects = true;
	
	bool canDoLeft =     (depthLeft > 0 && (openSides & Dir3_Left) != 0);
	bool canDoRight =    (depthRight > 0 && (openSides & Dir3_Right) != 0);
	bool canDoDown =     (depthDown > 0 && (openSides & Dir3_Down) != 0);
	bool canDoUp =       (depthUp > 0 && (openSides & Dir3_Up) != 0);
	bool canDoBackward = (depthBackward > 0 && (openSides & Dir3_Backward) != 0);
	bool canDoForward =  (depthForward > 0 && (openSides & Dir3_Forward) != 0);
	
	if (canDoUp &&
		((depthUp <= 0.5f && canStepUp) ||
		((!canDoRight    || depthUp <= depthRight) &&
		 (!canDoDown     || depthUp <= depthDown) &&
		 (!canDoLeft     || depthUp <= depthLeft) &&
		 (!canDoBackward || depthUp <= depthBackward) &&
		 (!canDoForward  || depthUp <= depthForward))))
	{
		result.depth = depthUp;
		result.direction = Dir3_Up;
		result.normal = Vec3_Up;
	}
	else if (canDoLeft &&
		(!canDoRight    || depthLeft <= depthRight) &&
		(!canDoDown     || depthLeft <= depthDown) &&
		(!canDoUp       || depthLeft <= depthUp) &&
		(!canDoBackward || depthLeft <= depthBackward) &&
		(!canDoForward  || depthLeft <= depthForward))
	{
		result.depth = depthLeft;
		result.direction = Dir3_Left;
		result.normal = Vec3_Left;
	}
	else if (canDoRight &&
		(!canDoLeft     || depthRight <= depthLeft) &&
		(!canDoDown     || depthRight <= depthDown) &&
		(!canDoUp       || depthRight <= depthUp) &&
		(!canDoBackward || depthRight <= depthBackward) &&
		(!canDoForward  || depthRight <= depthForward))
	{
		result.depth = depthRight;
		result.direction = Dir3_Right;
		result.normal = Vec3_Right;
	}
	else if (canDoBackward &&
		(!canDoRight    || depthBackward <= depthRight) &&
		(!canDoDown     || depthBackward <= depthDown) &&
		(!canDoUp       || depthBackward <= depthUp) &&
		(!canDoLeft     || depthBackward <= depthLeft) &&
		(!canDoForward  || depthBackward <= depthForward))
	{
		result.depth = depthBackward;
		result.direction = Dir3_Backward;
		result.normal = Vec3_Backward;
	}
	else if (canDoForward &&
		(!canDoRight    || depthForward <= depthRight) &&
		(!canDoDown     || depthForward <= depthDown) &&
		(!canDoUp       || depthForward <= depthUp) &&
		(!canDoBackward || depthForward <= depthBackward) &&
		(!canDoLeft     || depthForward <= depthLeft))
	{
		result.depth = depthForward;
		result.direction = Dir3_Forward;
		result.normal = Vec3_Forward;
	}
	else if (canDoDown &&
		(!canDoRight    || depthDown <= depthRight) &&
		(!canDoLeft     || depthDown <= depthLeft) &&
		(!canDoUp       || depthDown <= depthUp) &&
		(!canDoBackward || depthDown <= depthBackward) &&
		(!canDoForward  || depthDown <= depthForward))
	{
		result.depth = depthDown;
		result.direction = Dir3_Down;
		result.normal = Vec3_Down;
	}
	else
	{
		Assert(false);
	}
	
	return result;
}

// +==============================+
// |         GetAABBInfo          |
// +==============================+
struct AABBInfo_t
{
	v3 min;
	v3 max;
	
	v3 corners[8];
	v3 axises[6];
};
AABBInfo_t GetAABBInfo(AABB3_t aabb)
{
	AABBInfo_t result = {};
	
	result.min = aabb.center - (aabb.size / 2.0f);
	result.max = aabb.center + (aabb.size / 2.0f);
	
	result.corners[0] = NewVec3(result.min.x, result.min.y, result.min.z);
	result.corners[1] = NewVec3(result.max.x, result.min.y, result.min.z);
	result.corners[2] = NewVec3(result.max.x, result.max.y, result.min.z);
	result.corners[3] = NewVec3(result.min.x, result.max.y, result.min.z);
	result.corners[4] = NewVec3(result.min.x, result.min.y, result.max.z);
	result.corners[5] = NewVec3(result.max.x, result.min.y, result.max.z);
	result.corners[6] = NewVec3(result.max.x, result.max.y, result.max.z);
	result.corners[7] = NewVec3(result.min.x, result.max.y, result.max.z);
	
	result.axises[0] = NewVec3( 1,  0,  0);
	result.axises[1] = NewVec3(-1,  0,  0);
	result.axises[2] = NewVec3( 0,  1,  0);
	result.axises[3] = NewVec3( 0, -1,  0);
	result.axises[4] = NewVec3( 0,  0,  1);
	result.axises[5] = NewVec3( 0,  0, -1);
	
	return result;
}

// +==============================+
// |        GetFrustumInfo        |
// +==============================+
struct FrustumInfo_t
{
	v3 rightVector;
	
	v3 nearPlaneCenter;
	v3 farPlaneCenter;
	
	Vec2_t nearPlaneSize;
	Vec2_t farPlaneSize;
	
	v3 corners[8];
	v3 axises[6];
	
	AABB3_t boundingBox;
};
FrustumInfo_t GetFrustumInfo(Frustum_t frustum)
{
	FrustumInfo_t result = {};
	
	result.rightVector = Vec3Cross(frustum.direction, frustum.upVector);
	
	result.nearPlaneCenter = frustum.origin + (frustum.direction * frustum.zNear);
	result.farPlaneCenter = frustum.origin + (frustum.direction * frustum.zFar);
	
	r32 tanX = TanR32(frustum.fov.x / 2.0f);
	r32 tanY = TanR32(frustum.fov.y / 2.0f);
	
	result.nearPlaneSize.x = 2.0f * frustum.zNear * tanX;
	result.nearPlaneSize.y = 2.0f * frustum.zNear * tanY;
	
	result.farPlaneSize.x = 2.0f * frustum.zFar * tanX;
	result.farPlaneSize.y = 2.0f * frustum.zFar * tanY;
	
	result.corners[0] = result.nearPlaneCenter + 
		-(result.rightVector * result.nearPlaneSize.x / 2.0f) +
		-(frustum.upVector * result.nearPlaneSize.y / 2.0f);
	result.corners[1] = result.nearPlaneCenter + 
		 (result.rightVector * result.nearPlaneSize.x / 2.0f) +
		-(frustum.upVector * result.nearPlaneSize.y / 2.0f);
	result.corners[2] = result.nearPlaneCenter + 
		-(result.rightVector * result.nearPlaneSize.x / 2.0f) +
		 (frustum.upVector * result.nearPlaneSize.y / 2.0f);
	result.corners[3] = result.nearPlaneCenter + 
		 (result.rightVector * result.nearPlaneSize.x / 2.0f) +
		 (frustum.upVector * result.nearPlaneSize.y / 2.0f);
		 
	result.corners[4] = result.farPlaneCenter + 
		-(result.rightVector * result.farPlaneSize.x / 2.0f) +
		-(frustum.upVector * result.farPlaneSize.y / 2.0f);
	result.corners[5] = result.farPlaneCenter + 
		 (result.rightVector * result.farPlaneSize.x / 2.0f) +
		-(frustum.upVector * result.farPlaneSize.y / 2.0f);
	result.corners[6] = result.farPlaneCenter + 
		-(result.rightVector * result.farPlaneSize.x / 2.0f) +
		 (frustum.upVector * result.farPlaneSize.y / 2.0f);
	result.corners[7] = result.farPlaneCenter + 
		 (result.rightVector * result.farPlaneSize.x / 2.0f) +
		 (frustum.upVector * result.farPlaneSize.y / 2.0f);
	
	result.axises[0] = -frustum.direction;                                                                   //Near Face
	result.axises[1] = frustum.direction;                                                                    //Far Face 
	result.axises[2] = Vec3Normalize(Vec3Cross(result.corners[4] - result.corners[0], -result.rightVector)); //Up Face
	result.axises[3] = Vec3Normalize(Vec3Cross(result.corners[7] - result.corners[3], result.rightVector));  //Down Face
	result.axises[4] = Vec3Normalize(Vec3Cross(result.corners[4] - result.corners[0], -frustum.upVector));   //Left Face
	result.axises[5] = Vec3Normalize(Vec3Cross(result.corners[5] - result.corners[1], frustum.upVector));    //Right Face
	
	v3 min = NewVec3(
		MinR32(
		result.corners[0].x,
		result.corners[1].x,
		result.corners[2].x,
		result.corners[3].x,
		result.corners[4].x,
		result.corners[5].x,
		result.corners[6].x,
		result.corners[7].x),
		MinR32(
		result.corners[0].y,
		result.corners[1].y,
		result.corners[2].y,
		result.corners[3].y,
		result.corners[4].y,
		result.corners[5].y,
		result.corners[6].y,
		result.corners[7].y),
		MinR32(
		result.corners[0].z,
		result.corners[1].z,
		result.corners[2].z,
		result.corners[3].z,
		result.corners[4].z,
		result.corners[5].z,
		result.corners[6].z,
		result.corners[7].z));
	
	v3 max = NewVec3(
		MaxR32(
		result.corners[0].x,
		result.corners[1].x,
		result.corners[2].x,
		result.corners[3].x,
		result.corners[4].x,
		result.corners[5].x,
		result.corners[6].x,
		result.corners[7].x),
		MaxR32(
		result.corners[0].y,
		result.corners[1].y,
		result.corners[2].y,
		result.corners[3].y,
		result.corners[4].y,
		result.corners[5].y,
		result.corners[6].y,
		result.corners[7].y),
		MaxR32(
		result.corners[0].z,
		result.corners[1].z,
		result.corners[2].z,
		result.corners[3].z,
		result.corners[4].z,
		result.corners[5].z,
		result.corners[6].z,
		result.corners[7].z));
	
	result.boundingBox = NewAABB3((min+max)/2.0f, max-min);
	
	return result;
}

// +==============================+
// |        FrustumVsAABB         |
// +==============================+
//TODO: Change this to use seperating-axis theorem logic
//		rather than AABB vs AABB intersection
bool FrustumVsAABB(Frustum_t frustum, AABB3_t aabb)
{
	FrustumInfo_t fInfo = GetFrustumInfo(frustum);
	AABBInfo_t boundsInfo = GetAABBInfo(fInfo.boundingBox);
	AABBInfo_t aabbInfo = GetAABBInfo(aabb);
	
	if (boundsInfo.min.x <= aabbInfo.max.x &&
		boundsInfo.min.y <= aabbInfo.max.y &&
		boundsInfo.min.z <= aabbInfo.max.z &&
		boundsInfo.max.x >= aabbInfo.min.x &&
		boundsInfo.max.y >= aabbInfo.min.y &&
		boundsInfo.max.z >= aabbInfo.min.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// +==============================+
// |   Seperating Axis Theorem    |
// +==============================+
//NOTE: Returns true if there is a seperating axis (the polygons aren't intersecting)
//      Seperating axis is give pointing from polygon1 towards polygon2.
//      Depth will be negative for seperation and positive for intersection.
//      sepOffsetOut will be filled with information about how far along the seperation axis polygon1 extends from it's center
bool SeperatingAxisCheck(u32 numNormals, const v2* normals, u32 numVerts1, const v2* verts1, u32 numVerts2, const v2* verts2, r32* depthOut = nullptr, v2* sepAxisOut = nullptr, r32* sepOffsetOut = nullptr)
{
	Assert(normals != nullptr);
	if (numVerts1 == 0) { return false; }
	Assert(verts1 != nullptr);
	if (numVerts2 == 0) { return false; }
	Assert(verts2 != nullptr);
	
	v2 polyCenter1 = Vec2_Zero;
	if (sepOffsetOut != nullptr)
	{
		for (u32 vIndex = 0; vIndex < numVerts1; vIndex++) { polyCenter1 += verts1[vIndex]; }
		polyCenter1 = polyCenter1 / (r32)numVerts1;
	}
	
	bool findMin = (depthOut != nullptr || sepAxisOut != nullptr || sepOffsetOut != nullptr);
	bool foundSeperation = false;
	bool filledDepth = false;
	r32 minDepth = 0.0f;
	for (u32 nIndex = 0; nIndex < numNormals; nIndex++)
	{
		v2 normal = normals[nIndex];
		r32 centerDot = Vec2Dot(polyCenter1, normal);
		r32 min1 = Vec2Dot(verts1[0], normal);
		r32 max1 = min1;
		r32 min2 = Vec2Dot(verts2[0], normal);
		r32 max2 = min2;
		for (u32 vIndex = 1; (vIndex < numVerts1 || vIndex < numVerts2); vIndex++)
		{
			if (vIndex < numVerts1)
			{
				r32 dot1 = Vec2Dot(verts1[vIndex], normal);
				if (dot1 < min1) { min1 = dot1; }
				if (dot1 > max1) { max1 = dot1; }
			}
			if (vIndex < numVerts2)
			{
				r32 dot2 = Vec2Dot(verts2[vIndex], normal);
				if (dot2 < min2) { min2 = dot2; }
				if (dot2 > max2) { max2 = dot2; }
			}
		}
		
		if (min1 < min2 && max1 < min2) // [--1--] [--2--]
		{
			r32 newDepth = -(min2 - max1);
			if (!foundSeperation || newDepth > minDepth)
			{
				if (depthOut != nullptr) { *depthOut = newDepth; }
				if (sepAxisOut != nullptr) { *sepAxisOut = normal; }
				if (sepOffsetOut != nullptr) { *sepOffsetOut = max1 - centerDot; }
				foundSeperation = true;
				minDepth = newDepth;
			}
		}
		else if (min2 < min1 && max2 < min1) // [--2--] [--1--]
		{
			r32 newDepth = -(min1 - max2);
			if (!foundSeperation || newDepth > minDepth)
			{
				if (depthOut != nullptr) { *depthOut = newDepth; }
				if (sepAxisOut != nullptr) { *sepAxisOut = -normal; }
				if (sepOffsetOut != nullptr) { *sepOffsetOut = centerDot - min1; }
				foundSeperation = true;
				minDepth = newDepth;
			}
		}
		else if (!foundSeperation && findMin)
		{
			if (min1 <= min2 && max1 >= max2) //[--1--[==2==]--1--] Overlap
			{
				r32 depthRight = max1 - min2;
				r32 depthLeft = max2 - min1;
				if (depthRight <= depthLeft) //poly1 will move left
				{
					if (!filledDepth || depthRight < minDepth)
					{
						if (depthOut != nullptr) { *depthOut = depthRight; }
						if (sepAxisOut != nullptr) { *sepAxisOut = normal; }
						if (sepOffsetOut != nullptr) { *sepOffsetOut = max1 - centerDot; }
						minDepth = depthRight;
						filledDepth = true;
					}
				}
				else //poly1 will move right
				{
					if (!filledDepth || depthLeft < minDepth)
					{
						if (depthOut != nullptr) { *depthOut = depthLeft; }
						if (sepAxisOut != nullptr) { *sepAxisOut = -normal; }
						if (sepOffsetOut != nullptr) { *sepOffsetOut = centerDot - min1; }
						minDepth = depthLeft;
						filledDepth = true;
					}
				}
			}
			else if (min2 <= min1 && max2 >= max1) //[--2--[==1==]--2--] Overlap
			{
				r32 depthRight = max2 - min1;
				r32 depthLeft = max1 - min2;
				if (depthRight <= depthLeft) //poly1 will move right
				{
					if (!filledDepth || depthRight < minDepth)
					{
						if (depthOut != nullptr) { *depthOut = depthRight; }
						if (sepAxisOut != nullptr) { *sepAxisOut = -normal; }
						if (sepOffsetOut != nullptr) { *sepOffsetOut = centerDot - min1; }
						minDepth = depthRight;
						filledDepth = true;
					}
				}
				else //poly1 will move left
				{
					if (!filledDepth || depthLeft < minDepth)
					{
						if (depthOut != nullptr) { *depthOut = depthLeft; }
						if (sepAxisOut != nullptr) { *sepAxisOut = normal; }
						if (sepOffsetOut != nullptr) { *sepOffsetOut = max1 - centerDot; }
						minDepth = depthLeft;
						filledDepth = true;
					}
				}
			}
			else if (min1 <= min2 && max1 >= min2) //[--1--[==]--2--] Overlap
			{
				r32 depth = max1 - min2;
				if (!filledDepth || depth < minDepth) //poly1 will move left
				{
					if (depthOut != nullptr) { *depthOut = depth; }
					if (sepAxisOut != nullptr) { *sepAxisOut = normal; }
					if (sepOffsetOut != nullptr) { *sepOffsetOut = max1 - centerDot; }
					minDepth = depth;
					filledDepth = true;
				}
			}
			else if (min2 <= min1 && max2 >= min1) //[--2--[==]--1--] Overlap
			{
				r32 depth = max1 - min2;
				if (!filledDepth || depth < minDepth) //poly1 will move right
				{
					if (depthOut != nullptr) { *depthOut = depth; }
					if (sepAxisOut != nullptr) { *sepAxisOut = normal; }
					if (sepOffsetOut != nullptr) { *sepOffsetOut = centerDot - min1; }
					minDepth = depth;
					filledDepth = true;
				}
			}
			else { Assert(false); } //This shouldn't really be possible
		}
		
		if (foundSeperation && !findMin) { return true; } //early out if no extra information is required
	}
	
	return foundSeperation;
}

v2* GetPolygonNormals(MemoryArena_t* memArena, u32 numPolyVerts, const v2* polyVerts, u32* numNormalsOut = nullptr)
{
	Assert(polyVerts != nullptr || numPolyVerts == 0);
	if (numNormalsOut != nullptr) { *numNormalsOut = 0; }
	if (numPolyVerts == 0) { return nullptr; }
	
	if (numNormalsOut != nullptr) { *numNormalsOut = numPolyVerts; }
	if (memArena == nullptr) { return nullptr; }
	
	v2* result = PushArray(memArena, v2, numPolyVerts);
	for (u32 vIndex = 0; vIndex < numPolyVerts; vIndex++)
	{
		v2 vert1 = polyVerts[vIndex];
		v2 vert2 = polyVerts[(vIndex+1) % numPolyVerts];
		result[vIndex] = Vec2PerpLeft(Vec2Normalize(vert2 - vert1));
	}
	
	return result;
}

bool IsTriangleClockwise(v2 p0, v2 p1, v2 p2)
{
	return (Vec2Dot(p2 - p0, Vec2PerpRight(p1 - p0)) >= 0);
}
bool IsInsideTriangle(v2 test, v2 p0, v2 p1, v2 p2)
{
	v2 perp0 = Vec2PerpRight(p1 - p0);
	v2 perp1 = Vec2PerpRight(p2 - p1);
	v2 perp2 = Vec2PerpRight(p0 - p2);
	return (
		SignOfR32(Vec2Dot(p2 - p0, perp0)) == SignOfR32(Vec2Dot(test - p0, perp0)) &&
		SignOfR32(Vec2Dot(p0 - p1, perp1)) == SignOfR32(Vec2Dot(test - p1, perp1)) &&
		SignOfR32(Vec2Dot(p1 - p2, perp2)) == SignOfR32(Vec2Dot(test - p2, perp2))
	);
}

#ifdef _MY_RECTANGLES_H
bool Obb2Intersects(obb2 box1, obb2 box2, r32* depthOut = nullptr, v2* sepAxisOut = nullptr, r32* sepOffsetOut = nullptr)
{
	v2 normals[4];
	normals[0] = NewVec2(CosR32(box1.rotation), SinR32(box1.rotation));
	normals[1] = Vec2PerpRight(normals[0]);
	normals[2] = NewVec2(CosR32(box2.rotation), SinR32(box2.rotation));
	normals[3] = Vec2PerpRight(normals[2]);
	v2 verts1[4];
	verts1[0] = box1.center - (box1.width/2)*normals[0] - (box1.height/2)*normals[1];
	verts1[1] = box1.center + (box1.width/2)*normals[0] - (box1.height/2)*normals[1];
	verts1[2] = box1.center - (box1.width/2)*normals[0] + (box1.height/2)*normals[1];
	verts1[3] = box1.center + (box1.width/2)*normals[0] + (box1.height/2)*normals[1];
	v2 verts2[4];
	verts2[0] = box2.center - (box2.width/2)*normals[2] - (box2.height/2)*normals[3];
	verts2[1] = box2.center + (box2.width/2)*normals[2] - (box2.height/2)*normals[3];
	verts2[2] = box2.center - (box2.width/2)*normals[2] + (box2.height/2)*normals[3];
	verts2[3] = box2.center + (box2.width/2)*normals[2] + (box2.height/2)*normals[3];
	return !SeperatingAxisCheck(4, &normals[0], 4, &verts1[0], 4, &verts2[0], depthOut, sepAxisOut, sepOffsetOut);
}
bool Obb2IntersectsRec(obb2 box, rec rectangle, r32* depthOut = nullptr, v2* sepAxisOut = nullptr, r32* sepOffsetOut = nullptr)
{
	obb2 box2 = NewObb2(rectangle);
	return Obb2Intersects(box, box2, depthOut, sepAxisOut, sepOffsetOut);
}
bool RecIntersectsObb2(rec rectangle, obb2 box)
{
	obb2 box2 = NewObb2(rectangle);
	return Obb2Intersects(box, box2);
}
obb2 GetTriangleObb2(v2 pos1, v2 pos2, v2 pos3)
{
	bool isClockwise = IsTriangleClockwise(pos1, pos2, pos3);
	r32 minThickness = 0;
	obb2 result = Obb2_Zero;
	for (u32 vIndex = 0; vIndex < 3; vIndex++)
	{
		v2 p1 = pos1; v2 p2 = pos2; v2 p3 = pos3;
		if (vIndex == 1) { p1 = pos2; p2 = pos3; p3 = pos1; }
		if (vIndex == 2) { p1 = pos3; p2 = pos1; p3 = pos2; }
		v2 normalVec = isClockwise ? Vec2PerpRight(Vec2Normalize(p1 - p3)) : Vec2PerpLeft(Vec2Normalize(p1 - p3));
		r32 thickness = Vec2Dot(p2 - p1, normalVec);
		if (vIndex == 0 || thickness < minThickness)
		{
			minThickness = thickness;
			v2 rightNormal = Vec2PerpLeft(normalVec);
			r32 downDot1 = Vec2Dot(p1, normalVec);
			r32 downDot2 = Vec2Dot(p2, normalVec);
			r32 downDot3 = Vec2Dot(p3, normalVec);
			r32 rightDot1 = Vec2Dot(p1, rightNormal);
			r32 rightDot2 = Vec2Dot(p2, rightNormal);
			r32 rightDot3 = Vec2Dot(p3, rightNormal);
			r32 downMin = MinR32(downDot1, downDot2, downDot3);
			r32 downMax = MaxR32(downDot1, downDot2, downDot3);
			r32 rightMin = MinR32(rightDot1, rightDot2, rightDot3);
			r32 rightMax = MaxR32(rightDot1, rightDot2, rightDot3);
			result.center = ((rightMax + rightMin)/2) * rightNormal + ((downMax + downMin) / 2) * normalVec;
			result.width = rightMax-rightMin;
			result.height = downMax-downMin;
			result.rotation = AtanR32(rightNormal.y, rightNormal.x);
			if (result.rotation < 0) { result.rotation += Pi32*2; }
			if (result.rotation >= Pi32*2) { result.rotation -= Pi32*2; }
		}
	}
	return result;
}
#endif

v2 BezierCurve3Point2(v2 start, v2 control, v2 end, r32 time)
{
	return NewVec2(
		((1 - time)*(1 - time)*start.x) + (2*time*(1 - time)*control.x) + time*time*end.x,
		((1 - time)*(1 - time)*start.y) + (2*time*(1 - time)*control.y) + time*time*end.y
	);
}

#endif // _MY_MATH_H