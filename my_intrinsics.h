/*
File:   my_intrinsics.h
Author: Taylor Robbins
Date:   10\05\2017
*/

#ifndef _MY_INTRINSICS
#define _MY_INTRINSICS

using namespace std; //NOTE: We need this for min i guess?

#define Square(value) ((value) * (value))
#define Cube(value)   ((value) * (value) * (value))

inline r32 AbsR32(r32 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}
inline r64 AbsR64(r64 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}
inline i8 AbsI8(i8 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}
inline i16 AbsI16(i16 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}
inline i32 AbsI32(i32 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}
inline i64 AbsI64(i64 value)
{
	if (value < 0) { return -value; }
	else { return value; }
}

inline bool BasicallyEqualR32(r32 value1, r32 value2, r32 tolerance = 0.001f)
{
	return (AbsR32(value1 - value2) <= tolerance);
}
inline bool BasicallyEqualR64(r64 value1, r64 value2, r64 tolerance = 0.001f)
{
	return (AbsR64(value1 - value2) <= tolerance);
}

inline r32 SinR32(r32 angle)
{
	return sinf(angle);
}
inline r64 SinR64(r64 angle)
{
	return sin(angle);
}
inline r32 CosR32(r32 angle)
{
	return cosf(angle);
}
inline r64 CosR64(r64 angle)
{
	return cos(angle);
}
inline r32 TanR32(r32 angle)
{
	return tanf(angle);
}
inline r64 TanR64(r64 angle)
{
	return tan(angle);
}
inline r32 AsinR32(r32 value)
{
	return asinf(value);
}
inline r64 AsinR64(r64 value)
{
	return asin(value);
}
inline r32 AcosR32(r32 value)
{
	return acosf(value);
}
inline r64 AcosR64(r64 value)
{
	return acos(value);
}
inline r32 AtanR32(r32 y, r32 x)
{
	return atan2f(y, x);
}
inline r64 AtanR64(r64 y, r64 x)
{
	return atan2(y, x);
}

inline r32 RoundR32(r32 value)
{
	r32 result = roundf(value);
	return result;
}
inline i32 RoundR32i(r32 value)
{
	i32 result = (i32)roundf(value);
	return result;
}
inline r32 FloorR32(r32 value)
{
	r32 result = floorf(value);
	return result;
}
inline i32 FloorR32i(r32 value)
{
	i32 result = (i32)floorf(value);
	return result;
}
inline r32 CeilR32(r32 value)
{
	r32 result = ceilf(value);
	return result;
}
inline i32 CeilR32i(r32 value)
{
	i32 result = (i32)ceilf(value);
	return result;
}

//TODO: These functions might not work for negative numbers?
//      We need to check how the modulo operator works on negative numbers because we seem to be getting some weird results in various places
inline i32 CeilDivI32(i32 dividend, i32 divisor)
{
	if ((dividend % divisor) == 0) { return dividend / divisor; }
	else { return (dividend / divisor) + 1; }
}
inline u32 CeilDivU32(u32 dividend, u32 divisor)
{
	if ((dividend % divisor) == 0) { return dividend / divisor; }
	else { return (dividend / divisor) + 1; }
}

//TODO: These functions might not work for negative numbers?
//      We need to check how the modulo operator works on negative numbers because we seem to be getting some weird results in various places
inline i32 CeilToI32(i32 value, i32 chunkSize)
{
	if ((value%chunkSize) == 0) { return value; }
	return value + (chunkSize - (value%chunkSize));
}
inline u32 CeilToU32(u32 value, u32 chunkSize)
{
	if ((value%chunkSize) == 0) { return value; }
	return value + (chunkSize - (value%chunkSize));
}

inline r64 RoundR64(r64 value)
{
	r64 result = round(value);
	return result;
}
inline i64 RoundR64i(r64 value)
{
	i64 result = (i64)round(value);
	return result;
}
inline r64 FloorR64(r64 value)
{
	r64 result = floor(value);
	return result;
}
inline i64 FloorR64i(r64 value)
{
	i64 result = (i64)floor(value);
	return result;
}
inline r64 CeilR64(r64 value)
{
	r64 result = ceil(value);
	return result;
}
inline i64 CeilR64i(r64 value)
{
	i64 result = (i64)ceil(value);
	return result;
}

inline r32 DecimalPartR32(r32 value)
{
	r32 floor = (r32)FloorR32(value);
	return AbsR32(value - floor);
}
inline r64 DecimalPartR64(r64 value)
{
	r64 floor = (r64)FloorR64(value);
	return AbsR64(value - floor);
}

inline r32 ModR32(r32 numerator, r32 denominator)
{
	return fmodf(numerator, denominator);
}
inline r64 ModR64(r64 numerator, r64 denominator)
{
	return fmod(numerator, denominator);
}

//This is like SinR32 but with linear up/down motion
inline r32 SawR32(r32 angle)
{
	r32 x = (angle-Pi32/2) / (Pi32);
	i32 section = FloorR32i(x);
	r32 offset = x - (r32)section;
	if ((section%2) == 0)
	{
		return -1 + (offset * 2);
	}
	else
	{
		return 1 - (offset * 2);
	}
}

inline r32 ClampR32(r32 value, r32 min, r32 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline r64 ClampR64(r64 value, r64 min, r64 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline i8 ClampI8(i8 value, i8 min, i8 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline i16 ClampI16(i16 value, i16 min, i16 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline i32 ClampI32(i32 value, i32 min, i32 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline i64 ClampI64(i64 value, i64 min, i64 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline u8 ClampU8(u8 value, u8 min, u8 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline u16 ClampU16(u16 value, u16 min, u16 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline u32 ClampU32(u32 value, u32 min, u32 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}
inline u64 ClampU64(u64 value, u64 min, u64 max)
{
	if (value < min) { return min; }
	else if (value > max) { return max; }
	else { return value; }
}

inline r32 LerpR32(r32 val1, r32 val2, r32 amount)
{
	return (val1 + (val2 - val1) * amount);
}
inline r64 LerpR64(r64 val1, r64 val2, r64 amount)
{
	return (val1 + (val2 - val1) * amount);
}

inline r32 SignOfR32(r32 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}
inline r64 SignOfR64(r64 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}
inline i8 SignOfI8(i8 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}
inline i16 SignOfI16(i16 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}
inline i32 SignOfI32(i32 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}
inline i64 SignOfI64(i64 value)
{
	     if (value < 0) { return -1; }
	else if (value > 0) { return  1; }
	else                { return  0; }
}

inline r32 SqrtR32(r32 value)
{
	return sqrtf(value);
}
inline r64 SqrtR64(r64 value)
{
	return sqrt(value);
}

inline r32 CbrtR32(r32 value)
{
	return cbrtf(value);
}
inline r64 CbrtR64(r64 value)
{
	return cbrt(value);
}

inline r32 PowR32(r32 value, r32 power)
{
	return powf(value, power);
}
inline r64 PowR64(r64 value, r64 power)
{
	return pow(value, power);
}

inline u32 RandU32(u32 min, u32 max)
{
	r64 scaled = (r64)rand() / RAND_MAX;
	if (scaled == 1.0f) { return max-1; }
	return (u32)((max - min)*scaled) + min;
}
inline i32 RandI32(i32 min, i32 max)
{
	r64 scaled = (r64)rand() / RAND_MAX;
	return (i32)((max - min)*scaled) + min;
}
inline i64 RandI64(i64 min, i64 max)
{
	r64 scaled = (r64)rand() / RAND_MAX;
	return RoundR64i((max - min)*scaled) + min;
}
inline r32 RandR32(r32 min, r32 max)
{
	r64 scaled = (r64)rand() / RAND_MAX;
	return (r32)((max - min)*scaled) + min;
}
inline r64 RandR64(r64 min, r64 max)
{
	r64 scaled = (r64)rand() / RAND_MAX;
	return ((max - min)*scaled) + min;
}

inline u8 FlagsDiffU8(u8 flags1, u8 flags2)
{
	u8 diff = (u8)(flags1 ^ flags2); //TODO: Can we get a garunteed u8 XOR on VS2013 and not have to do a u8 cast?
	u8 result = 0;
	while (diff != 0)
	{
		if (diff & 0x01) { result++; }
		diff >>= 1;
	}
	return result;
}

// +--------------------------------------------------------------+
// |                Primitive Conversion Functions                |
// +--------------------------------------------------------------+
//TODO: Make these functions work without pointer arithmetic
inline u8 StoreI8inU8(i8 value)
{
	u8 result = *((u8*)&value);
	return result;
}
inline i8 ReadI8FromU8(u8 value)
{
	i8 result = *((i8*)&value);
	return result;
}
inline i8 ClampI32toI8(i32 value)
{
	if (value > 127) { return 127; }
	if (value < -128) { return -128; }
	return (i8)value;
}
inline u8 ClampI32toU8(i32 value)
{
	if (value > 255) { return 255; }
	if (value < 0) { return 0; }
	return (u8)value;
}
inline i16 ClampI32toI16(i32 value)
{
	if (value > 32767) { return 32767; }
	if (value < -32768) { return -32768; }
	return (i16)value;
}
inline u32 ClampI32toU32(i32 value)
{
	if (value < 0) { return 0; }
	return (u32)value;
}
inline i32 ClampU32toI32(u32 value)
{
	if (value > 0x7FFFFFFF) { return (i32)0x7FFFFFFF; }
	return (i32)value;
}
inline i16 ClampR32toI16(r32 value)
{
	i32 roundedValue = RoundR32i(value);
	if (roundedValue > 0x7FFF) { return 0x7FFF; }
	else if (roundedValue < -0x8000) { return -0x8000; }
	else { return (i16)roundedValue; }
}

// +--------------------------------------------------------------+
// |                        Min Functions                         |
// +--------------------------------------------------------------+
inline u32 MinU32(u32 value1, u32 value2)
{
	return min(value1, value2);
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3)
{
	return min(value1, min(value2, value3));
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3, u32 value4)
{
	return min(value1, min(value2, min(value3, value4)));
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5)
{
	return min(value1, min(value2, min(value3, min(value4, value5))));
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, value6)))));
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6, u32 value7)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, value7))))));
}
inline u32 MinU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6, u32 value7, u32 value8)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, min(value7, value8)))))));
}

inline i32 MinI32(i32 value1, i32 value2)
{
	return min(value1, value2);
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3)
{
	return min(value1, min(value2, value3));
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3, i32 value4)
{
	return min(value1, min(value2, min(value3, value4)));
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5)
{
	return min(value1, min(value2, min(value3, min(value4, value5))));
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, value6)))));
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6, i32 value7)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, value7))))));
}
inline i32 MinI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6, i32 value7, i32 value8)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, min(value7, value8)))))));
}

inline r32 MinR32(r32 value1, r32 value2)
{
	return min(value1, value2);
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3)
{
	return min(value1, min(value2, value3));
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3, r32 value4)
{
	return min(value1, min(value2, min(value3, value4)));
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5)
{
	return min(value1, min(value2, min(value3, min(value4, value5))));
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, value6)))));
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6, r32 value7)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, value7))))));
}
inline r32 MinR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6, r32 value7, r32 value8)
{
	return min(value1, min(value2, min(value3, min(value4, min(value5, min(value6, min(value7, value8)))))));
}

// +--------------------------------------------------------------+
// |                        Max Functions                         |
// +--------------------------------------------------------------+
inline u32 MaxU32(u32 value1, u32 value2)
{
	return max(value1, value2);
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3)
{
	return max(value1, max(value2, value3));
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3, u32 value4)
{
	return max(value1, max(value2, max(value3, value4)));
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5)
{
	return max(value1, max(value2, max(value3, max(value4, value5))));
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, value6)))));
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6, u32 value7)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, value7))))));
}
inline u32 MaxU32(u32 value1, u32 value2, u32 value3, u32 value4, u32 value5, u32 value6, u32 value7, u32 value8)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, max(value7, value8)))))));
}

inline i32 MaxI32(i32 value1, i32 value2)
{
	return max(value1, value2);
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3)
{
	return max(value1, max(value2, value3));
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3, i32 value4)
{
	return max(value1, max(value2, max(value3, value4)));
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5)
{
	return max(value1, max(value2, max(value3, max(value4, value5))));
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, value6)))));
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6, i32 value7)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, value7))))));
}
inline i32 MaxI32(i32 value1, i32 value2, i32 value3, i32 value4, i32 value5, i32 value6, i32 value7, i32 value8)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, max(value7, value8)))))));
}

inline r32 MaxR32(r32 value1, r32 value2)
{
	return max(value1, value2);
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3)
{
	return max(value1, max(value2, value3));
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3, r32 value4)
{
	return max(value1, max(value2, max(value3, value4)));
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5)
{
	return max(value1, max(value2, max(value3, max(value4, value5))));
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, value6)))));
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6, r32 value7)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, value7))))));
}
inline r32 MaxR32(r32 value1, r32 value2, r32 value3, r32 value4, r32 value5, r32 value6, r32 value7, r32 value8)
{
	return max(value1, max(value2, max(value3, max(value4, max(value5, max(value6, max(value7, value8)))))));
}

inline r32 LerpClampR32(r32 val1, r32 val2, r32 amount)
{
	return ClampR32(val1 + (val2 - val1) * amount, MinR32(val1, val2), MaxR32(val1, val2));
}
// inline r64 LerpClampR64(r64 val1, r64 val2, r64 amount)
// {
// 	return ClampR64(val1 + (val2 - val1) * amount, MinR64(val1, val2), MaxR64(val1, val2));
// }

// +--------------------------------------------------------------+
// |                     Threading Intrinsics                     |
// +--------------------------------------------------------------+
#if WINDOWS_COMPILATION

//TODO: Figure out if we actually need the _mm_sfence() instruction
#define ThreadingWriteBarrier() _WriteBarrier(); _mm_sfence()
#define ThreadingReadBarrier() _ReadBarrier()
#define ThreadSafeIncrement(variablePntr) InterlockedIncrement(variablePntr)
#define ThreadSafeClaimByBool(variablePntr) !_interlockedbittestandset((LONG volatile*)(variablePntr), 0)

#elif OSX_COMPILATION

//TODO: Implement these intrinsics

#elif LINUX_COMPILATION

//TODO: Implement these intrinsics

#endif

#endif // _MY_INTRINSICS