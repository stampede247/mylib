/*
File:   my_primitives3D.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file is a dumping ground for 3D primitive creation functions.
	** All of these functions use PrimitiveVertex_t as they vertex type. This structure is expected to be transposed into whatever Vertex_t structure is used the application.
	** All primitives are built using unindexed vertices that represent a triangle list
	** While the PrimitiveVertex_t has support for colors, normals, and texture coordinates, not all primitive functions make full use of these values
	** The faceIndex value in the PrimitiveVertex_t can be used to distinguish between various parts of the primitive (for example, left face of a cube, vs right face)
*/

//TODO: The Cylinder functions are newer and work a bit different than the other two.
//      We should make these functions all match in style.

#ifndef _PRIMITIVES_3D_H
#define _PRIMITIVES_3D_H

struct PrimitiveVertex_t
{
	union
	{
		v3 position;
		struct
		{
			r32 x, y, z;
		};
		struct
		{
			r32 pX, pY, pZ;
		};
	};
	union
	{
		v2 texCoord;
		struct
		{
			r32 tX, tY;
		};
	};
	union
	{
		v3 normal;
		struct
		{
			r32 nX, nY, nZ;
		};
	};
	Color_t color;
	i32 faceIndex;
};

// +--------------------------------------------------------------+
// |                        Cube Primitive                        |
// +--------------------------------------------------------------+
//faceIndex order: left=0, right=1, back=2, front=3, bottom=4, top=5
// All triangles are defined in clockwise order
// Face Layout:
//   1 +---+ 2/4
//     |1 /|
//     | / |
//     |/ 2|
// 0/3 +---+ 5
// top face texture coordinates orient the top of the image towards the back side (-z) of the cube
// bottom face texture coordinates orient the top of the image towards the front side (+z) of the cube

u32 CubePrimitiveGetNumVerts()
{
	return 3*2*6;
}

void CubePrimitiveCreate(v3 bottomBackLeft, v3 size, Color_t color, rec sourceRec, PrimitiveVertex_t* vertsOut)
{
	Assert(vertsOut != nullptr);
	
	v3 bottomBackRight  = bottomBackLeft + NewVec3(size.width, 0, 0);
	v3 bottomFrontRight = bottomBackLeft + NewVec3(size.width, 0, size.depth);
	v3 bottomFrontLeft  = bottomBackLeft + NewVec3(0,          0, size.depth);
	
	v3 topBackLeft   = bottomBackLeft + NewVec3(0,          size.height, 0);
	v3 topBackRight  = bottomBackLeft + NewVec3(size.width, size.height, 0);
	v3 topFrontRight = bottomBackLeft + NewVec3(size.width, size.height, size.depth);
	v3 topFrontLeft  = bottomBackLeft + NewVec3(0,          size.height, size.depth);
	
	v2 texTopLeft = sourceRec.topLeft;
	v2 texTopRight = sourceRec.topLeft + NewVec2(sourceRec.width, 0);
	v2 texBottomRight = sourceRec.topLeft + sourceRec.size;
	v2 texBottomLeft = sourceRec.topLeft + NewVec2(0, sourceRec.height);
	
	PrimitiveVertex_t* vert = &vertsOut[0];
	
	// +==============================+
	// |         Left Face -X         |
	// +==============================+
	vert->position = bottomBackLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	vert->position = topBackLeft;     vert->texCoord = texTopLeft;     vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	vert->position = topFrontLeft;    vert->texCoord = texTopRight;    vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	
	vert->position = bottomBackLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	vert->position = topFrontLeft;    vert->texCoord = texTopRight;    vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	vert->position = bottomFrontLeft; vert->texCoord = texBottomRight; vert->normal = Vec3_Left; vert->color = color; vert->faceIndex = 0; vert++;
	
	// +==============================+
	// |        Right Face +X         |
	// +==============================+
	vert->position = bottomFrontRight; vert->texCoord = texBottomLeft;  vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	vert->position = topFrontRight;    vert->texCoord = texTopLeft;     vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	vert->position = topBackRight;     vert->texCoord = texTopRight;    vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	
	vert->position = bottomFrontRight; vert->texCoord = texBottomLeft;  vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	vert->position = topBackRight;     vert->texCoord = texTopRight;    vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	vert->position = bottomBackRight;  vert->texCoord = texBottomRight; vert->normal = Vec3_Right; vert->color = color; vert->faceIndex = 1; vert++;
	
	// +==============================+
	// |         Back Face -Z         |
	// +==============================+
	vert->position = bottomBackRight; vert->texCoord = texBottomLeft;  vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	vert->position = topBackRight;    vert->texCoord = texTopLeft;     vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	vert->position = topBackLeft;     vert->texCoord = texTopRight;    vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	
	vert->position = bottomBackRight; vert->texCoord = texBottomLeft;  vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	vert->position = topBackLeft;     vert->texCoord = texTopRight;    vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	vert->position = bottomBackLeft;  vert->texCoord = texBottomRight; vert->normal = Vec3_Back; vert->color = color; vert->faceIndex = 2; vert++;
	
	// +==============================+
	// |        Front Face +Z         |
	// +==============================+
	vert->position = bottomFrontLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	vert->position = topFrontLeft;     vert->texCoord = texTopLeft;     vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	vert->position = topFrontRight;    vert->texCoord = texTopRight;    vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	
	vert->position = bottomFrontLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	vert->position = topFrontRight;    vert->texCoord = texTopRight;    vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	vert->position = bottomFrontRight; vert->texCoord = texBottomRight; vert->normal = Vec3_Front; vert->color = color; vert->faceIndex = 3; vert++;
	
	// +==============================+
	// |        Bottom Face -Y        |
	// +==============================+
	vert->position = bottomBackLeft;   vert->texCoord = texBottomLeft;  vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	vert->position = bottomFrontLeft;  vert->texCoord = texTopLeft;     vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	vert->position = bottomFrontRight; vert->texCoord = texTopRight;    vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	
	vert->position = bottomBackLeft;   vert->texCoord = texBottomLeft;  vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	vert->position = bottomFrontRight; vert->texCoord = texTopRight;    vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	vert->position = bottomBackRight;  vert->texCoord = texBottomRight; vert->normal = Vec3_Bottom; vert->color = color; vert->faceIndex = 4; vert++;
	
	// +==============================+
	// |         Top Face +Y          |
	// +==============================+
	vert->position = topFrontLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
	vert->position = topBackLeft;   vert->texCoord = texTopLeft;     vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
	vert->position = topBackRight;  vert->texCoord = texTopRight;    vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
	
	vert->position = topFrontLeft;  vert->texCoord = texBottomLeft;  vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
	vert->position = topBackRight;  vert->texCoord = texTopRight;    vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
	vert->position = topFrontRight; vert->texCoord = texBottomRight; vert->normal = Vec3_Top; vert->color = color; vert->faceIndex = 5; vert++;
}

PrimitiveVertex_t* CubePrimitiveCreateArena(v3 bottomBackLeft, v3 size, Color_t color, rec sourceRec, MemoryArena_t* arenaPntr, u32* numVertsOut = nullptr)
{
	u32 numVerts = CubePrimitiveGetNumVerts();
	if (numVertsOut != nullptr) { *numVertsOut = numVerts; }
	PrimitiveVertex_t* result = PushArray(arenaPntr, PrimitiveVertex_t, numVerts);
	CubePrimitiveCreate(bottomBackLeft, size, color, sourceRec, result);
	return result;
}

// +--------------------------------------------------------------+
// |                       Sphere Primitive                       |
// +--------------------------------------------------------------+
//faceIndex corrisponds to the ring index that triangle was made to represent. this goes from 0 (bottom cap) to numRings (top cap)

u32 SpherePrimitiveGetNumVerts(u32 numRings, u32 numSegments)
{
	Assert(numRings >= 1);
	Assert(numSegments >= 3);
	u32 numTriangles = numSegments * 2; //top and bottom triangles
	numTriangles += numSegments*2 * (numRings-1);
	return numTriangles*3;
}

//TODO: Implement texture coordinates on this somehow
void SpherePrimitiveCreate(v3 center, r32 radius, u32 numRings, u32 numSegments, Color_t color, rec sourceRec, PrimitiveVertex_t* vertsOut)
{
	Assert(vertsOut != nullptr);
	Assert(numRings >= 1);
	Assert(numSegments >= 3);
	
	r32 ringStep = Pi32/(r32)(numRings+1);
	r32 radiusStep = 2*Pi32/(r32)(numSegments);
	
	u32 expectedNumVerts = SpherePrimitiveGetNumVerts(numRings, numSegments);
	PrimitiveVertex_t* vert = &vertsOut[0];
	
	v3 topCenter = center + NewVec3(0, radius, 0);
	v3 bottomCenter = center - NewVec3(0, radius, 0);
	
	for(u32 rIndex = 0; rIndex <= numRings; rIndex++) 
	{
		r32 ringAngle = -Pi32/2 + ringStep*rIndex;
		r32 ringY = center.y + SinR32(ringAngle)*radius;
		r32 ringRadius = CosR32(ringAngle)*radius;
		r32 nextRingAngle = -Pi32/2 + ringStep*(rIndex+1);
		r32 nextRingY = center.y + SinR32(nextRingAngle)*radius;
		r32 nextRingRadius = CosR32(nextRingAngle)*radius;
		
		// r32 ringY = (center.y - radius) + (rIndex * ringStep);
		// r32 ringRadius = radius * CosR32(AsinR32((ringY - center.y)/radius));
		// r32 nextRingY = (center.y - radius) + ((rIndex+1) * ringStep);
		// r32 nextRingRadius = radius * CosR32(AsinR32((nextRingY - center.y)/radius));
		
		for(u32 sIndex = 0; sIndex < numSegments; sIndex++)
		{
			r32 angle1 = sIndex * radiusStep;
			r32 angle2 = (sIndex+1) * radiusStep;
			v3 ringPos2 = NewVec3(center.x + CosR32(angle1) * ringRadius, ringY, center.z + SinR32(angle1) * ringRadius);
			v3 ringPos1 = NewVec3(center.x + CosR32(angle2) * ringRadius, ringY, center.z + SinR32(angle2) * ringRadius);
			v3 nextRingPos2 = NewVec3(center.x + CosR32(angle1) * nextRingRadius, nextRingY, center.z + SinR32(angle1) * nextRingRadius);
			v3 nextRingPos1 = NewVec3(center.x + CosR32(angle2) * nextRingRadius, nextRingY, center.z + SinR32(angle2) * nextRingRadius);
			v3 ringNorm2 = Vec3Normalize(ringPos2 - center);
			v3 ringNorm1 = Vec3Normalize(ringPos1 - center);
			v3 nextRingNorm2 = Vec3Normalize(nextRingPos2 - center);
			v3 nextRingNorm1 = Vec3Normalize(nextRingPos1 - center);
			
			//bottom cap
			if (rIndex == 0)
			{
				Assert(vert + 3 <= vertsOut+expectedNumVerts);
				vert->position = bottomCenter; vert->texCoord = Vec2_Zero; vert->normal = Vec3_Down;     vert->color = color; vert->faceIndex = 0; vert++;
				vert->position = nextRingPos1; vert->texCoord = Vec2_Zero; vert->normal = nextRingNorm1; vert->color = color; vert->faceIndex = 0; vert++;
				vert->position = nextRingPos2; vert->texCoord = Vec2_Zero; vert->normal = nextRingNorm2; vert->color = color; vert->faceIndex = 0; vert++;
			}
			
			//Ring
			if (rIndex > 0 && rIndex < numRings)
			{
				Assert(vert + 6 <= vertsOut+expectedNumVerts);
				vert->position = ringPos1;     vert->texCoord = Vec2_Zero; vert->normal = ringNorm1;     vert->color = color; vert->faceIndex = rIndex; vert++;
				vert->position = nextRingPos1; vert->texCoord = Vec2_Zero; vert->normal = nextRingNorm1; vert->color = color; vert->faceIndex = rIndex; vert++;
				vert->position = nextRingPos2; vert->texCoord = Vec2_Zero; vert->normal = nextRingNorm2; vert->color = color; vert->faceIndex = rIndex; vert++;
				
				vert->position = ringPos1;     vert->texCoord = Vec2_Zero; vert->normal = ringNorm1;     vert->color = color; vert->faceIndex = rIndex; vert++;
				vert->position = nextRingPos2; vert->texCoord = Vec2_Zero; vert->normal = nextRingNorm2; vert->color = color; vert->faceIndex = rIndex; vert++;
				vert->position = ringPos2;     vert->texCoord = Vec2_Zero; vert->normal = ringNorm2;     vert->color = color; vert->faceIndex = rIndex; vert++;
			}
			
			//top cap
			if (rIndex == numRings)
			{
				Assert(vert + 3 <= vertsOut+expectedNumVerts);
				vert->position = topCenter; vert->texCoord = Vec2_Zero; vert->normal = Vec3_Up;   vert->color = color; vert->faceIndex = numRings; vert++;
				vert->position = ringPos2;  vert->texCoord = Vec2_Zero; vert->normal = ringNorm2; vert->color = color; vert->faceIndex = numRings; vert++;
				vert->position = ringPos1;  vert->texCoord = Vec2_Zero; vert->normal = ringNorm1; vert->color = color; vert->faceIndex = numRings; vert++;
			}
		}
	}
}

PrimitiveVertex_t* SpherePrimitiveCreateArena(v3 center, r32 radius, u32 numRings, u32 numSegments, Color_t color, rec sourceRec, MemoryArena_t* arenaPntr, u32* numVertsOut = nullptr)
{
	u32 numVerts = SpherePrimitiveGetNumVerts(numRings, numSegments);
	if (numVertsOut != nullptr) { *numVertsOut = numVerts; }
	PrimitiveVertex_t* result = PushArray(arenaPntr, PrimitiveVertex_t, numVerts);
	SpherePrimitiveCreate(center, radius, numRings, numSegments, color, sourceRec, result);
	return result;
}

// +--------------------------------------------------------------+
// |                      Cylinder Primitive                      |
// +--------------------------------------------------------------+
//faceIndex order: 0=bottom, 1=sides, 2=top

u32 CylinderPrimitiveGetNumVerts(u32 numSegments)
{
	return 3*4*numSegments;
}

//TODO: Implement texture coordinates on this somehow
void CylinderPrimitiveCreate(v3 bottomCenter, r32 height, r32 radius, u32 numSegments, Color_t color, rec sourceRec, PrimitiveVertex_t* vertsOut)
{
	Assert(vertsOut != nullptr);
	u32 expectedNumVerts = CylinderPrimitiveGetNumVerts(numSegments);
	
	r32 radiusStep = 2*Pi32/(r32)(numSegments);
	PrimitiveVertex_t* vert = &vertsOut[0];
	v3 topCenter = bottomCenter + NewVec3(0, height, 0);
	
	for (u32 sIndex = 0; sIndex < numSegments; sIndex++)
	{
		Assert(vert + 4*3 <= vertsOut + expectedNumVerts);
		
		r32 angle1 = radiusStep * (sIndex+1);
		r32 angle2 = radiusStep * sIndex;
		v3 bottomPos1 = bottomCenter + NewVec3(CosR32(angle1)*radius, 0, SinR32(angle1)*radius);
		v3 bottomPos2 = bottomCenter + NewVec3(CosR32(angle2)*radius, 0, SinR32(angle2)*radius);
		v3 topPos1 = topCenter + NewVec3(CosR32(angle1)*radius, 0, SinR32(angle1)*radius);
		v3 topPos2 = topCenter + NewVec3(CosR32(angle2)*radius, 0, SinR32(angle2)*radius);
		v3 normal1 = NewVec3(CosR32(angle1), 0, SinR32(angle1));
		v3 normal2 = NewVec3(CosR32(angle2), 0, SinR32(angle2));
		
		//bottom
		vert->position = bottomCenter; vert->texCoord = Vec2_Zero; vert->normal = Vec3_Down; vert->color = color; vert->faceIndex = 0; vert++;
		vert->position = bottomPos1;   vert->texCoord = Vec2_Zero; vert->normal = Vec3_Down; vert->color = color; vert->faceIndex = 0; vert++;
		vert->position = bottomPos2;   vert->texCoord = Vec2_Zero; vert->normal = Vec3_Down; vert->color = color; vert->faceIndex = 0; vert++;
		
		//side
		vert->position = bottomPos1; vert->texCoord = Vec2_Zero; vert->normal = normal1; vert->color = color; vert->faceIndex = 1; vert++;
		vert->position = topPos1;    vert->texCoord = Vec2_Zero; vert->normal = normal1; vert->color = color; vert->faceIndex = 1; vert++;
		vert->position = topPos2;    vert->texCoord = Vec2_Zero; vert->normal = normal2; vert->color = color; vert->faceIndex = 1; vert++;
		
		vert->position = bottomPos1; vert->texCoord = Vec2_Zero; vert->normal = normal1; vert->color = color; vert->faceIndex = 1; vert++;
		vert->position = topPos2;    vert->texCoord = Vec2_Zero; vert->normal = normal2; vert->color = color; vert->faceIndex = 1; vert++;
		vert->position = bottomPos2; vert->texCoord = Vec2_Zero; vert->normal = normal2; vert->color = color; vert->faceIndex = 1; vert++;
		
		//top
		vert->position = topCenter; vert->texCoord = Vec2_Zero; vert->normal = Vec3_Up; vert->color = color; vert->faceIndex = 2; vert++;
		vert->position = topPos2;   vert->texCoord = Vec2_Zero; vert->normal = Vec3_Up; vert->color = color; vert->faceIndex = 2; vert++;
		vert->position = topPos1;   vert->texCoord = Vec2_Zero; vert->normal = Vec3_Up; vert->color = color; vert->faceIndex = 2; vert++;
	}
}

PrimitiveVertex_t* CylinderPrimitiveCreateArena(v3 bottomCenter, r32 height, r32 radius, u32 numSegments, Color_t color, rec sourceRec, MemoryArena_t* arenaPntr, u32* numVertsOut = nullptr)
{
	u32 numVerts = CylinderPrimitiveGetNumVerts(numSegments);
	if (numVertsOut != nullptr) { *numVertsOut = numVerts; }
	PrimitiveVertex_t* result = PushArray(arenaPntr, PrimitiveVertex_t, numVerts);
	CylinderPrimitiveCreate(bottomCenter, height, radius, numSegments, color, sourceRec, result);
	return result;
}

#if 0
struct PrimitiveVertex_t
{
	r32 x, y, z, w;
	r32 r, g, b, a;
	r32 tX, tY;
	r32 nX, nY, nZ;
};

struct CubePrimitiveInfo_t
{
	r32 sideLength;
	bool centered;
	
	u32 numVertices;
	u32 numTriangles;
	
	r32 volume;
	r32 surfaceArea;
};
CubePrimitiveInfo_t GetCubePrimitiveInfo(r32 sideLength, bool centered)
{
	return {
		sideLength, centered,
		
		6*6, 6*2,
		
		sideLength*sideLength*sideLength,
		sideLength*sideLength*6,
	};
}

void CubePrimitive(PrimitiveVertex_t* vertexBuffer, r32 sideLength, bool centered)
{
	Vec3_t TBL, TBR, TFL, TFR, BBL, BBR, BFL, BFR; 
	
	if (centered)
	{
		TBL = { (-sideLength/2.0f),  (sideLength/2.0f), (-sideLength/2.0f) };
		TBR = {  (sideLength/2.0f),  (sideLength/2.0f), (-sideLength/2.0f) };
		TFL = { (-sideLength/2.0f),  (sideLength/2.0f),  (sideLength/2.0f) };
		TFR = {  (sideLength/2.0f),  (sideLength/2.0f),  (sideLength/2.0f) };
		BBL = { (-sideLength/2.0f), (-sideLength/2.0f), (-sideLength/2.0f) };
		BBR = {  (sideLength/2.0f), (-sideLength/2.0f), (-sideLength/2.0f) };
		BFL = { (-sideLength/2.0f), (-sideLength/2.0f),  (sideLength/2.0f) };
		BFR = {  (sideLength/2.0f), (-sideLength/2.0f),  (sideLength/2.0f) };
	}
	else
	{
		TBL = {       0.0f, sideLength,       0.0f };
		TBR = { sideLength, sideLength,       0.0f };
		TFL = {       0.0f, sideLength, sideLength };
		TFR = { sideLength, sideLength, sideLength };
		BBL = {       0.0f,       0.0f,       0.0f };
		BBR = { sideLength,       0.0f,       0.0f };
		BFL = {       0.0f,       0.0f, sideLength };
		BFR = { sideLength,       0.0f, sideLength };
	}
	
	//Back Face
	vertexBuffer[0].x = TBL.x; vertexBuffer[0].y = TBL.y; vertexBuffer[0].z = TBL.z;
	vertexBuffer[0].r = 1;     vertexBuffer[0].g = 1;     vertexBuffer[0].b = 1; vertexBuffer[0].a = 1;
	vertexBuffer[0].tX = 1;    vertexBuffer[0].tY = 0; 
	vertexBuffer[0].nX = 0;    vertexBuffer[0].nY = 0;    vertexBuffer[0].nZ = -1;
	
	vertexBuffer[1].x = TBR.x; vertexBuffer[1].y = TBR.y; vertexBuffer[1].z = TBR.z;
	vertexBuffer[1].r = 1;     vertexBuffer[1].g = 1;     vertexBuffer[1].b = 1; vertexBuffer[1].a = 1;
	vertexBuffer[1].tX = 0;    vertexBuffer[1].tY = 0; 
	vertexBuffer[1].nX = 0;    vertexBuffer[1].nY = 0;    vertexBuffer[1].nZ = -1;
	
	vertexBuffer[2].x = BBR.x; vertexBuffer[2].y = BBR.y; vertexBuffer[2].z = BBR.z;
	vertexBuffer[2].r = 1;     vertexBuffer[2].g = 1;     vertexBuffer[2].b = 1; vertexBuffer[2].a = 1;
	vertexBuffer[2].tX = 0;    vertexBuffer[2].tY = 1; 
	vertexBuffer[2].nX = 0;    vertexBuffer[2].nY = 0;    vertexBuffer[2].nZ = -1;
	
	
	vertexBuffer[3].x = TBL.x; vertexBuffer[3].y = TBL.y; vertexBuffer[3].z = TBL.z;
	vertexBuffer[3].r = 1;     vertexBuffer[3].g = 1;     vertexBuffer[3].b = 1; vertexBuffer[3].a = 1;
	vertexBuffer[3].tX = 1;    vertexBuffer[3].tY = 0; 
	vertexBuffer[3].nX = 0;    vertexBuffer[3].nY = 0;    vertexBuffer[3].nZ = -1;
	
	vertexBuffer[4].x = BBR.x; vertexBuffer[4].y = BBR.y; vertexBuffer[4].z = BBR.z;
	vertexBuffer[4].r = 1;     vertexBuffer[4].g = 1;     vertexBuffer[4].b = 1; vertexBuffer[4].a = 1;
	vertexBuffer[4].tX = 0;    vertexBuffer[4].tY = 1;
	vertexBuffer[4].nX = 0;    vertexBuffer[4].nY = 0;    vertexBuffer[4].nZ = -1;
	
	vertexBuffer[5].x = BBL.x; vertexBuffer[5].y = BBL.y; vertexBuffer[5].z = BBL.z;
	vertexBuffer[5].r = 1;     vertexBuffer[5].g = 1;     vertexBuffer[5].b = 1; vertexBuffer[5].a = 1;
	vertexBuffer[5].tX = 1;    vertexBuffer[5].tY = 1; 
	vertexBuffer[5].nX = 0;    vertexBuffer[5].nY = 0;    vertexBuffer[5].nZ = -1;
	
	
	//Right Face
	vertexBuffer[6].x = TBR.x; vertexBuffer[6].y = TBR.y; vertexBuffer[6].z = TBR.z;
	vertexBuffer[6].r = 1;     vertexBuffer[6].g = 1;     vertexBuffer[6].b = 1; vertexBuffer[6].a = 1;
	vertexBuffer[6].tX = 1;    vertexBuffer[6].tY = 0; 
	vertexBuffer[6].nX = 1;    vertexBuffer[6].nY = 0;    vertexBuffer[6].nZ = 0;
	
	vertexBuffer[7].x = TFR.x; vertexBuffer[7].y = TFR.y; vertexBuffer[7].z = TFR.z;
	vertexBuffer[7].r = 1;     vertexBuffer[7].g = 1;     vertexBuffer[7].b = 1; vertexBuffer[7].a = 1;
	vertexBuffer[7].tX = 0;    vertexBuffer[7].tY = 0; 
	vertexBuffer[7].nX = 1;    vertexBuffer[7].nY = 0;    vertexBuffer[7].nZ = 0;
	
	vertexBuffer[8].x = BFR.x; vertexBuffer[8].y = BFR.y; vertexBuffer[8].z = BFR.z;
	vertexBuffer[8].r = 1;     vertexBuffer[8].g = 1;     vertexBuffer[8].b = 1; vertexBuffer[8].a = 1;
	vertexBuffer[8].tX = 0;    vertexBuffer[8].tY = 1; 
	vertexBuffer[8].nX = 1;    vertexBuffer[8].nY = 0;    vertexBuffer[8].nZ = 0;
	
	
	vertexBuffer[9].x = TBR.x; vertexBuffer[9].y = TBR.y; vertexBuffer[9].z = TBR.z;
	vertexBuffer[9].r = 1;     vertexBuffer[9].g = 1;     vertexBuffer[9].b = 1; vertexBuffer[9].a = 1;
	vertexBuffer[9].tX = 1;    vertexBuffer[9].tY = 0; 
	vertexBuffer[9].nX = 1;    vertexBuffer[9].nY = 0;    vertexBuffer[9].nZ = 0;
	
	vertexBuffer[10].x = BFR.x; vertexBuffer[10].y = BFR.y; vertexBuffer[10].z = BFR.z;
	vertexBuffer[10].r = 1;     vertexBuffer[10].g = 1;     vertexBuffer[10].b = 1; vertexBuffer[10].a = 1;
	vertexBuffer[10].tX = 0;    vertexBuffer[10].tY = 1; 
	vertexBuffer[10].nX = 1;    vertexBuffer[10].nY = 0;    vertexBuffer[10].nZ = 0;
	
	vertexBuffer[11].x = BBR.x; vertexBuffer[11].y = BBR.y; vertexBuffer[11].z = BBR.z;
	vertexBuffer[11].r = 1;     vertexBuffer[11].g = 1;     vertexBuffer[11].b = 1; vertexBuffer[11].a = 1;
	vertexBuffer[11].tX = 1;    vertexBuffer[11].tY = 1; 
	vertexBuffer[11].nX = 1;    vertexBuffer[11].nY = 0;    vertexBuffer[11].nZ = 0;
	
	
	//Front Face
	vertexBuffer[12].x = TFR.x; vertexBuffer[12].y = TFR.y; vertexBuffer[12].z = TFR.z;
	vertexBuffer[12].r = 1;     vertexBuffer[12].g = 1;     vertexBuffer[12].b = 1; vertexBuffer[12].a = 1;
	vertexBuffer[12].tX = 1;    vertexBuffer[12].tY = 0; 
	vertexBuffer[12].nX = 0;    vertexBuffer[12].nY = 0;    vertexBuffer[12].nZ = 1;
	
	vertexBuffer[13].x = TFL.x; vertexBuffer[13].y = TFL.y; vertexBuffer[13].z = TFL.z;
	vertexBuffer[13].r = 1;     vertexBuffer[13].g = 1;     vertexBuffer[13].b = 1; vertexBuffer[13].a = 1;
	vertexBuffer[13].tX = 0;    vertexBuffer[13].tY = 0; 
	vertexBuffer[13].nX = 0;    vertexBuffer[13].nY = 0;    vertexBuffer[13].nZ = 1;
	
	vertexBuffer[14].x = BFL.x; vertexBuffer[14].y = BFL.y; vertexBuffer[14].z = BFL.z;
	vertexBuffer[14].r = 1;     vertexBuffer[14].g = 1;     vertexBuffer[14].b = 1; vertexBuffer[14].a = 1;
	vertexBuffer[14].tX = 0;    vertexBuffer[14].tY = 1; 
	vertexBuffer[14].nX = 0;    vertexBuffer[14].nY = 0;    vertexBuffer[14].nZ = 1;
	
	
	vertexBuffer[15].x = TFR.x; vertexBuffer[15].y = TFR.y; vertexBuffer[15].z = TFR.z;
	vertexBuffer[15].r = 1;     vertexBuffer[15].g = 1;     vertexBuffer[15].b = 1; vertexBuffer[15].a = 1;
	vertexBuffer[15].tX = 1;    vertexBuffer[15].tY = 0; 
	vertexBuffer[15].nX = 0;    vertexBuffer[15].nY = 0;    vertexBuffer[15].nZ = 1;
	
	vertexBuffer[16].x = BFL.x; vertexBuffer[16].y = BFL.y; vertexBuffer[16].z = BFL.z;
	vertexBuffer[16].r = 1;     vertexBuffer[16].g = 1;     vertexBuffer[16].b = 1; vertexBuffer[16].a = 1;
	vertexBuffer[16].tX = 0;    vertexBuffer[16].tY = 1; 
	vertexBuffer[16].nX = 0;    vertexBuffer[16].nY = 0;    vertexBuffer[16].nZ = 1;
	
	vertexBuffer[17].x = BFR.x; vertexBuffer[17].y = BFR.y; vertexBuffer[17].z = BFR.z;
	vertexBuffer[17].r = 1;     vertexBuffer[17].g = 1;     vertexBuffer[17].b = 1; vertexBuffer[17].a = 1;
	vertexBuffer[17].tX = 1;    vertexBuffer[17].tY = 1; 
	vertexBuffer[17].nX = 0;    vertexBuffer[17].nY = 0;    vertexBuffer[17].nZ = 1;
	
	
	//Left Face
	vertexBuffer[18].x = TFL.x; vertexBuffer[18].y = TFL.y; vertexBuffer[18].z = TFL.z;
	vertexBuffer[18].r = 1;     vertexBuffer[18].g = 1;     vertexBuffer[18].b = 1; vertexBuffer[18].a = 1;
	vertexBuffer[18].tX = 1;    vertexBuffer[18].tY = 0; 
	vertexBuffer[18].nX = -1;   vertexBuffer[18].nY = 0;    vertexBuffer[18].nZ = 0;
	
	vertexBuffer[19].x = TBL.x; vertexBuffer[19].y = TBL.y; vertexBuffer[19].z = TBL.z;
	vertexBuffer[19].r = 1;     vertexBuffer[19].g = 1;     vertexBuffer[19].b = 1; vertexBuffer[19].a = 1;
	vertexBuffer[19].tX = 0;    vertexBuffer[19].tY = 0; 
	vertexBuffer[19].nX = -1;   vertexBuffer[19].nY = 0;    vertexBuffer[19].nZ = 0;
	
	vertexBuffer[20].x = BBL.x; vertexBuffer[20].y = BBL.y; vertexBuffer[20].z = BBL.z;
	vertexBuffer[20].r = 1;     vertexBuffer[20].g = 1;     vertexBuffer[20].b = 1; vertexBuffer[20].a = 1;
	vertexBuffer[20].tX = 0;    vertexBuffer[20].tY = 1; 
	vertexBuffer[20].nX = -1;   vertexBuffer[20].nY = 0;    vertexBuffer[20].nZ = 0;
	
	
	vertexBuffer[21].x = TFL.x; vertexBuffer[21].y = TFL.y; vertexBuffer[21].z = TFL.z;
	vertexBuffer[21].r = 1;     vertexBuffer[21].g = 1;     vertexBuffer[21].b = 1; vertexBuffer[21].a = 1;
	vertexBuffer[21].tX = 1;    vertexBuffer[21].tY = 0; 
	vertexBuffer[21].nX = -1;   vertexBuffer[21].nY = 0;    vertexBuffer[21].nZ = 0;
	
	vertexBuffer[22].x = BBL.x; vertexBuffer[22].y = BBL.y; vertexBuffer[22].z = BBL.z;
	vertexBuffer[22].r = 1;     vertexBuffer[22].g = 1;     vertexBuffer[22].b = 1; vertexBuffer[22].a = 1;
	vertexBuffer[22].tX = 0;    vertexBuffer[22].tY = 1; 
	vertexBuffer[22].nX = -1;   vertexBuffer[22].nY = 0;    vertexBuffer[22].nZ = 0;
	
	vertexBuffer[23].x = BFL.x; vertexBuffer[23].y = BFL.y; vertexBuffer[23].z = BFL.z;
	vertexBuffer[23].r = 1;     vertexBuffer[23].g = 1;     vertexBuffer[23].b = 1; vertexBuffer[23].a = 1;
	vertexBuffer[23].tX = 1;    vertexBuffer[23].tY = 1; 
	vertexBuffer[23].nX = -1;   vertexBuffer[23].nY = 0;    vertexBuffer[23].nZ = 0;
	
	
	//Top Face
	vertexBuffer[24].x = TBR.x; vertexBuffer[24].y = TBR.y; vertexBuffer[24].z = TBR.z;
	vertexBuffer[24].r = 1;     vertexBuffer[24].g = 1;     vertexBuffer[24].b = 1; vertexBuffer[24].a = 1;
	vertexBuffer[24].tX = 1;    vertexBuffer[24].tY = 0; 
	vertexBuffer[24].nX = 0;    vertexBuffer[24].nY = 1;    vertexBuffer[24].nZ = 0;
	
	vertexBuffer[25].x = TBL.x; vertexBuffer[25].y = TBL.y; vertexBuffer[25].z = TBL.z;
	vertexBuffer[25].r = 1;     vertexBuffer[25].g = 1;     vertexBuffer[25].b = 1; vertexBuffer[25].a = 1;
	vertexBuffer[25].tX = 0;    vertexBuffer[25].tY = 0; 
	vertexBuffer[25].nX = 0;    vertexBuffer[25].nY = 1;    vertexBuffer[25].nZ = 0;
	
	vertexBuffer[26].x = TFL.x; vertexBuffer[26].y = TFL.y; vertexBuffer[26].z = TFL.z;
	vertexBuffer[26].r = 1;     vertexBuffer[26].g = 1;     vertexBuffer[26].b = 1; vertexBuffer[26].a = 1;
	vertexBuffer[26].tX = 0;    vertexBuffer[26].tY = 1; 
	vertexBuffer[26].nX = 0;    vertexBuffer[26].nY = 1;    vertexBuffer[26].nZ = 0;
	
	
	vertexBuffer[27].x = TBR.x; vertexBuffer[27].y = TBR.y; vertexBuffer[27].z = TBR.z;
	vertexBuffer[27].r = 1;     vertexBuffer[27].g = 1;     vertexBuffer[27].b = 1; vertexBuffer[27].a = 1;
	vertexBuffer[27].tX = 1;    vertexBuffer[27].tY = 0; 
	vertexBuffer[27].nX = 0;    vertexBuffer[27].nY = 1;    vertexBuffer[27].nZ = 0;
	
	vertexBuffer[28].x = TFL.x; vertexBuffer[28].y = TFL.y; vertexBuffer[28].z = TFL.z;
	vertexBuffer[28].r = 1;     vertexBuffer[28].g = 1;     vertexBuffer[28].b = 1; vertexBuffer[28].a = 1;
	vertexBuffer[28].tX = 0;    vertexBuffer[28].tY = 1; 
	vertexBuffer[28].nX = 0;    vertexBuffer[28].nY = 1;    vertexBuffer[28].nZ = 0;
	
	vertexBuffer[29].x = TFR.x; vertexBuffer[29].y = TFR.y; vertexBuffer[29].z = TFR.z;
	vertexBuffer[29].r = 1;     vertexBuffer[29].g = 1;     vertexBuffer[29].b = 1; vertexBuffer[29].a = 1;
	vertexBuffer[29].tX = 1;    vertexBuffer[29].tY = 1; 
	vertexBuffer[29].nX = 0;    vertexBuffer[29].nY = 1;    vertexBuffer[29].nZ = 0;
	
	
	//Bottom Face
	vertexBuffer[30].x = BFR.x; vertexBuffer[30].y = BFR.y; vertexBuffer[30].z = BFR.z;
	vertexBuffer[30].r = 1;     vertexBuffer[30].g = 1;     vertexBuffer[30].b = 1; vertexBuffer[30].a = 1;
	vertexBuffer[30].tX = 1;    vertexBuffer[30].tY = 0; 
	vertexBuffer[30].nX = 0;    vertexBuffer[30].nY = -1;    vertexBuffer[30].nZ = 0;
	
	vertexBuffer[31].x = BFL.x; vertexBuffer[31].y = BFL.y; vertexBuffer[31].z = BFL.z;
	vertexBuffer[31].r = 1;     vertexBuffer[31].g = 1;     vertexBuffer[31].b = 1; vertexBuffer[31].a = 1;
	vertexBuffer[31].tX = 0;    vertexBuffer[31].tY = 0; 
	vertexBuffer[31].nX = 0;    vertexBuffer[31].nY = -1;    vertexBuffer[31].nZ = 0;
	
	vertexBuffer[32].x = BBL.x; vertexBuffer[32].y = BBL.y; vertexBuffer[32].z = BBL.z;
	vertexBuffer[32].r = 1;     vertexBuffer[32].g = 1;     vertexBuffer[32].b = 1; vertexBuffer[32].a = 1;
	vertexBuffer[32].tX = 0;    vertexBuffer[32].tY = 1; 
	vertexBuffer[32].nX = 0;    vertexBuffer[32].nY = -1;    vertexBuffer[32].nZ = 0;
	
	
	vertexBuffer[33].x = BFR.x; vertexBuffer[33].y = BFR.y; vertexBuffer[33].z = BFR.z;
	vertexBuffer[33].r = 1;     vertexBuffer[33].g = 1;     vertexBuffer[33].b = 1; vertexBuffer[33].a = 1;
	vertexBuffer[33].tX = 1;    vertexBuffer[33].tY = 0; 
	vertexBuffer[33].nX = 0;    vertexBuffer[33].nY = -1;    vertexBuffer[33].nZ = 0;
	
	vertexBuffer[34].x = BBL.x; vertexBuffer[34].y = BBL.y; vertexBuffer[34].z = BBL.z;
	vertexBuffer[34].r = 1;     vertexBuffer[34].g = 1;     vertexBuffer[34].b = 1; vertexBuffer[34].a = 1;
	vertexBuffer[34].tX = 0;    vertexBuffer[34].tY = 1; 
	vertexBuffer[34].nX = 0;    vertexBuffer[34].nY = -1;    vertexBuffer[34].nZ = 0;
	
	vertexBuffer[35].x = BBR.x; vertexBuffer[35].y = BBR.y; vertexBuffer[35].z = BBR.z;
	vertexBuffer[35].r = 1;     vertexBuffer[35].g = 1;     vertexBuffer[35].b = 1; vertexBuffer[35].a = 1;
	vertexBuffer[35].tX = 1;    vertexBuffer[35].tY = 1; 
	vertexBuffer[35].nX = 0;    vertexBuffer[35].nY = -1;    vertexBuffer[35].nZ = 0;
}


struct SpherePrimitiveInfo_t
{
	r32 radius;
	u32 numRings;
	u32 numSectors;
	
	u32 numVertices;
	u32 numIndices;
	u32 numTriangles;
	
	r32 volume;
	r32 surfaceArea;
};
SpherePrimitiveInfo_t GetSpherePrimitiveInfo(r32 radius, u32 numRings, u32 numSectors)
{
	return {
		radius, numRings, numSectors,
		
		numRings*numSectors,
		(numRings-1) * (numSectors-1) * 6,
		(numRings-1) * (numSectors-1) * 2,
		
		(4.0f*Pi32*radius*radius*radius)/3.0f,
		4.0f*Pi32*radius*radius,
		
	};
}

void SpherePrimitive(PrimitiveVertex_t* vertexBuffer, u32* indexBuffer, r32 radius, u32 numRings, u32 numSectors)
{
	r32 const stepR = 1.0f/(r32)(numRings-1);
	r32 const stepS = 1.0f/(r32)(numSectors-1);
	u32 currentRing, currentSector;

	PrimitiveVertex_t* currentVertex = vertexBuffer;
	for(currentRing = 0; currentRing < numRings; currentRing++) 
	{
		for(currentSector = 0; currentSector < numSectors; currentSector++) 
		{
			*currentVertex = {};
			
			r32 const y = sinf(-(Pi32/2) + Pi32 * currentRing * stepR);
			r32 const x = cosf(2*Pi32 * currentSector * stepS) * sinf(Pi32 * currentRing * stepR);
			r32 const z = sinf(2*Pi32 * currentSector * stepS) * sinf(Pi32 * currentRing * stepR);
			
			currentVertex->x = x * radius;
			currentVertex->y = y * radius;
			currentVertex->z = z * radius;
			
			currentVertex->r = 1.0f;
			currentVertex->g = 1.0f;
			currentVertex->b = 1.0f;
			currentVertex->a = 1.0f;
			
			currentVertex->tX = currentSector*stepS;
			currentVertex->tY = currentRing*stepR;
			
			currentVertex->nX = x;
			currentVertex->nY = y;
			currentVertex->nZ = z;
			
			currentVertex++;
		}
	}
	
	u32 numIndices = 0;
	u32* currentIndex = indexBuffer;
	for(currentRing = 0; currentRing < numRings-1; currentRing++) 
	{
		for(currentSector = 0; currentSector < numSectors-1; currentSector++) 
		{
			*currentIndex =  currentRing    * numSectors +  currentSector;    currentIndex++; numIndices++;
			*currentIndex =  currentRing    * numSectors + (currentSector+1); currentIndex++; numIndices++;
			*currentIndex = (currentRing+1) * numSectors +  currentSector;    currentIndex++; numIndices++;
			
			*currentIndex = (currentRing+1) * numSectors +  currentSector;    currentIndex++; numIndices++;
			*currentIndex =  currentRing    * numSectors + (currentSector+1); currentIndex++; numIndices++;
			*currentIndex = (currentRing+1) * numSectors + (currentSector+1); currentIndex++; numIndices++;
			
			// DEBUG_WriteLine("2 Triangles");
		}
	}
	
	SpherePrimitiveInfo_t info = GetSpherePrimitiveInfo(radius, numRings, numSectors);
	Assert(numIndices == info.numIndices);
}
#endif

#endif //_PRIMITIVES_3D_H