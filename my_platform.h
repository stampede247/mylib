/*
File:   my_platform.h
Author: Taylor Robbins
Date:   10\23\2020
Description:
	** Detects which platform is being compiled on\for and sets up the proper defines that we will use everywhere else
	** This file can be included directly before mylib.h or you can just use mylib.h if you don't need to declarations beforehand
*/

#ifndef _MY_PLATFORM_H
#define _MY_PLATFORM_H

// +--------------------------------------------------------------+
// |                    Check Required Defines                    |
// +--------------------------------------------------------------+
#if !defined(WINDOWS_COMPILATION) && !defined(OSX_COMPILATION) && !defined(LINUX_COMPILATION)
	#error You must define WINDOWS_COMPILATION, OSX_COMPILATION, or LINUX_COMPILATION before including mylib.h
#endif
#if defined(WINDOWS_COMPILATION)
	#if defined(OSX_COMPILATION) || defined(LINUX_COMPILATION)
		#error You must only define one compilation type before including mylib.h
	#endif
#endif
#if defined(OSX_COMPILATION)
	#if defined(LINUX_COMPILATION)
		#error You must only define one compilation type before including mylib.h
	#endif
#endif

#if defined(WINDOWS_COMPILATION)
	#undef WINDOWS_COMPILATION
	#define WINDOWS_COMPILATION 1
#else
	#define WINDOWS_COMPILATION 0
#endif

#if defined(OSX_COMPILATION)
	#undef OSX_COMPILATION
	#define OSX_COMPILATION 1
#else
	#define OSX_COMPILATION 0
#endif

#if defined(LINUX_COMPILATION)
	#undef LINUX_COMPILATION
	#define LINUX_COMPILATION 1
#else
	#define LINUX_COMPILATION 0
#endif

#endif //  _MY_PLATFORM_H
