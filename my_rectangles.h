/*
File:   my_rectangles.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the Rectangle_t and Rectanglei_t structures (alias rec, and reci)
	** and implements all the base functions that operate on or create them

This file is #included by default from mylib.h
*/

#ifndef _MY_RECTANGLES_H
#define _MY_RECTANGLES_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
struct Rectangle_t
{
	union
	{
		v2 topLeft;
		struct { r32 x,    y; };
		struct { r32 left, top; };
	};
	union
	{
		v2 size;
		struct { r32 w,     h; };
		struct { r32 width, height; };
	};
	
	inline Rectangle_t& operator += (v2 vector)
	{
		this->topLeft += vector;
		return *this;
	}
	inline Rectangle_t& operator -= (v2 vector)
	{
		this->topLeft -= vector;
		return *this;
	}
};

struct Rectanglei_t
{
	union
	{
		v2i topLeft;
		struct { i32 x,    y; };
		struct { i32 left, top; };
	};
	union
	{
		v2i size;
		struct { i32 w,     h; };
		struct { i32 width, height; };
	};
};

struct OBB2_t
{
	union
	{
		v2 center;
		v2 position;
		struct { r32 centerX, centerY; };
		struct { r32 x, y; };
	};
	union
	{
		v2 size;
		v2 extents;
		struct { r32 width, height; };
		struct { r32 w, h; };
	};
	r32 rotation;
};

// +--------------------------------------------------------------+
// |                        Alias Typedefs                        |
// +--------------------------------------------------------------+
typedef Rectangle_t  rec;
typedef Rectanglei_t reci;
typedef OBB2_t       obb2;

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline rec NewRec(r32 left, r32 top, r32 width, r32 height)
{
	rec result = {};
	result.left = left;
	result.top = top;
	result.width = width;
	result.height = height;
	return result;
}
inline rec NewRec(v2 topLeft, v2 size)
{
	rec result = {};
	result.topLeft = topLeft;
	result.size = size;
	return result;
}
inline rec NewRec(reci rectangle)
{
	rec result;
	result.x = (r32)rectangle.x;
	result.y = (r32)rectangle.y;
	result.width = (r32)rectangle.width;
	result.height = (r32)rectangle.height;
	return result;
}
inline rec NewRecCentered(v2 center, v2 size)
{
	rec result = {};
	result.x = center.x - size.x/2.0f;
	result.y = center.y - size.y/2.0f;
	result.w = size.x;
	result.h = size.y;
	return result;
}
inline rec NewRecCentered(r32 x, r32 y, r32 width, r32 height)
{
	rec result = {};
	result.x = x - width/2.0f;
	result.y = y - height/2.0f;
	result.w = width;
	result.h = height;
	return result;
}
inline rec NewRecBetween(v2 point1, v2 point2)
{
	rec result;
	result.x = MinR32(point1.x, point2.x);
	result.y = MinR32(point1.y, point2.y);
	result.width = MaxR32(point1.x, point2.x) - result.x;
	result.height = MaxR32(point1.y, point2.y) - result.y;
	return result;
}

inline reci NewReci(i32 left, i32 top, i32 width, i32 height)
{
	reci result = {};
	result.left = left;
	result.top = top;
	result.width = width;
	result.height = height;
	return result;
}
inline reci NewReci(v2i topLeft, v2i size)
{
	reci result = {};
	result.topLeft = topLeft;
	result.size = size;
	return result;
}
inline reci NewReciCentered(v2i center, v2i size)
{
	Assert((size.x%2) == 0);
	Assert((size.y%2) == 0);
	
	reci result = {};
	result.x = center.x - size.x/2;
	result.y = center.y - size.y/2;
	result.w = size.x;
	result.h = size.y;
	return result;
}
inline reci NewReciBetween(v2i point1, v2i point2)
{
	reci result;
	result.x = MinI32(point1.x, point2.x);
	result.y = MinI32(point1.y, point2.y);
	result.width = MaxI32(point1.x, point2.x) - result.x;
	result.height = MaxI32(point1.y, point2.y) - result.y;
	return result;
}

inline obb2 NewObb2(r32 x, r32 y, r32 width, r32 height, r32 rotation)
{
	obb2 result;
	result.x = x;
	result.y = y;
	result.width = width;
	result.height = height;
	result.rotation = rotation;
	return result;
}
inline obb2 NewObb2(v2 center, v2 size, r32 rotation)
{
	obb2 result;
	result.center = center;
	result.size = size;
	result.rotation = rotation;
	return result;
}
inline obb2 NewObb2(rec rectangle)
{
	obb2 result;
	result.center = rectangle.topLeft + rectangle.size/2;
	result.size = rectangle.size;
	result.rotation = 0;
	return result;
}
inline obb2 NewObb2Offset(v2 position, v2 origin, v2 size, r32 rotation)
{
	obb2 result;
	result.center = position + Vec2Rotate(size/2 - origin, rotation);
	result.size = size;
	result.rotation = rotation;
	return result;
}

// +--------------------------------------------------------------+
// |                     Simple Value Defines                     |
// +--------------------------------------------------------------+
#define Rec_Zero  NewRec(0, 0, 0, 0)
#define Reci_Zero NewReci(0, 0, 0, 0)
#define Obb2_Zero NewObb2(0, 0, 0, 0, 0)

// +--------------------------------------------------------------+
// |                Basic Math Operation Functions                |
// +--------------------------------------------------------------+
rec RecShift(const rec& rectangle, v2 vector)
{
	rec result;
	result.topLeft = rectangle.topLeft + vector;
	result.size = rectangle.size;
	return result;
}
rec RecScale(const rec& rectangle, r32 amount)
{
	rec result;
	result.topLeft = rectangle.topLeft * amount;
	result.size = rectangle.size * amount;
	return result;
}
rec RecScale2(const rec& rectangle, v2 amount)
{
	rec result;
	result.x = rectangle.x * amount.x;
	result.y = rectangle.y * amount.y;
	result.width = rectangle.width * amount.x;
	result.height = rectangle.height * amount.y;
	return result;
}
rec RecExpand(const rec& rectangle, v2 vector)
{
	rec result;
	result.topLeft = rectangle.topLeft;
	result.size = rectangle.size + vector;
	return result;
}
rec RecInflate(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x - scalar;
	result.y = rectangle.y - scalar;
	result.width = rectangle.width + scalar*2;
	result.height = rectangle.height + scalar*2;
	return result;
}
rec RecInflate2(const rec& rectangle, v2 vector)
{
	rec result;
	result.topLeft = rectangle.topLeft - vector;
	result.size = rectangle.size + vector*2;
	return result;
}
rec RecInflateX(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x - scalar;
	result.y = rectangle.y;
	result.width = rectangle.width + scalar*2;
	result.height = rectangle.height;
	return result;
}
rec RecInflateY(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x;
	result.y = rectangle.y - scalar;
	result.width = rectangle.width;
	result.height = rectangle.height + scalar*2;
	return result;
}
rec RecDeflate(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x + scalar;
	result.y = rectangle.y + scalar;
	result.width = rectangle.width - scalar*2;
	result.height = rectangle.height - scalar*2;
	return result;
}
rec RecDeflate2(const rec& rectangle, v2 vector)
{
	rec result;
	result.topLeft = rectangle.topLeft + vector;
	result.size = rectangle.size - vector*2;
	return result;
}
rec RecDeflateX(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x + scalar;
	result.y = rectangle.y;
	result.width = rectangle.width - scalar*2;
	result.height = rectangle.height;
	return result;
}
rec RecDeflateY(const rec& rectangle, r32 scalar)
{
	rec result;
	result.x = rectangle.x;
	result.y = rectangle.y + scalar;
	result.width = rectangle.width;
	result.height = rectangle.height - scalar*2;
	return result;
}
rec RecSquarify(const rec& rectangle)
{
	rec result;
	r32 sideLength = rectangle.width;
	if (rectangle.width > rectangle.height) { sideLength = rectangle.height; }
	result.x = rectangle.x + rectangle.width/2 - sideLength/2;
	result.y = rectangle.y + rectangle.height/2 - sideLength/2;
	result.width = sideLength;
	result.height = sideLength;
	return result;
}
rec RecInvertY(const rec& rectangle)
{
	rec result;
	result.x = rectangle.x;
	result.y = rectangle.y + rectangle.height;
	result.width = rectangle.width;
	result.height = -rectangle.height;
	return result;
}
rec RecInvertX(const rec& rectangle)
{
	rec result;
	result.x = rectangle.x + rectangle.height;
	result.y = rectangle.y;
	result.width = -rectangle.width;
	result.height = rectangle.height;
	return result;
}


reci ReciShift(const reci& rectangle, v2i vector)
{
	reci result;
	result.topLeft = rectangle.topLeft + vector;
	result.size = rectangle.size;
	return result;
}
reci ReciScale(const reci& rectangle, i32 amount)
{
	reci result;
	result.topLeft = rectangle.topLeft * amount;
	result.size = rectangle.size * amount;
	return result;
}
reci ReciScale2(const reci& rectangle, v2i amount)
{
	reci result;
	result.x = rectangle.x * amount.x;
	result.y = rectangle.y * amount.y;
	result.width = rectangle.width * amount.x;
	result.height = rectangle.height * amount.y;
	return result;
}
reci ReciExpand(const reci& rectangle, v2i vector)
{
	reci result;
	result.topLeft = rectangle.topLeft;
	result.size = rectangle.size + vector;
	return result;
}
reci ReciInflate(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x - scalar;
	result.y = rectangle.y - scalar;
	result.width = rectangle.width + scalar*2;
	result.height = rectangle.height + scalar*2;
	return result;
}
reci ReciInflate2(const reci& rectangle, v2i vector)
{
	reci result;
	result.topLeft = rectangle.topLeft - vector;
	result.size = rectangle.size + vector*2;
	return result;
}
reci ReciInflateX(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x - scalar;
	result.y = rectangle.y;
	result.width = rectangle.width + scalar*2;
	result.height = rectangle.height;
	return result;
}
reci ReciInflateY(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x;
	result.y = rectangle.y - scalar;
	result.width = rectangle.width;
	result.height = rectangle.height + scalar*2;
	return result;
}
reci ReciDeflate(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x + scalar;
	result.y = rectangle.y + scalar;
	result.width = rectangle.width - scalar*2;
	result.height = rectangle.height - scalar*2;
	return result;
}
reci ReciDeflate2(const reci& rectangle, v2i vector)
{
	reci result;
	result.topLeft = rectangle.topLeft + vector;
	result.size = rectangle.size - vector*2;
	return result;
}
reci ReciDeflateX(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x + scalar;
	result.y = rectangle.y;
	result.width = rectangle.width - scalar*2;
	result.height = rectangle.height;
	return result;
}
reci ReciDeflateY(const reci& rectangle, i32 scalar)
{
	reci result;
	result.x = rectangle.x;
	result.y = rectangle.y + scalar;
	result.width = rectangle.width;
	result.height = rectangle.height - scalar*2;
	return result;
}
reci ReciSquarify(const reci& rectangle)
{
	reci result;
	i32 sideLength = rectangle.width;
	if (rectangle.width > rectangle.height) { sideLength = rectangle.height; }
	result.x = rectangle.x + rectangle.width/2 - sideLength/2;
	result.y = rectangle.y + rectangle.height/2 - sideLength/2;
	result.width = sideLength;
	result.height = sideLength;
	return result;
}
reci ReciInvertX(const reci& rectangle)
{
	reci result;
	result.x = rectangle.x + rectangle.width;
	result.y = rectangle.y;
	result.width = -rectangle.width;
	result.height = rectangle.height;
	return result;
}
reci ReciInvertY(const reci& rectangle)
{
	reci result;
	result.x = rectangle.x;
	result.y = rectangle.y + rectangle.height;
	result.width = rectangle.width;
	result.height = -rectangle.height;
	return result;
}

obb2 Obb2Shift(const obb2& box, v2 vector)
{
	obb2 result;
	result.center = box.center + vector;
	result.size = box.size;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Scale(const obb2& box, r32 amount)
{
	obb2 result;
	result.center = box.center * amount;
	result.size = box.size * amount;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Scale2(const obb2& box, v2 amount)
{
	obb2 result;
	result.x = box.x * amount.x;
	result.y = box.y * amount.y;
	result.width = box.width * amount.x;
	result.height = box.height * amount.y;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Expand(const obb2& box, v2 vector)
{
	obb2 result;
	result.center = box.center + NewVec2(CosR32(box.rotation) * (vector.x/2), SinR32(box.rotation) * (vector.y/2));
	result.size = box.size + vector;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Inflate(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width + scalar*2;
	result.height = box.height + scalar*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Inflate2(const obb2& box, v2 vector)
{
	obb2 result;
	result.center = box.center;
	result.size = box.size + vector*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2InflateX(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width + scalar*2;
	result.height = box.height;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2InflateY(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width;
	result.height = box.height + scalar*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Deflate(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width - scalar*2;
	result.height = box.height - scalar*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2Deflate2(const obb2& box, v2 vector)
{
	obb2 result;
	result.center = box.center;
	result.size = box.size - vector*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2DeflateX(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width - scalar*2;
	result.height = box.height;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2DeflateY(const obb2& box, r32 scalar)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width;
	result.height = box.height - scalar*2;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2InvertX(const obb2& box)
{
	obb2 result;
	result.center = box.center;
	result.width = -box.width;
	result.height = box.height;
	result.rotation = box.rotation;
	return result;
}
obb2 Obb2InvertY(const obb2& box)
{
	obb2 result;
	result.center = box.center;
	result.width = box.width;
	result.height = -box.height;
	result.rotation = box.rotation;
	return result;
}
v2 GetObb2RelativePos(const obb2& box, v2 point)
{
	v2 result;
	v2 rotVec = NewVec2(CosR32(box.rotation), SinR32(box.rotation));
	result.x = Vec2Dot(point - box.center, rotVec);
	result.y = Vec2Dot(point - box.center, Vec2PerpRight(rotVec));
	return result;
}
v2 GetObb2WorldPoint(const obb2& box, v2 relativeOffset)
{
	v2 result = box.center;
	v2 rotVec = NewVec2(CosR32(box.rotation), SinR32(box.rotation));
	result += (-box.width/2 + relativeOffset.x) * rotVec;
	result += (-box.height/2 + relativeOffset.y) * Vec2PerpRight(rotVec);
	return result;
}

// +--------------------------------------------------------------+
// |                      Operator Overloads                      |
// +--------------------------------------------------------------+
inline rec operator + (rec rectangle, v2 vector)   { return RecShift(rectangle, vector); }
inline rec operator - (rec rectangle, v2 vector)   { return RecShift(rectangle, -vector); }
inline rec operator * (rec rectangle, r32 scale)   { return RecScale(rectangle, scale); }
inline rec operator / (rec rectangle, r32 scale)   { return RecScale(rectangle, 1/scale); }

inline reci operator + (reci rectangle, v2i vector) { return ReciShift(rectangle, vector); }
inline reci operator - (reci rectangle, v2i vector) { return ReciShift(rectangle, -vector); }
inline reci operator * (reci rectangle, i32 scale)  { return ReciScale(rectangle, scale); }
inline reci operator / (reci rectangle, i32 scale)  { return ReciScale(rectangle, 1/scale); }

inline obb2 operator + (obb2 box, v2 vector) { return Obb2Shift(box, vector); }
inline obb2 operator - (obb2 box, v2 vector) { return Obb2Shift(box, -vector); }
inline obb2 operator * (obb2 box, r32 scale) { return Obb2Scale(box, scale); }
inline obb2 operator / (obb2 box, r32 scale) { return Obb2Scale(box, 1/scale); }

inline bool operator == (rec left, rec right) { return  (left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height); }
inline bool operator != (rec left, rec right) { return !(left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height); }

inline bool operator == (reci left, reci right) { return  (left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height); }
inline bool operator != (reci left, reci right) { return !(left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height); }

inline bool operator == (obb2 left, obb2 right) { return  (left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height && left.rotation == right.rotation); }
inline bool operator != (obb2 left, obb2 right) { return !(left.x == right.x && left.y == right.y && left.width == right.width && left.height == right.height && left.rotation == right.rotation); }

// +--------------------------------------------------------------+
// |                      Helpful Functions                       |
// +--------------------------------------------------------------+
v2 GetObb2Vertex(obb2 box, u32 vIndex)
{
	u32 actualIndex = (vIndex % 4);
	v2 rightVec = NewVec2(CosR32(box.rotation), SinR32(box.rotation));
	v2 downVec = Vec2PerpRight(rightVec);
	r32 halfWidth = box.width/2;
	r32 halfHeight = box.height/2;
	//given in clockwise order
	switch (vIndex)
	{
		case 0: return NewVec2(box.x - rightVec.x*halfWidth - downVec.x*halfHeight, box.y - rightVec.y*halfWidth - downVec.y*halfHeight); //topLeft
		case 1: return NewVec2(box.x + rightVec.x*halfWidth - downVec.x*halfHeight, box.y + rightVec.y*halfWidth - downVec.y*halfHeight); //topRight
		case 2: return NewVec2(box.x - rightVec.x*halfWidth + downVec.x*halfHeight, box.y - rightVec.y*halfWidth + downVec.y*halfHeight); //bottomRight
		case 3: return NewVec2(box.x + rightVec.x*halfWidth + downVec.x*halfHeight, box.y + rightVec.y*halfWidth + downVec.y*halfHeight); //bottomLeft
		default: Assert(false); return box.center;
	}
}
rec GetObb2Rec(obb2 box)
{
	v2 rightVec = NewVec2(CosR32(box.rotation), SinR32(box.rotation));
	v2 downVec = Vec2PerpRight(rightVec);
	r32 halfWidth = box.width/2;
	r32 halfHeight = box.height/2;
	r32 minX = MinR32(
		box.x - rightVec.x*halfWidth - downVec.x*halfHeight,
		box.x + rightVec.x*halfWidth - downVec.x*halfHeight,
		box.x - rightVec.x*halfWidth + downVec.x*halfHeight,
		box.x + rightVec.x*halfWidth + downVec.x*halfHeight
	);
	r32 maxX = MaxR32(
		box.x - rightVec.x*halfWidth - downVec.x*halfHeight,
		box.x + rightVec.x*halfWidth - downVec.x*halfHeight,
		box.x - rightVec.x*halfWidth + downVec.x*halfHeight,
		box.x + rightVec.x*halfWidth + downVec.x*halfHeight
	);
	r32 minY = MinR32(
		box.y - rightVec.y*halfWidth - downVec.y*halfHeight,
		box.y + rightVec.y*halfWidth - downVec.y*halfHeight,
		box.y - rightVec.y*halfWidth + downVec.y*halfHeight,
		box.y + rightVec.y*halfWidth + downVec.y*halfHeight
	);
	r32 maxY = MaxR32(
		box.y - rightVec.y*halfWidth - downVec.y*halfHeight,
		box.y + rightVec.y*halfWidth - downVec.y*halfHeight,
		box.y - rightVec.y*halfWidth + downVec.y*halfHeight,
		box.y + rightVec.y*halfWidth + downVec.y*halfHeight
	);
	rec result;
	result.x = minX;
	result.y = minY;
	result.width = maxX - minX;
	result.height = maxY - minY;
	return result;
}

bool IsInsideRec(rec rectangle, v2 point, bool inclusive = true)
{
	if (inclusive)
	{
		return (
			point.x >= rectangle.x &&
			point.y >= rectangle.y &&
			point.x <= rectangle.x + rectangle.width &&
			point.y <= rectangle.y + rectangle.height
		);
	}
	else
	{
		return (
			point.x > rectangle.x &&
			point.y > rectangle.y &&
			point.x < rectangle.x + rectangle.width &&
			point.y < rectangle.y + rectangle.height
		);
	}
}
bool IsInsideRec(rec rectangle, v2i point, bool inclusive = true)
{
	if (inclusive)
	{
		return (
			point.x >= rectangle.x &&
			point.y >= rectangle.y &&
			point.x <= rectangle.x + rectangle.width &&
			point.y <= rectangle.y + rectangle.height
		);
	}
	else
	{
		return (
			point.x > rectangle.x &&
			point.y > rectangle.y &&
			point.x < rectangle.x + rectangle.width &&
			point.y < rectangle.y + rectangle.height
		);
	}
}
bool IsCoordInsideRec(rec rectangle, v2 coordinate)
{
	return (
		coordinate.x >= rectangle.x &&
		coordinate.y >= rectangle.y &&
		coordinate.x < rectangle.x + rectangle.width &&
		coordinate.y < rectangle.y + rectangle.height
	);
}
bool IsInsideReci(reci rectangle, v2 point, bool inclusive = true)
{
	if (inclusive)
	{
		return (
			point.x >= rectangle.x &&
			point.y >= rectangle.y &&
			point.x <= rectangle.x + rectangle.width &&
			point.y <= rectangle.y + rectangle.height
		);
	}
	else
	{
		return (
			point.x > rectangle.x &&
			point.y > rectangle.y &&
			point.x < rectangle.x + rectangle.width &&
			point.y < rectangle.y + rectangle.height
		);
	}
}
bool IsInsideReci(reci rectangle, v2i point, bool inclusive = true)
{
	if (inclusive)
	{
		return (
			point.x >= rectangle.x &&
			point.y >= rectangle.y &&
			point.x <= rectangle.x + rectangle.width &&
			point.y <= rectangle.y + rectangle.height
		);
	}
	else
	{
		return (
			point.x > rectangle.x &&
			point.y > rectangle.y &&
			point.x < rectangle.x + rectangle.width &&
			point.y < rectangle.y + rectangle.height
		);
	}
}
bool IsCoordInsideReci(reci rectangle, v2i coordinate)
{
	return (
		coordinate.x >= rectangle.x &&
		coordinate.y >= rectangle.y &&
		coordinate.x < rectangle.x + rectangle.width &&
		coordinate.y < rectangle.y + rectangle.height
	);
}
bool IsTilePosInReci(reci rectangle, v2i tilePos)
{
	return (
		tilePos.x >= rectangle.x &&
		tilePos.y >= rectangle.y &&
		tilePos.x < rectangle.x + rectangle.width &&
		tilePos.y < rectangle.y + rectangle.height
	);
}
bool IsInsideObb2(obb2 box, v2 point, bool inclusive = true)
{
	v2 relativePos = GetObb2RelativePos(box, point);
	if (inclusive)
	{
		if (AbsR32(relativePos.x) > box.width/2) { return false; }
		if (AbsR32(relativePos.y) > box.height/2) { return false; }
		return true;
	}
	else
	{
		if (AbsR32(relativePos.x) >= box.width/2) { return false; }
		if (AbsR32(relativePos.y) >= box.height/2) { return false; }
		return true;
	}
}

bool IsRecInside(rec recOuter, rec recInner, bool inclusive = true)
{
	if (inclusive)
	{
		if (recInner.x < recOuter.x) { return false; }
		if (recInner.y < recOuter.y) { return false; }
		if (recInner.x + recInner.width > recOuter.x + recOuter.width) { return false; }
		if (recInner.y + recInner.height > recOuter.y + recOuter.height) { return false; }
		return true;
	}
	else
	{
		if (recInner.x <= recOuter.x) { return false; }
		if (recInner.y <= recOuter.y) { return false; }
		if (recInner.x + recInner.width >= recOuter.x + recOuter.width) { return false; }
		if (recInner.y + recInner.height >= recOuter.y + recOuter.height) { return false; }
		return true;
	}
}
bool IsObb2InsideRec(rec recOuter, obb2 boxInner)
{
	if (!IsInsideRec(recOuter, GetObb2Vertex(boxInner, 0))) { return false; }
	if (!IsInsideRec(recOuter, GetObb2Vertex(boxInner, 1))) { return false; }
	if (!IsInsideRec(recOuter, GetObb2Vertex(boxInner, 2))) { return false; }
	if (!IsInsideRec(recOuter, GetObb2Vertex(boxInner, 3))) { return false; }
	return true;
}
bool IsRecInsideObb2(obb2 boxOuter, rec recInner)
{
	if (!IsInsideObb2(boxOuter, NewVec2(recInner.x, recInner.y))) { return false; }
	if (!IsInsideObb2(boxOuter, NewVec2(recInner.x + recInner.width, recInner.y))) { return false; }
	if (!IsInsideObb2(boxOuter, NewVec2(recInner.x, recInner.y + recInner.height))) { return false; }
	if (!IsInsideObb2(boxOuter, NewVec2(recInner.x + recInner.width, recInner.y + recInner.height))) { return false; }
	return true;
}

bool RecIntersects(rec rectangle1, rec rectangle2, bool inclusive = true)
{
	if (inclusive)
	{
		if (rectangle1.x <= rectangle2.x + rectangle2.width &&
			rectangle1.x + rectangle1.width >= rectangle2.x &&
			rectangle1.y <= rectangle2.y + rectangle2.height &&
			rectangle1.y + rectangle1.height >= rectangle2.y)
		{
			return true;
		}
	}
	else
	{
		if (rectangle1.x < rectangle2.x + rectangle2.width &&
			rectangle1.x + rectangle1.width > rectangle2.x &&
			rectangle1.y < rectangle2.y + rectangle2.height &&
			rectangle1.y + rectangle1.height > rectangle2.y)
		{
			return true;
		}
	}
	
	return false;
}
bool ReciIntersects(reci rectangle1, reci rectangle2, bool inclusive = true)
{
	if (inclusive)
	{
		if (rectangle1.x <= rectangle2.x + rectangle2.width &&
			rectangle1.x + rectangle1.width >= rectangle2.x &&
			rectangle1.y <= rectangle2.y + rectangle2.height &&
			rectangle1.y + rectangle1.height >= rectangle2.y)
		{
			return true;
		}
	}
	else
	{
		if (rectangle1.x < rectangle2.x + rectangle2.width &&
			rectangle1.x + rectangle1.width > rectangle2.x &&
			rectangle1.y < rectangle2.y + rectangle2.height &&
			rectangle1.y + rectangle1.height > rectangle2.y)
		{
			return true;
		}
	}
	
	return false;
}
//NOTE: Obb2Intersects is defined in my_math.h

rec ScaleRecTo(rec rectangle, rec innerRec, r32* scaleOut = nullptr)
{
	r32 scale = innerRec.width / rectangle.width;
	if (innerRec.height / rectangle.height > scale)
	{
		scale = innerRec.height / rectangle.height;
	}
	if (scaleOut != nullptr) { *scaleOut = scale; }
	
	rec result = NewRecCentered(innerRec.topLeft + innerRec.size/2, rectangle.size*scale);
	return result;
}

rec ScaleRecToFitInsideHorizontally(rec rectangle, rec innerRec, r32* scaleOut = nullptr)
{
	r32 scale = innerRec.width / rectangle.width;
	if (scaleOut != nullptr) { *scaleOut = scale; }
	
	rec result = NewRecCentered(innerRec.topLeft + innerRec.size/2, rectangle.size*scale);
	return result;
}

rec RecOverlap(rec rectangle1, rec rectangle2)
{
	r32 minX = MaxR32(rectangle1.x, rectangle2.x);
	r32 minY = MaxR32(rectangle1.y, rectangle2.y);
	r32 maxX = MinR32(rectangle1.x+rectangle1.width, rectangle2.x+rectangle2.width);
	r32 maxY = MinR32(rectangle1.y+rectangle1.height, rectangle2.y+rectangle2.height);
	rec result = NewRec(minX, minY, MaxR32(maxX-minX, 0), MaxR32(maxY-minY, 0));
	return result;
}
rec RecBoth(rec rectangle1, rec rectangle2)
{
	r32 minX = MinR32(rectangle1.x, rectangle1.x + rectangle1.width, rectangle2.x, rectangle2.x + rectangle2.width);
	r32 minY = MinR32(rectangle1.y, rectangle1.y + rectangle1.height, rectangle2.y, rectangle2.y + rectangle2.height);
	r32 maxX = MaxR32(rectangle1.x, rectangle1.x + rectangle1.width, rectangle2.x, rectangle2.x + rectangle2.width);
	r32 maxY = MaxR32(rectangle1.y, rectangle1.y + rectangle1.height, rectangle2.y, rectangle2.y + rectangle2.height);
	return NewRec(minX, minY, maxX - minX, maxY - minY);
}

reci ReciOverlap(reci rectangle1, reci rectangle2)
{
	i32 minX = MaxI32(rectangle1.x, rectangle2.x);
	i32 minY = MaxI32(rectangle1.y, rectangle2.y);
	i32 maxX = MinI32(rectangle1.x+rectangle1.width, rectangle2.x+rectangle2.width);
	i32 maxY = MinI32(rectangle1.y+rectangle1.height, rectangle2.y+rectangle2.height);
	reci result = NewReci(minX, minY, MaxI32(maxX-minX, 0), MaxI32(maxY-minY, 0));
	return result;
}
reci ReciBoth(reci rectangle1, reci rectangle2)
{
	i32 minX = MinI32(rectangle1.x, rectangle1.x + rectangle1.width, rectangle2.x, rectangle2.x + rectangle2.width);
	i32 minY = MinI32(rectangle1.y, rectangle1.y + rectangle1.height, rectangle2.y, rectangle2.y + rectangle2.height);
	i32 maxX = MaxI32(rectangle1.x, rectangle1.x + rectangle1.width, rectangle2.x, rectangle2.x + rectangle2.width);
	i32 maxY = MaxI32(rectangle1.y, rectangle1.y + rectangle1.height, rectangle2.y, rectangle2.y + rectangle2.height);
	return NewReci(minX, minY, maxX - minX, maxY - minY);
}

//TODO: There is a problem with this function. It needs to be tested and fixed!
//NOTE: Meeting point is the point on the rectangle closest to the circle center
bool RecOverlapsCircle(rec rectangle, v2 circleCenter, r32 circleRadius, v2* meetingPointOut = nullptr)
{
	if (circleCenter.x + circleRadius < rectangle.x) { return false; } //left of rec
	if (circleCenter.x - circleRadius > rectangle.x + rectangle.width) { return false; } //right of rec
	if (circleCenter.y + circleRadius < rectangle.y) { return false; } //above rec
	if (circleCenter.y - circleRadius > rectangle.y + rectangle.height) { return false; } //below rec
	
	r32 centerDists = Vec2Length(circleCenter - (rectangle.topLeft + rectangle.size/2));
	r32 diagLength = Vec2Length(rectangle.size);
	if (centerDists > circleRadius + diagLength/2) { return false; } //close to corner but not quite intersecting
	
	if (meetingPointOut != nullptr)
	{
		//center is inside circle
		if (circleCenter.x >= rectangle.x && circleCenter.x <= rectangle.x + rectangle.width &&
			circleCenter.y >= rectangle.y && circleCenter.y <= rectangle.y + rectangle.height)
		{
			*meetingPointOut = circleCenter;
		}
		//otherwise 8 Verona regions
		else if (circleCenter.x < rectangle.x && circleCenter.y < rectangle.y) //top left
		{
			*meetingPointOut = rectangle.topLeft;
		}
		else if (circleCenter.x > rectangle.x+rectangle.width && circleCenter.y < rectangle.y) //top right
		{
			*meetingPointOut = rectangle.topLeft + NewVec2(rectangle.width, 0);
		}
		else if (circleCenter.x < rectangle.x && circleCenter.y > rectangle.y+rectangle.height) //bottom left
		{
			*meetingPointOut = rectangle.topLeft + NewVec2(0, rectangle.height);
		}
		else if (circleCenter.x > rectangle.x+rectangle.width && circleCenter.y > rectangle.y+rectangle.height) //bottom right
		{
			*meetingPointOut = rectangle.topLeft + rectangle.size;
		}
		else if (circleCenter.x < rectangle.x) //left side
		{
			*meetingPointOut = NewVec2(rectangle.x, circleCenter.y);
		}
		else if (circleCenter.x > rectangle.x+rectangle.width) //right side
		{
			*meetingPointOut = NewVec2(rectangle.x+rectangle.width, circleCenter.y);
		}
		else if (circleCenter.y < rectangle.y) //top side
		{
			*meetingPointOut = NewVec2(circleCenter.x, rectangle.y);
		}
		else if (circleCenter.y > rectangle.y+rectangle.width) //bottom side
		{
			*meetingPointOut = NewVec2(circleCenter.x, rectangle.y+rectangle.height);
		}
		else
		{
			Assert(false); //Don't think there's any other cases here
			*meetingPointOut = circleCenter;
		}
		
		if (Vec2Length(*meetingPointOut - circleCenter) > circleRadius) //TODO: This is a temporary solution
		{
			return false;
		}
	}
	return true;
}

r32 DistanceToRec(rec rectangle, v2 point)
{
	if (point.x < rectangle.x)
	{
		if (point.y < rectangle.y)
		{
			return Vec2Length(NewVec2(rectangle.x - point.x, rectangle.y - point.y));
		}
		else if (point.y > rectangle.y + rectangle.height)
		{
			return Vec2Length(NewVec2(rectangle.x - point.x, rectangle.y + rectangle.height - point.y));
		}
		else
		{
			return rectangle.x - point.x;
		}
	}
	else if (point.x > rectangle.x + rectangle.width)
	{
		if (point.y < rectangle.y)
		{
			return Vec2Length(NewVec2(rectangle.x + rectangle.width - point.x, rectangle.y - point.y));
		}
		else if (point.y > rectangle.y + rectangle.height)
		{
			return Vec2Length(NewVec2(rectangle.x + rectangle.width - point.x, rectangle.y + rectangle.height - point.y));
		}
		else
		{
			return point.x - (rectangle.x + rectangle.width);
		}
	}
	else if (point.y < rectangle.y)
	{
		return rectangle.y - point.y;
	}
	else if (point.y > rectangle.y + rectangle.height)
	{
		return point.y - (rectangle.y + rectangle.height);
	}
	else { return 0; } //inside
}

rec RecExtend(rec rectangle, u8 sides, r32 amount)
{
	rec result = rectangle;
	if (IsFlagSet(sides, Dir2_Right)) { result.width  += amount; }
	if (IsFlagSet(sides, Dir2_Down))  { result.height += amount; }
	if (IsFlagSet(sides, Dir2_Left))  { result.x -= amount; result.width  += amount; }
	if (IsFlagSet(sides, Dir2_Up))    { result.y -= amount; result.height += amount; }
	return result;
}
rec RecExtend(rec rectangle, Dir2_t sides, r32 amount)
{
	return RecExtend(rectangle, (u8)sides, amount);
}
reci ReciExtend(reci rectangle, u8 sides, i32 amount)
{
	reci result = rectangle;
	if (IsFlagSet(sides, Dir2_Right)) { result.width  += amount; }
	if (IsFlagSet(sides, Dir2_Down))  { result.height += amount; }
	if (IsFlagSet(sides, Dir2_Left))  { result.x -= amount; result.width  += amount; }
	if (IsFlagSet(sides, Dir2_Up))    { result.y -= amount; result.height += amount; }
	return result;
}
reci ReciExtend(reci rectangle, Dir2_t sides, i32 amount)
{
	return ReciExtend(rectangle, (u8)sides, amount);
}

v2 GetRectSupport(rec rectangle, v2 dirVec)
{
	r32 topLeft     = Vec2Dot(rectangle.topLeft,                                              dirVec);
	r32 topRight    = Vec2Dot(rectangle.topLeft + NewVec2(rectangle.width, 0),                dirVec);
	r32 bottomLeft  = Vec2Dot(rectangle.topLeft + NewVec2(0, rectangle.height),               dirVec);
	r32 bottomRight = Vec2Dot(rectangle.topLeft + NewVec2(rectangle.width, rectangle.height), dirVec);
	if (topLeft >= topRight && topLeft >= bottomLeft && topLeft >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(0, 0);
	}
	else if (topRight >= bottomLeft && topRight >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(rectangle.width, 0);
	}
	else if (bottomLeft >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(0, rectangle.height);
	}
	else
	{
		return rectangle.topLeft + NewVec2(rectangle.width, rectangle.height);
	}
}
v2 GetRectSupport(rec rectangle, r32 direction)
{
	return GetRectSupport(rectangle, NewVec2(CosR32(direction), SinR32(direction)));
}

v2 GetRectPerimeterPos(rec rectangle, r32 lerpValue)
{
	r32 aspectRatio = (AbsR32(rectangle.width) / (AbsR32(rectangle.width) + AbsR32(rectangle.height))) / 2;
	lerpValue = ModR32(lerpValue, 1.0f);
	if (lerpValue < aspectRatio) { return NewVec2(rectangle.x + rectangle.width * (lerpValue / aspectRatio), rectangle.y); }
	else if (lerpValue < 0.5f) { return NewVec2(rectangle.x + rectangle.width, rectangle.y + rectangle.height * ((lerpValue - aspectRatio) / (0.5f - aspectRatio))); }
	else if (lerpValue < 0.5f + aspectRatio) { return NewVec2(rectangle.x + rectangle.width * (1.0f - (lerpValue - 0.5f) / aspectRatio), rectangle.y + rectangle.height); }
	else { return NewVec2(rectangle.x, rectangle.y + rectangle.height * (1.0f - (lerpValue - 0.5f - aspectRatio) / (0.5f - aspectRatio))); }
}

#endif //  _MY_RECTANGLES_H
