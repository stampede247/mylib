/*
File:   my_fifo.h
Author: Taylor Robbins
Date:   12\06\2018
Description:
	
*/

#ifndef _MY_FIFO_H
#define _MY_FIFO_H

#define FIFO_DYNAMIC_CHUNK_SIZE 1024

typedef enum
{
	FifoType_StaticDrop = 0x00,
	FifoType_StaticOverwrite,
	FifoType_Dynamic,
	FifoType_NumTypes,
} FifoType_t;

struct Fifo_t
{
	MemoryArena_t* allocArena;
	FifoType_t type;
	u32 head;
	u32 tail;
	u32 size;
	u8* buffer;
};

void CreateFifo(Fifo_t* fifo, MemoryArena_t* memArena, u32 size, FifoType_t type)
{
	ClearPointer(fifo);
	fifo->allocArena = memArena;
	fifo->type = type;
	fifo->head = 0;
	fifo->tail = 0;
	
	fifo->size = size;
	if (fifo->size > 0)
	{
		fifo->buffer = PushArray(memArena, u8, fifo->size);
	}
	else
	{
		fifo->buffer = nullptr;
	}
}

void DeleteFifo(Fifo_t* fifo)
{
	Assert(fifo != nullptr);
	if (fifo->buffer != nullptr)
	{
		Assert(fifo->allocArena != nullptr);
		ArenaPop(fifo->allocArena, fifo->buffer);
	}
	ClearPointer(fifo);
}

u32 FifoLength(const Fifo_t* fifo)
{
	Assert(fifo != nullptr);
	Assert(fifo->head < fifo->size && fifo->tail < fifo->size);
	
	if (fifo->head >= fifo->tail)
	{
		return fifo->head - fifo->tail;
	}
	else
	{
		return (fifo->size - fifo->tail) + fifo->head;
	}
}

bool FifoPush(Fifo_t* fifo, u8 newByte)
{
	u32 newHead = ((fifo->head + 1) % fifo->size);
	if (fifo->size == 0 || newHead == fifo->tail)
	{
		if (fifo->type == FifoType_StaticDrop)
		{
			return false; //no space, drop the new byte
		}
		else if (fifo->type == FifoType_StaticOverwrite)
		{
			fifo->tail = ((fifo->tail + 1) % fifo->size); //move the tail up, dropping an old byte
		}
		else if (fifo->type == FifoType_Dynamic)
		{
			Assert(fifo->allocArena != nullptr);
			
			u32 newSpaceSize = fifo->size + FIFO_DYNAMIC_CHUNK_SIZE;
			u8* newSpace = PushArray(fifo->allocArena, u8, newSpaceSize);
			Assert(newSpace != nullptr);
			
			if (fifo->buffer != nullptr && fifo->head != fifo->tail)
			{
				if (fifo->head > fifo->tail)
				{
					u32 fifoLength = fifo->head - fifo->tail;
					MyMemCopy(newSpace, &fifo->buffer[fifo->tail], fifoLength);
					fifo->tail = 0;
					fifo->head = fifoLength;
				}
				else
				{
					u32 upperLength = fifo->size - fifo->tail;
					u32 lowerLength = fifo->head;
					MyMemCopy(&newSpace[0], &fifo->buffer[fifo->tail], upperLength);
					MyMemCopy(&newSpace[upperLength], &fifo->buffer[0], lowerLength);
					fifo->tail = 0;
					fifo->head = upperLength + lowerLength;
				}
			}
			
			if (fifo->buffer != nullptr)
			{
				ArenaPop(fifo->allocArena, fifo->buffer);
			}
			fifo->buffer = newSpace;
			fifo->size = newSpaceSize;
			Assert(fifo->tail < fifo->size);
			Assert(fifo->head < fifo->size);
		}
		else
		{
			return false; //fifo type is invalid!
		}
	}
	
	fifo->buffer[fifo->head] = newByte;
	fifo->head = ((fifo->head + 1) % fifo->size);
	return true;
}

bool FifoPushArray(Fifo_t* fifo, const u8* arrayPntr, u32 arraySize)
{
	NotNull(fifo);
	if (arraySize == 0) { return true; }
	NotNull(arrayPntr);
	bool result = true;
	for (u32 bIndex = 0; bIndex < arraySize; bIndex++)
	{
		if (!FifoPush(fifo, arrayPntr[bIndex])) { result = false; }
	}
	return result;
}

u8 FifoGet(const Fifo_t* fifo, u32 index)
{
	Assert(fifo != nullptr);
	Assert(index < FifoLength(fifo));
	return fifo->buffer[(fifo->tail + index) % fifo->size];
}

bool FifoGetArray(const Fifo_t* fifo, u8* arrayPntr, u32 numBytes)
{
	NotNull(fifo);
	if (numBytes == 0) { return true; }
	NotNull(arrayPntr);
	if (numBytes > FifoLength(fifo)) { return false; }
	for (u32 bIndex = 0; bIndex < numBytes; bIndex++)
	{
		arrayPntr[bIndex] = FifoGet(fifo, bIndex);
	}
	return true;
}

u8 FifoGetLast(const Fifo_t* fifo)
{
	Assert(fifo != nullptr);
	Assert(fifo->head != fifo->tail);
	if (fifo->head == 0) { return fifo->buffer[fifo->size-1]; }
	else { return fifo->buffer[fifo->head-1]; }
}

u8 FifoPop(Fifo_t* fifo)
{
	Assert(fifo != nullptr);
	Assert(FifoLength(fifo) > 0);
	u8 result = fifo->buffer[fifo->tail];
	fifo->tail = ((fifo->tail + 1) % fifo->size);
	return result;
}

u8* FifoUnwrap(const Fifo_t* fifo, MemoryArena_t* memArena, bool addNullTerm = false)
{
	Assert(fifo != nullptr);
	Assert(memArena != nullptr);
	u32 fifoLength = FifoLength(fifo);
	if (fifoLength == 0 && !addNullTerm) { return nullptr; }
	u8* result = PushArray(memArena, u8, fifoLength + (addNullTerm ? 1 : 0));
	for (u32 cIndex = 0; cIndex < fifoLength; cIndex++)
	{
		u8 nextChar = FifoGet(fifo, cIndex);
		result[cIndex] = nextChar;
	}
	if (addNullTerm)
	{
		result[fifoLength] = '\0';
	}
	return result;
}

bool FifoPeekCommand8(const Fifo_t* fifo, u8 attnChar = 0x7E, u8* payloadLengthOut = nullptr)
{
	Assert(fifo != nullptr);
	
	bool foundAttnChar = false;
	u32 cmdIndex = 0;
	u8 payloadLength = 0x00;
	
	u32 bIndex = 0;
	while (bIndex < FifoLength(fifo))
	{
		u8 newByte = FifoGet(fifo, bIndex);
		if (!foundAttnChar)
		{
			if (newByte == attnChar)
			{
				foundAttnChar = true;
				cmdIndex = 1;
			}
		}
		else
		{
			if (cmdIndex == 2)
			{
				payloadLength = newByte;
			}
			cmdIndex++;
			if (cmdIndex >= 3 && cmdIndex >= 3+(u32)payloadLength)
			{
				if (payloadLengthOut != nullptr) { *payloadLengthOut = payloadLength; }
				return true;
			}
		}
		bIndex++;
	}
	//If we broke out of the loop without returning then there's no commands ready
	return false;
}

u8* FifoPopCommand8(Fifo_t* fifo, MemoryArena_t* memArena, u8 attnChar = 0x7E)
{
	Assert(fifo != nullptr);
	
	u8 cmdPayloadLength = 0;
	bool foundCmd = FifoPeekCommand8(fifo, attnChar, &cmdPayloadLength);
	if (!foundCmd) { return nullptr; }
	
	//pop invalid data first
	while (FifoLength(fifo) > 0 && FifoGet(fifo, 0) != attnChar)
	{
		FifoPop(fifo);
	}
	
	u32 totalCmdSize = 3 + (u32)cmdPayloadLength;
	Assert(FifoLength(fifo) >= totalCmdSize);
	u8* result = PushArray(memArena, u8, totalCmdSize);
	
	result[0] = FifoPop(fifo); Assert(result[0] == attnChar);
	result[1] = FifoPop(fifo);
	result[2] = FifoPop(fifo); Assert(result[2] == cmdPayloadLength);
	for (u32 bIndex = 0; bIndex < cmdPayloadLength; bIndex++)
	{
		result[3 + bIndex] = FifoPop(fifo);
	}
	
	return result;
}

bool FifoPeekCommand16(const Fifo_t* fifo, u8 attnChar = 0x7E, u16* payloadLengthOut = nullptr)
{
	Assert(fifo != nullptr);
	
	bool foundAttnChar = false;
	u32 cmdIndex = 0;
	u16 payloadLength = 0x0000;
	
	u32 bIndex = 0;
	while (bIndex < FifoLength(fifo))
	{
		u8 newByte = FifoGet(fifo, bIndex);
		if (!foundAttnChar)
		{
			if (newByte == attnChar)
			{
				foundAttnChar = true;
				cmdIndex = 1;
			}
		}
		else
		{
			if (cmdIndex == 2)
			{
				payloadLength |= ((u16)newByte << 0);
			}
			if (cmdIndex == 3)
			{
				payloadLength |= ((u16)newByte << 8);
			}
			cmdIndex++;
			if (cmdIndex >= 4 && cmdIndex >= 4+(u32)payloadLength)
			{
				if (payloadLengthOut != nullptr) { *payloadLengthOut = payloadLength; }
				return true;
			}
		}
		bIndex++;
	}
	//If we broke out of the loop without returning then there's no commands ready
	return false;
}

u8* FifoPopCommand16(Fifo_t* fifo, MemoryArena_t* memArena, u8 attnChar = 0x7E)
{
	Assert(fifo != nullptr);
	
	u16 cmdPayloadLength = 0;
	bool foundCmd = FifoPeekCommand16(fifo, attnChar, &cmdPayloadLength);
	if (!foundCmd) { return nullptr; }
	
	//pop invalid data first
	while (FifoLength(fifo) > 0 && FifoGet(fifo, 0) != attnChar)
	{
		FifoPop(fifo);
	}
	
	u32 totalCmdSize = 4 + (u32)cmdPayloadLength;
	Assert(FifoLength(fifo) >= totalCmdSize);
	u8* result = PushArray(memArena, u8, totalCmdSize);
	
	result[0] = FifoPop(fifo); Assert(result[0] == attnChar);
	result[1] = FifoPop(fifo);
	result[2] = FifoPop(fifo); Assert(result[2] == (u8)(cmdPayloadLength>>0));
	result[3] = FifoPop(fifo); Assert(result[3] == (u8)(cmdPayloadLength>>8));
	for (u32 bIndex = 0; bIndex < cmdPayloadLength; bIndex++)
	{
		result[4 + bIndex] = FifoPop(fifo);
	}
	
	return result;
}

bool FifoPeekLine(const Fifo_t* fifo, u32* lineLengthOut = nullptr)
{
	Assert(fifo != nullptr);
	for (u32 bIndex = 0; bIndex < FifoLength(fifo); bIndex++)
	{
		if (FifoGet(fifo, bIndex) == '\n')
		{
			if (lineLengthOut != nullptr) { *lineLengthOut = bIndex; }
			return true;
		}
	}
	if (FifoLength(fifo) == fifo->size-1)
	{
		if (lineLengthOut != nullptr) { *lineLengthOut = fifo->size-1; }
		return true;
	}
	return false;
}

char* FifoPopLine(Fifo_t* fifo, MemoryArena_t* memArena)
{
	Assert(fifo != nullptr);
	
	u32 lineLength = 0;
	bool foundLine = FifoPeekLine(fifo, &lineLength);
	if (!foundLine) { return nullptr; }
	char* result = PushArray(memArena, char, lineLength+1);
	NotNull(result);
	for (u32 bIndex = 0; bIndex < lineLength; bIndex++)
	{
		result[bIndex] = (char)FifoPop(fifo);
	}
	result[lineLength] = '\0';
	FifoPop(fifo); //pop-off \n character as well
	return result;
}

#endif //  _MY_FIFO_H
