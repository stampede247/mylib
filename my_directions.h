/*
File:   my_directions.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines the Dir2_t, Dir3_t, and Axis_t enumerations
	** and implements all the related helper functions

This file is #included by default from mylib.h
*/

#ifndef _MY_DIRECTIONS_H
#define _MY_DIRECTIONS_H

// +--------------------------------------------------------------+
// |                   Enumeration Definitions                    |
// +--------------------------------------------------------------+
typedef enum
{
	Dir2_None =   0x00,
	Dir2_Right =  0x01,
	Dir2_Left =   0x02,
	Dir2_Up =     0x04,
	Dir2_Down =   0x08,
	Dir2_All =    0x0F,
} Dir2_t;

typedef enum
{
	Dir2Ex_TopLeft     = 0x10,
	Dir2Ex_TopRight    = 0x20,
	Dir2Ex_BottomLeft  = 0x40,
	Dir2Ex_BottomRight = 0x80,
	Dir2Ex_All         = 0xFF,
} Dir2Ex_t;

typedef enum
{
	Dir3_None =     0x00,
	Dir3_Right =    0x01,
	Dir3_Left =     0x02,
	Dir3_Up =       0x04,
	Dir3_Down =     0x08,
	Dir3_Forward =  0x10,
	Dir3_Backward = 0x20,
	Dir3_All =      0x3F,
} Dir3_t;

typedef enum
{
	Axis_None = 0x00,
	Axis_X =    0x01,
	Axis_Y =    0x02,
	Axis_Z =    0x04,
	Axis_W =    0x08,
} Axis_t;

// +--------------------------------------------------------------+
// |                      Helpful Functions                       |
// +--------------------------------------------------------------+
inline v2 NewVec2(Dir2_t dir2)
{
	v2 result = {};
	if (IsFlagSet(dir2, Dir2_Right)) { result.x += 1.0f; }
	if (IsFlagSet(dir2, Dir2_Left))  { result.x -= 1.0f; }
	if (IsFlagSet(dir2, Dir2_Down))  { result.y += 1.0f; }
	if (IsFlagSet(dir2, Dir2_Up))    { result.y -= 1.0f; }
	return result;
}
inline v2i NewVec2i(Dir2_t dir2)
{
	v2i result = {};
	if (IsFlagSet(dir2, Dir2_Right)) { result.x += 1; }
	if (IsFlagSet(dir2, Dir2_Left))  { result.x -= 1; }
	if (IsFlagSet(dir2, Dir2_Down))  { result.y += 1; }
	if (IsFlagSet(dir2, Dir2_Up))    { result.y -= 1; }
	return result;
}

inline v3 NewVec3(Dir3_t dir3)
{
	v3 result = {};
	if (IsFlagSet(dir3, Dir3_Right))    { result.x += 1.0f; }
	if (IsFlagSet(dir3, Dir3_Left))     { result.x -= 1.0f; }
	if (IsFlagSet(dir3, Dir3_Down))     { result.y += 1.0f; }
	if (IsFlagSet(dir3, Dir3_Up))       { result.y -= 1.0f; }
	if (IsFlagSet(dir3, Dir3_Forward))  { result.z += 1.0f; }
	if (IsFlagSet(dir3, Dir3_Backward)) { result.z -= 1.0f; }
	return result;
}
inline v3i NewVec3i(Dir3_t dir3)
{
	v3i result = {};
	if (IsFlagSet(dir3, Dir3_Right))    { result.x += 1; }
	if (IsFlagSet(dir3, Dir3_Left))     { result.x -= 1; }
	if (IsFlagSet(dir3, Dir3_Down))     { result.y += 1; }
	if (IsFlagSet(dir3, Dir3_Up))       { result.y -= 1; }
	if (IsFlagSet(dir3, Dir3_Forward))  { result.z += 1; }
	if (IsFlagSet(dir3, Dir3_Backward)) { result.z -= 1; }
	return result;
}

inline Dir2_t NewDir2(v2 vector)
{
	if (vector == Vec2_Zero) { return Dir2_None; }
	if (AbsR32(vector.x) >= AbsR32(vector.y))
	{
		if (vector.x < 0) { return Dir2_Left; }
		else { return Dir2_Right; }
	}
	else
	{
		if (vector.y < 0) { return Dir2_Up; }
		else { return Dir2_Down; }
	}
}
inline Dir2_t NewDir2(v2i vector)
{
	if (vector == Vec2i_Zero) { return Dir2_None; }
	if (AbsI32(vector.x) >= AbsI32(vector.y))
	{
		if (vector.x < 0) { return Dir2_Left; }
		else { return Dir2_Right; }
	}
	else
	{
		if (vector.y < 0) { return Dir2_Up; }
		else { return Dir2_Down; }
	}
}

inline u8 Dir2Index(Dir2_t direction)
{
	switch (direction)
	{
		case Dir2_Right: return 0;
		case Dir2_Left:  return 1;
		case Dir2_Up:    return 2;
		case Dir2_Down:  return 3;
		default: return 0;
	}
}
inline Dir2_t Dir2ByIndex(u8 index)
{
	switch (index)
	{
		case 0: return Dir2_Right;
		case 1: return Dir2_Left;
		case 2: return Dir2_Up;
		case 3: return Dir2_Down;
		default: return Dir2_None;
	}
}
inline Dir2_t Dir2ByIndexClockwise(u8 index)
{
	switch (index)
	{
		case 0: return Dir2_Down;
		case 1: return Dir2_Left;
		case 2: return Dir2_Up;
		case 3: return Dir2_Right;
		default: return Dir2_Down;
	}
}

inline r32 Dir2Angle(Dir2_t direction)
{
	switch (direction)
	{
		case Dir2_Right: return 0;
		case Dir2_Down:  return Pi32/2.0f;
		case Dir2_Left:  return Pi32;
		case Dir2_Up:    return Pi32*3.0f/2.0f;
		default: return 0;
	}
}

inline Dir2_t Dir2Opposite(Dir2_t dir2)
{
	switch (dir2)
	{
		case Dir2_Down:  return Dir2_Up;
		case Dir2_Up:    return Dir2_Down;
		case Dir2_Right: return Dir2_Left;
		case Dir2_Left:  return Dir2_Right;
		default: return Dir2_None;
	};
}

const char* GetDir2String(Dir2_t dir2)
{
	switch (dir2)
	{
		case Dir2_None:  return "None";
		case Dir2_Right: return "Right";
		case Dir2_Left:  return "Left";
		case Dir2_Up:    return "Up";
		case Dir2_Down:  return "Down";
		default: return "Unknown";
	};
}
char GetDir2Char(Dir2_t dir2)
{
	switch (dir2)
	{
		case Dir2_None:  return 'x';
		case Dir2_Right: return '>';
		case Dir2_Left:  return '<';
		case Dir2_Up:    return '^';
		case Dir2_Down:  return 'v';
		default: return '?';
	};
}

inline Dir2_t Dir2Clockwise(Dir2_t dir2)
{
	switch (dir2)
	{
		case Dir2_Down:  return Dir2_Left;
		case Dir2_Left:  return Dir2_Up;
		case Dir2_Up:    return Dir2_Right;
		case Dir2_Right: return Dir2_Down;
		default: return Dir2_Down;
	}
}
inline Dir2_t Dir2Clockwise(Dir2_t dir2, u32 numTurns)
{
	Dir2_t result = dir2;
	for (u32 tIndex = 0; tIndex < numTurns; tIndex++)
	{
		result = Dir2Clockwise(result);
	}
	return result;
}

inline Dir2_t Dir2CounterClockwise(Dir2_t dir2)
{
	switch (dir2)
	{
		case Dir2_Down:  return Dir2_Right;
		case Dir2_Left:  return Dir2_Down;
		case Dir2_Up:    return Dir2_Left;
		case Dir2_Right: return Dir2_Up;
		default: return Dir2_Down;
	}
}
inline Dir2_t Dir2CounterClockwise(Dir2_t dir2, u32 numTurns)
{
	Dir2_t result = dir2;
	for (u32 tIndex = 0; tIndex < numTurns; tIndex++)
	{
		result = Dir2CounterClockwise(result);
	}
	return result;
}

//This function returns radians ALWAYS in the clockwise direction so GetDir2RotationFrom(Dir2_Right, Dir2_Down) returns Pi32/2
inline r32 GetDir2RotationFrom(Dir2_t fromDir, Dir2_t toDir)
{
	switch (fromDir)
	{
		case Dir2_Right:
		{
			switch (toDir)
			{
				case Dir2_Right: return 0;
				case Dir2_Down: return Pi32/2;
				case Dir2_Left: return Pi32;
				case Dir2_Up: return Pi32*3/2;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Down:
		{
			switch (toDir)
			{
				case Dir2_Down: return 0;
				case Dir2_Left: return Pi32/2;
				case Dir2_Up: return Pi32;
				case Dir2_Right: return Pi32*3/2;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Left:
		{
			switch (toDir)
			{
				case Dir2_Left: return 0;
				case Dir2_Up: return Pi32/2;
				case Dir2_Right: return Pi32;
				case Dir2_Down: return Pi32*3/2;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Up:
		{
			switch (toDir)
			{
				case Dir2_Up: return 0;
				case Dir2_Right: return Pi32/2;
				case Dir2_Down: return Pi32;
				case Dir2_Left: return Pi32*3/2;
				default: Assert(false); return 0;
			}
		}
		default: Assert(false); return 0;
	}
}

inline u8 GetDir2NumTurnsClockwise(Dir2_t fromDir, Dir2_t toDir)
{
	switch (fromDir)
	{
		case Dir2_Right:
		{
			switch (toDir)
			{
				case Dir2_Right: return 0;
				case Dir2_Down: return 1;
				case Dir2_Left: return 2;
				case Dir2_Up: return 3;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Down:
		{
			switch (toDir)
			{
				case Dir2_Down: return 0;
				case Dir2_Left: return 1;
				case Dir2_Up: return 2;
				case Dir2_Right: return 3;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Left:
		{
			switch (toDir)
			{
				case Dir2_Left: return 0;
				case Dir2_Up: return 1;
				case Dir2_Right: return 2;
				case Dir2_Down: return 3;
				default: Assert(false); return 0;
			}
		}
		case Dir2_Up:
		{
			switch (toDir)
			{
				case Dir2_Up: return 0;
				case Dir2_Right: return 1;
				case Dir2_Down: return 2;
				case Dir2_Left: return 3;
				default: Assert(false); return 0;
			}
		}
		default: Assert(false); return 0;
	}
}

//TODO: Using whiles is really inefficient when the angle values become large. Maybe we should use division and rounding to achieve a flowing point % equivelent?
inline r32 AngleDiff(r32 fromAngle, r32 toAngle)
{
	if (toAngle <= fromAngle)
	{
		while ((fromAngle - toAngle) >= Pi32)
		{
			toAngle += 2*Pi32;
		}
	}
	else
	{
		while ((toAngle - fromAngle) >= Pi32)
		{
			toAngle -= 2*Pi32;
		}
	}
	return toAngle - fromAngle;
}

inline r32 AngleOpposite(r32 angle)
{
	angle += Pi32;
	while (angle >= 2*Pi32) { angle -= 2*Pi32; }
	return angle;
}

inline v2 RotateVec2NumTurnsClockwise(v2 vector, u32 numTurnsClockwise)
{
	if ((numTurnsClockwise%4) == 0) { return vector; } //multiple of 4 turns is basically no turns
	if ((numTurnsClockwise%4) == 1) { return Vec2PerpRight(vector); } //1 turn clockwise is perp right
	if ((numTurnsClockwise%4) == 2) { return -vector; } //2 turns clockwise is opposite
	if ((numTurnsClockwise%4) == 3) { return Vec2PerpLeft(vector); } //3 turns clockwise is perp left
	Assert(false); //shouldn't make it here ever
	return vector;
}
inline v2i RotateVec2iNumTurnsClockwise(v2i vector, u32 numTurnsClockwise)
{
	if ((numTurnsClockwise%4) == 0) { return vector; } //multiple of 4 turns is basically no turns
	if ((numTurnsClockwise%4) == 1) { return Vec2iPerpRight(vector); } //1 turn clockwise is perp right
	if ((numTurnsClockwise%4) == 2) { return -vector; } //2 turns clockwise is opposite
	if ((numTurnsClockwise%4) == 3) { return Vec2iPerpLeft(vector); } //3 turns clockwise is perp left
	Assert(false); //shouldn't make it here ever
	return vector;
}

Dir2_t Dir2Abs(Dir2_t direction)
{
	u8 result = 0x00;
	if (IsFlagSet(direction, Dir2_Left) || IsFlagSet(direction, Dir2_Right)) { FlagSet(result, Dir2_Right); }
	if (IsFlagSet(direction, Dir2_Up)   || IsFlagSet(direction, Dir2_Down))  { FlagSet(result, Dir2_Down);  }
	return (Dir2_t)result;
}

u8 Dir2BitwiseCount(u8 bitwiseFieldOfDirs)
{
	return (IsFlagSet(bitwiseFieldOfDirs, Dir2_Down) ? 1 : 0) +
		(IsFlagSet(bitwiseFieldOfDirs, Dir2_Left) ? 1 : 0) +
		(IsFlagSet(bitwiseFieldOfDirs, Dir2_Up) ? 1 : 0) +
		(IsFlagSet(bitwiseFieldOfDirs, Dir2_Right) ? 1 : 0);
}

#endif //  _MY_DIRECTIONS_H
