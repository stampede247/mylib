/*
File:   my_parsingFunctions.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file contains a bunch of parsing functions that turn strings into different
	** usable types (e.g. int32, real32, vec3, etc.)

This file is #included by default from mylib.h
*/

#include "my_charClasses.h"

#ifndef _MY_PARSING_FUNCTIONS_H
#define _MY_PARSING_FUNCTIONS_H

//TODO: Fix/check all overflow errors. We did a fix in TryParseU32 that may need to happen here
bool TryParseU64(const char* str, u32 numCharacters, u64* outValue)
{
	u64 aggregateValue = 0;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					//no negatives for unsigned
					return false;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				u64 oldAggregate = aggregateValue;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
					
				//if our new value is less then the one we had before then
				//the value has wrapped.
				if (aggregateValue < oldAggregate)
				{
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr) { *outValue = (u64)aggregateValue; }
	return true;
}

//TODO: Fix/check all overflow errors. We did a fix in TryParseU32 that may need to happen here
bool TryParseI32(const char* str, u32 numCharacters, i32* outValue)
{
	u32 aggregateValue = 0;
	bool isNegative = false;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					isNegative = true;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
				else if (isNegative)
				{
					//we already found a '-' and now a + so the string is invalid
					return false;
				}
				else
				{
					//no effect since isNegative defaults to false
					isNegative = false;
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				u32 oldAggregate = aggregateValue;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				//if our new value is less then the one we had before then
				//the value has wrapped.
				if (aggregateValue < oldAggregate)
				{
					return false;
				}
				
				if (aggregateValue & (1 << 31))
				{
					//the number has reached past the maximum value of a signed i32
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr)
	{
		if (isNegative)
		{
			*outValue = -(i32)aggregateValue;
		}
		else
		{
			*outValue = (i32)aggregateValue;
		}
	}
	return true;
}

bool TryParseU32(const char* str, u32 numCharacters, u32* outValue)
{
	u32 aggregateValue = 0;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					//no negatives for unsigned
					return false;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				u32 oldAggregate = aggregateValue;
				
				//if we are more than 1/10 of the maximum u32 value then multiplying by 10 will have unintended results
				if (aggregateValue > 429496729)
				{
					return false;
				}
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				//if our new value is less then the one we had before then
				//the value has wrapped.
				if (aggregateValue < oldAggregate)
				{
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr) { *outValue = (u32)aggregateValue; }
	return true;
}

//TODO: Fix/check all overflow errors. We did a fix in TryParseU32 that may need to happen here
bool TryParseI16(const char* str, u32 numCharacters, int16* outValue)
{
	if (numCharacters == 0) 
	{
		Assert(false);
	}
	
	u32 aggregateValue = 0;
	bool isNegative = false;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					isNegative = true;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
				else if (isNegative)
				{
					//we already found a '-' and now a + so the string is invalid
					return false;
				}
				else
				{
					//no effect since isNegative defaults to false
					isNegative = false;
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				if (aggregateValue > INT16_MAX)
				{
					//the number has reached past the maximum value of a signed i32
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr)
	{
		if (isNegative)
		{
			*outValue = -(int16)aggregateValue;
		}
		else
		{
			*outValue = (int16)aggregateValue;
		}
	}
	return true;
}

//TODO: Fix/check all overflow errors. We did a fix in TryParseU32 that may need to happen here
bool TryParseU16(const char* str, u32 numCharacters, u16* outValue)
{
	u32 aggregateValue = 0;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					//no negatives for unsigned integers
					return false;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
				else
				{
					//no effect
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				if (aggregateValue > UINT16_MAX)
				{
					//the number has reached past the maximum value of a signed i32
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr) { *outValue = (u16)aggregateValue; }
	return true;
}

//TODO: Fix/check all overflow errors. We did a fix in TryParseU32 that may need to happen here
bool TryParseU8(const char* str, u32 numCharacters, u8* outValue)
{
	u32 aggregateValue = 0;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (str[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case ' ':
			{
				if (foundFirstNumeric)
				{
					//space after numerical values indicates the end of the number
					endLoop = true;
				}
				else
				{
					//ignore the leading whitespace
				}
			} break;
			
			case '-':
			{
				if (foundFirstNumeric)
				{
					//a '-' sign after numerics is invalid
					return false;
				}
				else
				{
					//no negatives for unsigned integers
					return false;
				}
			} break;
			
			case '+':
			{
				if (foundFirstNumeric)
				{
					//a '+' sign after numerics is invalid
					return false;
				}
				else
				{
					//no effect
				}
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			case '2': // 0011 0010
			case '3': // 0011 0011
			case '4': // 0011 0100
			case '5': // 0011 0101
			case '6': // 0011 0110
			case '7': // 0011 0111
			case '8': // 0011 1000
			case '9': // 0011 1001
			{
				foundFirstNumeric = true;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 10;
				
				//make sure the high nibble is correct
				//Assert((str[cIndex] & 0xF0) == 0b00110000);
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(str[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				if (aggregateValue > UINT8_MAX)
				{
					//the number has reached past the maximum value of a signed i32
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr) { *outValue = (u8)aggregateValue; }
	return true;
}

//TODO: Move away from using atof. Or at least try and see if atof will handle any error cases
bool TryParseR32(const char* nullTermString, r32* outValue)
{
	Assert(nullTermString != nullptr);
	if (nullTermString[0] == '\0') { return false; } //empty string doesn't count as parsable r32
	for (u32 cIndex = 0; nullTermString[cIndex] != '\0'; cIndex++)
	{
		char nextChar = nullTermString[cIndex];
		if (!IsCharClassNumeric((u8)nextChar) && !IsCharClassWhitespace((u8)nextChar) && nextChar != '-' && nextChar != '.')
		{
			return false; //found invalid character for floating point number
		}
	}
	r64 r64Value = atof(nullTermString);
	if (isinf(r64Value) || isnan(r64Value)) { return false; }
	if (outValue != nullptr) { *outValue = (r32)r64Value; }
	return true;
}

bool TryParseBool(const char* str, u32 numCharacters, bool* outValue)
{
	if (numCharacters == 0) 
	{
		if (outValue != nullptr) { *outValue = false; }
		return false;
	}
	
	//NOTE: Partial words are valid booleans
	//		e.g. "Fal" will translate to false and "t" will translate to true
	
	if (MyStrCompare(str, "1", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = true; }
		return true;
	}
	if (MyStrCompare(str, "0", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = false; }
		return true;
	}
	if (MyStrCompare(str, "true", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = true; }
		return true;
	}
	if (MyStrCompare(str, "True", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = true; }
		return true;
	}
	if (MyStrCompare(str, "false", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = false; }
		return true;
	}
	if (MyStrCompare(str, "False", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = false; }
		return true;
	}
	if (MyStrCompare(str, "on", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = true; }
		return true;
	}
	if (MyStrCompare(str, "On", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = true; }
		return true;
	}
	if (MyStrCompare(str, "off", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = false; }
		return true;
	}
	if (MyStrCompare(str, "Off", numCharacters) == 0)
	{
		if (outValue != nullptr) { *outValue = false; }
		return true;
	}
	
	return false;
}

bool TryParseBinaryI32(const char* binaryStr, u32 numCharacters, i32* outValue)
{
	u32 aggregateValue = 0;
	bool foundFirstNumeric = false;
	bool endLoop = false;
	
	for (u32 cIndex = 0; cIndex < numCharacters && !endLoop; cIndex++)
	{
		switch (binaryStr[cIndex])
		{
			case '\0':
			{
				endLoop = true;
			} break;
			
			case '0': // 0011 0000
			case '1': // 0011 0001
			{
				foundFirstNumeric = true;
				
				//if values came before they need to be shifted up one place
				aggregateValue *= 2;
				
				//make sure the high nibble is correct
				//Assert((binaryStr[cIndex] & 0xF0) == 0b00110000);
				//our number equivelant should lie in the low nibble
				u8 numericValue = (u8)(binaryStr[cIndex] & 0x0F);
				
				aggregateValue += numericValue;
				
				//check the top bit
				if (aggregateValue & (1 << 31))
				{
					//the number has reached past the maximum value of a signed i32
					return false;
				}
			} break;
			
			default:
			{
				//any other characters denote an invalid integer
				return false;
			} break;
		};
	}
	
	if (!foundFirstNumeric)
	{
		//no numerics where found. That's an invalid integer
		return false;
	}
	
	if (outValue != nullptr) { *outValue = (i32)aggregateValue; }
	return true;
}

u32* ParseNumbersInString(MemoryArena_t* arenaPntr, const char* str, u32 strLength, u32* numNumbersOut = nullptr)
{
	Assert(str != nullptr);
	
	if (strLength == 0)
	{
		if (numNumbersOut != nullptr) { *numNumbersOut = 0; }
		return nullptr;
	}
	
	u32* result = nullptr;
	u32 resultCount = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numNumbers = 0;
		u32 numStartPos = 0;
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			if (!IsCharClassNumeric((u8)str[cIndex]))
			{
				if (numStartPos < cIndex)
				{
					u32 parsedValue = 0;
					if (TryParseU32(&str[numStartPos], cIndex-numStartPos, &parsedValue))
					{
						if (pass == 1)
						{
							Assert(result != nullptr && numNumbers < resultCount);
							result[numNumbers] = parsedValue;
						}
						numNumbers++;
					}
				}
				numStartPos = cIndex+1;
			}
		}
		if (numStartPos < strLength)
		{
			u32 parsedValue = 0;
			if (TryParseU32(&str[numStartPos], strLength-numStartPos, &parsedValue))
			{
				if (pass == 1)
				{
					Assert(result != nullptr && numNumbers < resultCount);
					result[numNumbers] = parsedValue;
				}
				numNumbers++;
			}
		}
		
		if (pass == 0)
		{
			if (numNumbersOut != nullptr) { *numNumbersOut = numNumbers; }
			
			if (arenaPntr == nullptr || numNumbers == 0)
			{
				return nullptr;
			}
			else
			{
				result = PushArray(arenaPntr, u32, numNumbers);
				resultCount = numNumbers;
			}
		}
		else
		{
			Assert(numNumbers == resultCount);
		}
	}
	
	return result;
}

bool TryParseHexU32(const char* str, u32 numCharacters, u32* outValue)
{
	Assert(str != nullptr);
	while (numCharacters > 0 && IsCharClassWhitespace((u8)str[0])) { str++; numCharacters--; } //chop off leading whitespace
	while (numCharacters > 0 && IsCharClassWhitespace((u8)str[numCharacters-1])) { numCharacters--; } //chop off trailing whitespace
	if (numCharacters == 0) { return false; }
	if (numCharacters > 8) { return false; }
	if (!IsStringHex(str, numCharacters)) { return false; }
	
	u32 result = 0x00000000;
	for (u32 cIndex = 0; cIndex < numCharacters; cIndex++)
	{
		result <<= 4;
		result |= GetHexCharValue(str[cIndex]);
	}
	if (outValue != nullptr) { *outValue = result; }
	return true;
}

//NOTE: This function always expects 2 bytes in strPntr. No spaces are allowed
bool TryParseHexU8(const char* strPntr, u8* outValue)
{
	if (!IsCharClassHexChar((u8)strPntr[0]) || !IsCharClassHexChar((u8)strPntr[1]))
	{
		return false;
	}
	u8 value = (u8)((GetHexCharValue(strPntr[0]) << 4) | GetHexCharValue(strPntr[1]));
	if (outValue != nullptr) { *outValue = value; }
	return true;
}

u8* TryParseHexBytes(const char* strPntr, u32 strLength, MemoryArena_t* memArena, u32* numBytesOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	Assert(numBytesOut != nullptr);
	if (strLength == 0)
	{
		*numBytesOut = 0;
		return nullptr;
	}
	
	u32 numBytesFound = 0;
	for (u32 cIndex = 0; cIndex+2 <= strLength; cIndex++)
	{
		u8 byteValue = 0;
		if (TryParseHexU8(&strPntr[cIndex], &byteValue))
		{
			numBytesFound++;
			cIndex++;
		}
	}
	
	*numBytesOut = numBytesFound;
	if (memArena == nullptr) { return nullptr; }
	
	u8* result = PushArray(memArena, u8, numBytesFound);
	u32 bIndex = 0;
	for (u32 cIndex = 0; cIndex+2 <= strLength; cIndex++)
	{
		u8 byteValue = 0;
		if (TryParseHexU8(&strPntr[cIndex], &byteValue))
		{
			Assert(bIndex < numBytesFound);
			result[bIndex] = byteValue;
			bIndex++;
			cIndex++;
		}
	}
	Assert(bIndex == numBytesFound);
	
	return result;
}

bool TryParseIPv4(const char* strPntr, u32 strLength, u8* outValues)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strPntr == nullptr || strLength == 0) { return false; }
	
	u8 numPeriodsFound = 0;
	u32 lastPeriodIndex = 0;
	for (u32 cIndex = 0; cIndex <= strLength; cIndex++)
	{
		char c = (cIndex >= strLength ? '.' : strPntr[cIndex]);
		if (c == '.')
		{
			if (numPeriodsFound >= 4) { return false; } //too many numbers
			if (cIndex <= lastPeriodIndex) { return false; } //empty number
			
			u16 parsedValue = 0;
			if (!TryParseU16(&strPntr[lastPeriodIndex], cIndex - lastPeriodIndex, &parsedValue) || parsedValue > 255)
			{
				return false; //Not a valid u8 number
			}
			
			if (outValues != nullptr) { outValues[numPeriodsFound] = (u8)parsedValue; }
			numPeriodsFound++;
			lastPeriodIndex = cIndex+1;
		}
	}
	
	if (numPeriodsFound < 4)
	{
		return false; //not enough numbers found
	}
	else
	{
		return true;
	}
}

struct UrlParseInfo_t
{
	bool valid;
	
	const char* protocolPntr; //nullptr means no protocol specified
	u32 protocolLength;
	
	const char* hostnamePntr; //includes subdomain, name, and suffix
	u32 hostnameLength;
	
	const char* subdomainPntr; //nullptr means no subdomain specified
	u32 subdomainLength;
	
	const char* namePntr; //nullptr means no name specified
	u32 nameLength;
	
	const char* suffixPntr; //nullptr means no suffix (TLD) specified
	u32 suffixLength;
	
	const char* pathPntr; //nullptr means no path specified
	u32 pathLength;
	
	const char* varsPntr; //nullptr means no vars specified
	u32 varsLength;
	
	i32 port; //-1 means no port specified
};

bool TryParseUrl(const char* strPntr, u32 strLength, UrlParseInfo_t* infoOut = nullptr, bool requireSubdomain = true, bool requireProtocol = false, bool requirePath = false)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (infoOut != nullptr) { ClearPointer(infoOut); }
	if (strPntr == nullptr || strLength == 0) { return false; }
	
	UrlParseInfo_t info = {};
	info.port = -1;
	
	bool isValid = true;
	i32 protocolSep = -1;
	i32 subdomainSep = -1;
	i32 suffixSep = -1;
	i32 pathSep = -1;
	i32 varsSep = -1;
	i32 portSep = -1;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char c = strPntr[cIndex];
		char nextChar = (cIndex+1 < strLength) ? strPntr[cIndex+1] : '\0';
		char nextNextChar = (cIndex+2 < strLength) ? strPntr[cIndex+2] : '\0';
		
		if (c == ':' && nextChar == '/' && nextNextChar == '/')
		{
			if (protocolSep == -1 && subdomainSep == -1 && subdomainSep == -1 && pathSep == -1 && varsSep == -1)
			{
				protocolSep = (i32)cIndex;
				cIndex += 2; //skip over "//"
			}
			else //protocol seperator "://" found in the wrong place
			{
				isValid = false;
				break;
			}
		}
		else if (c == '.')
		{
			if (pathSep == -1 && varsSep == -1 && portSep == -1)
			{
				if (suffixSep == -1)
				{
					suffixSep = (i32)cIndex;
				}
				else if (subdomainSep == -1)
				{
					subdomainSep = suffixSep;
					suffixSep = (i32)cIndex;
				}
				else //Too many '.' in hostname
				{
					isValid = false;
					break;
				}
			}
			else //found . after path/vars/port seperator
			{
				isValid = false;
				break;
			}
		}
		else if (c == '/')
		{
			if (pathSep == -1 && portSep == -1)
			{
				if (suffixSep != -1)
				{
					pathSep = (i32)cIndex;
				}
				else //No suffix found before path seperator
				{
					isValid = false;
					break;
				}
			}
			else if (portSep != -1) //no '/' should come after the port ':' seperator
			{
				isValid = false;
				break;
			}
			else
			{
				//it's fine to have multiple '/' in the path or vars
			}
		}
		else if (c == ':')
		{
			if (portSep == -1)
			{
				if (suffixSep != -1)
				{
					portSep = (i32)cIndex;
				}
				else //No suffix found before port seperator
				{
					isValid = false;
					break;
				}
			}
			else //Multiple port ':' seperators found
			{
				isValid = false;
				break;
			}
		}
		else if (c == '?')
		{
			if (varsSep == -1)
			{
				if (suffixSep != -1 && portSep == -1)
				{
					varsSep = (i32)cIndex;
				}
				else if (suffixSep == -1) //No suffix found before vars seperator
				{
					isValid = false;
					break;
				}
				else //vars seperator found after port seperator
				{
					isValid = false;
					break;
				}
			}
			else //Multiple vars '?' seperators found
			{
				isValid = false;
				break;
			}
		}
	}
	
	if (!isValid || suffixSep == -1 || (requirePath && pathSep == -1) || (requireProtocol && protocolSep == -1) || (requireSubdomain && subdomainSep == -1))
	{
		if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
		return false;
	}
	Assert(protocolSep == -1 || protocolSep < suffixSep); //protocol should come before suffix
	Assert(protocolSep == -1 || subdomainSep == -1 || protocolSep < subdomainSep); //protocol should also come before subdomain (if it exists)
	Assert(subdomainSep == -1 || subdomainSep < suffixSep); //Subdomain should come bfore suffix
	Assert(pathSep == -1 || suffixSep < pathSep); //suffix should come before path
	Assert(varsSep == -1 || suffixSep < varsSep); //suffix should come before vars
	Assert(portSep == -1 || suffixSep < portSep); //suffix should come before port
	Assert(pathSep == -1 || varsSep == -1 || pathSep < varsSep); //path should come before vars
	Assert(pathSep == -1 || portSep == -1 || pathSep < portSep); //path should come before port
	Assert(varsSep == -1 || portSep == -1 || varsSep < portSep); //vars should come before port
	
	i32 hostnameStart = (protocolSep == -1) ? 0 : protocolSep + 3;
	i32 hostnameEnd = (i32)strLength;
	if (pathSep != -1) { hostnameEnd = pathSep; }
	else if (varsSep != -1) { hostnameEnd = varsSep; }
	else if (portSep != -1) { hostnameEnd = portSep; }
	Assert(hostnameStart >= 0 && (u32)hostnameStart < strLength);
	Assert(hostnameEnd >= 0 && (u32)hostnameEnd <= strLength);
	Assert(hostnameStart != hostnameEnd);
	
	if (hostnameStart == subdomainSep) //Empty sub-domain found
	{
		if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
		return false;
	}
	else if (hostnameStart == suffixSep || subdomainSep == suffixSep) //Empty name found
	{
		if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
		return false;
	}
	else if (hostnameEnd == suffixSep+1) //Empty suffix found
	{
		if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
		return false;
	}
	
	if (protocolSep != -1)
	{
		info.protocolPntr = strPntr;
		info.protocolLength = (u32)protocolSep;
	}
	
	info.hostnamePntr = &strPntr[hostnameStart];
	info.hostnameLength = (u32)(hostnameEnd - hostnameStart);
	
	if (subdomainSep != -1)
	{
		info.subdomainPntr = &strPntr[hostnameStart];
		info.subdomainLength = (u32)(subdomainSep - hostnameStart);
		
		info.namePntr = &strPntr[subdomainSep+1];
		info.nameLength = (u32)(suffixSep - (subdomainSep+1));
	}
	else
	{
		info.namePntr = &strPntr[hostnameStart];
		info.nameLength = (u32)(suffixSep - hostnameStart);
	}
	
	info.suffixPntr = &strPntr[suffixSep+1];
	info.suffixLength = (u32)(hostnameEnd - (suffixSep+1));
	
	if (pathSep != -1)
	{
		i32 pathEnd = (i32)strLength;
		if (varsSep != -1) { pathEnd = varsSep; }
		else if (portSep != -1) { pathEnd = portSep; }
		
		info.pathPntr = &strPntr[pathSep];
		info.pathLength = (u32)(pathEnd - pathSep);
	}
	
	if (varsSep != -1)
	{
		i32 varsEnd = (i32)strLength;
		if (portSep != -1) { varsEnd = portSep; }
		if (varsSep+1 < varsEnd)
		{
			info.varsPntr = &strPntr[varsSep+1];
			info.varsLength = (u32)(varsEnd - (varsSep+1));
		}
		else //empty vars list
		{
			if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
			return false;
		}
	}
	
	if (portSep != -1)
	{
		if ((u32)portSep+1 < strLength)
		{
			const char* portPntr = &strPntr[portSep + 1];
			u32 portLength = (u32)(strLength - (portSep + 1));
			u16 portValue = 0;
			if (TryParseU16(portPntr, portLength, &portValue))
			{
				info.port = (i32)portValue;
			}
			else //port was an invalid number
			{
				if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
				return false;
			}
		}
		else //No number found after port seperator
		{
			if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
			return false;
		}
	}
	
	if (infoOut != nullptr) { MyMemCopy(infoOut, &info, sizeof(UrlParseInfo_t)); }
	return true;
}

#endif //  _MY_PARSING_FUNCTIONS_H
