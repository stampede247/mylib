import os, sys, re

# +--------------------------------------------------------------+
# |                        Imported Stuff                        |
# +--------------------------------------------------------------+
def IsWhitespace(char):
#
	if (char == '\t' or char == ' ' or char == '\n' or char == '\r'):
	#
		return True
	#
	else:
	#
		return False
	#
#

def IsLower(char):
#
	return ord(char) >= ord('a') and ord(char) <= ord('z')
#

def IsUpper(char):
#
	return ord(char) >= ord('A') and ord(char) <= ord('Z')
#

def IsLetter(char):
#
	return IsLower() or IsUpper()
#

def IsNumber(char):
#
	return ord(char) >= ord('0') and ord(char) <= ord('9')
#

def IsIndentChar(char):
#
	if (IsUpper(char) or IsLower(char) or IsNumber(char) or char == '_'):
	#
		return True
	#
	else: return False
#

class LexicalPiece():
#
	def __init__(self):
	#
		self.type = "Empty"
		self.value = ""
	#
	
	def __str__(self):
	#
		return "LexicalPiece(%s \"%s\")" % (self.type, self.value)
	#
	
	def __repr__(self):
	#
		if   (self.type == "Syntax"):     return "%s" % self.value
		elif (self.type == "Identifier"): return "\"%s\"" % self.value
		else: return "[%s]" % self.value
	#
#

def LexicalParse(inputStr):
#
	results = []
	lastSyntaxChar = 0
	
	for cIndex in range(0, len(inputStr)+1):
	#
		c = " "
		if (cIndex < len(inputStr)):
		#
			c = inputStr[cIndex]
		#
		
		if (IsIndentChar(c)):
		#
			pass # print("I:", c)
		#
		else:
		#
			newPartStr = inputStr[lastSyntaxChar:cIndex]
			
			if (len(newPartStr) > 0):
			#
				newPiece = LexicalPiece()
				newPiece.type = "Identifier"
				newPiece.value = newPartStr
				results.append(newPiece)
				# print("Piece:", newPiece)
			#
			
			if (IsWhitespace(c) == False):
			#
				syntaxPiece = LexicalPiece()
				syntaxPiece.type = "Syntax"
				syntaxPiece.value = c
				results.append(syntaxPiece)
				# print("S: \'%c\'" % c)
			#
			
			lastSyntaxChar = cIndex+1
		#
	#
	
	return results
#

def StringifyLexicalPieces(pieces):
#
	result = ""
	
	wasSyntax = True
	for piece in pieces:
	#
		if (piece.type == "Syntax"):
		#
			result += piece.value
			wasSyntax = True
		#
		elif (piece.type == "Identifier"):
		#
			if (wasSyntax == False): result += " "
			result += piece.value
			wasSyntax = False
		#
		else:
		#
			print("Unknown piece type: %s" % (piece.type))
		#
	#
	
	return result
#

class CppFunction():
#
	def __init__(self):
	#
		self.valid = False
		self.fullStr = ""
		self.name = ""
		self.returnType = ""
		self.parameterTypes = []
		self.parametersOptional = []
		self.parameters = []
		self.lexicalPieces = []
	#
	
	def __init__(self, functionStr):
	#
		self.valid = False
		self.fullStr = functionStr
		self.name = ""
		self.returnType = ""
		self.parameterTypes = []
		self.parametersOptional = []
		self.parameters = []
		self.lexicalPieces = []
		
		self.NewTryParse(functionStr)
	#
	
	def __repr__(self):
	#
		if (self.valid):
		#
			result = "%s %s(" % (self.returnType, self.name)
			paramStr = ""
			for pIndex in range(0, len(self.parameters)):
			#
				paramName = self.parameters[pIndex]
				paramType = self.parameters[pIndex]
				if (paramStr != ""): paramStr += ", "
				if (paramType != ""): paramStr += paramType
				paramStr += paramName
			#
			result += paramStr + ")"
			return result
		#
		else: return "Invalid CppFunction"
	#
	
	def NewTryParse(self, functionStr):
	#
		self.valid = True
		self.name = ""
		self.returnType = ""
		self.parameterTypes = []
		self.parametersOptional = []
		self.parameters = []
		functionStr = functionStr.replace("...", "args")
		self.lexicalPieces = LexicalParse(functionStr)
		# print("Lexical Pieces:", self.lexicalPieces)
		
		if (len(self.lexicalPieces) == 0):
		#
			print("No lexical pieces found")
			self.valid = False
			return False
		#
		
		beforeParenPieces = []
		parameterPieceLists = []
		
		foundOpenParens = False
		foundCloseParens = False
		newParamPieces = []
		for piece in self.lexicalPieces:
		#
			if (piece.type == "Syntax" and piece.value == "("):
			#
				if (foundOpenParens):
				#
					print("Found more than one open parenthesis!")
					self.valid = False
					return False
				#
				elif (foundCloseParens):
				#
					print("Found open parenthesis after close parenthesis!")
					self.valid = False
					return False
				#
				else:
				#
					foundOpenParens = True
				#
			#
			elif (piece.type == "Syntax" and piece.value == ")"):
			#
				if (foundCloseParens):
				#
					print("Found more than one close parenthesis!")
					self.valid = False
					return False
				#
				elif (foundOpenParens == False):
				#
					print("Found close parenthesis before open parenthesis!")
					self.valid = False
					return False
				#
				else:
				#
					foundCloseParens = True
					if (len(newParamPieces) > 0):
					#
						parameterPieceLists.append(newParamPieces)
						newParamPieces = []
					#
				#
			#
			elif (piece.type == "Syntax" and piece.value == ","):
			#
				if (foundOpenParens == False):
				#
					print("Found comma before open parenthesis")
					self.valid = False
					return False
				#
				elif (foundCloseParens):
				#
					print("Found comma after close parenthesis")
					self.valid = False
					return False
				#
				else:
				#
					if (len(newParamPieces) > 0):
					#
						parameterPieceLists.append(newParamPieces)
						newParamPieces = []
					#
				#
			#
			else:
			#
				if (foundOpenParens == False):
				#
					beforeParenPieces.append(piece)
				#
				elif (foundCloseParens == False):
				#
					newParamPieces.append(piece)
				#
				else:
				#
					pass # print("Ignoring part after close parnthesis: %s" % (piece))
				#
			#
		#
		
		if (foundOpenParens == False):
		#
			print("No open parenthesis found")
			self.valid = False
			return False
		#
		elif (foundCloseParens == False):
		#
			print("Found open parenthesis with no close parenthesis")
			self.valid = False
			return False
		#
		
		# print("Found %u pieces before parenthesis:" % (len(beforeParenPieces)))
		# for piece in beforeParenPieces: print("\t", piece)
		# print("Found %u parameters:" % (len(parameterPieceLists)))
		# for parameterPieces in parameterPieceLists:
		# #
		# 	print("\t%u Parts:" % (len(parameterPieces)))
		# 	for piece in parameterPieces: print("\t\t", piece)
		# #
		
		# We need to parse the function name and return type
		if (len(beforeParenPieces) == 0):
		#
			print("No lexical pieces before open parenthesis")
			self.valid = False
			return False
		#
		elif (beforeParenPieces[-1].type == "Syntax"):
		#
			print("Found syntax, not function name, before parenthesis: %s" % (beforeParenPieces[-1]))
			self.valid = False
			return False
		#
		elif (len(beforeParenPieces) == 1):
		#
			print("Warning: No function return type found")
			self.name = beforeParenPieces[0].value
			self.returnType = ""
		#
		else:
		#
			self.returnType = StringifyLexicalPieces(beforeParenPieces[0:-1])
			self.name = beforeParenPieces[-1].value
		#
		
		# Now we need to parse each parameter piece list
		
		for pIndex in range(0, len(parameterPieceLists)):
		#
			pieceList = parameterPieceLists[pIndex]
			if (len(pieceList) == 0):
			#
				print("Parameter[%u] has no pieces" % (pIndex))
				self.valid = False
				return False
			#
			elif (len(pieceList) == 1 and pieceList[0].type == "Syntax"):
			#
				print("Parameter[%u] ended in Syntax piece, not Identifier" % (pIndex))
				self.valid = False
				return False
			#
			elif (len(pieceList) == 1):
			#
				# print("Warning: No type for parameter[%u]" % (pIndex))
				self.parameterTypes.append("")
				self.parametersOptional.append(False)
				self.parameters.append(pieceList[0].value)
			#
			else:
			#
				beforeEquals = []
				afterEquals = []
				containsEquals = False
				for piece in pieceList:
				#
					if (piece.type == "Syntax" and piece.value == "="):
					#
						if (containsEquals == False):
						#
							containsEquals = True
						#
						else:
						#
							print("Found more than one equals in parameter[%u]" % (pIndex))
							self.valid = False
							return False
						#
					#
					else:
					#
						if (containsEquals): afterEquals.append(piece)
						else:               beforeEquals.append(piece)
					#
				#
				
				if (len(beforeEquals) == 0):
				#
					print("No items found before = in parameter[%u]" % (pIndex))
					self.valid = False
					return False
				#
				elif (beforeEquals[-1].type == "Syntax"):
				#
					print("Expected parameter name, not Syntax, before = in parameter[%u]" % (pIndex))
					self.valid = False
					return False
				#
				elif (containsEquals and len(afterEquals) == 0):
				#
					print("Expected assignment after = in parameter[%u]" % (pIndex))
					self.valid = False
					return False
				#
				elif (len(beforeEquals) == 1):
				#
					print("Warning: No type for parameter[%u]" % (pIndex))
					self.parameterTypes.append("")
					self.parametersOptional.append(containsEquals)
					self.parameters.append(beforeEquals[0].value)
				#
				else:
				#
					self.parameterTypes.append(StringifyLexicalPieces(beforeEquals[0:-1]))
					self.parametersOptional.append(containsEquals)
					self.parameters.append(beforeEquals[-1].value)
				#
			#
		#
	#
#end of CppFunction class

def GetFilesRecursive(directoryPath):
#
	result = []
	dirPathFull = directoryPath
	if (not dirPathFull.endswith("/") and not dirPathFull.endswith("\\")):
	#
		dirPathFull = dirPathFull + "/"
		# print("adding / to \"" + directoryPath + "\"")
	#
	# print("Searching \"" + dirPathFull + "\"")
	
	for filename in os.listdir(directoryPath):
	#
		filePath = dirPathFull + filename
		if (os.path.isfile(filePath)):
		#
			# print("File:", filename)
			result.append(filePath)
		#
		elif (os.path.isdir(filePath)):
		#
			newResults = GetFilesRecursive(filePath)
			result.extend(newResults)
		#
		else:
		#
			print("Unknown item in directory:", filePath)
		#
	#
	
	return result
#

# +--------------------------------------------------------------+
# |                          Parse File                          |
# +--------------------------------------------------------------+
def StripComments(line, lineStartedComment):
#
	isComment = lineStartedComment
	isBlockComment = lineStartedComment
	
	#TODO: Actually strip the comments here
	
	return line
#

def ParseFile(fileContents):
#
	functions = []
	fileSize = len(fileContents)
	
	lineStart = 0
	lastChar = chr(0)
	isString = False
	stringStart = 0
	isComment = False
	isBlockComment = False
	commentStart = 0
	lineStartedComment = False
	
	lineNumber = 0
	cIndex = 0
	while (cIndex < fileSize):
	#
		newChar = fileContents[cIndex]
		nextChar = chr(0)
		if (cIndex+1 < fileSize): nextChar = fileContents[cIndex+1]
		
		if (newChar == '\n' or newChar == '\r'):
		#
			line = fileContents[lineStart:cIndex]
			lineContent = StripComments(line, lineStartedComment)
			if (len(lineContent) > 0):
			#
				#TODO: Process the line
				pass
			#
			lineStart = cIndex+1
			if (cIndex+1 < fileSize):
			#
				if (newChar == '\n' and nextChar == '\r'): cIndex += 1
				if (newChar == '\r' and nextChar == '\n'): cIndex += 1
			#
			if (isComment and not isBlockComment):
			#
				isComment = False
				# if (cIndex > commentStart): print("Comment", lineNumber+1, ":", fileContents[commentStart:cIndex])
			#
			if (isString and lastChar != '\\'): isString = False
			lineStartedComment = isComment
			lineNumber += 1
		#
		elif (newChar == '/'):
		#
			if (isComment and isBlockComment):
			#
				if (lastChar == '*'):
				#
					isComment = False
					isBlockComment = False
					# if (cIndex-1 > commentStart): print("Comment:", fileContents[commentStart:cIndex-1])
				#
			#
			elif (not isComment and not isString):
			#
				if (nextChar == '/'):
				#
					isComment = True
					commentStart = cIndex+2
				#
				elif (nextChar == '*'):
				#
					isComment = True
					isBlockComment = True
					commentStart = cIndex+2
				#
			#
		#
		elif (newChar == '\"'):
		#
			if (not isComment):
			#
				if (not isString):
				#
					isString = True
					stringStart = cIndex+1
				#
				elif (lastChar != '\\'):
				#
					isString = False
					# if (stringStart != cIndex): print("String", lineNumber+1, ":", fileContents[stringStart:cIndex])
				#
			#
		#
		
		lastChar = newChar
		cIndex += 1
	#
	# print(numEmptyLines, "empty lines")
	return functions
#

# +--------------------------------------------------------------+
# |                            Script                            |
# +--------------------------------------------------------------+
if (len(sys.argv) != 3):
#
	print("Usage: GenerateFunctionTable.py [code_folder_path] [output_file_path]")
	exit()
#

codeFolderPath = sys.argv[1]
outputFilePath = sys.argv[2]

files = GetFilesRecursive(codeFolderPath)
print("Found", len(files), "files")

fIndex = 0
for filePath in files:
#
	if (filePath.endswith(".cpp") or filePath.endswith(".h") or filePath.endswith(".c")):
	#
		file = open(filePath)
		fileContents = file.read()
		file.close()
		newFunctions = ParseFile(fileContents)
	#
	print(fIndex+1, "/", len(files), "processed (", filePath, ")")
	fIndex += 1
	# break
#

# function = CppFunction("int    main()")
# if (function.valid):
