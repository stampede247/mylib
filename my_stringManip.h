/*
File:   my_stringManip.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file contains functions that operate on C strings (const char*)
	** Some of them can be performed in place,
	** some require knowing the size of the buffer to make sure they don't overflow,
	** and some require a pointer to a MemoryArena_t to allocate new space to hold the result

This file is #included by default from mylib.h
*/

#ifndef _MY_STRING_MANIP_H
#define _MY_STRING_MANIP_H

//TODO: This really should be renamed to be something like "String_t" or whatever
struct StrSplitPiece_t
{
	u32 length;
	const char* pntr;
};

//This macros expands into: nullTermString, MyStrLength32(nullTermString) allowing you to easily pass
//null-terminated strings to functions that require a strLength
#define NtStr(nullTermString) nullTermString, MyStrLength32(nullTermString)

#define BufferStringCopy(array, string) do         \
{                                                  \
	strncpy((array), string, ArrayCount(array)-1); \
	(array)[ArrayCount(array)-1] = '\0';           \
} while (0)
#define BufferStringCopyEx(arrayPntr, arrayLength, string) do \
{                                                             \
	strncpy((arrayPntr), (string), (arrayLength)-1);          \
	(arrayPntr)[(arrayLength)-1] = '\0';                      \
} while (0)
#define BufferPrint(array, formatStr, ...) do                                      \
{                                                                                  \
	int snprintfResult = snprintf(array, sizeof(array), formatStr, ##__VA_ARGS__); \
	Assert(snprintfResult >= 0 && (u32)snprintfResult < sizeof(array));            \
	array[snprintfResult] = '\0';                                                  \
} while (0)
#define BufferPrintEx(arrayPntr, arrayLength, formatStr, ...) do                         \
{                                                                                        \
	int snprintfResult = snprintf((arrayPntr), (arrayLength), formatStr, ##__VA_ARGS__); \
	Assert(snprintfResult >= 0 && (u32)snprintfResult < (arrayLength));                  \
	(arrayPntr)[snprintfResult] = '\0';                                                  \
} while (0)

bool BufferIsNullTerminated(const char* buffer, u32 bufferSize)
{
	Assert(buffer != nullptr);
	for (u32 cIndex = 0; cIndex < bufferSize; cIndex++)
	{
		if (buffer[cIndex] == '\0') { return true; }
	}
	return false;
}

u32 MeasurePrint(const char* formatStr, ...)
{
	va_list args;
	va_start(args, formatStr);
	u32 length = (u32)MyVaListPrintf(nullptr, 0, formatStr, args);//Measure first
	va_end(args);
	return length;
}

char* MallocPrint(const char* formatStr, ...)
{
	char* result = "";
	u32 resultSize = 0;
	
	va_list args;
	va_start(args, formatStr);
	resultSize = (u32)MyVaListPrintf(result, 0, formatStr, args);/*Measure first*/
	va_end(args);
	
	result = (char*)MyMalloc(resultSize+1);
	
	va_start(args, formatStr);
	MyVaListPrintf(result, resultSize+1, formatStr, args); /*Real printf*/
	va_end(args);
	
	result[resultSize] = '\0';
	return result;
}

void TwoPassPrint(u32* numCharsPntr, char* resultPntr, u32 resultLength, const char* formatStr, ...)
{
	if (numCharsPntr == nullptr) { return; }
	if (formatStr == nullptr) { return; }
	
	char* printResult = "";
	u32 newPrintSize = 0;
	va_list args;
	va_start(args, formatStr);
	newPrintSize = (u32)MyVaListPrintf(printResult, 0, formatStr, args);/*Measure first*/
	va_end(args);
	
	if (resultPntr != nullptr && *numCharsPntr + newPrintSize <= resultLength)
	{
		printResult = &resultPntr[*numCharsPntr];
		va_start(args, formatStr);
		MyVaListPrintf(printResult, newPrintSize+1, formatStr, args); /*Real printf*/
		va_end(args);
	}
	
	*numCharsPntr += newPrintSize;
}

void StrReplaceCharInPlace(char* str, u32 strLength, char oldChar, char newChar)
{
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (str[cIndex] == oldChar)
		{
			str[cIndex] = newChar;
		}
	}
}

char* StrReplaceChar(MemoryArena_t* arenaPntr, const char* str, u32 strLength, char oldChar, char newChar)
{
	char* result = (char*)ArenaPush(arenaPntr, strLength+1);
	MyMemCopy(result, str, strLength);
	result[strLength] = '\0';
	StrReplaceCharInPlace(result, strLength, oldChar, newChar);
	return result;
}

void StrReplaceInPlace(char* str, u32 strLength, const char* target, const char* replace, u32 replaceLength)
{
	Assert(str != nullptr);
	Assert(target != nullptr);
	Assert(replace != nullptr);
	
	u32 cIndex = 0;
	while (cIndex < strLength)
	{
		if (cIndex + replaceLength <= strLength)
		{
			if (MyMemCompare(&str[cIndex], target, replaceLength) == 0)
			{
				MyMemCopy(&str[cIndex], replace, replaceLength);
				cIndex += replaceLength;
			}
			else
			{
				//Character is already correct
				cIndex++;
			}
		}
		else
		{
			//Character is already correct
			cIndex++;
		}
	}
	
	Assert(cIndex == strLength);
}

char* StrReplace(MemoryArena_t* arenaPntr, const char* str, u32 strLength, const char* target, u32 targetLength, const char* replace, u32 replaceLength, u32* newLengthOut = nullptr)
{
	Assert(str != nullptr);
	Assert(target != nullptr);
	Assert(replace != nullptr);
	
	u32 resultLength = 0;
	
	// +==============================+
	// |         Measurement          |
	// +==============================+
	u32 cIndex = 0;
	while (cIndex < strLength)
	{
		if (cIndex + targetLength <= strLength)
		{
			if (MyMemCompare(&str[cIndex], target, targetLength) == 0)
			{
				resultLength += replaceLength;
				cIndex += targetLength;
			}
			else
			{
				resultLength++;
				cIndex++;
			}
		}
		else
		{
			resultLength++;
			cIndex++;
		}
	}
	
	if (newLengthOut != nullptr) { *newLengthOut = resultLength; }
	if (arenaPntr == nullptr)
	{
		//The whole point without an arenaPntr is to measure the new length
		Assert(newLengthOut != nullptr);
		return nullptr;
	}
	
	// +==============================+
	// |           Allocate           |
	// +==============================+
	char* result = (char*)ArenaPush(arenaPntr, resultLength+1);
	
	// +==============================+
	// |       Fill Allocation        |
	// +==============================+
	u32 fromIndex = 0;
	u32 toIndex = 0;
	while (fromIndex < strLength)
	{
		Assert(fromIndex < strLength);
		Assert(toIndex < resultLength);
		
		if (fromIndex + targetLength <= strLength)
		{
			if (MyMemCompare(&str[fromIndex], target, targetLength) == 0)
			{
				Assert(toIndex + replaceLength <= resultLength);
				MyMemCopy(&result[toIndex], replace, replaceLength);
				toIndex += replaceLength;
				fromIndex += targetLength;
			}
			else
			{
				result[toIndex] = str[fromIndex];
				toIndex++;
				fromIndex++;
			}
		}
		else
		{
			result[toIndex] = str[fromIndex];
			toIndex++;
			fromIndex++;
		}
	}
	
	Assert(toIndex == resultLength);
	result[resultLength] = '\0';
	return result;
}

void StrSpliceInPlace(char* str, u32 strLength, u32 spliceStart, u32 spliceLength, const char* replace)
{
	Assert(str != nullptr);
	Assert(replace != nullptr);
	Assert(spliceLength <= strLength);
	Assert(spliceStart <= strLength);
	Assert(spliceStart + spliceLength <= strLength);
	
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (cIndex >= spliceStart && cIndex < spliceStart+spliceLength)
		{
			str[cIndex] = replace[cIndex-spliceStart];
		}
	}
}

char* StrSplice(MemoryArena_t* arenaPntr, const char* str, u32 strLength, u32 spliceStart, u32 spliceLength, const char* replace, u32 replaceLength)
{
	Assert(str != nullptr);
	Assert(replace != nullptr);
	Assert(spliceLength <= strLength);
	Assert(spliceStart <= strLength);
	Assert(spliceStart + spliceLength <= strLength);
	
	u32 resultLength = strLength - spliceLength + replaceLength;
	
	char* result = (char*)ArenaPush(arenaPntr, resultLength+1);
	
	char* charPntr = result;
	MyMemCopy(charPntr, &str[0], spliceStart); charPntr += spliceStart;
	MyMemCopy(charPntr, replace, replaceLength); charPntr += replaceLength;
	MyMemCopy(charPntr, &str[spliceStart + spliceLength], strLength - (spliceStart + spliceLength)); charPntr += strLength - (spliceStart + spliceLength);
	Assert(charPntr == result + resultLength);
	
	// u32 fromIndex = 0;
	// u32 toIndex = 0;
	// while (fromIndex < strLength)
	// {
	// 	if (fromIndex == spliceStart)
	// 	{
	// 		MyMemCopy(&result[toIndex], replace, replaceLength);
	// 		fromIndex += spliceLength;
	// 		toIndex += replaceLength;
			
	// 		if (fromIndex == strLength) { break; }
	// 	}
		
	// 	result[toIndex] = str[fromIndex];
	// 	toIndex++;
	// 	fromIndex++;
	// }
	
	// Assert(fromIndex == strLength);
	// Assert(toIndex == resultLength);
	
	result[resultLength] = '\0';
	return result;
}

char* MyStrCat(MemoryArena_t* arenaPntr, const char* str1, u32 str1Length, const char* str2, u32 str2Length)
{
	char* result = StrSplice(arenaPntr, str1, str1Length, str1Length, 0, str2, str2Length);
	return result;
}
char* MyStrCatNt(MemoryArena_t* arenaPntr, const char* nullTermStr1, const char* nullTermStr2)
{
	NotNull(nullTermStr1);
	NotNull(nullTermStr2);
	return MyStrCat(arenaPntr, nullTermStr1, MyStrLength32(nullTermStr1), nullTermStr2, MyStrLength32(nullTermStr2));
}

char* GetFileNamePart(char* filePath) //pre-declared in mylib.h
{
	Assert(filePath != nullptr);
	
	char* result = filePath;
	u32 cIndex = 0;
	
	while (filePath[cIndex] != '\0')
	{
		if (filePath[cIndex] == '/' || filePath[cIndex] == '\\')
		{
			result = &filePath[cIndex+1];
		}
		cIndex++;
	}
	
	return result;
}
const char* GetFileNamePart(const char* filePath) //const variant, pre-declared in mylib.h
{
	return (const char*)GetFileNamePart((char*)filePath);
}

StrSplitPiece_t GetPathDirectoryPart(char* filePath, u32 filePathLength)
{
	Assert(filePath != nullptr || filePathLength == 0);
	StrSplitPiece_t result = {};
	result.pntr = filePath;
	result.length = 0;
	for (u32 cIndex = 0; cIndex < filePathLength; cIndex++)
	{
		if (filePath[cIndex] == '/' || filePath[cIndex] == '\\')
		{
			result.length = cIndex+1;
		}
	}
	return result;
}
StrSplitPiece_t GetPathDirectoryPart(const char* filePath, u32 filePathLength) //const variant
{
	return GetPathDirectoryPart((char*)filePath, filePathLength);
}

StrSplitPiece_t GetPathDirectoryPartNt(char* filePath)
{
	NotNull(filePath);
	return GetPathDirectoryPart((char*)filePath, MyStrLength32(filePath));
}
StrSplitPiece_t GetPathDirectoryPartNt(const char* filePath) //const variant
{
	NotNull(filePath);
	return GetPathDirectoryPart((char*)filePath, MyStrLength32(filePath));
}

StrSplitPiece_t GetPathFilePart(char* filePath, u32 filePathLength)
{
	Assert(filePath != nullptr || filePathLength == 0);
	StrSplitPiece_t result = {};
	result.pntr = filePath;
	result.length = filePathLength;
	for (u32 cIndex = 0; cIndex < filePathLength; cIndex++)
	{
		if (filePath[cIndex] == '/' || filePath[cIndex] == '\\')
		{
			result.pntr = &filePath[cIndex+1];
			result.length = filePathLength - (cIndex+1);
		}
	}
	return result;
}
StrSplitPiece_t GetPathFilePart(const char* filePath, u32 filePathLength) //const variant
{
	return GetPathFilePart((char*)filePath, filePathLength);
}

StrSplitPiece_t GetPathFilePartNt(char* filePath)
{
	NotNull(filePath);
	return GetPathFilePart(filePath, MyStrLength32(filePath));
}
StrSplitPiece_t GetPathFilePartNt(const char* filePath) //const variant
{
	NotNull(filePath);
	return GetPathFilePart((char*)filePath, MyStrLength32(filePath));
}

bool GetPathDirAndFileParts(char* filePath, u32 filePathLength, StrSplitPiece_t* dirPieceOut, StrSplitPiece_t* filePieceOut)
{
	Assert(filePath != nullptr || filePathLength == 0);
	if (dirPieceOut != nullptr) { dirPieceOut->pntr = filePath; dirPieceOut->length = 0; }
	if (filePieceOut != nullptr) { filePieceOut->pntr = filePath; filePieceOut->length = filePathLength; }
	bool result = false;
	for (u32 cIndex = 0; cIndex < filePathLength; cIndex++)
	{
		if (filePath[cIndex] == '/' || filePath[cIndex] == '\\')
		{
			if (dirPieceOut != nullptr) { dirPieceOut->pntr = filePath; dirPieceOut->length = cIndex+1; }
			if (filePieceOut != nullptr) { filePieceOut->pntr = &filePath[cIndex+1]; filePieceOut->length = filePathLength - (cIndex+1); }
			result = true;
		}
	}
	return result;
}
bool GetPathDirAndFileParts(const char* filePath, u32 filePathLength, StrSplitPiece_t* dirPieceOut, StrSplitPiece_t* filePieceOut) //const variant
{
	return GetPathDirAndFileParts((char*)filePath, filePathLength, dirPieceOut, filePieceOut);
}

bool GetPathDirAndFilePartsNt(char* filePath, StrSplitPiece_t* dirPieceOut, StrSplitPiece_t* filePieceOut)
{
	NotNull(filePath);
	return GetPathDirAndFileParts(filePath, MyStrLength32(filePath), dirPieceOut, filePieceOut);
}
bool GetPathDirAndFilePartsNt(const char* filePath, StrSplitPiece_t* dirPieceOut, StrSplitPiece_t* filePieceOut) //const variant
{
	NotNull(filePath);
	return GetPathDirAndFileParts((char*)filePath, MyStrLength32(filePath), dirPieceOut, filePieceOut);
}

char* GetFilePathLastParts(char* filePath, u32 numParts) //pre-declared in mylib.h
{
	Assert(filePath != nullptr);
	Assert(numParts > 0);
	u32 filePathLength = MyStrLength32(filePath);
	u32 numPartsFound = 0;
	for (u32 cIndex = filePathLength; cIndex > 0; cIndex--)
	{
		char nextChar = filePath[cIndex-1];
		if (nextChar == '/' || nextChar == '\\')
		{
			numPartsFound++;
			if (numPartsFound >= numParts)
			{
				return &filePath[cIndex];
			}
		}
	}
	return filePath;
}
const char* GetFilePathLastParts(const char* filePath, u32 numParts) //const variant, pre-declared in mylib.h
{
	return (const char*)GetFilePathLastParts((char*)filePath, numParts);
}

const char* GetFileExtPart(const char* filePath, bool includePeriod = false, u32* indexOut = nullptr)
{
	Assert(filePath != nullptr);
	
	const char* result = nullptr;
	u32 cIndex = 0;
	
	while (filePath[cIndex] != '\0')
	{
		if (filePath[cIndex] == '.' || filePath[cIndex] == '.')
		{
			if (includePeriod)
			{
				result = &filePath[cIndex];
				if (indexOut != nullptr) { *indexOut = cIndex; }
			}
			else
			{
				result = &filePath[cIndex+1];
				if (indexOut != nullptr) { *indexOut = cIndex+1; }
			}
		}
		cIndex++;
	}
	
	if (result == nullptr && indexOut != nullptr) { *indexOut = cIndex; }
	return result;
}

char* MyUrlEscape(MemoryArena_t* arenaPntr, const char* str, u32 strLength, u32* newStrLengthOut = nullptr)
{
	u32 newStrLength = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char c = str[cIndex];
		if (IsCharClassAlphaNumeric((u8)c) || c == ' ')
		{
			newStrLength += 1;
		}
		else
		{
			newStrLength += 3; //For %XX notation
		}
	}
	
	if (newStrLengthOut != nullptr) { *newStrLengthOut = newStrLength; }
	if (arenaPntr == nullptr) { return nullptr; }
	
	char* result = PushArray(arenaPntr, char, newStrLength+1);
	Assert(result != nullptr);
	
	u32 toIndex = 0;
	for (u32 fromIndex = 0; fromIndex < strLength; fromIndex++)
	{
		Assert(toIndex < newStrLength);
		if (IsCharClassAlphaNumeric((u8)str[fromIndex]))
		{
			result[toIndex++] = str[fromIndex];
		}
		else if (str[fromIndex] == ' ')
		{
			result[toIndex++] = '+';
		}
		else
		{
			Assert(toIndex+3 <= newStrLength);
			result[toIndex++] = '%';
			result[toIndex++] = UpperHexChar((u8)str[fromIndex]);
			result[toIndex++] = LowerHexChar((u8)str[fromIndex]);
		}
	}
	Assert(toIndex == newStrLength);
	
	result[newStrLength] = '\0';
	return result;
}

StrSplitPiece_t* SplitString(MemoryArena_t* arenaPntr, const char* str, u32 strLength, const char* delineator, u32 delineatorLength, u32* numPiecesOut = nullptr, char escapeCharacter = '\0')
{
	Assert(str != nullptr);
	Assert(delineator != nullptr);
	Assert(delineatorLength > 0);
	
	if (strLength == 0)
	{
		if (numPiecesOut != nullptr) { *numPiecesOut = 0; }
		return nullptr;
	}
	
	u32 numResults = 0;
	StrSplitPiece_t* results = nullptr;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numPieces = 0;
		u32 pieceStart = 0;
		bool justEscaped = false;
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			bool isEscaped = false;
			if (escapeCharacter != '\0')
			{
				if (!justEscaped && cIndex > 0 && str[cIndex-1] == escapeCharacter) { isEscaped = true; }
				else { justEscaped = false; }
			}
			if (!isEscaped && cIndex+delineatorLength <= strLength && MyStrCompare(&str[cIndex], delineator, delineatorLength) == 0)
			{
				if (pass == 1)
				{
					Assert(results != nullptr);
					Assert(numPieces < numResults);
					results[numPieces].length = cIndex - pieceStart;
					results[numPieces].pntr = &str[pieceStart];
				}
				numPieces++;
				pieceStart = cIndex + delineatorLength;
				cIndex += delineatorLength-1;
			}
		}
		if (pieceStart < strLength)
		{
			if (pass == 1)
			{
				Assert(results != nullptr);
				Assert(numPieces < numResults);
				results[numPieces].length = strLength - pieceStart;
				results[numPieces].pntr = &str[pieceStart];
			}
			numPieces++;
		}
		
		if (pass == 0)
		{
			if (numPieces == 0)
			{
				if (numPiecesOut != nullptr) { *numPiecesOut = 0; }
				return nullptr;
			}
			else
			{
				if (numPiecesOut != nullptr) { *numPiecesOut = numPieces; }
				if (arenaPntr == nullptr) { return nullptr; }
				numResults = numPieces;
				results = PushArray(arenaPntr, StrSplitPiece_t, numResults);
			}
		}
		else
		{
			Assert(numPieces == numResults);
		}
	}
	
	return results;
}
StrSplitPiece_t* SplitStringWithQuotes(MemoryArena_t* arenaPntr, const char* str, u32 strLength, const char* delineator, u32 delineatorLength, u32* numPiecesOut = nullptr, char escapeCharacter = '\0')
{
	Assert(str != nullptr);
	Assert(delineator != nullptr);
	Assert(delineatorLength > 0);
	
	if (strLength == 0)
	{
		if (numPiecesOut != nullptr) { *numPiecesOut = 0; }
		return nullptr;
	}
	
	u32 numResults = 0;
	StrSplitPiece_t* results = nullptr;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numPieces = 0;
		u32 pieceStart = 0;
		bool insideQuotes = false;
		bool justFoundEndQuote = false;
		bool justEscaped = false;
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			bool isEscaped = false;
			if (escapeCharacter != '\0')
			{
				if (!justEscaped && cIndex > 0 && str[cIndex-1] == escapeCharacter) { isEscaped = true; }
				else { justEscaped = false; }
			}
			if (!isEscaped && cIndex+delineatorLength <= strLength && MyStrCompare(&str[cIndex], delineator, delineatorLength) == 0 && !insideQuotes)
			{
				if (pass == 1)
				{
					Assert(results != nullptr);
					Assert(numPieces < numResults);
					results[numPieces].length = cIndex - pieceStart;
					if (justFoundEndQuote) { results[numPieces].length--; }
					results[numPieces].pntr = &str[pieceStart];
				}
				numPieces++;
				pieceStart = cIndex + delineatorLength;
				cIndex += delineatorLength-1;
			}
			justFoundEndQuote = false;
			if (str[cIndex] == '\"' && !isEscaped)
			{
				if (!insideQuotes)
				{
					insideQuotes = true;
					if (pieceStart == cIndex) { pieceStart = cIndex+1; }
				}
				else
				{
					insideQuotes = false;
					justFoundEndQuote = true;
				}
			}
		}
		if (pieceStart < strLength)
		{
			if (pass == 1)
			{
				Assert(results != nullptr);
				Assert(numPieces < numResults);
				results[numPieces].length = strLength - pieceStart;
				if (justFoundEndQuote) { results[numPieces].length--; }
				results[numPieces].pntr = &str[pieceStart];
			}
			numPieces++;
		}
		
		if (pass == 0)
		{
			if (numPieces == 0)
			{
				if (numPiecesOut != nullptr) { *numPiecesOut = 0; }
				return nullptr;
			}
			else
			{
				if (numPiecesOut != nullptr) { *numPiecesOut = numPieces; }
				if (arenaPntr == nullptr) { return nullptr; }
				numResults = numPieces;
				results = PushArray(arenaPntr, StrSplitPiece_t, numResults);
			}
		}
		else
		{
			Assert(numPieces == numResults);
		}
	}
	
	return results;
}

u32 CountStringParts(const char* str, u32 strLength, const char* delineator, u32 delineatorLength)
{
	Assert(str != nullptr);
	Assert(delineator != nullptr);
	Assert(delineatorLength > 0);
	
	if (strLength == 0) { return 0; }
	
	u32 numPieces = 0;
	u32 pieceStart = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (cIndex+delineatorLength <= strLength && MyStrCompare(&str[cIndex], delineator, delineatorLength) == 0)
		{
			numPieces++;
			pieceStart = cIndex + delineatorLength;
			cIndex += delineatorLength-1;
		}
	}
	if (pieceStart < strLength)
	{
		numPieces++;
	}
	
	return numPieces;
}
u32 CountStringPartsWithQuotes(const char* str, u32 strLength, const char* delineator, u32 delineatorLength)
{
	Assert(str != nullptr);
	Assert(delineator != nullptr);
	Assert(delineatorLength > 0);
	
	if (strLength == 0) { return 0; }
	
	u32 numPieces = 0;
	u32 pieceStart = 0;
	bool insideQuotes = false;
	bool justFoundEndQuote = false;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (cIndex+delineatorLength <= strLength && MyStrCompare(&str[cIndex], delineator, delineatorLength) == 0 && !insideQuotes)
		{
			numPieces++;
			pieceStart = cIndex + delineatorLength;
			cIndex += delineatorLength-1;
		}
		justFoundEndQuote = false;
		if (str[cIndex] == '\"')
		{
			if (!insideQuotes)
			{
				insideQuotes = true;
				if (pieceStart == cIndex) { pieceStart = cIndex+1; }
			}
			else if (cIndex == 0 || (cIndex > 0 && str[cIndex-1] != '\\'))
			{
				insideQuotes = false;
				justFoundEndQuote = true;
			}
		}
	}
	if (pieceStart < strLength)
	{
		numPieces++;
	}
	
	return numPieces;
}

bool DoesSplitPieceEqual(const StrSplitPiece_t* piece, const char* compareStr)
{
	Assert(piece != nullptr);
	Assert(piece->pntr != nullptr || piece->length == 0);
	u32 compareStrLength = 0;
	if (compareStr != nullptr) { compareStrLength = MyStrLength32(compareStr); }
	if (piece->length != compareStrLength) { return false; }
	if (compareStrLength == 0) { return true; }
	else if (MyMemCompare(piece->pntr, compareStr, compareStrLength) == 0) { return true; }
	else { return false; }
}
bool DoesSplitPieceEqualIgnoreCase(const StrSplitPiece_t* piece, const char* compareStr, u32 compareStrLength)
{
	Assert(piece != nullptr);
	Assert(piece->pntr != nullptr || piece->length == 0);
	if (piece->length != compareStrLength) { return false; }
	if (compareStrLength == 0) { return true; }
	else if (StrCompareIgnoreCase(piece->pntr, compareStr, compareStrLength)) { return true; }
	else { return false; }
}
bool DoesSplitPieceEqualIgnoreCaseNt(const StrSplitPiece_t* piece, const char* compareStr)
{
	u32 compareStrLength = (compareStr != nullptr) ? MyStrLength32(compareStr) : 0;
	return DoesSplitPieceEqualIgnoreCase(piece, compareStr, compareStrLength);
}
void TrimSplitPieceWhitespace(StrSplitPiece_t* piece)
{
	Assert(piece != nullptr);
	while (piece->length > 0 && IsCharClassWhitespace((u8)piece->pntr[0])) { piece->pntr++; piece->length--; }
	while (piece->length > 0 && IsCharClassWhitespace((u8)piece->pntr[piece->length-1])) { piece->length--; }
}

static const char Base64Chars[64] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};
u8 GetBase64CharIndex(char c)
{
	if (c >= 'A' && c <= 'Z') { return (u8)(c - 'A'); }
	if (c >= 'a' && c <= 'z') { return (u8)(26 + (c - 'a')); }
	if (c >= '0' && c <= '9') { return (u8)(26*2 + (c - '0')); }
	if (c == '+') { return (u8)(26*2 + 10 + 0); }
	if (c == '/') { return (u8)(26*2 + 10 + 1); }
	if (c == '=') { return (u8)0; }
	return 0;
}

char* Base64Encode(MemoryArena_t* arenaPntr, const char* str, u32 strLength, u32* encodedLengthOut = nullptr)
{
	Assert(str != nullptr || strLength == 0);
	
	u32 encodedLength = 4 * (u32)CeilR32i((r32)strLength / 3);
	if (encodedLengthOut != nullptr) { *encodedLengthOut = encodedLength; }
	if (arenaPntr == nullptr) { return nullptr; }
	if (strLength == 0) { return nullptr; }
	
	char* result = PushArray(arenaPntr, char, encodedLength+1);
	
	u32 outPos = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex += 3)
	{
		bool c2Exists = (cIndex+1 < strLength);
		bool c3Exists = (cIndex+2 < strLength);
		
		char c1 = str[cIndex];
		char c2 = c2Exists ? str[cIndex+1] : '\0';
		char c3 = c3Exists ? str[cIndex+2] : '\0';
		
		Assert(outPos+4 <= encodedLength);
		result[outPos++] = Base64Chars[(c1 & 0xFC) >> 2];
		result[outPos++] = Base64Chars[((c1 & 0x03) << 4) + ((c2 & 0xF0) >> 4)];
		result[outPos++] = (c2Exists ? Base64Chars[((c2 & 0x0F) << 2) + ((c3 & 0xC0) >> 6)] : '=');
		result[outPos++] = (c3Exists ? Base64Chars[c3 & 0x3F] : '=');
	}
	Assert(outPos == encodedLength);
	result[encodedLength] = '\0';
	
	return result;
}

char* Base64Decode(MemoryArena_t* arenaPntr, const char* str, u32 strLength, u32* decodedLengthOut = nullptr)
{
	Assert(str != nullptr || strLength == 0);
	
	u32 decodedLength = 3 * (u32)CeilR32i((r32)strLength / 4);
	if (decodedLengthOut != nullptr) { *decodedLengthOut = decodedLength; }
	if (arenaPntr == nullptr) { return nullptr; }
	if (strLength == 0) { return nullptr; }
	
	char* result = PushArray(arenaPntr, char, decodedLength+1);
	
	u32 outPos = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex += 4)
	{
		char i1 = (char)GetBase64CharIndex(str[cIndex]);
		char i2 = (char)GetBase64CharIndex((cIndex+1 < strLength) ? str[cIndex+1] : '=');
		char i3 = (char)GetBase64CharIndex((cIndex+2 < strLength) ? str[cIndex+2] : '=');
		char i4 = (char)GetBase64CharIndex((cIndex+3 < strLength) ? str[cIndex+3] : '=');
		
		Assert(outPos+3 <= decodedLength);
		result[outPos++] = (i1 << 2) + ((i2 & 0x30) >> 4);
		result[outPos++] = ((i2 & 0x0F) << 4) + ((i3 & 0x3C) >> 2);
		result[outPos++] = ((i3 & 0x03) << 6) + i4;
	}
	Assert(outPos == decodedLength);
	result[decodedLength] = '\0';
	
	return result;
}

i32 FindChar(const char* str, u32 strLength, char targetChar)
{
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (str[cIndex] == targetChar) { return (i32)cIndex; }
	}
	return -1;
}

u32 FindWordBound(const char* str, u32 strLength, u32 startIndex, bool forward)
{
	Assert(str != nullptr);
	
	u32 result = startIndex;
	if (forward && result < strLength) { result++; }
	if (!forward && result > 0) { result--; }
	
	while (result > 0 && result < strLength)
	{
		char rightChar = str[result];
		char leftChar  = str[result-1];
		
		char prevChar = leftChar;
		char nextChar = rightChar;
		if (!forward) { prevChar = rightChar; nextChar = leftChar; }
		
		if (IsCharClassWord((u8)prevChar) && !IsCharClassWord((u8)nextChar))
		{
			break;
		}
		
		if (forward) { result++; }
		else { result--; }
	}
	
	return result;
}

u32 FindLineEnd(const char* str, u32 strLength, u32 startIndex)
{
	Assert(str != nullptr);
	if (startIndex >= strLength) { return strLength; }
	
	for (u32 cIndex = startIndex; cIndex < strLength; cIndex++)
	{
		if (str[cIndex] == '\n')
		{
			return cIndex;
		}
	}
	
	return strLength;
}

u32 FindLineStart(const char* str, u32 startIndex)
{
	Assert(str != nullptr);
	if (startIndex == 0) { return 0; }
	
	for (u32 cIndex = startIndex; cIndex > 0; cIndex--)
	{
		if (str[cIndex-1] == '\n')
		{
			return cIndex;
		}
	}
	
	return 0;
}

char* SanatizeString(MemoryArena_t* memArena, const char* str, u32 strLength, bool removeCarriageReturn, bool removeNewLine, u32* resultLengthOut)
{
	if (strLength == 0)
	{
		if (resultLengthOut != nullptr) { *resultLengthOut = 0; }
		return nullptr;
	}
	Assert(str != nullptr);
	
	u32 resultLength = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char c = str[cIndex];
		if (IsCharClassPrintable((u8)c) || c == '\t')
		{
			resultLength++;
		}
		else if (c == '\r')
		{
			if (!removeCarriageReturn) { resultLength++; }
		}
		else if (c == '\n')
		{
			if (!removeNewLine) { resultLength++; }
		}
		else
		{
			//Invalid character. Don't include in sanatized result
		}
	}
	
	if (resultLengthOut != nullptr) { *resultLengthOut = resultLength; }
	if (memArena == nullptr) { return nullptr; }
	
	char* result = PushArray(memArena, char, resultLength+1);
	if (result == nullptr) { return result; }
	
	u32 outIndex = 0;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char c = str[cIndex];
		if (IsCharClassPrintable((u8)c) || c == '\t')
		{
			Assert(outIndex < resultLength);
			result[outIndex] = c;
			outIndex++;
		}
		else if (c == '\r')
		{
			if (!removeCarriageReturn)
			{
				Assert(outIndex < resultLength);
				result[outIndex] = c;
				outIndex++;
			}
		}
		else if (c == '\n')
		{
			if (!removeNewLine)
			{
				Assert(outIndex < resultLength);
				result[outIndex] = c;
				outIndex++;
			}
		}
		else
		{
			//Invalid character. Don't include in sanatized result
		}
	}
	
	Assert(outIndex == resultLength);
	result[resultLength] = '\0';
	return result;
}

bool SearchMatch(const char* strPntr, u32 strLength, const char* matchStr, u32 matchStrLength, bool caseSensitive = false, bool allowCharSkips = true)
{
	Assert(strPntr != nullptr);
	Assert(matchStr != nullptr || matchStrLength == 0);
	if (matchStrLength == 0) { return true; }
	
	u32 matchStart = 0;
	do
	{
		u32 matchIndex = 0;
		bool foundMatchStart = false;
		for (u32 cIndex = matchStart; cIndex < strLength; cIndex++)
		{
			char c = strPntr[cIndex];
			char matchChar = matchStr[matchIndex];
			if (!caseSensitive)
			{
				c = GetLowercaseChar(c);
				matchChar = GetLowercaseChar(matchChar);
			}
			
			if (c == matchChar)
			{
				if (matchIndex == 0)
				{
					matchStart = cIndex;
					foundMatchStart = true;
				}
				matchIndex++;
				if (matchIndex >= matchStrLength)
				{
					return true;
				}
			}
			else if (!allowCharSkips)
			{
				matchIndex = 0;
			}
		}
		
		if (!foundMatchStart)
		{
			matchStart = matchStrLength; //break the loop
		}
		else if (allowCharSkips)
		{
			matchStart = matchStart+1;
		}
	} while (allowCharSkips && matchStart < matchStrLength);
	
	return false;
}

//extensions should be of the form "png" (no period included)
bool IsFilePathExtension(const char* pathPntr, u32 pathLength, const char* extensionPntr, u32 extensionLength)
{
	Assert(pathPntr != nullptr || pathLength == 0);
	Assert(extensionPntr != nullptr || extensionLength == 0);
	if (pathLength < extensionLength) { return false; }
	if (extensionLength == 0) { return false; }
	u32 extensionStartIndex = pathLength;
	for (u32 cIndex = 0; cIndex < pathLength; cIndex++)
	{
		if (pathPntr[cIndex] == '\\' || pathPntr[cIndex] == '/') { extensionStartIndex = pathLength; }
		if (pathPntr[cIndex] == '.') { extensionStartIndex = cIndex+1; }
	}
	if (extensionStartIndex != pathLength - extensionLength) { return false; }
	if (StrCompareIgnoreCase(&pathPntr[extensionStartIndex], extensionPntr, extensionLength)) { return true; }
	else { return false; }
}
bool IsFilePathExtensionNt(const char* pathPntr, const char* extensionPntr)
{
	return IsFilePathExtension(pathPntr, MyStrLength32(pathPntr), extensionPntr, MyStrLength32(extensionPntr));
}

#if WINDOWS_COMPILATION
char* ConvertWideStrInArena(MemoryArena_t* memArena, const wchar_t* wideStr, u32* newLengthOut = nullptr)
{
	Assert(memArena != nullptr);
	Assert(wideStr != nullptr);
	if (newLengthOut != nullptr) { *newLengthOut = 0; }
	
	#if 0
	
	size_t wideStrLength = MyWideStrLength(wideStr);
	if (wideStrLength == 0)
	{
		char* result = PushArray(memArena, char, 1);
		if (result == nullptr) { return nullptr; }
		result[0] = '\0';
		if (newLengthOut != nullptr) { *newLengthOut = 0; }
		return result;
	}
	
	BOOL containsUnknowns = FALSE;
	int convertResult = WideCharToMultiByte(CP_ACP, NULL, wideStr, (int)wideStrLength, nullptr, 0, NULL, &containsUnknowns);
	if (convertResult > 0)
	{
		u32 resultLength = (u32)convertResult;
		char* result = PushArray(memArena, char, resultLength+1);
		if (result == nullptr) { return nullptr; }
		if (resultLength > 0)
		{
			convertResult = WideCharToMultiByte(CP_ACP, NULL, wideStr, (int)wideStrLength, result, resultLength, NULL, NULL);
			Assert(convertResult == (i32)resultLength);
		}
		result[resultLength] = '\0';
		if (newLengthOut != nullptr) { *newLengthOut = resultLength; }
		return result;
	}
	else { return nullptr; }
	
	#else
	
	u32 numCharsRequired = 0;
	for (u32 cIndex = 0; wideStr[cIndex] != '\0'; cIndex++)
	{
		wchar_t wideChar = wideStr[cIndex];
		BOOL isUnknownChar = FALSE;
		int convertResult = WideCharToMultiByte(CP_UTF8, NULL, &wideChar, 1, nullptr, 0, NULL, &isUnknownChar);
		if (!isUnknownChar && convertResult == 1) { numCharsRequired += (u32)convertResult; Assert(convertResult <= 4); }
		else { numCharsRequired += 1; }
	}
	if (numCharsRequired == 0) { return nullptr; }
	char* result = PushArray(memArena, char, numCharsRequired+1);
	if (result == nullptr) { return nullptr; }
	MyMemSet(result, 0x00, numCharsRequired+1);
	u32 resultIndex = 0;
	for (u32 cIndex = 0; wideStr[cIndex] != '\0'; cIndex++)
	{
		wchar_t wideChar = wideStr[cIndex];
		char charsBuffer[4];
		BOOL isUnknownChar = FALSE;
		int convertResult = WideCharToMultiByte(CP_UTF8, NULL, &wideChar, 1, &charsBuffer[0], ArrayCount(charsBuffer), NULL, &isUnknownChar);
		if (!isUnknownChar && convertResult == 1)
		{
			Assert(resultIndex + (u32)convertResult <= numCharsRequired);
			MyMemCopy(&result[resultIndex], &charsBuffer[0], (u32)convertResult);
			resultIndex += (u32)convertResult;
		}
		else
		{
			Assert(resultIndex + (u32)convertResult <= numCharsRequired);
			#if 0
			if (convertResult >= 0 && convertResult <= 9) { result[resultIndex] = '0' + (u8)convertResult; }
			else { result[resultIndex] = unknownChar; }
			#else
			result[resultIndex] = '?';
			#endif
			resultIndex += 1;
		}
	}
	Assert(resultIndex == numCharsRequired);
	result[resultIndex] = '\0';
	if (newLengthOut != nullptr) { *newLengthOut = resultIndex; }
	return result;
	
	#endif
}
#endif

char* EncodeStringUrl(MemoryArena_t* memArena, const char* strPntr, u32 strLength, u32* encodedLengthOut = nullptr)
{
	Assert(strPntr != nullptr || strLength == 0);
	
	u32 resultSize = 0;
	char* result = nullptr;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 charIndex = 0;
		
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			char thisChar = strPntr[cIndex];
			if (IsCharClassAlphaNumeric((u8)thisChar) || thisChar == '-' || thisChar == '_' || thisChar == '.' || thisChar == '~')
			{
				if (result != nullptr)
				{
					Assert(charIndex+1 <= resultSize);
					result[charIndex] = thisChar;
				}
				charIndex++;
			}
			else if (thisChar == ' ')
			{
				if (result != nullptr)
				{
					Assert(charIndex+1 <= resultSize);
					result[charIndex] = '+';
				}
				charIndex++;
			}
			else if (IsCharClassPrintable((u8)thisChar) || thisChar == '\n' || thisChar == '\r' || thisChar == '\t')
			{
				if (result != nullptr)
				{
					Assert(charIndex+3 <= resultSize);
					result[charIndex+0] = '%';
					result[charIndex+1] = UpperHexChar((u8)thisChar);
					result[charIndex+2] = LowerHexChar((u8)thisChar);
				}
				charIndex += 3;
			}
			else
			{
				//throw away the invalid char
			}
		}
		
		if (pass == 0)
		{
			resultSize = charIndex;
			if (encodedLengthOut) { *encodedLengthOut = resultSize; }
			if (memArena == nullptr) { return nullptr; }
			result = PushArray(memArena, char, resultSize+1);
			if (result == nullptr) { return result; }
		}
		else
		{
			Assert(charIndex == resultSize);
			result[resultSize] = '\0';
		}
	}
	return result;
}
char* EncodeStringUrlNt(MemoryArena_t* memArena, const char* nullTermString, u32* encodedLengthOut = nullptr)
{
	NotNull(nullTermString);
	return EncodeStringUrl(memArena, nullTermString, MyStrLength32(nullTermString), encodedLengthOut);
}

char* EncodePostFieldsUrl(MemoryArena_t* arenaPntr, u32 numFields, const char* const* keys, const char* const* values, u32* dataLengthOut)
{
	NotNull(arenaPntr);
	
	u32 resultSize = 0;
	char* result = nullptr;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 charIndex = 0;
		
		for (u32 pIndex = 0; pIndex < numFields; pIndex++)
		{
			const char* keyStr = keys[pIndex];
			const char* valueStr = values[pIndex];
			u32 keyLength = MyStrLength32(keyStr);
			u32 valueLength = MyStrLength32(valueStr);
			
			if (pIndex > 0)
			{
				if (result != nullptr)
				{
					Assert(charIndex+1 <= resultSize);
					result[charIndex] = '&';
				}
				charIndex++;
			}
			
			MemoryArena_t bufferArena = {};
			if (result != nullptr) { InitBufferArena(&bufferArena, &result[charIndex], resultSize+1 - charIndex, true); }
			u32 encodedKeyLength = 0;
			char* encodedKeyStr = EncodeStringUrl((result != nullptr) ? &bufferArena : nullptr, keyStr, keyLength, &encodedKeyLength);
			charIndex += encodedKeyLength;
			
			if (result != nullptr)
			{
				Assert(charIndex+1 <= resultSize);
				result[charIndex] = '=';
			}
			charIndex++;
			
			if (result != nullptr) { InitBufferArena(&bufferArena, &result[charIndex], resultSize+1 - charIndex, true); }
			u32 encodedValueLength = 0;
			char* encodedValueStr = EncodeStringUrl((result != nullptr) ? &bufferArena : nullptr, valueStr, valueLength, &encodedValueLength);
			charIndex += encodedValueLength;
		}
		
		if (pass == 0)
		{
			resultSize = charIndex;
			if (dataLengthOut != nullptr) { *dataLengthOut = resultSize; }
			result = PushArray(arenaPntr, char, resultSize+1);
			NotNull(result);
		}
		else
		{
			Assert(charIndex == resultSize);
			result[resultSize] = '\0';
		}
	}
	
	return result;
}

char* EncodeStringJson(MemoryArena_t* arenaPntr, const char* strPntr, u32 strLength, u32* encodedLengthOut = nullptr)
{
	Assert(strPntr != nullptr || strLength == 0);
	
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 charIndex = 0;
		
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			char nextChar = strPntr[cIndex];
			if (nextChar == '\"' || nextChar == '\\')
			{
				if (result != nullptr)
				{
					Assert(charIndex+2 <= resultSize);
					result[charIndex+0] = '\\';
					result[charIndex+1] = nextChar;
				}
				charIndex += 2;
			}
			else if (nextChar == '\n')
			{
				if (result != nullptr)
				{
					Assert(charIndex+2 <= resultSize);
					result[charIndex+0] = '\\';
					result[charIndex+1] = 'n';
				}
				charIndex += 2;
			}
			else if (IsCharClassPrintable((u8)nextChar) || nextChar == '\t' || (u8)nextChar >= 0x80)
			{
				if (result != nullptr)
				{
					Assert(charIndex+1 <= resultSize);
					result[charIndex] = nextChar;
				}
				charIndex++;
			}
			else
			{
				if (result != nullptr)
				{
					Assert(charIndex+6 <= resultSize);
					result[charIndex+0] = '\\';
					result[charIndex+1] = 'u';
					result[charIndex+2] = '0';
					result[charIndex+3] = '0';
					result[charIndex+4] = UpperHexChar((u8)nextChar);
					result[charIndex+5] = LowerHexChar((u8)nextChar);
				}
				charIndex += 6;
			}
		}
		
		if (pass == 0)
		{
			resultSize = charIndex;
			if (encodedLengthOut != nullptr) { *encodedLengthOut = resultSize; }
			if (arenaPntr == nullptr) { return nullptr; }
			result = PushArray(arenaPntr, char, resultSize+1);
			NotNull(result);
		}
		else
		{
			Assert(charIndex == resultSize);
			result[resultSize] = '\0';
		}
	}
	return result;
}
char* EncodeStringJsonNt(MemoryArena_t* arenaPntr, const char* nullTermString, u32* encodedLengthOut = nullptr)
{
	NotNull(nullTermString);
	return EncodeStringJson(arenaPntr, nullTermString, MyStrLength32(nullTermString), encodedLengthOut);
}

//TODO: Add an option to eat errors and just omit the problem sections
char* DecodeStringJson(MemoryArena_t* arenaPntr, const char* strPntr, u32 strLength, u32* decodedLengthOut = nullptr)
{
	Assert(strPntr != nullptr || strLength == 0);
	
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 charIndex = 0;
		
		for (u32 cIndex = 0; cIndex < strLength; cIndex++)
		{
			char thisChar = strPntr[cIndex];
			char nextChar = (cIndex+1 < strLength) ? strPntr[cIndex+1] : '\0';
			if (thisChar == '\\')
			{
				if (nextChar == 'n')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\n'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == 'r')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\r'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == 'b')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\b'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == 'f')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\f'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == 't')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\t'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == '\\')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\\'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == '/')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '/'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == '"')
				{
					if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = '\"'; }
					charIndex++;
					cIndex += 1;
				}
				else if (nextChar == 'u')
				{
					if (cIndex+6 <= resultSize)
					{
						//TODO: Handle the full 16-bit unicode points and
						//      convert into UTF-8 bytes
						char hexChar0 = strPntr[cIndex+2];
						char hexChar1 = strPntr[cIndex+3];
						char hexChar2 = strPntr[cIndex+4];
						char hexChar3 = strPntr[cIndex+5];
						if (hexChar0 == '0' && hexChar0 == '0')
						{
							if (IsCharClassHexChar((u8)hexChar2) && IsCharClassHexChar((u8)hexChar3))
							{
								u8 codepointValue = (u8)((GetHexCharValue(hexChar2) << 4) | (GetHexCharValue(hexChar2) << 0));
								if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = (char)codepointValue; }
								charIndex++;
								cIndex += 5;
							}
							else
							{
								return nullptr;
							}
						}
						else
						{
							return nullptr;
						}
					}
					else
					{
						return nullptr;
					}
				}
				else
				{
					return nullptr;
				}
			}
			else if (IsCharClassPrintable((u8)thisChar) || thisChar == '\t' || (u8)thisChar >= 0x80)
			{
				if (result != nullptr) { Assert(charIndex+1 <= resultSize); result[charIndex] = thisChar; }
				charIndex++;
			}
			else
			{
				return nullptr;
			}
		}
		
		if (pass == 0)
		{
			resultSize = charIndex;
			if (decodedLengthOut != nullptr) { *decodedLengthOut = resultSize; }
			if (arenaPntr == nullptr) { return nullptr; }
			result = PushArray(arenaPntr, char, resultSize+1);
			NotNull(result);
		}
		else
		{
			Assert(charIndex == resultSize);
			result[resultSize] = '\0';
		}
	}
	return result;
}
char* DecodeStringJsonNt(MemoryArena_t* arenaPntr, const char* nullTermString, u32* decodedLengthOut = nullptr)
{
	NotNull(nullTermString);
	return DecodeStringJson(arenaPntr, nullTermString, MyStrLength32(nullTermString), decodedLengthOut);
}

u32 InsertStringInPlace(char* str, u32 strLength, u32 maxLength, u32 insertIndex, const char* insertStr, u32 insertStrLength)
{
	Assert(str != nullptr);
	Assert(insertStr != nullptr);
	Assert(insertIndex <= strLength);
	
	u32 fromIndex = strLength;
	u32 toIndex = strLength + insertStrLength;
	while (true)
	{
		Assert(toIndex < maxLength);
		
		str[toIndex] = str[fromIndex]; //NOTE: This will also write the \0
		
		if (fromIndex == insertIndex)
		{
			for (u32 cIndex = insertStrLength; cIndex > 0; cIndex--)
			{
				Assert(toIndex > 0);
				str[toIndex-1] = insertStr[cIndex-1];
				toIndex--;
			}
		}
		
		if (toIndex == 0) { break; }
		toIndex--;
		fromIndex--;
	}
	
	Assert(toIndex == 0);
	Assert(fromIndex == 0);
	return strLength + insertStrLength;
}

u32 RemoveStringRegionInPlace(char* str, u32 strLength, u32 regionStartIndex, u32 regionEndIndex)
{
	Assert(str != nullptr);
	Assert(regionStartIndex <= strLength);
	Assert(regionEndIndex <= strLength);
	if (regionStartIndex == regionEndIndex) { return strLength; } //nothing to remove
	
	u32 regionMin = (u32)MinI32((i32)regionStartIndex, (i32)regionEndIndex);
	u32 regionMax = (u32)MaxI32((i32)regionStartIndex, (i32)regionEndIndex);
	
	u32 fromIndex = 0;
	u32 toIndex = 0;
	while (fromIndex <= strLength)
	{
		if (fromIndex >= regionMin && fromIndex < regionMax)
		{
			//Don't insert the character
		}
		else
		{
			str[toIndex] = str[fromIndex]; //NOTE: This will also write the \0
			toIndex++;
		}
		
		fromIndex++;
	}
	
	return toIndex-1;
}

#endif //  _MY_STRING_MANIP_H
