/*
File:   my_random.h
Author: Taylor Robbins
Date:   04\29\2020
*/

//https://www.ece.ucsb.edu/~psen/Papers/EGSR12_FastApproximateBlueNoise.pdf

#ifndef _MY_RANDOM_H
#define _MY_RANDOM_H

#define RAND_FLOAT_PRECISION_R32 8000000UL //8 million
#define RAND_FLOAT_PRECISION_R64 400000000000000UL //400 trillion

typedef enum
{
	RandomType_Fixed = 0x00,
	RandomType_Incremental,
	RandomType_LinearCongruential32,
	RandomType_LinearCongruential64,
	RandomType_NumTypes,
} RandomType_t;

struct RandomSeries_t
{
	RandomType_t type;
	bool seeded;
	u64 state;
	u64 defaultIncrement;
	u64 generationCount; //how many numbers have been generated since the series was seeded
};

void CreateRandomSeries(RandomSeries_t* series, RandomType_t type = RandomType_LinearCongruential64, u64 defaultIncrement = 1)
{
	Assert(series != nullptr);
	ClearPointer(series);
	series->type = type;
	series->seeded = false;
	series->defaultIncrement = defaultIncrement;
	series->generationCount = 0;
}

void SeedRandomSeriesU32(RandomSeries_t* series, u32 seed)
{
	Assert(series != nullptr);
	Assert(series->type < RandomType_NumTypes);
	switch (series->type)
	{
		case RandomType_Fixed:
		case RandomType_Incremental:
		case RandomType_LinearCongruential32:
		case RandomType_LinearCongruential64:
		{
			series->state = (u64)seed;
		} break;
		default: Assert_(false); break;
	}
	series->seeded = true;
	series->generationCount = 0;
}
void SeedRandomSeriesU64(RandomSeries_t* series, u64 seed)
{
	Assert(series != nullptr);
	Assert(series->type < RandomType_NumTypes);
	switch (series->type)
	{
		case RandomType_Fixed:
		case RandomType_Incremental:
		case RandomType_LinearCongruential32:
		case RandomType_LinearCongruential64:
		{
			series->state = seed;
		} break;
		default: Assert_(false); break;
	}
	series->seeded = true;
	series->generationCount = 0;
}

void StepRandomSeries(RandomSeries_t* series, u64 numberOfSteps = 1)
{
	Assert(series != nullptr);
	Assert(series->type < RandomType_NumTypes);
	Assert(series->seeded);
	
	switch (series->type)
	{
		case RandomType_Fixed:
		{
			//Fixed doesn't do anything, it just keeps a fixed number
		} break;
		case RandomType_Incremental:
		{
			if (series->state < UINT64_MAX - numberOfSteps) { series->state += numberOfSteps; }
			else { series->state = numberOfSteps - (UINT64_MAX - numberOfSteps - series->state); }
			series->generationCount += numberOfSteps;
		} break;
		case RandomType_LinearCongruential32:
		{
			//n(x+1) = n(x) * A + C modulo M
			for (u64 sIndex = 0; sIndex < numberOfSteps; sIndex++)
			{
				u64 newState = (series->state * 1103515245UL + 12345UL) & 0xFFFFFFFFFFFFFFFFUL;
				series->state = newState;
			}
			series->generationCount += numberOfSteps;
		} break;
		case RandomType_LinearCongruential64:
		{
			//n(x+1) = n(x) * A + C modulo M
			//Values taken from https://nuclear.llnl.gov/CNP/rng/rngman/node4.html
			for (u64 sIndex = 0; sIndex < numberOfSteps; sIndex++)
			{
				u64 newState = (series->state * 2862933555777941757UL + 3037000493UL) & 0xFFFFFFFFFFFFFFFFUL;
				series->state = newState;
			}
			series->generationCount += numberOfSteps;
		} break;
		default: Assert_(false); break;
	}
}

u32 GetRandU32(RandomSeries_t* series)
{
	StepRandomSeries(series, series->defaultIncrement);
	return (u32)(series->state % UINT32_MAX);
}
//NOTE: doesn't include max as a possibility, if max is less than min then the numbers will be swapped
u32 GetRandU32(RandomSeries_t* series, u32 min, u32 max)
{
	if (max < min) { u32 temp = max; max = min; min = temp; }
	u32 result = GetRandU32(series);
	if (min == max) { return min; }
	result = (result % (max-min)) + min;
	return result;
}

u64 GetRandU64(RandomSeries_t* series)
{
	StepRandomSeries(series, series->defaultIncrement);
	return series->state;
}
//NOTE: doesn't include max as a possibility, if max is less than min then the numbers will be swapped
u64 GetRandU64(RandomSeries_t* series, u64 min, u64 max)
{
	if (max < min) { u64 temp = max; max = min; min = temp; }
	u64 result = GetRandU64(series);
	if (min == max) { return min; }
	result = (result % (max-min)) + min;
	return result;
}

r32 GetRandR32(RandomSeries_t* series)
{
	u32 integerRandom = (u32)(GetRandU64(series) % RAND_FLOAT_PRECISION_R32);
	r32 result = (r32)integerRandom / (r32)(RAND_FLOAT_PRECISION_R32);
	return result;
}
r32 GetRandR32(RandomSeries_t* series, r32 min, r32 max)
{
	r32 resultPercent = GetRandR32(series);
	if (min == max) { return min; }
	return min + (resultPercent * (max-min));
}

r64 GetRandR64(RandomSeries_t* series)
{
	u64 integerRandom = (GetRandU64(series) % RAND_FLOAT_PRECISION_R64);
	r64 result = (r64)integerRandom / (r64)(RAND_FLOAT_PRECISION_R64);
	return result;
}
r64 GetRandR64(RandomSeries_t* series, r64 min, r64 max)
{
	r64 resultPercent = GetRandR64(series);
	if (min == max) { return min; }
	return min + (resultPercent * (max-min));
}

u8 GetRandU8(RandomSeries_t* series)
{
	StepRandomSeries(series, series->defaultIncrement);
	return (u8)(series->state % UINT8_MAX);
}
//NOTE: doesn't include max as a possibility, if max is less than min then the numbers will be swapped
u8 GetRandU8(RandomSeries_t* series, u8 min, u8 max)
{
	if (max < min) { u8 temp = max; max = min; min = temp; }
	u8 result = GetRandU8(series);
	if (min == max) { return min; }
	result = (u8)((result % (max-min)) + min);
	return result;
}

u16 GetRandU16(RandomSeries_t* series)
{
	StepRandomSeries(series, series->defaultIncrement);
	return (u16)(series->state % UINT16_MAX);
}
//NOTE: doesn't include max as a possibility, if max is less than min then the numbers will be swapped
u16 GetRandU16(RandomSeries_t* series, u16 min, u16 max)
{
	if (max < min) { u16 temp = max; max = min; min = temp; }
	u16 result = GetRandU16(series);
	if (min == max) { return min; }
	result = (u16)((result % (max-min)) + min);
	return result;
}

i8 GetRandI8(RandomSeries_t* series)
{
	StepRandomSeries(series, series->defaultIncrement);
	u8 resultU8 = (u8)(series->state % UINT8_MAX);
	if (resultU8 >= UINT8_MAX/2) { return (i8)(-(resultU8 - (UINT8_MAX/2))) - 1; }
	else { return (i8)resultU8; }
}
//NOTE: doesn't include max as a possibility, if max is less than min then the numbers will be swapped
i8 GetRandI8(RandomSeries_t* series, i8 min, i8 max)
{
	if (max < min) { i8 temp = max; max = min; min = temp; }
	i8 result = GetRandI8(series);
	if (min == max) { return min; }
	result = (result % (max-min)) + min;
	return result;
}

bool GetRandBool(RandomSeries_t* series, r32 successRate = 0.5f)
{
	return (GetRandU32(series, 0, 100000) < (u32)(100000 * successRate));
}

v2 GetBlueNoisePosInRec(u64 seed, rec bounds, u32 numOfPositions, u32 index)
{
	Assert(numOfPositions > 0);
	Assert(numOfPositions <= 0x7FFFFFFF); //TODO: Honestly this just breaks down for really large numbers because numRows*numCols can be bigger than u32 max
	if (bounds.width == 0 || bounds.height == 0) { return bounds.topLeft; }
	if (index >= numOfPositions) { index = (index % numOfPositions); }
	RandomSeries_t tempSeries;
	CreateRandomSeries(&tempSeries);
	SeedRandomSeriesU64(&tempSeries, seed);
	StepRandomSeries(&tempSeries, index*2);
	
	r32 aspectRatio = bounds.width / bounds.height;
	v2i arraySize = Vec2i_Zero;
	if (aspectRatio >= 1.0f) //wide bounds
	{
		u32 numRows = (u32)FloorR32i(SqrtR32((r32)numOfPositions));
		if (numRows < 1) { numRows = 1; }
		while (numRows > 1 && (((r32)numOfPositions / (r32)(numRows-1)) / (r32)(numRows-1)) < aspectRatio)
		{
			numRows--;
		}
		arraySize = NewVec2i(CeilR32i((r32)numOfPositions / (r32)numRows), (i32)numRows);
	}
	else //tall bounds
	{
		u32 numColumns = (u32)FloorR32i(SqrtR32((r32)numOfPositions));
		if (numColumns < 1) { numColumns = 1; }
		while (numColumns > 1 && ((r32)(numColumns-1) / ((r32)numOfPositions / (r32)(numColumns-1))) > aspectRatio)
		{
			numColumns--;
		}
		arraySize = NewVec2i((i32)numColumns, CeilR32i((r32)numOfPositions / (r32)numColumns));
	}
	
	u32 actualIndex = index;
	i32 numEmptySpots = (arraySize.width * arraySize.height) - (i32)numOfPositions;
	if (numEmptySpots > 0)
	{
		i32 skipPeriod = CeilR32i((r32)(arraySize.width * arraySize.height) / (r32)numEmptySpots);
		// if (index == 0) { MyLibPrintLine_D("%d emptySpots, %d skipPeriod", numEmptySpots, skipPeriod); }
		if (skipPeriod > 0) { actualIndex += ((index + skipPeriod/2) / skipPeriod); }
	}
	
	// MyLibPrintLine_D("Array Size for %.1fx%.1f box and %u points: (%d, %d)", bounds.width, bounds.height, numOfPositions, arraySize.width, arraySize.height);
	// return bounds.topLeft + bounds.size/2;
	
	v2 cellSize = Vec2Divide(bounds.size, NewVec2(arraySize));
	v2i gridPos = NewVec2i((i32)(actualIndex % arraySize.width), (i32)(actualIndex / arraySize.width));
	v2 offset = Vec2Multiply(NewVec2(gridPos), cellSize) + cellSize/2;
	
	v2 jitter = NewVec2(GetRandR32(&tempSeries)*2 - 1.0f, GetRandR32(&tempSeries)*2 - 0.5f);
	jitter = Vec2Multiply(jitter, cellSize/4);
	offset += jitter;
	
	return bounds.topLeft + offset;
}

void RandomMixArray_(RandomSeries_t* series, void* arrayItems, u32 itemSize, u32 numItems)
{
	Assert(series != nullptr);
	Assert(itemSize > 0);
	if (numItems == 0) { return; }
	u8 swapBuffer[64];
	Assert(itemSize <= sizeof(swapBuffer));
	Assert(arrayItems != nullptr);
	u8* itemsBytePntr = (u8*)arrayItems;
	for (u32 iIndex = 0; iIndex < numItems; iIndex++)
	{
		u32 swapIndex = GetRandU32(series, 0, numItems);
		Assert(swapIndex < numItems);
		if (swapIndex != iIndex)
		{
			MyMemCopy(&swapBuffer[0], &itemsBytePntr[itemSize * swapIndex], itemSize);
			MyMemCopy(&itemsBytePntr[itemSize * swapIndex], &itemsBytePntr[itemSize * iIndex], itemSize);
			MyMemCopy(&itemsBytePntr[itemSize * iIndex], &swapBuffer[0], itemSize);
		}
	}
}

#define RandomMixArray(series, arrayName) RandomMixArray_((series), &(arrayName), sizeof(arrayName[0]), ArrayCount(arrayName))
#define RandomMixArrayPntr(series, itemsPntr, numItems) RandomMixArray_((series), (itemsPntr), sizeof(*(itemsPntr)), (numItems))

u32* GenRandomIndexList(RandomSeries_t* series, MemoryArena_t* arena, u32 numItems)
{
	u32* result = PushArray(arena, u32, numItems);
	Assert(result != nullptr);
	for (u32 iIndex = 0; iIndex < numItems; iIndex++)
	{
		result[iIndex] = iIndex;
	}
	RandomMixArrayPntr(series, result, numItems);
	return result;
}

#endif //  _MY_RANDOM_H
