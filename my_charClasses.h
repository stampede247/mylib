/*
File:   charClasses.h
Author: Taylor Robbins
Date:   10\01\2017
Description:
	** Holds a ton of functions that are useful for classifying ASCII characters

This file is #included by default from mylib.h
*/

#ifndef _CHAR_CLASSES_H
#define _CHAR_CLASSES_H

inline bool IsCharClassAlphabet(u8 c)
{
	return ((c >= 'A' && c <= 'Z') ||
		(c >= 'a' && c <= 'z'));
}

inline bool IsCharClassNumeric(u8 c)
{
	return (c >= '0' && c <= '9');
}

inline bool IsCharClassWhitespace(u8 c)
{
	return (c == '\n' || c == '\r' || c == '\t' || c == ' ');
}

inline bool IsCharClassControl(u8 c)
{
	return ((c >= 0x01 && c <= 0x31) || c == 0x7F);
}

inline bool IsCharClassExtendedAscii(u8 c)
{
	return (c >= 0x80 && c <= 0xFE);
}

inline bool IsCharClassPrintable(u8 c)
{
	return (c >= ' ' && c <= '~');
}

inline bool IsCharClassASCIIPrintable(u8 c)
{
	return (c > 0 && c < 255);
}

inline bool IsCharClassAlphaNumeric(u8 c)
{
	return (IsCharClassAlphabet(c) || IsCharClassNumeric(c));
}

inline bool IsCharClassSyntax(u8 c)
{
	return (IsCharClassPrintable(c) && !IsCharClassAlphaNumeric(c));
}

inline bool IsCharClassWord(u8 c)
{
	return (IsCharClassAlphaNumeric(c) || c == '_');
}

inline bool IsCharClassBeginningCharacter(u8 c)
{
	return (c == '(' || c == '[' || c == '<' || c == '"');
}

inline bool IsCharClassHexChar(u8 c)
{
	return (IsCharClassNumeric(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'));
}

inline char UpperHexChar(u8 value)
{
	u8 nibble = (u8)((value & 0xF0) >> 4);
	if (nibble < 10) { return ('0' + nibble); }
	else { return ('A' + (nibble-10)); }
}

inline char LowerHexChar(u8 value)
{
	u8 nibble = (u8)((value & 0x0F) >> 0);
	if (nibble < 10) { return ('0' + nibble); }
	else { return ('A' + (nibble-10)); }
}

inline u8 GetHexCharValue(char c)
{
	if (IsCharClassNumeric((u8)c)) { return (u8)(c - '0'); }
	if (c >= 'A' && c <= 'F') { return (u8)((c - 'A') + 10); }
	if (c >= 'a' && c <= 'f') { return (u8)((c - 'a') + 10); }
	return 0;
}

inline u8 ParseHexU8(const char* str)
{
	Assert(str != nullptr);
	return (u8)((GetHexCharValue(str[0])<<4) + GetHexCharValue(str[1]));
}

inline u16 ParseHexU16(const char* str)
{
	Assert(str != nullptr);
	return (u16)(((u16)GetHexCharValue(str[0])<<12) + ((u16)GetHexCharValue(str[1])<<8) + ((u16)GetHexCharValue(str[2])<<4) + (u16)GetHexCharValue(str[3]));
}

inline u32 ParseHexU32(const char* str)
{
	Assert(str != nullptr);
	return (u32)(
		((u32)GetHexCharValue(str[0])<<28) + ((u32)GetHexCharValue(str[1])<<24) + ((u32)GetHexCharValue(str[2])<<20) + ((u32)GetHexCharValue(str[3])<<16) +
		((u32)GetHexCharValue(str[4])<<12) + ((u32)GetHexCharValue(str[5])<<8) + ((u32)GetHexCharValue(str[6])<<4) + (u32)GetHexCharValue(str[7])
	);
}

inline bool IsStringHex(const char* strPntr, u32 strLength)
{
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		if (!IsCharClassHexChar((u8)strPntr[cIndex]))
		{
			return false;
		}
	}
	return true;
}

inline char GetLowercaseChar(char c)
{
	if (c >= 'A' && c <= 'Z') { return (c - 'A') + 'a'; }
	else { return c; }
}
inline char GetUppercaseChar(char c)
{
	if (c >= 'a' && c <= 'z') { return (c - 'a') + 'A'; }
	else { return c; }
}

inline bool StrCompareIgnoreCase(const char* str1, const char* str2, u32 numChars)
{
	for (u32 cIndex = 0; cIndex < numChars; cIndex++)
	{
		char c1 = str1[cIndex];
		char c2 = str2[cIndex];
		if (GetLowercaseChar(c1) != GetLowercaseChar(c2))
		{
			return false;
		}
		else if (c1 == '\0')
		{
			return true;
		}
	}
	return true;
}
inline bool StrCompareIgnoreCaseNt(const char* str1, const char* str2)
{
	for (u32 cIndex = 0; ; cIndex++)
	{
		char c1 = str1[cIndex];
		char c2 = str2[cIndex];
		if (GetLowercaseChar(c1) != GetLowercaseChar(c2))
		{
			return false;
		}
		else if (c1 == '\0' || c2 == '\0')
		{
			return true;
		}
	}
}

inline bool StrFindSubstringIgnoreCase(const char* strPntr, u32 strLength, const char* substrPntr, u32 substrLength)
{
	Assert(strPntr != nullptr || strLength == 0);
	Assert(substrPntr != nullptr || substrLength == 0);
	if (strLength == 0) { return false; }
	if (substrLength == 0) { return false; }
	for (u32 cIndex = 0; cIndex+substrLength <= strLength; cIndex++)
	{
		if (StrCompareIgnoreCase(&strPntr[cIndex], substrPntr, substrLength)) { return true; }
	}
	return false;
}
inline bool StrFindSubstringIgnoreCaseNt(const char* strPntr, const char* substrPntr)
{
	u32 strLength = MyStrLength32(strPntr);
	u32 substrLength = MyStrLength32(substrPntr);
	return StrFindSubstringIgnoreCase(strPntr, strLength, substrPntr, substrLength);
}

inline bool FindSubstringIndex(const char* strPntr, u32 strLength, const char* substrPntr, u32 substrLength, u32* indexOut = nullptr, bool ignoreCase = false)
{
	Assert(strPntr != nullptr || strLength == 0);
	Assert(substrLength > 0);
	Assert(substrPntr != nullptr);
	if (strLength == 0) { return false; }
	if (substrLength > strLength) { return false; }
	for (u32 cIndex = 0; cIndex + substrLength <= strLength; cIndex++)
	{
		bool matches = true;
		for (u32 cIndex2 = 0; cIndex2 < substrLength; cIndex2++)
		{
			if (ignoreCase)
			{
				if (GetLowercaseChar(strPntr[cIndex + cIndex2]) != GetLowercaseChar(substrPntr[cIndex2])) { matches = false; break; }
			}
			else
			{
				if (strPntr[cIndex + cIndex2] != substrPntr[cIndex2]) { matches = false; break; }
			}
		}
		if (matches)
		{
			if (indexOut != nullptr) { *indexOut = cIndex; }
			return true;
		}
	}
	return false;
}
inline bool FindSubstringIndexNt(const char* strPntr, const char* substrPntr, u32* indexOut = nullptr, bool ignoreCase = false)
{
	Assert(strPntr != nullptr);
	Assert(substrPntr != nullptr);
	u32 strLength = MyStrLength32(strPntr);
	u32 substrLength = MyStrLength32(substrPntr);
	return FindSubstringIndex(strPntr, strLength, substrPntr, substrLength, indexOut, ignoreCase);
}

#endif //  _CHAR_CLASSES_H
