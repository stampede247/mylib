#ifndef _D3D9_VERTEX_H
#define _D3D9_VERTEX_H

#include "mylib/myMath.h"
#include "mylib/colors.h"

#define VERTEX_FORMAT	(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define VERTEX2D_FORMAT (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)

struct Vertex_t
{
	union
	{
		Vec3_t position;
		struct
		{
			real32 x, y, z;
		};
	};
	
	union
	{
		Vec3_t normal;
		struct
		{
			real32 nX, nY, nZ;
		};
	};
	
	Color_t color;
	
	union
	{
		Vec2_t texCoord;
		struct
		{
			real32 tX, tY;
		};
	};
};

inline Vertex_t NewVertex(Vec3_t position, Vec3_t normal, Color_t color, Vec2_t texCoord)
{
	return { position, normal, color, texCoord };
}
inline Vertex_t NewVertex(Vec3_t position, Color_t color)
{
	return { position, NewVec3(0, 1, 0), color, NewVec2(0.0f, 0.0f) };
}

inline Vertex_t NewVertex(real32 positionX, real32 positionY, real32 positionZ,
	real32 normalX, real32 normalY, real32 normalZ,
	uint32 color, 
	real32 texCoordX, real32 texCoordY)
{
	return {
		{ positionX, positionY, positionZ },
		{ normalX, normalY, normalZ },
		{ color },
		{ texCoordX, texCoordY },	
	};
}

#endif // _D3D9_VERTEX_H