#include "mylib.h"

#if !defined(MYLIB_LIST_H)
#define MYLIB_LIST_H

struct test_struct
{
	int x;
	int y;
};

#if defined(_IOSTREAM_)
std::ostream& operator << (std::ostream& stream, test_struct& str)
{
	stream << "(" << str.x << ", " << str.y << ")";
	return stream;
}
#endif

class list_node
{
public:	
	//determines whether the list
	//class handles the clean up
	//of the struct
	bool32 personallyAllocated;
	test_struct* value;
	list_node* next;
	
	list_node()
	{
		
	}
	~list_node()
	{
		
	}
};

class list
{
private:
	list_node* First;
	list_node* Last;
	
	uint64 Count;

public:
	list()
	{
		this->Count = 0;
		this->First = nullptr;
		this->Last = nullptr;
	}
	~list()
	{
		list_node* current = this->First;
		while(current != nullptr)
		{
			current = current->next;
			if (current->personallyAllocated)
			{
				free(current->value);
			}
			free(current);
		}
	}
	
	uint64 count()
	{
		return this->Count;
	}
	
	list_node* first()
	{
		return this->First;
	}
	
	list_node* last()
	{
		return this->Last;
	}
	
	//the passed item is copied to newly allocated memory
	void add(const test_struct* newItem)
	{
		Assert(newItem);
		
		list_node* newNode = (list_node*)malloc(sizeof(list_node));
		memset(newNode, 0, sizeof(list_node));
		
		newNode->next = nullptr;
		
		newNode->value = (test_struct*)malloc(sizeof(test_struct));
		memcpy(newNode->value, newItem, sizeof(test_struct));
		
		//let ourselves know we need to clean ^ this second malloc ^
		//up when the item is destroyed
		newNode->personallyAllocated = true;
		
		if (this->First == nullptr)
		{
			//if first is empty then last should be too
			Assert(this->Last == nullptr);
			this->First = newNode;
			this->Last = newNode;
		}
		else
		{
			this->Last->next = newNode;
			this->Last = newNode;
		}
		
		this->Count++;
	}
	
	//the passed item is copied to newly allocated memory
	void insert(uint64 index, const test_struct* newItem)
	{
		Assert(index < this->Count);
			
		list_node* newNode = (list_node*)malloc(sizeof(list_node));
		memset(newNode, 0, sizeof(list_node));
		
		newNode->value = (test_struct*)malloc(sizeof(test_struct));
		memcpy(newNode->value, newItem, sizeof(test_struct));
		
		//let ourselves know we need to clean ^ this second malloc ^
		//up when the item is destroyed
		newNode->personallyAllocated = true;
		
		//step to node just before we want to insert
		list_node* current = this->First;
		for(int i = 0; i < index - 1; i++) current = current->next;
		
		newNode->next = current->next;
		current->next = newNode;
	}
	
	void removeAt(uint64 index)
	{
		Assert(index < this->Count);
		
		list_node* target = nullptr;
		if (index > 0)
		{
			//find and modify the node just before the one we want to remove
			list_node* current = this->First;
			for(int i = 0; i < index - 1; i++) current = current->next;
			
			list_node* previous = current;
			target = current->next;
			
			previous->next = target->next;
			
			//if we're removing the last node then 
			//we need to redirect the pointer to the node
			//just before it
			if (target == this->Last)
			{
				this->Last = previous;
			}
		}
		else
		{
			//if we're removing the first node then 
			//we need to redirect the pointer to the first
			//node item
			target = this->First;
			this->First = this->First->next;
			
			//removing the last item in the list we need to make sure
			//the last pointer is nullified along with the first pointer
			if (this->Count == 1)
			{
				this->Last = nullptr;
			}
		}
		
		if (target->personallyAllocated)
		{
			free(target->value);
		}
		free(target);
	}
	
	test_struct* operator [] (uint64 index)
	{
		Assert(index < this->Count);
		
		list_node* current = this->First;
		for(int i = 0; i < index; i++) current = current->next;
		
		return current->value;
	}
};

#if defined(_IOSTREAM_)
std::ostream& operator << (std::ostream& stream, list& list)
{
	stream << "list [" << list.count() << "]{" << endl;
	list_node* current = list.first();
	for(int i = 0; i < list.count(); i++)
	{
		stream << "\t[" << i << "] = " << *list[i] << endl;
	}
	stream << "}" << endl;
	return stream;
}
#endif

#endif