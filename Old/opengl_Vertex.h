#ifndef _OPENGL_VERTEX_H
#define _OPENGL_VERTEX_H

#include "myMath.h"
#include "colors.h"

struct Vertex_t
{
	Vec3_t position;
	Color_t color;
};

inline Vertex_t NewVertex(Vec3_t position, Vec3_t normal, Color_t color, Vec2_t texCoord)
{
	return { position, color };
}
inline Vertex_t NewVertex(Vec3_t position, Color_t color)
{
	return { position, color };
}
inline Vertex_t NewVertex(Vec3_t position)
{
	return { position };
}

inline Vertex_t NewVertex(real32 positionX, real32 positionY, real32 positionZ,
	real32 normalX, real32 normalY, real32 normalZ,
	uint32 color, 
	real32 texCoordX, real32 texCoordY)
{
	return {
		{ positionX, positionY, positionZ },
		{ color },
	};
}


#endif // _OPENGL_VERTEX_H