#ifndef _D3D9_RENDER_TARGET_H
#define _D3D9_RENDER_TARGET_H

#include "mylib/myMath.h"
#include "d3d9_Texture.h"

struct RenderTarget_t
{
	union
	{
		Vec2i_t size;
		struct
		{
			int32 width;
			int32 height;
		};
	};
	
	LPDIRECT3DSURFACE9 pntr;
};

void _CreateRenderTarget(LPDIRECT3DDEVICE9 d3dDevice, RenderTarget_t* renderTarget, int32 width, int32 height)
{
	Assert(d3dDevice != nullptr);
	Assert(renderTarget != nullptr);
	Assert(renderTarget->pntr == nullptr);
	
	if (FAILED(d3dDevice->CreateRenderTarget(
		width, height, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, false, &renderTarget->pntr, NULL)))
	{
		Assert(false);
		renderTarget->pntr = nullptr;
		return;
	}
	
	renderTarget->width = width;
	renderTarget->height = height;
}

RenderTarget_t _GetRenderTarget(LPDIRECT3DDEVICE9 d3dDevice, uint32 index)
{
	Assert(d3dDevice != nullptr);
	
	RenderTarget_t result = {};
	
	if (FAILED(d3dDevice->GetRenderTarget(index, &result.pntr)))
	{
		Assert(false);
		result.pntr = nullptr;
	}
	
	return result;
}

void _SetRenderTarget(LPDIRECT3DDEVICE9 d3dDevice, RenderTarget_t* renderTarget, uint32 index)
{
	Assert(d3dDevice != nullptr);
	Assert(renderTarget != nullptr);
	Assert(renderTarget->pntr != nullptr);
	
	if (FAILED(d3dDevice->SetRenderTarget(index, renderTarget->pntr)))
	{
		//Unable to set the render target
		Assert(false);
	}
}

RenderTarget_t _GetTextureRenderTarget(LPDIRECT3DDEVICE9 d3dDevice, Texture_t* texture)
{
	Assert(d3dDevice != nullptr);
	Assert(texture != nullptr);
	Assert(texture->pntr != nullptr);
	
	RenderTarget_t result = {};
	
	if (FAILED(texture->pntr->GetSurfaceLevel(0, &result.pntr)))
	{
		Assert(false);
		result.pntr = nullptr;
	}
	else
	{
		Assert(result.pntr != nullptr);
	}
	
	return result;
}

#endif // _D3D9_RENDER_TARGET_H