
//Dependant on Texture2D.h

struct SpriteFontCharacter
{
	bool32 filled;//whether or not a texture is assigned for this character.
	char mapping;
	uint32 spriteX;
	uint32 spriteY;
	uint32 spriteWidth;
	uint32 spriteHeight;
	int32 xOffset;
	uint32 width;
	uint32 height;
};

class SpriteFont
{
private:
	bool32 loaded;
	
	Texture2D textureSheet;
	uint32 numValidCharacters;
	SpriteFontCharacter characters[256];
	
	uint32 lineHeight;
	uint32 charSpacing;
	uint32 lineSpacing;
	uint32 spaceWidth;
	uint32 tabWidth;
	uint32 defaultCharacter;
	
	uint32 CharacterSerializedSize() const
	{
		return sizeof(uint32)*6 + sizeof(int32) + sizeof(char);
	}
	//TODO: bool32 return here isn't used by the function
	bool32 DeserializeCharacter(byte* bytes, SpriteFontCharacter* character)
	{
		uint32 loc = 0;
		
		*character = {};
		character->mapping = 			*((char*)&bytes[loc]); loc += sizeof(char);
		character->spriteX = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		character->spriteY = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		character->spriteWidth = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		character->spriteHeight = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		character->height = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		character->xOffset = 			*((int32*)&bytes[loc]); loc += sizeof(int32);
		character->width = 				*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		
		Assert(loc == this->CharacterSerializedSize());
		
		return true;
	}
	
public:
	
	
	SpriteFont()
	{
		this->loaded = false;
		this->textureSheet = Texture2D();
		for(int i = 0; i < 256; i++)
			this->characters[i] = {};
		this->lineHeight = 20;
		this->charSpacing = 1;
		this->lineSpacing = 5;
		this->spaceWidth = 10;
		this->tabWidth = 40;
		this->numValidCharacters = 0;
		this->defaultCharacter = 0;
	}
	SpriteFont(const string& filename)//filename should have no extension
	{
		this->loaded = false;
		this->textureSheet = Texture2D();
		for(int i = 0; i < 256; i++)
			this->characters[i] = {};
		this->lineHeight = 20;
		this->charSpacing = 1;
		this->lineSpacing = 5;
		this->spaceWidth = 10;
		this->tabWidth = 40;
		this->numValidCharacters = 0;
		this->defaultCharacter = 0;
		
		LoadFromFiles(filename);
	}
	SpriteFont(const SpriteFont& other)
	{
		this->loaded = other.loaded;
		this->textureSheet = other.textureSheet;
		for(int i = 0; i < 256; i++)
			this->characters[i] = other.characters[i];
		this->lineHeight = other.lineHeight;
		this->lineSpacing = other.lineSpacing;
		this->charSpacing = other.charSpacing;
		this->spaceWidth = other.spaceWidth;
		this->tabWidth = other.tabWidth;
		this->numValidCharacters = other.numValidCharacters;
		this->defaultCharacter = other.defaultCharacter;
	}
	~SpriteFont()
	{
		
	}
	
	bool LoadFromFiles(const string& filename)//filename should have no extension
	{
		//TODO: Check if filename.png and filename.fnt exist
		FileContents file = ReadFile((filename + ".fnt").c_str());
		if (file.size == 0)
		{
			Log("Unable to open \"" + filename + ".fnt\".");
			return false;
		}
		
		Texture2D newTexture = Texture2D(filename + ".png",
			GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
		if (!newTexture.IsValid())
		{
			Log("\"" + filename + ".png\" was not loaded correctly.");
			if (newTexture.id != 0)
				newTexture.Unload();
			return false;
		}
		
		this->textureSheet = newTexture;
		this->loaded = false;
		
		uint64 loc = 0;
		byte* bytes = (byte*)file.bytes;
		
		uint32 numCharacters = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->lineHeight = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->spaceWidth = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->tabWidth = 			*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->defaultCharacter = 	*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->charSpacing = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		this->lineSpacing = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
		
		this->numValidCharacters = 0;
		for(int i = 0; i < ArrayCount(this->characters); i++)
			this->characters[i] = {};
		
		for(int i = 0; i < numCharacters; i++)
		{
			if (loc + CharacterSerializedSize() >= file.size)
			{
				Log("Reached the end of file before we found all characters.");
				Log("Read " + str(i) + " characters, expected " + 
					str(numCharacters) + " characters.");
				break;
			}
			
			uint32 index = 		*((uint32*)&bytes[loc]); loc += sizeof(uint32);
			if (index != i)
				Log("Index mismatch at " + str(i) + ":" + str(index) + ".");
			
			SpriteFontCharacter newCharacter = {};
			if (!DeserializeCharacter(&bytes[loc], &newCharacter))
			{
				Log("Deserialization of character " + str(i) + " was unsuccessful.");
				loc += CharacterSerializedSize();
				continue;
			}
			loc += CharacterSerializedSize();
			
			if (newCharacter.mapping >= ArrayCount(this->characters))
			{
				Log("The character mapping was outside the bounds of our ",
					"character mapping buffer.");
				continue;
			}
			
			if (this->characters[newCharacter.mapping].filled)
			{
				Log("Found duplicate character mapping in SpriteFont \"" + filename + "\" at index " + str(i));
			}
			
			this->characters[newCharacter.mapping] = newCharacter;
			this->characters[newCharacter.mapping].filled = true;
			this->numValidCharacters++;
		}
		
		CloseFile(file);
		
		this->loaded = true;
		return true;
	}
	
	void Unload()
	{
		this->textureSheet.Unload();
	}
	
	bool32 IsLoaded() const
	{
		return this->loaded;
	}
	uint32 NumValidCharacters() const
	{
		return this->numValidCharacters;
	}
	uint32 LineHeight() const
	{
		return this->lineHeight;
	}
	uint32 GetCharSpacing() const
	{
		return this->charSpacing;
	}
	uint32 GetLineSpacing() const
	{
		return this->lineSpacing;
	}
	uint32 GetSpaceWidth() const
	{
		return this->spaceWidth;
	}
	uint32 GetTabWidth() const
	{
		return this->tabWidth;
	}
	uint32 GetDefaultCharacterIndex() const
	{
		return this->defaultCharacter;
	}
	
	void SetCharSpacing(uint32 charSpacing)
	{
		this->charSpacing = charSpacing;
	}
	void SetLineSpacing(uint32 lineSpacing)
	{
		this->lineSpacing = lineSpacing;
	}
	void SetSpaceWidth(uint32 spaceWidth)
	{
		this->spaceWidth = spaceWidth;
	}
	void SetTabWidth(uint32 tabWidth)
	{
		this->tabWidth = tabWidth;
	}
	void SetDefaultCharacterIndex(uint32 defaultCharacterIndex)
	{
		this->defaultCharacter = defaultCharacterIndex;
	}
	
	SpriteFontCharacter GetCharacterFor(char c) const
	{
		return this->characters[c];
	}
	SpriteFontCharacter GetDefaultCharacter() const
	{
		return this->characters[this->defaultCharacter];
	}
	bool32 HasCharacterFor(char c) const
	{
		return this->characters[c].filled;
	}
	uint32 GetCharacterWidth(char c) const
	{
		return this->characters[c].width;
	}
	uint32 GetCharacterSpriteWidth(char c) const
	{
		return this->characters[c].spriteWidth;
	}
	uint32 GetCharacterHeight(char c) const
	{
		return this->characters[c].height;
	}
	uint32 GetCharacterSpriteHeight(char c) const
	{
		return this->characters[c].spriteHeight;
	}
	
	uint32 MeasureWidth(char c, bool32 measureSpriteSize = false) const
	{
		if (this->characters[c].filled)
		{
			if (measureSpriteSize)
				return this->characters[c].spriteWidth;
			else
				return this->characters[c].width;
		}
		else if (c == ' ')
		{
			return this->spaceWidth;
		}
		else if (c == '\t')
		{
			return this->tabWidth;
		}
		else if (c == '\n')
		{
			return 0;
		}
		else
		{
			return this->characters[this->defaultCharacter].width;
		}
	}
	uint32 MeasureWidth(const string& str, bool32 measureSpriteSize = false) const
	{
		uint32 maxWidth = 0;
		uint32 spriteProtrusion = 0;
		uint32 width = 0;
		for(int i = 0; i < str.length(); i++)
		{
			char c = str[i];
			if (c == '\n')
			{
				width = 0;
			}
			else if (c == ' ')
			{
				if (i > 0)
					width += this->charSpacing;
				width += this->spaceWidth;
			}
			else if (c == '\t')
			{
				if (i > 0)
					width += this->charSpacing;
				width += this->tabWidth;
			}
			else
			{
				if (i > 0)
					width += this->charSpacing;
				
				SpriteFontCharacter character = this->GetCharacterFor(c);
				width += character.width;
				if (measureSpriteSize)
				{
					if (character.spriteWidth - character.xOffset - character.width > 0)
						spriteProtrusion = character.spriteWidth - character.xOffset - character.width;
					else
						spriteProtrusion = 0;
				}
			}
			
			if (width + spriteProtrusion > maxWidth)
			{
				maxWidth = width + spriteProtrusion;
			}
		}
		
		return maxWidth;
	}
	uint32 MeasureHeight(char c, bool32 measureSpriteSize = false) const
	{
		if (measureSpriteSize)
		{
			return this->GetCharacterFor(c).spriteHeight;
		}
		else
		{
			return this->lineHeight;
		}
	}
	uint32 MeasureHeight(const string& str, bool32 measureSpriteSize = false) const
	{
		uint32 height = this->lineHeight;
		uint32 maxProtrusion = 0;
		for(int i = 0; i < str.length(); i++)
		{
			char c = str[i];
			if (c == '\n')
			{
				height += this->lineSpacing + this->lineHeight;
				maxProtrusion = 0;
			}
			else if (measureSpriteSize && c != ' ' && c != '\t')
			{
				SpriteFontCharacter character = this->GetCharacterFor(c);
				if ((int32)character.spriteHeight - (int32)character.height > 0)
				{
					uint32 spriteProtrusion = character.spriteHeight - character.height;
					if (spriteProtrusion > maxProtrusion)
					{
						maxProtrusion = spriteProtrusion;
					}
				}
			}
		}
		return height + maxProtrusion;
	}
	
	#if defined(MYLIB_SHADER_H)
	void RenderCharacter(Shader& shader, const VBO& rectVBO, vec2 position, char c, 
		float scale = 1.0f, bool bindTexture = true, bool bindVBO = true)
	{
		if (bindTexture)
		{
			shader.SetTexture2D("texture", this->textureSheet);
		}
		if (bindVBO)
		{
			shader.BindBuffer(rectVBO);
		}
		
		SpriteFontCharacter character = this->GetCharacterFor(c);
		
		vec2 pos = position;
		pos.y -= character.height * scale;
		pos.x -= character.xOffset * scale;
		
		vec4 sourceRec = vec4(0.0f, 0.0f, 1.0f, 1.0f);
		sourceRec.x = (real32)(character.spriteX) / (real32)this->textureSheet.width;
		sourceRec.y = (real32)(character.spriteY) / (real32)this->textureSheet.height;
		sourceRec.z = (real32)(character.spriteWidth) / (real32)this->textureSheet.width;
		sourceRec.w = (real32)(character.spriteHeight) / (real32)this->textureSheet.height;
		shader.SetVec4("sourceRec", sourceRec);
		
		mat4 worldMatrix = mat4(1.0f);
		worldMatrix = glm::translate(worldMatrix, vec3(pos.x, pos.y, 0.0f));
		worldMatrix = glm::scale(worldMatrix, 
			vec3(character.spriteWidth * scale, character.spriteHeight * scale, 1.0f));
		shader.SetMatrix4("worldMatrix", worldMatrix);
		
		//TODO: Make this draw mode optionally different?
		rectVBO.Draw(GL_TRIANGLES);
	}
	void RenderText(Shader& shader, const VBO& rectVBO, vec2 position, const string& str,
		float scale = 1.0f)
	{
		if (!this->loaded)
		{
			Log("Cannot render text with a font that has not been loaded correctly.");
			Assert(false);
			return;
		}
		
		shader.BindBuffer(rectVBO);
		shader.SetTexture2D("texture", this->textureSheet);
		
		vec2 pos = position + vec2(0, this->lineHeight * scale);
		for(int i = 0; i < str.length(); i++)
		{
			char c = str[i];
			if (c == '\n')
			{
				pos.x = position.x;
				pos.y += (this->lineHeight + this->lineSpacing) * scale;
			}
			else if (c == ' ')
			{
				pos.x += (this->spaceWidth + this->charSpacing) * scale;
			}
			else if (c == '\t')
			{
				pos.x += (this->tabWidth + this->charSpacing) * scale;
			}
			else
			{
				RenderCharacter(shader, rectVBO, pos, c, scale, false, false);
				
				SpriteFontCharacter character = this->GetCharacterFor(c);
				pos.x += (character.width + this->charSpacing) * scale;
			}
			
		}
	}
	#endif
};
