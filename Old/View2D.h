class View2D
{
private:
	vec2 position;
	double zoom;
	vec2 screenSize;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	
public:
	
	
	View2D()
	{
		this->position = vec2(0.0f, 0.0f);
		this->zoom = 1.0;
		this->screenSize = vec2(800, 600);
		RecalculateProjectionMatrix();
		RecalculateViewMatrix();
	}
	View2D(const View2D& other)
	{
		this->position = other.position;
		this->zoom = other.zoom;
		this->screenSize = other.screenSize;
		RecalculateProjectionMatrix();
		RecalculateViewMatrix();
	}
	~View2D()
	{
		
	}
	
	void RecalculateProjectionMatrix()
	{
		this->projectionMatrix = glm::scale(mat4(1.0f), 
			vec3(1.0f/(this->screenSize.x/2), 
				-1.0f/(this->screenSize.y/2), 1.0f));
	}
	void RecalculateViewMatrix()
	{
		this->viewMatrix = mat4(1.0f);
		this->viewMatrix *= glm::scale(mat4(1.0f),
				vec3(this->zoom, this->zoom, 1.0f));
		this->viewMatrix *= glm::translate(mat4(1.0f), 
				vec3(-this->position.x, -this->position.y, 0.0f));
	}
	
	void SetScreenSize(float width, float height)
	{
		this->screenSize = vec2(width, height);
		this->RecalculateProjectionMatrix();
	}
	void SetScreenSize(vec2 size)
	{
		this->screenSize = size;
		this->RecalculateProjectionMatrix();
	}
	vec2 GetScreenSize() const
	{
		return this->screenSize;
	}
	
	vec2 GetViewSize() const
	{
		return vec2(this->screenSize.x/this->zoom, this->screenSize.y/this->zoom);
	}
	vec2 GetViewTopLeft() const
	{
		vec2 viewSize = this->GetViewSize();
		return this->position - vec2(viewSize.x / 2, viewSize.y / 2);
	}
	
	void SetPosition(float x, float y)
	{
		this->position = vec2(x, y);
		this->RecalculateViewMatrix();
	}
	void SetPosition(vec2 position)
	{
		this->position = position;
		this->RecalculateViewMatrix();
	}
	void AddPosition(vec2 add)
	{
		this->position = vec2(this->position.x + add.x, this->position.y + add.y);
		RecalculateViewMatrix();
	}
	vec2 GetPosition() const
	{
		return this->position;
	}
	
	void SetZoom(double zoom)
	{
		this->zoom = zoom;
		this->RecalculateViewMatrix();
	}
	double GetZoom() const
	{
		return this->zoom;
	}
	
	// void SetRotation(double rotation)
	// {
	// 	this->rotation = rotation;
	// 	this->RecalculateViewMatrix();
	// }
	// double GetRotation() const
	// {
	// 	return this->rotation;
	// }
	
	mat4 GetViewMatrix() const
	{
		return this->viewMatrix;
	}
	mat4 GetProjectionMatrix() const
	{
		return this->projectionMatrix;
	}
	
	vec2 ToWorld(vec2 screenPos) const
	{
		vec2 topLeft = this->GetViewTopLeft();
		return vec2((screenPos.x/this->zoom)+topLeft.x, (screenPos.y/this->zoom)+topLeft.y);
	}
	vec2 ToScreen(vec2 worldPos) const
	{
		vec2 topLeft = this->GetViewTopLeft();
		return vec2((worldPos.x-topLeft.x)*zoom, (worldPos.y-topLeft.y)*zoom);
	}
};