#ifndef _OPENGL_VERTEX_BUFFER_H
#define _OPENGL_VERTEX_BUFFER_H

#include "opengl_Vertex.h"

struct VertexArray_t
{
	GLuint id;
};
struct VertexBuffer_t
{
	bool32 isStatic;
	uint32 numVertices;
	
	GLuint id;
};
struct IndexBuffer_t
{
	uint32 numIndices;
	
	GLuint id;
};

void _CreateVertexArray(VertexArray_t* vertexArray)
{
	Assert(vertexArray != nullptr);
	
	glGenVertexArrays(1, &vertexArray->id);
	glBindVertexArray(vertexArray->id);
}

void _CreateVertexBuffer(VertexBuffer_t* vertexBuffer, uint32 numVertices, bool32 isStatic)
{
	Assert(vertexBuffer != nullptr);
	
	glGenBuffers(1, &vertexBuffer->id);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id);
	
	vertexBuffer->numVertices = numVertices;
	vertexBuffer->isStatic = isStatic;
}

void _FillVertexBuffer(VertexBuffer_t* vertexBuffer, Vertex_t* vertices)
{
	Assert(vertexBuffer != nullptr);
	Assert(vertexBuffer->isStatic);//TODO: Handle non static buffers
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id);
	glBufferData(GL_ARRAY_BUFFER, 
		sizeof(Vertex_t) * vertexBuffer->numVertices,
		vertices, GL_STATIC_DRAW);
}

#endif // _OPENGL_VERTEX_BUFFER_H