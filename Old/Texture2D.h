
class Texture2D
{
private:
	bool32 isValid;
	string errorStr;
	
	
public:
	uint32 id;
	string filepath;
	unsigned int width;
	unsigned int height;
	
	
	Texture2D()
	{
		this->isValid = false;
		this->errorStr = "Empty constructor called.";
		this->id = 0;
		this->filepath = "";
		this->width = 0;
		this->height = 0;
	}
	Texture2D(const string& filepath,
		GLenum minFilter = GL_NEAREST_MIPMAP_NEAREST,
		GLenum magFilter = GL_NEAREST, 
		GLenum wrapS = GL_REPEAT, 
		GLenum wrapT = GL_REPEAT)
	{
		this->isValid = true;
		this->errorStr = "";
		this->id = 0;
		this->filepath = "";
		this->width = 0;
		this->height = 0;
		
		this->LoadFromPNG(filepath, minFilter, magFilter, wrapS, wrapT);
	}
	Texture2D(uint32 width, uint32 height, const uint32* imageData)
	{
		this->isValid = true;
		this->errorStr = "";
		this->id = 0;
		this->filepath = "";
		this->width = 0;
		this->height = 0;
		
		this->LoadFromData(width, height, imageData);
	}
	Texture2D(const Texture2D& other)
	{
		this->isValid = other.isValid;
		this->errorStr = other.errorStr;
		this->id = other.id;
		this->filepath = other.filepath;
		this->width = other.width;
		this->height = other.height;
	}
	~Texture2D()
	{
		
	}
	
	bool32 LoadFromPNG(const string& filepath,
		GLenum minFilter = GL_NEAREST_MIPMAP_NEAREST,
		GLenum magFilter = GL_NEAREST, 
		GLenum wrapS = GL_REPEAT, 
		GLenum wrapT = GL_REPEAT)
	{
		
		byte* imageData;
		unsigned width, height;
		{
			unsigned error;

			error = lodepng_decode32_file(&imageData, &width, &height, filepath.c_str());
			
			if(error)
			{
				Log("LodePNG error occurred while loading file \"" + filepath + "\"");
				this->isValid = false;
				this->errorStr = "error " + str(error) + ": " + lodepng_error_text(error);
				return false;
			}
		}
		
		if (this->id != 0)
		{
			Log("Trying to load another image into an already filled Texture2D.");
			this->isValid = false;
			this->errorStr = "You must call Unload() before loading another image.";
			return false;
		}

		GLuint newID;
		glGenTextures(1, &newID);
		glBindTexture(GL_TEXTURE_2D, newID);
		
		if (!CheckOpenGLErrors())
		{
			this->isValid = false;
			this->errorStr = "An OpenGL error occurred during texture generation.";
			return false;
		}
		
		glTexImage2D(
			GL_TEXTURE_2D, 		//bound texture type
			0,					//image level
			GL_RGBA,			//internal format
			width,				//image width
			height,				//image height
			0,					//border
			GL_RGBA,			//format
			GL_UNSIGNED_BYTE,	//type
			imageData);			//data
		
		
		// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glGenerateMipmap(GL_TEXTURE_2D);
		
		free(imageData);
		
		if (!CheckOpenGLErrors())
		{
			this->isValid = false;
			this->errorStr = "An OpenGL error occurred during texture filling.";
			return false;
		}
		
		this->id = newID;
		this->filepath = filepath;
		this->width = width;
		this->height = height;
		this->isValid = true;
		this->errorStr = "";
		
		return true;
	}
	bool32 LoadFromData(uint32 width, uint32 height, const uint32* imageData)
	{
		
		GLuint newID;
		glGenTextures(1, &newID);
		glBindTexture(GL_TEXTURE_2D, newID);
		
		if (!CheckOpenGLErrors())
		{
			this->isValid = false;
			this->errorStr = "An OpenGL error occurred during texture generation.";
			return false;
		}
		
		glTexImage2D(
			GL_TEXTURE_2D, 		//bound texture type
			0,					//image level
			GL_RGBA,			//internal format
			width,				//image width
			height,				//image height
			0,					//border
			GL_RGBA,			//format
			GL_UNSIGNED_BYTE,	//type
			imageData);			//data
		
		
		// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glGenerateMipmap(GL_TEXTURE_2D);
		
		if (!CheckOpenGLErrors())
		{
			this->isValid = false;
			this->errorStr = "An OpenGL error occurred during texture filling.";
			return false;
		}
		
		this->id = newID;
		this->filepath = "CUSTOM_DATA";
		this->width = width;
		this->height = height;
		this->isValid = true;
		this->errorStr = "";
		return true;
	}
	
	void Unload()
	{
		if (this->id == 0)
		{
			Log("Tried to unload an already empty texture.");
			return;
		}
		
		glDeleteTextures(1, &this->id);
		
		this->id = 0;
		this->isValid = false;
		this->errorStr = "Texture has been deleted.";
		this->width = 0;
		this->height = 0;
	}
	
	bool32 IsValid() const
	{
		return this->isValid;
	}
	string GetError() const
	{
		return this->errorStr;
	}
};

Texture2D CreateDotTexture()
{
	uint32 data = 0xFFFFFFFF;
	return Texture2D(1, 1, &data);
}

