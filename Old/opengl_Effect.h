#ifndef _OPENGL_EFFECT_H
#define _OPENGL_EFFECT_H

struct EffectCompileLog_t
{
	GLint status;
	uint32 logLength;
	char log[256];
};

struct Effect_t
{
	GLuint vertShader;
	GLuint fragShader;
	GLuint id;
	
	union
	{
		EffectCompileLog_t logs[3];
		struct
		{
			EffectCompileLog_t vertLog;
			EffectCompileLog_t fragLog;
			EffectCompileLog_t programLog;
		};
	};
};

bool32 _CreateEffectFromFiles(Effect_t* effect,
	const void* vertEffectSource, const void* fragEffectSource)
{
	Assert(effect != nullptr);
	Assert(vertEffectSource != nullptr);
	Assert(fragEffectSource != nullptr);
	
	effect->vertShader = glCreateShader(GL_VERTEX_SHADER);
	effect->fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	
	GLint vertStatus = GL_FALSE;
	glShaderSource(effect->vertShader, 1, (char**)&vertEffectSource, NULL);
	glCompileShader(effect->vertShader);
	glGetShaderiv(effect->vertShader, GL_COMPILE_STATUS, &vertStatus);
	
	GLint fragStatus = GL_FALSE;
	glShaderSource(effect->fragShader, 1, (char**)&fragEffectSource, NULL);
	glCompileShader(effect->fragShader);
	glGetShaderiv(effect->fragShader, GL_COMPILE_STATUS, &fragStatus);
	
	GLint linkStatus = GL_FALSE;
	effect->id = glCreateProgram();
	glAttachShader(effect->id, effect->vertShader);
	glAttachShader(effect->id, effect->fragShader);
	glLinkProgram(effect->id);
	glGetShaderiv(effect->id, GL_LINK_STATUS, &linkStatus);
	
	effect->vertLog.status = vertStatus;
	effect->fragLog.status = fragStatus;
	effect->programLog.status = linkStatus;
	
	GLint logLength;
	
	glGetShaderiv(effect->vertShader, GL_INFO_LOG_LENGTH, &logLength);
	effect->vertLog.logLength = (uint32)logLength;
	ClearArray(effect->vertLog.log);
	if (effect->vertLog.logLength > 0)
	{
		Assert(effect->vertLog.logLength < ArrayCount(effect->vertLog.log));
		glGetShaderInfoLog(effect->vertShader, effect->vertLog.logLength, NULL, effect->vertLog.log);
	}
	
	glGetShaderiv(effect->fragShader, GL_INFO_LOG_LENGTH, &logLength);
	effect->fragLog.logLength = (uint32)logLength;
	ClearArray(effect->fragLog.log);
	if (effect->fragLog.logLength > 0)
	{
		Assert(effect->fragLog.logLength < ArrayCount(effect->fragLog.log));
		glGetShaderInfoLog(effect->fragShader, effect->fragLog.logLength, NULL, effect->fragLog.log);
	}
	
	glGetShaderiv(effect->id, GL_INFO_LOG_LENGTH, &logLength);
	effect->programLog.logLength = (uint32)logLength;
	if (effect->programLog.logLength > 0)
	{
		Assert(effect->programLog.logLength < ArrayCount(effect->programLog.log));
		glGetProgramInfoLog(effect->id, effect->programLog.logLength, NULL, effect->programLog.log);
	}
	
	// glDetachShader(effect->id, effect->vertShader);
	// glDetachShader(effect->id, effect->fragShader);
	
	// glDeleteShader(effect->vertShader);
	// glDeleteShader(effect->fragShader);
	
	if (vertStatus == GL_TRUE && fragStatus == GL_TRUE && linkStatus == GL_TRUE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void _DeleteEffect(Effect_t* effect)
{
	//TODO: Release ids and stuff
}

#endif // _OPENGL_EFFECT_H