//this file contains the classes and functions to help facilitate
//binding of C-Functions to lua.

#include <luahelpers.h>

#define LUA_ARG_VOID			0
#define LUA_ARG_INT				1
#define LUA_ARG_UINT			2
#define LUA_ARG_REAL			3
#define LUA_ARG_STRING			4

typedef int32 LuaArgumentType;

const char* GetArgumentTypeStr(LuaArgumentType type)
{
	switch(type)
	{
		case LUA_ARG_VOID:		return "void";
		case LUA_ARG_INT:		return "int";
		case LUA_ARG_UINT:		return "uint";
		case LUA_ARG_REAL:		return "real";
		case LUA_ARG_STRING:	return "string";
		default: 				return "unknown";
	};
}

class LuaArgs
{
private:
	uint32 numArguments;
	void** arguments;
	LuaArgumentType* argumentTypes;
	
public:
	
	LuaArgs()
	{
		this->numArguments = 0;
		this->arguments = nullptr;
		this->argumentTypes = nullptr;
	}
	LuaArgs(const LuaArgs& other)
	{
		this->Resize(other.numArguments);
		for(int i = 0; i < other.numArguments; i++)
		{
			this->SetValue(i, other.argumentTypes[i], other.arguments[i]);
		}
	}
	~LuaArgs()
	{
		if (this->arguments != nullptr)
		{
			for(uint32 i = 0; i < this->numArguments; i++)
			{
				delete this->arguments[i];
			}
			
			delete this->arguments;
		}
	}
	
	void Resize(uint32 newSize)
	{
		if (newSize == this->numArguments) return;
		
		if (newSize < this->numArguments)
		{
			for(int i = newSize; i < this->numArguments; i++)
			{
				delete this->arguments[i];
				this->arguments[i] = nullptr;
			}
		}
		
		if (this->arguments == nullptr)
		{
			this->arguments = new void*[newSize];
			this->argumentTypes = new LuaArgumentType[newSize];
		}
		else
		{
			void** newList = new void*[newSize];
			LuaArgumentType* newTypeList = new LuaArgumentType[newSize];
			
			for(int i = 0; i < this->numArguments && i < newSize; i++)
			{
				newList[i] = this->arguments[i];
				newTypeList[i] = this->argumentTypes[i];
			}
			
			delete this->arguments;
			this->arguments = newList;
			
			delete this->argumentTypes;
			this->argumentTypes = newTypeList;
		}
		
		if (newSize > this->numArguments)
		{
			for(int i = this->numArguments; i < newSize; i++)
			{
				this->arguments[i] = nullptr;
				this->argumentTypes[i] = LUA_ARG_VOID;
			}
		}
		
		this->numArguments = newSize;
	}
	
	void SetValue(uint32 index, LuaArgumentType type, const void* newValue)
	{
		if (this->arguments == nullptr) return;
		if (index >= this->numArguments) return;
		
		if (this->arguments[index] == nullptr || this->argumentTypes[index] != type)
		{
			if (this->arguments[index] != nullptr) delete this->arguments[index];
			switch(type)
			{
				case LUA_ARG_INT:		this->arguments[index] = malloc(sizeof(int64)); break;
				case LUA_ARG_UINT:		this->arguments[index] = malloc(sizeof(uint64)); break;
				case LUA_ARG_REAL:		this->arguments[index] = malloc(sizeof(real64)); break;
				case LUA_ARG_STRING:	this->arguments[index] = malloc(sizeof(string)); break;
				default:				this->arguments[index] = nullptr; break;
			};
			this->argumentTypes[index] = type; 
		}
		
		switch(type)
		{
			case LUA_ARG_INT:		*((int64*)this->arguments[index])	= *((int64*)newValue); 	break;
			case LUA_ARG_UINT:		*((uint64*)this->arguments[index])	= *((uint64*)newValue); break;
			case LUA_ARG_REAL:		*((real64*)this->arguments[index])	= *((real64*)newValue); break;
			case LUA_ARG_STRING:	*((string*)this->arguments[index])	= *((string*)newValue); break;
			default: break;
		};
	}
	void SetVoid(uint32 index)
	{
		this->SetValue(index, LUA_ARG_VOID, nullptr);
	}
	void SetInt8(uint32 index, int8 newValue)
	{
		int64 integer = (int64)newValue;
		this->SetValue(index, LUA_ARG_INT, (void*)&integer);
	}
	void SetInt16(uint32 index, int16 newValue)
	{
		int64 integer = (int64)newValue;
		this->SetValue(index, LUA_ARG_INT, (void*)&integer);
	}
	void SetInt32(uint32 index, int32 newValue)
	{
		int64 integer = (int64)newValue;
		this->SetValue(index, LUA_ARG_INT, (void*)&integer);
	}
	void SetInt64(uint32 index, int64 newValue)
	{
		this->SetValue(index, LUA_ARG_INT, (void*)&newValue);
	}
	void SetUInt8(uint32 index, uint8 newValue)
	{
		uint64 integer = (uint64)newValue;
		this->SetValue(index, LUA_ARG_UINT, (void*)&integer);
	}
	void SetUInt16(uint32 index, uint16 newValue)
	{
		uint64 integer = (uint64)newValue;
		this->SetValue(index, LUA_ARG_UINT, (void*)&integer);
	}
	void SetUInt32(uint32 index, uint32 newValue)
	{
		uint64 integer = (uint64)newValue;
		this->SetValue(index, LUA_ARG_UINT, (void*)&integer);
	}
	void SetUInt64(uint32 index, uint64 newValue)
	{
		this->SetValue(index, LUA_ARG_UINT, (void*)&newValue);
	}
	void SetReal32(uint32 index, real32 newValue)
	{
		real64 real = (real64)newValue;
		this->SetValue(index, LUA_ARG_REAL, (void*)&real);
	}
	void SetReal64(uint32 index, real64 newValue)
	{
		this->SetValue(index, LUA_ARG_REAL, (void*)&newValue);
	}
	void SetString(uint32 index, string newValue)
	{
		this->SetValue(index, LUA_ARG_STRING, (void*)&newValue);
	}
};

void my_lua_register(lua_State* state, const char* funcName, lua_CFunction function)
{
	lua_pushstring(state, funcName);
	lua_pushcclosure(state, function, 1);
	lua_setglobal(state, funcName);
}

const char* GetLuaCalledFunctionName(lua_State* state)
{
    return lua_tostring(state, lua_upvalueindex(1));
}

int GlobalHandleBinding(lua_State* state)
{
	Log("The binding was called");
	
	// for(int i = 0; i < GlobalBinder->numBoundFunctions; i++)
	// {
	// 	Log("[" + str(i) + "]  " + GlobalBinder->boundFunctions[i]->ToString());
	// }
	
	string funcName = GetLuaCalledFunctionName(state);
	
	Log("Function called was \"" + funcName + "\".");
	
	return 0;
}
