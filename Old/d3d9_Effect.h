#ifndef _D3D9_EFFECT_H
#define _D3D9_EFFECT_H

#include "mylib/myMath.h"
#include "mylib/d3d9_Texture.h"

struct Effect_t
{
	LPD3DXEFFECT pntr;
};

bool32 _CreateEffectFromFile(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* filepath)
{
	Assert(d3dDevice != nullptr);
	Assert(effect != nullptr);
	Assert(effect->pntr == nullptr);
	Assert(filepath != nullptr);
	
	LPD3DXBUFFER outputBuffer = nullptr;
	
	bool failed = FAILED(D3DXCreateEffectFromFile(d3dDevice, filepath, 	
		NULL, NULL, D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY, NULL, &effect->pntr, &outputBuffer));
	
	// DEBUG_PrintLine("HLSL Compilation of \"%s\" was a %s!", filename, failed ? "Failure" : "Success");
	
	if (outputBuffer != nullptr)
	{
		DWORD compileOutputLength = outputBuffer->GetBufferSize();
		char* compileOutput = (char*)outputBuffer->GetBufferPointer();
		if (compileOutput != nullptr && compileOutputLength > 0)
		{
			// DEBUG_PrintLine("%d byte compilation output:", compileOutputLength);
			// DEBUG_WriteLine(compileOutput);
			if (false || failed) { Assert(false); }
		}
	}
	
	if (!failed) { Assert(effect->pntr != nullptr); }
	
	return !failed;
}

void _DeleteEffect(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect)
{
	Assert(d3dDevice != nullptr);
	Assert(effect != nullptr);
	
	if (effect->pntr != nullptr)
	{
		effect->pntr->Release();
		effect->pntr = nullptr;
	}
}

void _EffectSetMatrix(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* matrixName, Matrix4_t matrix)
{
	effect->pntr->SetMatrix(matrixName, (D3DXMATRIX*)&matrix);
}

void _EffectSetTexture(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* textureName, Texture_t texture)
{
	D3DXHANDLE textureHandle = effect->pntr->GetParameterByName(0, textureName);
	effect->pntr->SetTexture(textureHandle, texture.pntr);
}

void _EffectSetFloat(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* vectorName, float value)
{
	effect->pntr->SetFloatArray(vectorName, (real32*)&value, 1);
}

void _EffectSetVec2(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* vectorName, Vec2_t vector)
{
	effect->pntr->SetFloatArray(vectorName, (real32*)&vector, 2);
}
void _EffectSetVec3(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* vectorName, Vec3_t vector)
{
	effect->pntr->SetFloatArray(vectorName, (real32*)&vector, 3);
}
//TODO: For when Vec4_t is an implemented type
// void _EffectSetVec4(LPDIRECT3DDEVICE9 d3dDevice, Effect_t* effect, const char* vectorName, Vec4_t vector)
// {
// 	effect->pntr->SetFloatArray(vectorName, (real32*)&vector, 4);
// }

#endif // _D3D9_EFFECT_H