//dependant on the string class
//Log(const string& str) must be defined for these helpers to log to
//make sure you have included lua header files before including this file

#ifndef LUA_HELPERS
#define LUA_HELPERS

//apparently we have to do this?
#ifdef __cplusplus
extern "C"
#endif
int luabound_help(lua_State* state)
{
	Log("Lua Console created by Taylor Robbins Oct. 1st 2016");

	// Let Lua know how many return values we've passed
	return 0;
}

int luabound_log(lua_State* state)
{
	// The number of function arguments will be on top of the stack.
	int numArgs = lua_gettop(state);

	// Log("log() was called with " + str(numArgs) + " arguments:");

	for (int i = 1; i <= numArgs; i++) 
	{
		int type = lua_type(state, i);
		switch(type)
		{
			case LUA_TNIL:
			{
				Log("nil");
				
			}break;
			case LUA_TNUMBER:
			{
				lua_Number num = lua_tonumber(state, i);
				lua_Integer integer = 0;
				if (lua_numbertointeger(num, &integer))
				{
					Log(str((long long)integer));
				}
				else
				{
					Log(str((double)num));
				}
				
			}break;
			case LUA_TBOOLEAN:
			{
				bool32 b = lua_toboolean(state, i);
				if (b) 	Log("true");
				else 	Log("false");
				
			}break;
			case LUA_TSTRING:
			{
				string str = lua_tostring(state, i);
				Log("\"" + str + "\"");
				
			}break;
			case LUA_TTABLE:
			{
				
			}break;
			case LUA_TFUNCTION:
			{
				lua_CFunction func = lua_tocfunction(state, i);
				const void* pntr = lua_topointer(state, i);
				char pntrLocStr[255] = {};
				sprintf(pntrLocStr, "%p", pntr);
				//TODO: Get more information about the function pointer somehow?
				Log("c_func_pntr {", pntrLocStr, "}");
				
			}break;
			case LUA_TUSERDATA:
			{
				
			}break;
			case LUA_TTHREAD:
			{
				
			}break;
			case LUA_TLIGHTUSERDATA:
			{
				
			}break;
			default:
				Log("Unknown LUA type passed to Log().");
				break;
		};
	}

	// Let Lua know how many return values we've passed
	return 0;
}

void bind_our_default_lua_functions(lua_State* state)
{
	lua_register(state, "help", luabound_help);
	lua_register(state, "log", luabound_log);
}

void print_error(lua_State* state) 
{
	// The error message is on top of the stack.
	// Fetch it, print it and then pop it off the stack.
	size_t length = 0;
	const char* message = lua_tolstring(state, -1, &length);
	string error = string(message, length);
	lua_pop(state, 1);
	Log("Lua Error: " + error);
}

#endif