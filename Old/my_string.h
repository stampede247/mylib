/*
File:   my_string.h
Author: Taylor Robbins
Date:   11\03\2017
*/

#include "mylib.h"

#if !defined(MYLIB_MYSTRING_H)
#define MYLIB_MYSTRING_H

using std::cout;
using std::endl;

#define MYLIB_STRING_BUFFER_INCREMENTS 			16

class string
{
private:
	char* Pntr;
	uint64 Length;
	uint64 BufferLength;
	
	void InitialAlloc(int initialLength = MYLIB_STRING_BUFFER_INCREMENTS)//TODO: Add initial size
	{
		this->BufferLength = 0;
		while (this->BufferLength < initialLength) this->BufferLength += MYLIB_STRING_BUFFER_INCREMENTS;
		
		this->Pntr = (char*)malloc(this->BufferLength);
		
		this->Length = 0;
		memset(this->Pntr, '\0', this->BufferLength);
	}
	
	void ReallocateFor(uint64 newDataSize)
	{
		Assert(newDataSize >= this->BufferLength);
		
		uint64 newBufferSize = this->BufferLength;
		while(newBufferSize < newDataSize) 
		{
			//TODO: Handle sizes larger than uint64_max
			newBufferSize += MYLIB_STRING_BUFFER_INCREMENTS;
		}
		
		cout << "Resizing buffer held at " << (void*)this->Pntr << " from " << this->BufferLength << " to " << newBufferSize << " bytes." << endl;
		char* newPntr = (char*)realloc(this->Pntr, newBufferSize);
		// if (this->Pntr != newPntr) cout << "Buffer moved from " << (void*)this->Pntr << " to " << (void*)newPntr << "." << endl;
		
		memset(&newPntr[this->BufferLength], '\0', newBufferSize - this->BufferLength);
		
		this->Pntr = newPntr;
		this->BufferLength = newBufferSize;
	}
	
public:
	string()
	{
		InitialAlloc();
	}
	string(const string& other)
	{
		cout << "Copy constructor is called" << endl;
		InitialAlloc(other.length() + 1);
		this->set(other);
	}
	string(const char* data)
	{
		uint64 dataSize = 0;
		while(data[dataSize] != '\0') dataSize++;
		InitialAlloc(dataSize + 1);
		this->set(data);
	}
	string(const char* data, uint64 length)
	{
		InitialAlloc(length + 1);
		this->set(data, length);
	}
	string(const char* data1, const char* data2)
	{
		uint64 dataSize1 = 0;
		while(data1[dataSize1] != '\0') dataSize1++;
		uint64 dataSize2 = 0;
		while(data2[dataSize2] != '\0') dataSize2++;
		InitialAlloc(dataSize1 + dataSize2 + 1);
		this->set(data1);
		this->append(data2);
	}
	string(const char* str, char ch)
	{
		InitialAlloc();
		this->set(str);
		this->append(ch);
	}
	~string()
	{
		cout << "Deallocating buffer at " << (void*)this->Pntr << endl;
		free(this->Pntr);
	}
	
	void set(char ch)
	{
		//this should never really fail as long as MYLIB_STRING_BUFFER_INCREMENTS
		//is greateer than 2. We do this instead of checking if we need to resize the
		//buffer because that class was not designed to work with 2 or less length
		//buffer increment sizes. If the functionality is desired later we can do
		//an actual check here
		Assert(this->BufferLength >= 2);
		
		this->Pntr[0] = ch;
		this->Pntr[1] = '\0';
		
		this->Length = 1;
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	void set(const char* newData)
	{
		uint64 newDataSize = 0;
		while(newData[newDataSize] != '\0')
		{
			//TODO: Handle things larger than uint64_max
			newDataSize++;
		}
		
		if (newDataSize + 1 > this->BufferLength)//+ 1 for '\0'
		{
			ReallocateFor(newDataSize + 1);
			Assert(newDataSize + 1 <= this->BufferLength);
		}
		
		for(int i = 0; i < newDataSize; i++)
		{
			this->Pntr[i] = newData[i];
		}
		
		this->Length = newDataSize;
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	void set(const char* newData, uint64 dataLength)
	{
		if (dataLength + 1 > this->BufferLength)//+ 1 for '\0'
		{
			ReallocateFor(dataLength + 1);
			Assert(dataLength + 1 <= this->BufferLength);
		}
		
		for(int i = 0; i < dataLength; i++)
		{
			this->Pntr[i] = newData[i];
		}
		
		this->Length = dataLength;
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	void set(const string& other)
	{
		if (other.length() + 1 > this->BufferLength)//+ 1 for '\0'
		{
			ReallocateFor(other.length() + 1);
			Assert(other.length() + 1 <= this->BufferLength);
		}
		
		for(int i = 0; i < other.length(); i++)
		{
			this->Pntr[i] = other[i];
		}
		
		this->Length = other.length();
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}

	void append(char ch)
	{
		if (this->Length + 1 == this->BufferLength)
		{
			ReallocateFor(this->Length + 1 + 1);
			Assert(this->Length + 1 + 1 <= this->BufferLength);
		}
		
		this->Pntr[this->Length] = ch;
		
		this->Length++;
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	void append(const char* newData)
	{
		uint64 newDataSize = 0;
		while(newData[newDataSize] != '\0')
		{
			//TODO: Handle things larger than uint64_max
			newDataSize++;
		}
		
		if (this->Length + newDataSize + 1 > this->BufferLength)//+ 1 for '\0'
		{
			ReallocateFor(this->Length + newDataSize + 1);
			Assert(this->Length + newDataSize + 1 <= this->BufferLength);
		}
		
		for(int i = 0; i < newDataSize; i++)
		{
			this->Pntr[this->Length + i] = newData[i];
		}
		
		this->Length = this->Length + newDataSize;
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	void append(const string& other)
	{
		if (this->Length + other.length() + 1 > this->BufferLength)
		{
			ReallocateFor(this->Length + other.length() + 1);
			Assert(this->Length + other.length() + 1 <= this->BufferLength);
		}
		
		for(int i = 0; i < other.length(); i++)
		{
			this->Pntr[this->Length + i] = other[i];
		}
		
		this->Length = this->Length + other.length();
		memset(&this->Pntr[this->Length], '\0', this->BufferLength - this->Length);
	}
	
	inline char* get() const
	{
		return this->Pntr;
	}
	inline char get(int index) const
	{
		if (index > this->Length)
		{
			Assert(!"Index was outside the bounds of the string.");
		}
		return this->Pntr[index];
	}
	
	inline uint64 length() const
	{
		return this->Length;
	}
	
	inline uint64 bufferLength() const
	{
		return this->BufferLength;
	}
	
	string& operator = (char ch)
	{
		this->set(ch);
		return *this;
	}
	string& operator = (const char* chPntr)
	{
		this->set(chPntr);
		return *this;
	}
	string& operator = (const string& other)
	{
		if (this != &other)
		{
			this->set(other);
		}
		return *this;
	}
	
	string& operator += (char ch)
	{
		this->append(ch);
		return *this;
	}
	string& operator += (const char* other)
	{
		this->append(other);
		return *this;
	}
	string& operator += (const string& other)
	{
		this->append(other);
		return *this;
	}
	
	char operator [] (uint64 index) const
	{
		if (index > this->Length)
		{
			Assert(!"Index was outside the bounds of the string.");
		}
		return this->Pntr[index];
	}
	
};

string operator + (string& str, char ch)
{
	return string(str.get(), ch);
}
string operator + (string& str, const char* chPntr)
{
	return string(str.get(), chPntr);
}
string operator + (const char* chPntr, string& str)
{
	return string(chPntr, str.get());
}
string operator + (string& str1, string& str2)
{
	return string(str1.get(), str2.get());
}
bool32 operator == (string& str, const char* chPntr)
{
	for(int i = 0; i < str.length(); i++)
	{
		if (str.get()[i] != chPntr[i])
		{
			return false;
		}
	}
	
	return true;
}
bool32 operator == (string& str1, string& str2)
{
	if (str1.length() != str2.length()) return false;
	for(int i = 0; i < str1.length(); i++)
	{
		if (str1.get()[i] != str2.get()[i])
		{
			return false;
		}
	}
	
	return true;
}
bool32 operator != (string& str, const char* chPntr)
{
	for(int i = 0; i < str.length(); i++)
	{
		if (str.get()[i] != chPntr[i])
		{
			return true;
		}
	}
	
	return false;
}
bool32 operator != (string& str1, string& str2)
{
	if (str1.length() != str2.length()) return true;
	for(int i = 0; i < str1.length(); i++)
	{
		if (str1.get()[i] != str2.get()[i])
		{
			return true;
		}
	}
	
	return false;
}

#if defined(_IOSTREAM_)
std::ostream& operator << (std::ostream& stream, string& str)
{
	stream << str.get();
	return stream;
}
#endif

#endif