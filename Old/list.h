#ifndef _MY_LIST_H
#define _MY_LIST_H

//TODO: This structure is not really type-safe. You can push any
//		memory onto the array but we expect certain sized chunks
//		to be passed to PushOnList. We could make this into a template
//		class if we want to go the c++ route.  

//NOTE: This class is currently designed to simply handle growing lists,
//		no removing or moving items

struct List_t
{
	uint32 itemSize;
	uint32 allocationStep; //in # of items
	
	uint32 count;
	uint32 capacity; //in # of items
	void* items;
};

void CreateList(List_t* list, uint32 itemSizeInBytes, uint32 numItemsAllocatePerStep)
{
	Assert(list != nullptr);
	Assert(itemSizeInBytes > 0);
	Assert(numItemsAllocatePerStep > 0);
	
	ClearPointer(list);
	list->allocationStep = numItemsAllocatePerStep;
	list->itemSize = itemSizeInBytes;
	
	list->items = malloc(list->itemSize * list->allocationStep);
	Assert(list->items != nullptr);
	
	list->capacity = list->allocationStep;
}

void PushOnList(List_t* list, void* newItem)
{
	Assert(list != nullptr);
	Assert(list->items != nullptr);
	Assert(newItem != nullptr);
	
	if (list->count >= list->capacity)
	{
		list->capacity += list->allocationStep;
		list->items = realloc(list->items, list->capacity * list->itemSize);
		Assert(list->items != nullptr);
	}
	
	byte* emptyLocation = (byte*)list->items;
	emptyLocation += list->itemSize * list->count;
	memcpy(emptyLocation, newItem, list->itemSize);
	
	list->count++;
}

void* GetListItem(List_t* list, uint32 index)
{
	Assert(list != nullptr);
	
	if (index >= list->count)
	{
		Assert(false);
		return nullptr;
	}
	
	return &((byte*)list->items)[index * list->itemSize];
}

void DeleteList(List_t* list)
{
	Assert(list != nullptr);
	
	if (list != nullptr && list->items != nullptr)
	{
		free(list->items);
		
		list->count = 0;
		list->capacity = 0;
		list->items = nullptr;
	}
}

void* FindInList(List_t* list, void* compareStruct, bool32 (*CompareFunc)(void* left, void* right))
{
	//NOTE: This uses memcmp to compare two different items.
	//		That means it will not always work if there is padding
	//		in the item's structure since this padding is technically
	//		undefined
	
	for(uint32 i = 0; i < list->count; i++)
	{
		void* item = GetListItem(list, i);
		if (CompareFunc(item, compareStruct))
		{
			return item;
		}
	}
	
	return nullptr;
}

#endif