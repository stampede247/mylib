#ifndef MYLIB_SHADER_H
#define MYLIB_SHADER_H

#define VERTEX_SHADER		GL_VERTEX_SHADER
#define GEOMETRY_SHADER		GL_GEOMETRY_SHADER
#define FRAGMENT_SHADER		GL_FRAGMENT_SHADER

class Shader
{
private:
	GLuint vShader;
	GLuint gShader;
	GLuint fShader;
	
	byte vaoMode; //corrisponds with VBO_MODE
	uint32 vao;
	GLint positionAttribute;
	GLint colorAttribute;
	GLint texCoord1Attribute;
	GLint texCoord2Attribute;
	GLint normalAttribute;
	
public:
	uint32 id;
	
	Shader()
	{
		this->vShader = 0;
		this->gShader = 0;
		this->fShader = 0;
		this->id = 0;
		this->vaoMode = VBO_MODE_UNDEFINED;
		this->vao = 0;
		this->positionAttribute = 0;
		this->colorAttribute = 0;
		this->texCoord1Attribute = 0;
		this->texCoord2Attribute = 0;
		this->normalAttribute = 0;
	}
	Shader(const Shader& other)
	{
		this->vShader = other.vShader;
		this->gShader = other.gShader;
		this->fShader = other.fShader;
		this->id = other.id;
		this->vaoMode = other.vaoMode;
		this->vao = other.vao;
		this->positionAttribute = other.positionAttribute;
		this->colorAttribute = other.colorAttribute;
		this->texCoord1Attribute = other.texCoord1Attribute;
		this->texCoord2Attribute = other.texCoord2Attribute;
		this->normalAttribute = other.normalAttribute;
	}
	~Shader()
	{
		//TODO: Ensure I don't need to delete anything
		//		that I've created locally
	}
	
	bool32 HasPart(GLenum type) const
	{
		const GLuint* part = nullptr;
		if (type == VERTEX_SHADER) part = &this->vShader;
		if (type == GEOMETRY_SHADER) part = &this->gShader;
		if (type == FRAGMENT_SHADER) part = &this->fShader;
		if (part == nullptr) return false;
		
		return (*part != 0);
	}
	bool32 IsCompiled() const
	{
		return (this->id != 0);
	}
	
	bool32 HasAttribute(byte attribute) const
	{
		if (attribute == VBO_ATT_POSITION)
			return (this->positionAttribute != -1);
		if (attribute == VBO_ATT_COLOR)
			return (this->colorAttribute != -1);
		if (attribute == VBO_ATT_TEXCOORD1)
			return (this->texCoord1Attribute != -1);
		if (attribute == VBO_ATT_TEXCOORD2)
			return (this->texCoord2Attribute != -1);
		if (attribute == VBO_ATT_NORMAL)
			return (this->normalAttribute != -1);
		
		Log("Checking a shader for an unknown attribute type: ", str(attribute));
		return false;
	}
	bool32 HasPositionAttribute() const
	{
		return HasAttribute(VBO_ATT_POSITION);
	}
	bool32 HasColorAttribute() const
	{
		return HasAttribute(VBO_ATT_COLOR);
	}
	bool32 HasTexCoordAttribute() const
	{
		return HasAttribute(VBO_ATT_TEXCOORD);
	}
	bool32 HasTexCoord1Attribute() const
	{
		return HasAttribute(VBO_ATT_TEXCOORD1);
	}
	bool32 HasTexCoord2Attribute() const
	{
		return HasAttribute(VBO_ATT_TEXCOORD2);
	}
	bool32 HasNormalAttribute() const
	{
		return HasAttribute(VBO_ATT_NORMAL);
	}
	
	bool32 AttachShaderPart(GLenum type, const char* code)
	{
		if (this->IsCompiled())
		{
			Log("The shader is already compiled. New parts cannot be linked.");
			return false;
		}
		if (this->HasPart(type))
		{
			Log("That shader part is already attached.");
			return false;
		}
		
		GLuint* part = nullptr;
		if (type == VERTEX_SHADER) part = &this->vShader;
		if (type == GEOMETRY_SHADER) part = &this->gShader;
		if (type == FRAGMENT_SHADER) part = &this->fShader;
		if (part == nullptr)
		{
			Log("Invalid type shader type passed: ", str(type), ".");
			return false;
		}
		
		*part = glCreateShader(type);
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while creating shader part.");
			*part = 0;
			return false;
		}
		
		glShaderSource(*part, 1, (const GLchar* const*)&code, NULL);
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while setting shader part source.");
			glDeleteShader(*part);
			*part = 0;
			return false;
		}
		
		glCompileShader(*part);
		
		char shaderLog[1028] = {};
		GLsizei logSize = 0;
		glGetShaderInfoLog(*part, ArrayCount(shaderLog), &logSize, shaderLog);
		if (logSize > 0)
		{
			Log("Compiled shader log output: \"", string(shaderLog), "\"");
		}
		
		GLint compileStatus;
		glGetShaderiv(*part, GL_COMPILE_STATUS, &compileStatus);
		if (!compileStatus)
		{
			Log("The shader part was not compiled correctly: \"", str(compileStatus), "\"");
			glDeleteShader(*part);
			*part = 0;
			return false;
		}
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while compiling the shader part.");
			glDeleteShader(*part);
			*part = 0;
			return false;
		}
		
		return true;
	}
	bool32 AttachVertexShader(const char* code)
	{
		return AttachShaderPart(VERTEX_SHADER, code);
	}
	bool32 AttachGeometryShader(const char* code)
	{
		return AttachShaderPart(GEOMETRY_SHADER, code);
	}
	bool32 AttachFragmentShader(const char* code)
	{
		return AttachShaderPart(FRAGMENT_SHADER, code);
	}
	
	bool32 Compile()
	{
		if (this->IsCompiled())
		{
			Log("The shader is already linked.");
			return false;
		}
		if (this->vShader == 0 || this->fShader == 0)
		{
			Log("You must attach both vertex and fragment shader parts before linking.");
			return false;
		}
		
		this->id = glCreateProgram();
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while creating shader program.");
			return false;
		}
		
		if (this->HasPart(VERTEX_SHADER))
		{
			glAttachShader(this->id, this->vShader);
			if (!CheckOpenGLErrors())
			{
				Log("OpenGL error occurred while linking vertex shader.");
				glDeleteProgram(this->id);
				this->id = 0;
				return false;
			}
		}
		if (this->HasPart(FRAGMENT_SHADER))
		{
			glAttachShader(this->id, this->fShader);
			if (!CheckOpenGLErrors())
			{
				Log("OpenGL error occurred while linking fragment shader.");
				glDeleteProgram(this->id);
				this->id = 0;
				return false;
			}
		}
		
		glLinkProgram(this->id);
		
		GLint linkStatus;
		glGetProgramiv(this->id, GL_LINK_STATUS, &linkStatus);
		
		if (!linkStatus)
		{
			Log("OpenGL error occurred while linking shader program.");
			glDeleteProgram(this->id);
			this->id = 0;
			return false;
		}
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while compiling shader program.");
			glDeleteProgram(this->id);
			this->id = 0;
			return false;
		}
		
		//in order to tell if the attribute actually doesn't exist
		//find the attribute and uniform locations that we normally use
		this->positionAttribute = 	glGetAttribLocation(this->id, "vPosition");
		if (this->positionAttribute == -1) Log("Compiled shader has no \"vPosition\"");
		this->colorAttribute = 		glGetAttribLocation(this->id, "vColor");
		if (this->colorAttribute == -1) Log("Compiled shader has no \"vColor\"");
		this->texCoord1Attribute = 	glGetAttribLocation(this->id, "vTexCoord1");
		if (this->texCoord1Attribute == -1) Log("Compiled shader has no \"vTexCoord1\"");
		this->texCoord2Attribute = 	glGetAttribLocation(this->id, "vTexCoord2");
		if (this->texCoord2Attribute == -1) Log("Compiled shader has no \"vTexCoord2\"");
		this->normalAttribute = 	glGetAttribLocation(this->id, "vNormal");
		if (this->normalAttribute == -1) Log("Compiled shader has no \"vNormal\"");
		
		return true;
	}
	
	//destroys the compiles shader on OpenGL end and
	//deletes all the shader parts
	void Destroy()
	{
		if (this->IsCompiled()) glDeleteProgram(this->id);
		if (this->HasPart(VERTEX_SHADER)) 	glDeleteShader(this->vShader);
		if (this->HasPart(GEOMETRY_SHADER)) glDeleteShader(this->gShader);
		if (this->HasPart(FRAGMENT_SHADER)) glDeleteShader(this->fShader);
		
		this->id = 0;
		this->vShader = 0;
		this->gShader = 0;
		this->fShader = 0;
	}
	
	bool32 Use()
	{
		if (!this->IsCompiled())
		{
			Log("The shader has not been compiled before trying to Use().");
			return false;
		}
		
		glUseProgram(this->id);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to Use() shader.");
			glUseProgram(0);
			return false;
		}
		
		return true;
	}
	
	bool32 BindView(const View& view)
	{
		mat4 viewMatrix = view.GetViewMatrix();
		mat4 projectionMatrix = view.GetProjectionMatrix();
		
		if (!this->SetMatrix4("viewMatrix", viewMatrix))
		{
			Log("We were unable to find the viewMatrix in the shader.");
			return false;
		}
		if (!this->SetMatrix4("projectionMatrix", projectionMatrix))
		{
			Log("We were unable to find the projectionMatrix in the shader.");
			return false;
		}
		if (!this->SetVec3("viewPos", view.GetPosition()))
		{
			// Log("We were unable to find the viewPos in the shader.");
			return false;
		}
		
		return true;
	}
	
	bool32 BindBuffer(const VBO& buffer)
	{
		if (!this->IsCompiled())
		{
			Log("The shader has not been compiled before binding a buffer.");
			return false;
		}
		if (!buffer.IsBufferCreated())
		{
			Log("We cannot bind the buffer to the shader because it is not fully created.");
			return false;
		}
		
		//ensure the VBO has all the required attributes for the shader to work
		if (  this->HasAttribute(VBO_ATT_POSITION) && 
			!buffer.HasAttribute(VBO_ATT_POSITION))
		{
			Log("The shader requires a position attribute which the VBO does not provide.");
			return false;
		}
		if (  this->HasAttribute(VBO_ATT_COLOR) && 
			!buffer.HasAttribute(VBO_ATT_COLOR))
		{
			Log("The shader requires a color attribute which the VBO does not provide.");
			return false;
		}
		if (  this->HasAttribute(VBO_ATT_TEXCOORD1) && 
			!buffer.HasAttribute(VBO_ATT_TEXCOORD1))
		{
			Log("The shader requires a texCoord attribute which the VBO does not provide.");
			return false;
		}
		if (  this->HasAttribute(VBO_ATT_TEXCOORD2) && 
			!buffer.HasAttribute(VBO_ATT_TEXCOORD2))
		{
			Log("The shader requires a second texCoord attribute which the VBO does not provide.");
			return false;
		}
		if (  this->HasAttribute(VBO_ATT_NORMAL) && 
			!buffer.HasAttribute(VBO_ATT_NORMAL))
		{
			Log("The shader requires a normal attribute which the VBO does not provide.");
			return false;
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, buffer.id);
		
		//create the VAO if it doesn't already match the buffer settings
		if (this->vao == 0 || this->vaoMode != buffer.GetMode())
		{
			if (this->vao != 0)
			{
				glBindVertexArray(0);
				glDeleteVertexArrays(1, &this->vao);
				this->vao = 0;
			}
			this->vaoMode = buffer.GetMode();
			
			glGenVertexArrays(1, &this->vao);
			if (!CheckOpenGLErrors())
			{
				Log("OpenGL error occurred while trying to create VAO for buffer.");
				return false;
			}
			
			glBindVertexArray(this->vao);
			if (!CheckOpenGLErrors())
			{
				Log("OpenGL error occurred while trying to bind new VAO for buffer.");
				glDeleteVertexArrays(1, &this->vao);
				return false;
			}
			
			if (this->HasPositionAttribute())
			{
				glVertexAttribPointer(this->positionAttribute, 3, GL_FLOAT, GL_FALSE, 
					buffer.GetVertexSize(), 
					(const void*)buffer.GetPositionOffset());
				glEnableVertexAttribArray(this->positionAttribute);
				if (!CheckOpenGLErrors())
				{
					Log("OpenGL error occurred while trying to set the position attribute on the VAO.");
					glDeleteVertexArrays(1, &this->vao);
					return false;
				}
			}
			if (this->HasColorAttribute())
			{
				glVertexAttribPointer(this->colorAttribute, 4, GL_FLOAT, GL_FALSE, 
					buffer.GetVertexSize(), 
					(const void*)buffer.GetColorOffset());
				glEnableVertexAttribArray(this->colorAttribute);
				if (!CheckOpenGLErrors())
				{
					Log("OpenGL error occurred while trying to set the color attribute on the VAO.");
					glDeleteVertexArrays(1, &this->vao);
					return false;
				}
			}
			if (this->HasTexCoord1Attribute())
			{
				glVertexAttribPointer(this->texCoord1Attribute, 2, GL_FLOAT, GL_FALSE, 
					buffer.GetVertexSize(), 
					(const void*)buffer.GetTexCoord1Offset());
				glEnableVertexAttribArray(this->texCoord1Attribute);
				if (!CheckOpenGLErrors())
				{
					Log("OpenGL error occurred while trying to set the texCoord1 attribute on the VAO.");
					glDeleteVertexArrays(1, &this->vao);
					return false;
				}
			}
			if (this->HasTexCoord2Attribute())
			{
				glVertexAttribPointer(this->texCoord2Attribute, 2, GL_FLOAT, GL_FALSE, 
					buffer.GetVertexSize(), 
					(const void*)buffer.GetTexCoord2Offset());
				glEnableVertexAttribArray(this->texCoord2Attribute);
				if (!CheckOpenGLErrors())
				{
					Log("OpenGL error occurred while trying to set the texCoord2 attribute on the VAO.");
					glDeleteVertexArrays(1, &this->vao);
					return false;
				}
			}
			if (this->HasNormalAttribute())
			{
				glVertexAttribPointer(this->normalAttribute, 3, GL_FLOAT, GL_FALSE, 
					buffer.GetVertexSize(), 
					(const void*)buffer.GetNormalOffset());
				glEnableVertexAttribArray(this->normalAttribute);
				if (!CheckOpenGLErrors())
				{
					Log("OpenGL error occurred while trying to set the normal attribute on the VAO.");
					glDeleteVertexArrays(1, &this->vao);
					return false;
				}
			}
		}

		glBindVertexArray(this->vao);
		
		return true;
	}
	
	bool32 SetColor(const char* uniformName, const vec4& color)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find color uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniform4f(location, color.x, color.y, color.z, color.w);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a color on the shader.");
			return false;
		}
		
		return true;
	}
	bool32 SetFloat(const char* uniformName, float value)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find vec2 uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniform1f(location, value);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a vec2 on the shader.");
			return false;
		}
		
		return true;
	}
	bool32 SetVec2(const char* uniformName, const vec2& vec)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find vec2 uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniform2f(location, vec.x, vec.y);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a vec2 on the shader.");
			return false;
		}
		
		return true;
	}
	bool32 SetVec3(const char* uniformName, const vec3& vec)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find vec3 uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniform3f(location, vec.x, vec.y, vec.z);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a vec3 on the shader.");
			return false;
		}
		
		return true;
	}
	bool32 SetVec4(const char* uniformName, const vec4& vec)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find vec4 uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a vec4 on the shader.");
			return false;
		}
		
		return true;
	}
	bool32 SetMatrix4(char* uniformName, const mat4& value)
	{
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find metrix uniform named \"", uniformName, "\".");
			return false;
		}
		
		glUniformMatrix4fv(location, 1, GL_FALSE, &value[0][0]);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a matrix4 on the shader named: \"", uniformName, "\"");
			return false;
		}
		
		return true;
	}
	bool32 SetTexture2D(char* uniformName, const Texture2D& texture, int active_index = 0)
	{
		if (!texture.IsValid())
		{
			Log("The texture was not valid and cannot be passed to the shader.");
			return false;
		}
		
		GLint location = glGetUniformLocation(this->id, uniformName);
		if (location == -1)
		{
			// Log("We could not find texture unfiorm named \"", uniformName, "\".");
			return false;
		}
		
		glUniform1i(location, active_index);
		glActiveTexture(GL_TEXTURE0 + active_index);
		glBindTexture(GL_TEXTURE_2D, texture.id);
		
		if (!CheckOpenGLErrors())
		{
			Log("OpenGL error occurred while trying to set a texture on the shader.");
			return false;
		}
		
		return true;
	}
};

#endif