#ifndef _OPENGL_RENDERING_H
#define _OPENGL_RENDERING_H

#include "myMath.h"
#include "opengl_Vertex.h"
#include "opengl_VertexBuffer.h"

struct RenderState_t
{
	VertexBuffer_t* boundVertexBuffer;
	IndexBuffer_t* boundIndexBuffer;
	Texture_t* boundTexture;
	Effect_t* boundEffect;
	
	Matrix4_t worldMatrix;
	Matrix4_t viewMatrix;
	Matrix4_t projectionMatrix;
	
};

//NOTE: We will automatically handle looking up "DiffuseTexture" in the boundEffect
void _BindTexture(RenderState_t* renderState, Texture_t* texture)
{
	Assert(renderState != nullptr);
	Assert(texture != nullptr);
	
	
	
	renderState->boundTexture = texture;
}

//NOTE: We will automatically handle looking up "WorldMatrix" in the boundEffect
void _SetWorldMatrix(RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(renderState != nullptr);
	
	
	
	renderState->worldMatrix = newMatrix;
}
//NOTE: We will automatically handle looking up "ViewMatrix" in the boundEffect
void _SetViewMatrix(RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(renderState != nullptr);
	
	
	
	renderState->viewMatrix = newMatrix;
}
//NOTE: We will automatically handle looking up "ProjectionMatrix" in the boundEffect
void _SetProjectionMatrix(RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(renderState != nullptr);
	
	
	
	renderState->projectionMatrix = newMatrix;
}

void _BindVertexBuffer(RenderState_t* renderState, VertexBuffer_t* buffer)
{
	Assert(renderState != nullptr);
	Assert(buffer != nullptr);
	
	glBindBuffer(GL_ARRAY_BUFFER, buffer->id);
	
	renderState->boundVertexBuffer = buffer;
}

void _BindIndexBuffer(RenderState_t* renderState, IndexBuffer_t* buffer)
{
	Assert(renderState != nullptr);
	Assert(buffer != nullptr);
	
	
	
	renderState->boundIndexBuffer = buffer;
}

void _BindEffect(RenderState_t* renderState, Effect_t* effect)
{
	Assert(renderState != nullptr);
	
	renderState->boundEffect = effect;
}

uint32 _BeginEffect(RenderState_t* renderState)
{
	Assert(renderState != nullptr);
	
	if (renderState->boundEffect != nullptr)
	{
		glUseProgram(renderState->boundEffect->id);
	}
	else
	{
		glUseProgram(0);
	}
	
	return 0;
}

void _EndEffect(RenderState_t* renderState)
{
	Assert(renderState != nullptr);
	
	if (renderState->boundEffect != nullptr)
	{
		glUseProgram(0);
	}
	else
	{
		//Don't need to do anything if we aren't using an effect
	}
}

void _RenderBoundBuffer(RenderState_t* renderState, GLuint primitiveType, uint32 vertexCount)
{
	Assert(renderState != nullptr);
	Assert(renderState->boundVertexBuffer != nullptr);
	
	glBindVertexArray(1);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, renderState->boundVertexBuffer->id);
	glVertexAttribPointer(
		0,                    //attribute 0
		3,                    //num values
		GL_FLOAT,             //type
		GL_FALSE,             //normalized?
		sizeof(Vertex_t),     //stride
		(void*)0              //array buffer offset
		);
	
	glDrawArrays(primitiveType, 0, vertexCount);
	
	glDisableVertexAttribArray(0);
}

//TODO: Add _RenderBoundVertexBufferIndexed function


#endif // _OPENGL_RENDERING_H