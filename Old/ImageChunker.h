
struct ImageChunkInfo
{
	uint32 x;
	uint32 y;
	uint32 width;
	uint32 height;
	uint64 numPixels;
};

#define IMAGE_CHUNKER_ALPHA_BIT			0x01
#define IMAGE_CHUNKER_CHECK_BIT			0x02
#define IMAGE_CHUNKER_MARKD_BIT			0x04
#define IMAGE_CHUNKER_TOCHK_BIT			0x08

using std::cout;
using std::endl;

class ImageChunker
{
private:
	//1: On first pass over image data we set bit 1 to represent alpha > or < tolerance.
	//2: On 2nd - nth pass we find the next pixel not marked on bit 2 and set it as our
	//		our start point for the next part. We set bit 3 on that pixel as well as all
	//		connecting pixels we find. We use bit 2 to indicate which one's we've checked
	//2.5: After each pass here we let the texture creator make a pass to copy all bit 3
	//		marked pixel locations into a texture. We thenalso make a pass to unset 
	//		bit 3 on all.
	//3: Once we don't find a starting pixel without bit 2 set we end our search for parts.
	
	//pointer to the start of an array of unsigned integers
	//one for each pixel in the image that was passed to us.
	uint8* flagArray;
	
	//our position to start check from when looking for the
	//next part's first pixel.
	uint32 currentY;
	
	//the size of the whole image ()
	uint32 width;
	uint32 height;
	
	inline bool32 AlphaBit(const uint8 flags)
	{
		return ((flags & IMAGE_CHUNKER_ALPHA_BIT) > 0);
	}
	inline bool32 CheckedBit(const uint8 flags)
	{
		return ((flags & IMAGE_CHUNKER_CHECK_BIT) > 0);
	}
	inline bool32 MarkedBit(const uint8 flags)
	{
		return ((flags & IMAGE_CHUNKER_MARKD_BIT) > 0);
	}
	inline bool32 ToCheckBit(const uint8 flags)
	{
		return ((flags & IMAGE_CHUNKER_TOCHK_BIT) > 0);
	}
	
	inline uint8 SetAlphaBit(const uint8 inValue)
	{
		return (inValue | IMAGE_CHUNKER_ALPHA_BIT);
	}
	inline uint8 SetCheckedBit(const uint8 inValue)
	{
		return (inValue | IMAGE_CHUNKER_CHECK_BIT);
	}
	inline uint8 SetMarkedBit(const uint8 inValue)
	{
		return (inValue | IMAGE_CHUNKER_MARKD_BIT);
	}
	inline uint8 SetToCheckBit(const uint8 inValue)
	{
		return (inValue | IMAGE_CHUNKER_TOCHK_BIT);
	}
	
	inline uint8 UnsetMarkedBit(const uint8 inValue)
	{
		return (inValue & (~IMAGE_CHUNKER_MARKD_BIT));
	}
	
	//returns number of pixels around this pixel that were added to check
	bool32 CheckPixel(uint32 x, uint32 y, ImageChunkInfo* toCheckArea, uint32* numPixelsToCheck)
	{
		if (AlphaBit(flagArray[x + y*width]))
		{
			flagArray[x + y*width] = SetCheckedBit(flagArray[x + y*width]);
			flagArray[x + y*width] = SetMarkedBit(flagArray[x + y*width]);
			
			uint64 checkPos = (x-1) + y*width;
			if (x > 0 
				&& AlphaBit(flagArray[checkPos]) &&
				!CheckedBit(flagArray[checkPos]) &&
				!ToCheckBit(flagArray[checkPos]))
			{
				flagArray[checkPos] = SetToCheckBit(flagArray[checkPos]);
				*numPixelsToCheck = *numPixelsToCheck + 1;
				if (toCheckArea->x > (x-1))
				{
					//expand the toCheckArea left a pixel
					toCheckArea->width += toCheckArea->x - (x-1);
					toCheckArea->x = (x-1);
				}
			}
			
			checkPos = (x+1) + y*width;
			if (x < width-1
				&& AlphaBit(flagArray[checkPos]) &&
				!CheckedBit(flagArray[checkPos]) &&
				!ToCheckBit(flagArray[checkPos]))
			{
				flagArray[checkPos] = SetToCheckBit(flagArray[checkPos]);
				*numPixelsToCheck = *numPixelsToCheck + 1;
				if (toCheckArea->x+toCheckArea->width <= (x+1))
				{
					//expand the toCheckArea right a pixel
					toCheckArea->width = (x+1) - toCheckArea->x + 1;
				}
			}
			
			checkPos = x + (y-1)*width;
			if (y > 0 
				&& AlphaBit(flagArray[checkPos]) &&
				!CheckedBit(flagArray[checkPos]) &&
				!ToCheckBit(flagArray[checkPos]))
			{
				flagArray[checkPos] = SetToCheckBit(flagArray[checkPos]);
				*numPixelsToCheck = *numPixelsToCheck + 1;
				if (toCheckArea->y > (y-1))
				{
					//expand the toCheckArea left a pixel
					toCheckArea->height += toCheckArea->y - (y-1);
					toCheckArea->y = (y-1);
				}
			}
			
			checkPos = x + (y+1)*width;
			if (y < height-1
				&& AlphaBit(flagArray[checkPos]) &&
				!CheckedBit(flagArray[checkPos]) &&
				!ToCheckBit(flagArray[checkPos]))
			{
				flagArray[checkPos] = SetToCheckBit(flagArray[checkPos]);
				*numPixelsToCheck = *numPixelsToCheck + 1;
				if (toCheckArea->y+toCheckArea->height <= (y+1))
				{
					//expand the toCheckArea right a pixel
					toCheckArea->height = (y+1) - toCheckArea->y + 1;
				}
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
public:
	
	ImageChunker(uint32 imageWidth, uint32 imageHeight, const uint32* pixelData,
		uint8 alphaMin = 1)
	{
		this->width = imageWidth;
		this->height = imageHeight;
		this->currentY = 0;
		this->flagArray = new uint8[imageWidth * imageHeight];
		
		for(int y = 0; y < imageHeight; y++)
		{
			for(int x = 0; x < imageWidth; x++)
			{
				uint8* pixel = (uint8*)&pixelData[x + y*imageWidth];
				uint8 alpha = pixel[3];
				
				if (alpha >= alphaMin)
				{
					//pixel can be ignored from here on out
					//cout << "(" << x << ", " << y << ") is not transparent." << endl;
					flagArray[x + y*imageWidth] = 0x01;
				}
				else
				{
					flagArray[x + y*imageWidth] = 0x00;
				}
			}
		}
	}
	~ImageChunker()
	{
		if (flagArray != nullptr)
		{
			delete[] flagArray;
		}
	}
	
	//returns whether or not the pixel at the specified location was
	//marked during the last pass as being a part of the current chunk
	bool32 IsPixelMarked(uint32 x, uint32 y)
	{
		return MarkedBit(this->flagArray[x + y*this->width]);
	}
	
	bool32 IsPixelChecked(uint32 x, uint32 y)
	{
		return CheckedBit(this->flagArray[x + y*this->width]);
	}
	
	bool32 IsPixelToBeChecked(uint32 x, uint32 y)
	{
		return ToCheckBit(this->flagArray[x + y*this->width]);
	}
	
	bool32 GetNextChunk(ImageChunkInfo* chunkInfo)
	{
		
		{
			bool32 found = false;
			uint32 foundX = 0;
			uint32 foundY = 0;
			for(int y = currentY; y < height; y++)
			{
				for(int x = 0; x < width; x++)
				{
					if (AlphaBit(flagArray[x + y*width]) &&
						!CheckedBit(flagArray[x + y*width]))
					{
						//found a non-transparent pixel that
						//hasn't been checked
						found = true;
						foundX = x;
						foundY = y;
						break;
					}
				}

				if (found) break;
			}
			
			if (!found)
			{
				//there are no more pixels that haven't been checked
				return false;
			}
			
			*chunkInfo = {};
			chunkInfo->x = foundX;
			chunkInfo->y = foundY;
			chunkInfo->width = 1;
			chunkInfo->height = 1;
			chunkInfo->numPixels = 1;
			
			currentY = foundY;
		}
		
		ImageChunkInfo toCheckArea = *chunkInfo;
		
		uint32 numPixelsToCheck = 0;
		Assert(this->CheckPixel(chunkInfo->x, chunkInfo->y, &toCheckArea, &numPixelsToCheck));

		while(numPixelsToCheck > 0)
		{
			bool32 foundNext = false;
			uint32 nextX = 0;
			uint32 nextY = 0;
			// if (chunkInfo->numPixels == 117)
			// {
			// 	Log("Break");
			// }
			//find next pixel marked "ToCheck" that hasn't been checked
			for(int y = toCheckArea.y; y <= toCheckArea.y+toCheckArea.height; y++)
			{
				for(int x = toCheckArea.x; x <= toCheckArea.x+toCheckArea.width; x++)
				{
					// if (x == toCheckArea.x+toCheckArea.height)
					// {
					// 	Log("Break");
					// }
					if (ToCheckBit(flagArray[x + y*width]) &&
						!CheckedBit(flagArray[x + y*width]))
					{
						foundNext = true;
						nextX = x;
						nextY = y;
						break;
					}
				}

				if (foundNext) break;
			}
			
			if (!foundNext)
			{
				Log("We didn't find any more pixels to check yet our counter was greater than zero.");
				break;
			}
			
			numPixelsToCheck--;//decrement since we are checking a pixel
			if (CheckPixel(nextX, nextY, &toCheckArea, &numPixelsToCheck))
			{
				chunkInfo->numPixels++;
				
				//expand the chunk if needed
				if (chunkInfo->x > nextX)
				{
					chunkInfo->width += chunkInfo->x - nextX;
					chunkInfo->x = nextX;
					// cout << "expanded chunk left: (" <<
					// 	chunkInfo->x << ", " << chunkInfo->y << ", " << 
					// 	chunkInfo->width << ", " << chunkInfo->height << ")" << endl;
				}
				if (chunkInfo->x+chunkInfo->width <= nextX)
				{
					chunkInfo->width = nextX - chunkInfo->x + 1;
					// cout << "expanded chunk right: (" <<
					// 	chunkInfo->x << ", " << chunkInfo->y << ", " << 
					// 	chunkInfo->width << ", " << chunkInfo->height << ")" << endl;
				}
				if (chunkInfo->y > nextY)
				{
					chunkInfo->height += chunkInfo->y - nextY;
					chunkInfo->y = nextY;
					// cout << "expanded chunk up: (" <<
					// 	chunkInfo->x << ", " << chunkInfo->y << ", " << 
					// 	chunkInfo->width << ", " << chunkInfo->height << ")" << endl;
				}
				if (chunkInfo->y+chunkInfo->height <= nextY)
				{
					chunkInfo->height = nextY - chunkInfo->y + 1;
					// cout << "expanded chunk down: (" <<
					// 	chunkInfo->x << ", " << chunkInfo->y << ", " << 
					// 	chunkInfo->width << ", " << chunkInfo->height << ")" << endl;
				}
			}
			
			// cout << "Checked (" << nextX << ", " << nextY << ") - " <<
			// 	numPixelsToCheck << " pixels to check" << endl;
		}
		
		return true;
	}
	
	void ClearChunk()
	{
		for(int y = 0; y < this->height; y++)
		{
			for(int x = 0; x < this->width; x++)
			{
				if (MarkedBit(this->flagArray[x + y*this->width]))
				{
					this->flagArray[x + y*this->width] = UnsetMarkedBit(this->flagArray[x + y*this->width]);
				}
			}
		}
	}
};

class TextureAtlus
{
private:
	uint32 numTextures;
	vec2* texturePositions;
	Texture2D* textures;
	
public:
	
	TextureAtlus()
	{
		this->numTextures = 0;
		this->textures = nullptr;
		this->texturePositions = nullptr;
	}
	TextureAtlus(const string& filepath, uint8 alphaMin = 1)
	{
		this->numTextures = 0;
		this->textures = nullptr;
		this->texturePositions = nullptr;
		
		this->LoadChunks(filepath, alphaMin);
	}
	TextureAtlus(const TextureAtlus& other)
	{
		this->numTextures = other.numTextures;
		this->textures = new Texture2D[other.numTextures];
		this->texturePositions = new vec2[other.numTextures];
		for(int i = 0; i < other.numTextures; i++)
		{
			this->textures[i] = other.textures[i];
			this->texturePositions[i] = other.texturePositions[i];
		}
	}
	~TextureAtlus()
	{
		if (this->textures != nullptr) free(this->textures);
		if (this->texturePositions != nullptr) free(this->texturePositions);
	}
	
	void LoadChunks(const string& filepath, uint8 alphaMin = 1)
	{
		uint32* imageData;
		unsigned width, height;
		{
			unsigned error;

			error = lodepng_decode32_file((byte**)&imageData, &width, &height, filepath.c_str());
			
			if(error)
			{
				Log("LodePNG error occurred while loading file \"" + filepath + "\"");
				return;
			}
		}
		
		ImageChunker* chunker = new ImageChunker(width, height, imageData, alphaMin);
		
		ImageChunkInfo chunk = {};
		while(chunker->GetNextChunk(&chunk))
		{
			uint32* chunkData = (uint32*)malloc(sizeof(uint32) * chunk.width * chunk.height);
			
			for(uint32 y = 0; y < chunk.height; y++)
			{
				for(uint32 x = 0; x < chunk.width; x++)
				{
					if (chunker->IsPixelMarked(chunk.x + x, chunk.y + y))
					{
						uint32* pixel = &imageData[(chunk.x+x) + (chunk.y+y)*width];
						chunkData[x + y*chunk.width] = *pixel;
					}
					else
					{
						chunkData[x + y*chunk.width] = 0x00000000;
					}
				}
			}
			
			this->textures = (Texture2D*)realloc(this->textures, sizeof(Texture2D) * (this->numTextures+1));
			memset(&this->textures[this->numTextures], '\0', sizeof(Texture2D));
			this->texturePositions = (vec2*)realloc(this->texturePositions, sizeof(vec2) * (this->numTextures+1));
			memset(&this->texturePositions[this->numTextures], '\0', sizeof(vec2));

			this->textures[this->numTextures] = Texture2D(chunk.width, chunk.height, chunkData);
			this->texturePositions[this->numTextures] = vec2(chunk.x, chunk.y);
			this->numTextures++;
			
			free(chunkData);
			chunk = {};
			chunker->ClearChunk();
		}
		
		delete chunker;
		free(imageData);
	}
	
	void Unload()
	{
		for(int i = 0; i < this->numTextures; i++)
		{
			if (this->textures[i].IsValid())
			{
				this->textures[i].Unload();
			}
		}
		if (this->textures != nullptr) free(this->textures);
		if (this->texturePositions != nullptr) free(this->texturePositions);
		this->textures = nullptr;
		this->numTextures = 0;
	}
	
	uint32 GetNumTextures() const
	{
		return this->numTextures;
	}
	Texture2D GetTexture(uint32 index) const
	{
		return this->textures[index];
	}
	vec2 GetTexturePosition(uint32 index) const
	{
		return this->texturePositions[index];
	}
};