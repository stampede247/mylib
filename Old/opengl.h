#include "mylib.h"

#if !defined(MYLIB_OPENGL_H)
#define MYLIB_OPENGL_H

bool CheckOpenGLErrors()
{
	GLenum glError = glGetError();
	if (glError != GL_NO_ERROR)
	{
		std::cout << "OpenGL error occurred: ";
		switch(glError)
		{
			case GL_INVALID_ENUM:
				std::cout << "GL_INVALID_ENUM";
				// Assert(false);
				break;
			case GL_INVALID_VALUE:
				std::cout << "GL_INVALID_VALUE";
				// Assert(false);
				break;
			case GL_INVALID_OPERATION: //1282
				std::cout << "GL_INVALID_OPERATION";
				// Assert(false);
				break;
			case GL_STACK_OVERFLOW:
				std::cout << "GL_STACK_OVERFLOW";
				// Assert(false);
				break;
			case GL_STACK_UNDERFLOW:
				std::cout << "GL_STACK_UNDERFLOW";
				// Assert(false);
				break;
			case GL_OUT_OF_MEMORY:
				std::cout << "GL_OUT_OF_MEMORY";
				// Assert(false);
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION";
				// Assert(false);
				break;
			case GL_CONTEXT_LOST:
				std::cout << "GL_CONTEXT_LOST";
				// Assert(false);
				break;
			case GL_TABLE_TOO_LARGE:
				std::cout << "GL_TABLE_TOO_LARGE";
				// Assert(false);
				break;
			default:
				std::cout << "UNKNOWN";
				// Assert(false);
				break;
		}
		std::cout << std::endl;
		return false;
	}
	else
	{
		return true;
	}
}

char* GetGLFWKeyName(int glfwKey)
{
	switch (glfwKey)
	{
		case GLFW_KEY_UNKNOWN: 			return "GLFW_KEY_UNKNOWN";
		case GLFW_KEY_SPACE: 			return "GLFW_KEY_SPACE";
		case GLFW_KEY_APOSTROPHE: 		return "GLFW_KEY_APOSTROPHE";
		case GLFW_KEY_COMMA: 			return "GLFW_KEY_COMMA";
		case GLFW_KEY_MINUS: 			return "GLFW_KEY_MINUS";
		case GLFW_KEY_PERIOD: 			return "GLFW_KEY_PERIOD";
		case GLFW_KEY_SLASH: 			return "GLFW_KEY_SLASH";
		case GLFW_KEY_0: 				return "GLFW_KEY_0";
		case GLFW_KEY_1: 				return "GLFW_KEY_1";
		case GLFW_KEY_2: 				return "GLFW_KEY_2";
		case GLFW_KEY_3: 				return "GLFW_KEY_3";
		case GLFW_KEY_4: 				return "GLFW_KEY_4";
		case GLFW_KEY_5: 				return "GLFW_KEY_5";
		case GLFW_KEY_6: 				return "GLFW_KEY_6";
		case GLFW_KEY_7: 				return "GLFW_KEY_7";
		case GLFW_KEY_8: 				return "GLFW_KEY_8";
		case GLFW_KEY_9: 				return "GLFW_KEY_9";
		case GLFW_KEY_SEMICOLON: 		return "GLFW_KEY_SEMICOLON";
		case GLFW_KEY_EQUAL: 			return "GLFW_KEY_EQUAL";
		case GLFW_KEY_A: 				return "GLFW_KEY_A";
		case GLFW_KEY_B: 				return "GLFW_KEY_B";
		case GLFW_KEY_C: 				return "GLFW_KEY_C";
		case GLFW_KEY_D: 				return "GLFW_KEY_D";
		case GLFW_KEY_E: 				return "GLFW_KEY_E";
		case GLFW_KEY_F: 				return "GLFW_KEY_F";
		case GLFW_KEY_G: 				return "GLFW_KEY_G";
		case GLFW_KEY_H: 				return "GLFW_KEY_H";
		case GLFW_KEY_I: 				return "GLFW_KEY_I";
		case GLFW_KEY_J: 				return "GLFW_KEY_J";
		case GLFW_KEY_K: 				return "GLFW_KEY_K";
		case GLFW_KEY_L: 				return "GLFW_KEY_L";
		case GLFW_KEY_M: 				return "GLFW_KEY_M";
		case GLFW_KEY_N: 				return "GLFW_KEY_N";
		case GLFW_KEY_O: 				return "GLFW_KEY_O";
		case GLFW_KEY_P: 				return "GLFW_KEY_P";
		case GLFW_KEY_Q: 				return "GLFW_KEY_Q";
		case GLFW_KEY_R: 				return "GLFW_KEY_R";
		case GLFW_KEY_S: 				return "GLFW_KEY_S";
		case GLFW_KEY_T: 				return "GLFW_KEY_T";
		case GLFW_KEY_U: 				return "GLFW_KEY_U";
		case GLFW_KEY_V: 				return "GLFW_KEY_V";
		case GLFW_KEY_W: 				return "GLFW_KEY_W";
		case GLFW_KEY_X: 				return "GLFW_KEY_X";
		case GLFW_KEY_Y: 				return "GLFW_KEY_Y";
		case GLFW_KEY_Z: 				return "GLFW_KEY_Z";
		case GLFW_KEY_LEFT_BRACKET: 	return "GLFW_KEY_LEFT_BRACKET";
		case GLFW_KEY_BACKSLASH: 		return "GLFW_KEY_BACKSLASH";
		case GLFW_KEY_RIGHT_BRACKET:	return "GLFW_KEY_RIGHT_BRACKET";
		case GLFW_KEY_GRAVE_ACCENT: 	return "GLFW_KEY_GRAVE_ACCENT";
		case GLFW_KEY_WORLD_1: 			return "GLFW_KEY_WORLD_1";
		case GLFW_KEY_WORLD_2: 			return "GLFW_KEY_WORLD_2";
		case GLFW_KEY_ESCAPE: 			return "GLFW_KEY_ESCAPE";
		case GLFW_KEY_ENTER: 			return "GLFW_KEY_ENTER";
		case GLFW_KEY_TAB: 				return "GLFW_KEY_TAB";
		case GLFW_KEY_BACKSPACE: 		return "GLFW_KEY_BACKSPACE";
		case GLFW_KEY_INSERT: 			return "GLFW_KEY_INSERT";
		case GLFW_KEY_DELETE: 			return "GLFW_KEY_DELETE";
		case GLFW_KEY_RIGHT: 			return "GLFW_KEY_RIGHT";
		case GLFW_KEY_LEFT: 			return "GLFW_KEY_LEFT";
		case GLFW_KEY_DOWN: 			return "GLFW_KEY_DOWN";
		case GLFW_KEY_UP: 				return "GLFW_KEY_UP";
		case GLFW_KEY_PAGE_UP: 			return "GLFW_KEY_PAGE_UP";
		case GLFW_KEY_PAGE_DOWN: 		return "GLFW_KEY_PAGE_DOWN";
		case GLFW_KEY_HOME: 			return "GLFW_KEY_HOME";
		case GLFW_KEY_END: 				return "GLFW_KEY_END";
		case GLFW_KEY_CAPS_LOCK: 		return "GLFW_KEY_CAPS_LOCK";
		case GLFW_KEY_SCROLL_LOCK: 		return "GLFW_KEY_SCROLL_LOCK";
		case GLFW_KEY_NUM_LOCK: 		return "GLFW_KEY_NUM_LOCK";
		case GLFW_KEY_PRINT_SCREEN: 	return "GLFW_KEY_PRINT_SCREEN";
		case GLFW_KEY_PAUSE: 			return "GLFW_KEY_PAUSE";
		case GLFW_KEY_F1: 				return "GLFW_KEY_F1";
		case GLFW_KEY_F2: 				return "GLFW_KEY_F2";
		case GLFW_KEY_F3: 				return "GLFW_KEY_F3";
		case GLFW_KEY_F4: 				return "GLFW_KEY_F4";
		case GLFW_KEY_F5: 				return "GLFW_KEY_F5";
		case GLFW_KEY_F6: 				return "GLFW_KEY_F6";
		case GLFW_KEY_F7: 				return "GLFW_KEY_F7";
		case GLFW_KEY_F8: 				return "GLFW_KEY_F8";
		case GLFW_KEY_F9: 				return "GLFW_KEY_F9";
		case GLFW_KEY_F10: 				return "GLFW_KEY_F10";
		case GLFW_KEY_F11: 				return "GLFW_KEY_F11";
		case GLFW_KEY_F12: 				return "GLFW_KEY_F12";
		case GLFW_KEY_F13: 				return "GLFW_KEY_F13";
		case GLFW_KEY_F14: 				return "GLFW_KEY_F14";
		case GLFW_KEY_F15: 				return "GLFW_KEY_F15";
		case GLFW_KEY_F16: 				return "GLFW_KEY_F16";
		case GLFW_KEY_F17: 				return "GLFW_KEY_F17";
		case GLFW_KEY_F18: 				return "GLFW_KEY_F18";
		case GLFW_KEY_F19: 				return "GLFW_KEY_F19";
		case GLFW_KEY_F20: 				return "GLFW_KEY_F20";
		case GLFW_KEY_F21: 				return "GLFW_KEY_F21";
		case GLFW_KEY_F22: 				return "GLFW_KEY_F22";
		case GLFW_KEY_F23: 				return "GLFW_KEY_F23";
		case GLFW_KEY_F24: 				return "GLFW_KEY_F24";
		case GLFW_KEY_F25: 				return "GLFW_KEY_F25";
		case GLFW_KEY_KP_0: 			return "GLFW_KEY_KP_0";
		case GLFW_KEY_KP_1: 			return "GLFW_KEY_KP_1";
		case GLFW_KEY_KP_2: 			return "GLFW_KEY_KP_2";
		case GLFW_KEY_KP_3: 			return "GLFW_KEY_KP_3";
		case GLFW_KEY_KP_4: 			return "GLFW_KEY_KP_4";
		case GLFW_KEY_KP_5: 			return "GLFW_KEY_KP_5";
		case GLFW_KEY_KP_6: 			return "GLFW_KEY_KP_6";
		case GLFW_KEY_KP_7: 			return "GLFW_KEY_KP_7";
		case GLFW_KEY_KP_8: 			return "GLFW_KEY_KP_8";
		case GLFW_KEY_KP_9: 			return "GLFW_KEY_KP_9";
		case GLFW_KEY_KP_DECIMAL: 		return "GLFW_KEY_KP_DECIMAL";
		case GLFW_KEY_KP_DIVIDE: 		return "GLFW_KEY_KP_DIVIDE";
		case GLFW_KEY_KP_MULTIPLY: 		return "GLFW_KEY_KP_MULTIPLY";
		case GLFW_KEY_KP_SUBTRACT: 		return "GLFW_KEY_KP_SUBTRACT";
		case GLFW_KEY_KP_ADD: 			return "GLFW_KEY_KP_ADD";
		case GLFW_KEY_KP_ENTER: 		return "GLFW_KEY_KP_ENTER";
		case GLFW_KEY_KP_EQUAL: 		return "GLFW_KEY_KP_EQUAL";
		case GLFW_KEY_LEFT_SHIFT: 		return "GLFW_KEY_LEFT_SHIFT";
		case GLFW_KEY_LEFT_CONTROL: 	return "GLFW_KEY_LEFT_CONTROL";
		case GLFW_KEY_LEFT_ALT: 		return "GLFW_KEY_LEFT_ALT";
		case GLFW_KEY_LEFT_SUPER: 		return "GLFW_KEY_LEFT_SUPER";
		case GLFW_KEY_RIGHT_SHIFT: 		return "GLFW_KEY_RIGHT_SHIFT";
		case GLFW_KEY_RIGHT_CONTROL:	return "GLFW_KEY_RIGHT_CONTROL";
		case GLFW_KEY_RIGHT_ALT: 		return "GLFW_KEY_RIGHT_ALT";
		case GLFW_KEY_RIGHT_SUPER: 		return "GLFW_KEY_RIGHT_SUPER";
		case GLFW_KEY_MENU: 			return "GLFW_KEY_MENU";
		default: 						return "Uknown";
	};
}

#endif