

class Ray
{
	vec2 position;
	vec2 direction;
	
	Ray()
	{
		this->position = vec2(0.0f, 0.0f);
		this->direction = vec2(1.0f, 0.0f);
	}
	Ray(vec2 direction)//auto-normalizes direction
	{
		this->position = vec2(0.0f, 0.0f);
		this->direction = glm::normalize(direction);
	}
	Ray(vec2 position, vec2 direction)//auto-normalizes direction
	{
		this->position = position;
		this->direction = glm::normalize(direction);
	}
	Ray(const Ray& other)
	{
		this->position = other.position;
		this->direction = other.direction;
	}
	~Ray()
	{
		
	}
	
	vec2 GetTangentVec() const
	{
		
	}
	
	vec2 GetTangentPos(vec2 pos) const
	{
		
	}
	
	float DistanceToTangent(vec2 pos) const
	{
		float forward = glm::dot(pos - this->position, this->direction);
		
	}
};

class ParabolicCurve
{
private:
	
	
public:
	vec2 pStart;
	vec2 pEnd;
	vec2 pControl;
	
	ParabolicCurve()
	{
		this->pStart = vec2(0.0f, 0.0f);
		this->pEnd = vec2(0.0f, 0.0f);
		this->pControl = vec2(0.0f, 0.0f);
	}
	ParabolicCurve(vec2 pStart, vec2 pEnd, vec2 pControl)
	{
		this->pStart = pStart;
		this->pEnd = pEnd;
		this->pControl = pControl;
	}
	ParabolicCurve(const ParabolicCurve& other)
	{
		this->pStart = other.pStart;
		this->pEnd = other.pEnd;
		this->pControl = other.pControl;
	}
	~ParabolicCurve()
	{
		
	}
	
	//[0] = pStart
	//[1] = pControl
	//[2] = pEnd
	vec2 GetPoint(int index) const
	{
		switch (index)
		{
			case 0: return this->pStart;
			case 1: return this->pControl;
			case 2: return this->pEnd;
			default: return vec2(0.0f, 0.0f);
		};
	}
	
	vec2 GetPosition (double t) const
	{
		//p(t) = ((1-t)^2)P0 + 2t(1-t)P1 + (t^2)P2
		return vec2(
			(1-t)*(1-t)*this->pStart.x + 2*t*(1-t)*this->pControl.x + (t)*(t)*this->pEnd.x,
			(1-t)*(1-t)*this->pStart.y + 2*t*(1-t)*this->pControl.y + (t)*(t)*this->pEnd.y);
	}
	
	//Used for left hand containment checking 
	//we will return positive numbers if the ray
	//passes from the underside to the topside of
	//the curve and negative numbers if it does
	//the opposite. We will return 0 of the ray
	//doesn't intersect or intersects twice
	int LeftHandCheckIntersects(const Ray& ray) const
	{
		
	}
};
