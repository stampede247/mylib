#ifndef D3D9_VERTEX_BUFFFER_H
#define D3D9_VERTEX_BUFFFER_H

#include "d3d9_Vertex.h"

struct VertexBuffer_t
{
	bool32 isStatic;
	uint32 stride;
	int32 numVertices;
	
	LPDIRECT3DVERTEXBUFFER9 pntr;
};

struct IndexBuffer_t
{
	uint32 stride;
	int32 numIndices;
	
	LPDIRECT3DINDEXBUFFER9 pntr;
};

void _CreateVertexBuffer(LPDIRECT3DDEVICE9 d3dDevice, VertexBuffer_t* buffer, int32 numVertices, bool32 isStatic)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	Assert(buffer->pntr == nullptr);
	
	buffer->numVertices = numVertices;
	buffer->stride = sizeof(Vertex_t);
	
	if (isStatic)
	{
		buffer->isStatic = true;
		if (FAILED(d3dDevice->CreateVertexBuffer(
			sizeof(Vertex_t) * numVertices, 
			0, VERTEX_FORMAT, D3DPOOL_MANAGED, 
			&buffer->pntr, NULL)))
		{
			Assert(false);
			buffer->numVertices = 0;
			buffer->pntr = nullptr;
			return;
		}
	}
	else
	{
		buffer->isStatic = false;
		if (FAILED(d3dDevice->CreateVertexBuffer(
			sizeof(Vertex_t) * numVertices, 
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, VERTEX_FORMAT, D3DPOOL_DEFAULT, 
			&buffer->pntr, NULL)))
		{
			Assert(false);
			buffer->numVertices = 0;
			buffer->pntr = nullptr;
			return;
		}
	}
}

void _DeleteVertexBuffer(LPDIRECT3DDEVICE9 d3dDevice, VertexBuffer_t* buffer)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	
	if (buffer->pntr == nullptr)
	{
		//The buffer does not exist
		// Assert(false);
		return;
	}
	
	buffer->pntr->Release();
	
	buffer->pntr = nullptr;
	buffer->numVertices = 0;
}

void _FillVertexBuffer(LPDIRECT3DDEVICE9 d3dDevice, VertexBuffer_t* buffer, Vertex_t* vertices)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	
	if (buffer->pntr == nullptr)
	{
		//You need to create the buffer first
		Assert(false);
		return;
	}
	
	Assert(vertices != nullptr);
	
	if (buffer->numVertices == 0)
	{
		//If the buffer contains no vertices then there is nothing to do
		// Assert(false);
		return;
	}
	
	void* bufferMemory = nullptr;
	if (buffer->isStatic)
	{
		buffer->pntr->Lock(0, 0, &bufferMemory, 0);
	}
	else
	{
		buffer->pntr->Lock(0, 0, &bufferMemory, D3DLOCK_DISCARD);
	}
	
	Assert(bufferMemory != nullptr);
	
	memcpy(bufferMemory, vertices, sizeof(Vertex_t) * buffer->numVertices);
	
	buffer->pntr->Unlock();
}

void _CreateIndexBuffer(LPDIRECT3DDEVICE9 d3dDevice, IndexBuffer_t* buffer, int32 numIndices)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	Assert(buffer->pntr == nullptr);
	
	buffer->numIndices = numIndices;
	buffer->stride = sizeof(uint32);
	
	if (FAILED(d3dDevice->CreateIndexBuffer(
		sizeof(uint32) * numIndices, 
		0, D3DFMT_INDEX32, D3DPOOL_MANAGED, 
		&buffer->pntr, NULL)))
	{
		Assert(false);
		buffer->numIndices = 0;
		buffer->pntr = nullptr;
		return;
	}
}

void _DeleteIndexBuffer(LPDIRECT3DDEVICE9 d3dDevice, IndexBuffer_t* buffer)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	
	if (buffer->pntr == nullptr)
	{
		//The buffer does not exist
		// Assert(false);
		return;
	}
	
	buffer->pntr->Release();
	
	buffer->pntr = nullptr;
	buffer->numIndices = 0;
}

void _FillIndexBuffer(LPDIRECT3DDEVICE9 d3dDevice, IndexBuffer_t* buffer, uint32* indices)
{
	Assert(buffer != nullptr);
	Assert(d3dDevice != nullptr);
	
	if (buffer->pntr == nullptr)
	{
		//You need to create the buffer first
		Assert(false);
		return;
	}
	
	Assert(indices != nullptr);
	
	if (buffer->numIndices == 0)
	{
		//If the buffer contains no indices then there is nothing to do
		// Assert(false);
		return;
	}
	
	void* bufferMemory = nullptr;
	buffer->pntr->Lock(0, 0, &bufferMemory, 0);
	
	Assert(bufferMemory != nullptr);
	
	memcpy(bufferMemory, indices, sizeof(uint32) * buffer->numIndices);
	
	buffer->pntr->Unlock();
}

#endif // D3D9_VERTEX_BUFFFER_H
