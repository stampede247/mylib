#ifndef _D3D9_TEXTURE_H
#define _D3D9_TEXTURE_H

struct Texture_t
{
	bool32 isError;
	
	union
	{
		Vec2i_t size;
		struct
		{
			int32 width;
			int32 height;
		};
	};
	
	LPDIRECT3DTEXTURE9 pntr;
};

void _CreateTexture(LPDIRECT3DDEVICE9 d3dDevice, Texture_t* texture, int32 width, int32 height, bool32 isRenderTarget = false)
{
	Assert(d3dDevice != nullptr);
	Assert(texture != nullptr);
	Assert(texture->pntr == nullptr);
	
	if (FAILED(d3dDevice->CreateTexture(width, height, 0, 
		(isRenderTarget ? D3DUSAGE_RENDERTARGET : 0), 
		D3DFMT_A8R8G8B8, 
		(isRenderTarget ? D3DPOOL_DEFAULT : D3DPOOL_MANAGED), 
		&texture->pntr, NULL)))
	{
		Assert(false);
		texture->pntr = nullptr;
		return;
	}
	
	Assert(texture->pntr != nullptr);
	
	texture->width = width;
	texture->height = height;
}

void _DeleteTexture(LPDIRECT3DDEVICE9 d3dDevice, Texture_t* texture)
{
	Assert(d3dDevice != nullptr);
	Assert(texture != nullptr);
	
	if (texture->pntr != nullptr)
	{
		texture->pntr->Release();
		*texture = {};
	}
	else
	{
		//The texture is already deleted, nothing to do
	}
}

void _FillTexture(LPDIRECT3DDEVICE9 d3dDevice, Texture_t* texture, void* textureData)
{
	Assert(d3dDevice != nullptr);
	Assert(texture != nullptr);
	Assert(texture->pntr != nullptr);
	
	uint32 sizeInBytes = texture->width * texture->height * 4;
	
	if (sizeInBytes > 0)
	{
		Assert(textureData != nullptr);
		
		D3DLOCKED_RECT lockedRect;
		texture->pntr->LockRect(0, &lockedRect, NULL, D3DLOCK_NOSYSLOCK);
		
		memcpy(lockedRect.pBits, textureData, sizeInBytes);
		
		texture->pntr->UnlockRect(0);
	}
	else
	{
		//The texture is empty
		Assert(false);
	}
}

#endif // _D3D9_TEXTURE_H