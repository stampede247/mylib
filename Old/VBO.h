#define VBO_ATT_POSITION		(0x01)
#define VBO_ATT_COLOR			(0x02)
#define VBO_ATT_TEXCOORD 		(0x04)
#define VBO_ATT_TEXCOORD1 		(0x04)
#define VBO_ATT_TEXCOORD2		(0x08)
#define VBO_ATT_NORMAL 			(0x10)

#define VBO_MODE_UNDEFINED		(0x00)
#define VBO_MODE_PCTN 			(VBO_ATT_POSITION | VBO_ATT_COLOR | VBO_ATT_TEXCOORD | VBO_ATT_NORMAL)
#define VBO_MODE_PCT 			(VBO_ATT_POSITION | VBO_ATT_COLOR | VBO_ATT_TEXCOORD)
#define VBO_MODE_PCN 			(VBO_ATT_POSITION | VBO_ATT_COLOR | VBO_ATT_NORMAL)
#define VBO_MODE_PC 			(VBO_ATT_POSITION | VBO_ATT_COLOR)
#define VBO_MODE_PTN 			(VBO_ATT_POSITION | VBO_ATT_TEXCOORD | VBO_ATT_NORMAL)
#define VBO_MODE_PT 			(VBO_ATT_POSITION | VBO_ATT_TEXCOORD)
#define VBO_MODE_PTTN 			(VBO_ATT_POSITION | VBO_ATT_TEXCOORD1 | VBO_ATT_TEXCOORD2 | VBO_ATT_NORMAL)
#define VBO_MODE_PCTTN 			(VBO_ATT_POSITION | VBO_ATT_COLOR | VBO_ATT_TEXCOORD1 | VBO_ATT_TEXCOORD2 | VBO_ATT_NORMAL)

struct Vertex_PCTN //position, color, texCoord, and normal
{
	vec3 position;
	vec4 color;
	vec2 texCoord;
	vec3 normal;
};
struct Vertex_PCT //position, color, and texCoord
{
	vec3 position;
	vec4 color;
	vec2 texCoord;
};
struct Vertex_PCN //position, color, and normal
{
	vec3 position;
	vec4 color;
	vec3 normal;
};
struct Vertex_PC //position, and color
{
	vec3 position;
	vec4 color;
};
struct Vertex_PTN //position, texCoord, and normal
{
	vec3 position;
	vec2 texCoord;
	vec3 normal;
};
struct Vertex_PT //position, and texCoord
{
	vec3 position;
	vec2 texCoord;
};
struct Vertex_PTTN //position, texCoord1, texCoord2, and normal
{
	vec3 position;
	vec2 texCoord1;
	vec2 texCoord2;
	vec3 normal;
};
struct Vertex_PCTTN //position, color, texCoord1, texCoord2, and normal
{
	vec3 position;
	vec4 color;
	vec2 texCoord1;
	vec2 texCoord2;
	vec3 normal;
};

class VBO
{
private:
	byte mode;
	
	uint32 numVertices;
	float* vertices;
	
public:
	uint32 id;
	
	VBO()
	{
		this->mode = VBO_MODE_UNDEFINED;
		
		this->numVertices = 0;
		this->vertices = nullptr;
		
		this->id = 0;
	}
	VBO(byte mode)
	{
		this->mode = mode;
		
		this->numVertices = 0;
		this->vertices = nullptr;
		
		this->id = 0;
	}
	VBO(const VBO& other)
	{
		this->mode = other.mode;
		this->numVertices = other.numVertices;
		
		uint64 numFloats = other.GetVertexNumFloats() * other.numVertices;
		this->vertices = (float*)malloc(numFloats * sizeof(float));
		
		for(uint64 i = 0; i < numFloats; i++) //copy float-by-float
		{
			this->vertices[i] = other.vertices[i];
		}
		
		this->id = 0;
	}
	~VBO()
	{
		//The buffer must be manually deleted from outside since
		//we have no way of copying it when this instance is passed
		//into a new instance and then destroyed
		// if (this->id != 0)
		// {
		// 	this->DeleteBuffer();
		// }
		
		if (this->vertices != nullptr)
		{
			this->Empty();
		}
	}
	
	bool32 IsModeSet() const
	{
		return (this->mode != VBO_MODE_UNDEFINED);
	}
	bool32 IsFilled() const
	{
		return (this->vertices != nullptr);
	}
	bool32 IsBufferCreated() const
	{
		return (this->id != 0);
	}
	
	//size of the whole buffer in bytes
	uint64 GetBufferSize() const
	{
		return this->GetVertexSize() * this->numVertices;
	}
	//size of each vertex in bytes
	uint32 GetVertexSize() const
	{
		return this->GetVertexNumFloats() * sizeof(float);
	}
	//the number of floats in each vertex
	uint32 GetVertexNumFloats() const
	{
		unsigned int numFloats = 0;
		if ((this->mode & VBO_ATT_POSITION) > 0) 	numFloats += 3;
		if ((this->mode & VBO_ATT_COLOR) > 0) 	numFloats += 4;
		if ((this->mode & VBO_ATT_TEXCOORD1) > 0) numFloats += 2;
		if ((this->mode & VBO_ATT_TEXCOORD2) > 0) numFloats += 2;
		if ((this->mode & VBO_ATT_NORMAL) > 0) 	numFloats += 3;
		return numFloats;
	}
	
	uint32 GetNumVertices() const
	{
		return this->numVertices;
	}
	
	bool32 HasAttribute(byte attribute) const
	{
		return ((this->mode & attribute) > 0);
	}
	bool32 HasPositionAttribute() const
	{
		return this->HasAttribute(VBO_ATT_POSITION);
	}
	bool32 HasColorAttribute() const
	{
		return this->HasAttribute(VBO_ATT_COLOR);
	}
	bool32 HasTexCoordAttribute() const
	{
		return this->HasAttribute(VBO_ATT_TEXCOORD);
	}
	bool32 HasTexCoord1Attribute() const
	{
		return this->HasAttribute(VBO_ATT_TEXCOORD1);
	}
	bool32 HasTexCoord2Attribute() const
	{
		return this->HasAttribute(VBO_ATT_TEXCOORD2);
	}
	bool32 HasNormalAttribute() const
	{
		return this->HasAttribute(VBO_ATT_NORMAL);
	}
	
	uint8 GetAttributeOffset(byte attribute) const
	{
		if (!this->HasAttribute(attribute))
		{
			Log("Asking for a byte offset for attribute that is not enabled on the VBO.");
			return 0;
		}
		
		uint8 offset = 0;
		
		if (attribute == VBO_ATT_POSITION)  		return offset;
		if ((this->mode & VBO_ATT_POSITION) > 0)		offset += 3 * sizeof(float);
		
		if (attribute == VBO_ATT_COLOR)  			return offset;
		if ((this->mode & VBO_ATT_COLOR) > 0)			offset += 4 * sizeof(float);
		
		if (attribute == VBO_ATT_TEXCOORD1)  		return offset;
		if ((this->mode & VBO_ATT_TEXCOORD1) > 0)		offset += 2 * sizeof(float);
		
		if (attribute == VBO_ATT_TEXCOORD2)  		return offset;
		if ((this->mode & VBO_ATT_TEXCOORD2) > 0)		offset += 2 * sizeof(float);
		
		if (attribute == VBO_ATT_NORMAL)  			return offset;
		if ((this->mode & VBO_ATT_NORMAL) > 0)		offset += 3 * sizeof(float);
		
		Log("Unknown attribute type: ", str(attribute), ". Cannot return offset in VBO.");
		return 0;
	}
	uint8 GetPositionOffset() const
	{
		return this->GetAttributeOffset(VBO_ATT_POSITION);
	}
	uint8 GetColorOffset() const
	{
		return this->GetAttributeOffset(VBO_ATT_COLOR);
	}
	uint8 GetTexCoordOffset() const
	{
		return this->GetAttributeOffset(VBO_ATT_TEXCOORD);
	}
	uint8 GetTexCoord1Offset() const
	{
		return this->GetAttributeOffset(VBO_ATT_TEXCOORD1);
	}
	uint8 GetTexCoord2Offset() const
	{
		return this->GetAttributeOffset(VBO_ATT_TEXCOORD2);
	}
	uint8 GetNormalOffset() const
	{
		return this->GetAttributeOffset(VBO_ATT_NORMAL);
	}
	
	byte GetMode() const
	{
		return this->mode;
	}
	void SetMode(byte newMode)
	{
		if (this->IsFilled() || this->IsBufferCreated())
		{
			Log("You cannot change the vertex mode when the buffer is filled.");
			return;
		}
		
		this->mode = newMode;
	}
	
	//The expected byte size depends on the currently
	//set mode for the VBO.
	//copies the vertex data into a local buffer
	bool32 Fill(const float* vertices, uint32 numVertices)
	{
		if (!this->IsModeSet())
		{
			Log("You must set the mode of the VBO before trying to fill it.");
			return false;
		}
		if (this->IsFilled())
		{
			//TODO: Should we auto-resize the buffer or fault?
			Log("There are already vertices in the buffer. Please call Empty() first.");
			return false;
		}
		
		this->numVertices = numVertices;
		uint64 numFloats = this->GetVertexNumFloats() * numVertices;
		this->vertices = (float*)malloc(numFloats * sizeof(float));
		
		for(uint64 i = 0; i < numFloats; i++) //copy float-by-float
		{
			this->vertices[i] = vertices[i];
		}
		
		return true;
	}
	
	//NOTE: This will bind a new buffer in OpenGL
	//passes the vertices to OpenGL
	bool32 CreateBuffer(GLenum bufferType = GL_STATIC_DRAW)
	{
		if (!this->IsModeSet())
		{
			Log("You must set the mode of the VBO before trying to pass it to OpenGL.");
			return false;
		}
		if (!this->IsFilled())
		{
			Log("There are no vertices to pass to OpenGL");
			return false;
		}
		// if (this->IsBufferCreated())
		// {
		// 	//TODO: should we auto-delete and recreate or fault?
		// 	Log("You must call DeleteBuffer() on the current buffer before creating a new one.");
		// 	return false;
		// }
		
		if (this->id == 0)
		{
			glGenBuffers(1, &this->id);
			if (!CheckOpenGLErrors())
			{
				Log("We ran into an error while trying to create a VBO.");
				this->id = 0;
				return false;
			}
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, this->id);
		if (!CheckOpenGLErrors())
		{
			Log("We ran into an error while trying to bind the new VBO.");
			this->DeleteBuffer();
			return false;
		}
		
		glBufferData(GL_ARRAY_BUFFER,
			this->GetBufferSize(),
			this->vertices, 
			bufferType);
		if (!CheckOpenGLErrors())
		{
			Log("We ran into an error while trying to bind the new VBO.");
			this->DeleteBuffer();
			return false;
		}
		
		return true;
	}
	
	//deletes the vertices on OpenGL's side (not locally)
	bool32 DeleteBuffer() 
	{
		if (!this->IsBufferCreated())
		{
			Log("Tried to unload an already empty VBO.");
			return false;
		}
		
		glDeleteBuffers(1, &this->id);
		
		this->id = 0;
		return true;
	}
	
	//deletes the vertices locally (make sure you call DeleteBuffer first)
	bool32 Empty() 
	{
		// if (this->IsBufferCreated())
		// {
		// 	Log("You must call DeleteBuffer() to release the buffer from OpenGL.");
		// 	return false;
		// }
		if (!this->IsFilled())
		{
			Log("There are no vertices to empty.");
			return false;
		}
		
		free(this->vertices);
		
		this->numVertices = 0;
		this->vertices = nullptr;
		
		return true;
	}
	
	//We cannot ensure you have bound this VBO before
	//drawing it, that is up to you to ensure
	void Draw(GLenum primitiveType) const
	{
		glDrawArrays(primitiveType, 0, this->numVertices);
	}
};