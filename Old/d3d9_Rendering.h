#ifndef _D3D9_RENDERING_H
#define _D3D9_RENDERING_H

#include "mylib/myMath.h"
#include "mylib/d3d9_Vertex.h"
#include "mylib/d3d9_VertexBuffer.h"
#include "mylib/d3d9_Texture.h"
#include "mylib/d3d9_Effect.h"

struct RenderState_t
{
	Texture_t* boundTexture;
	VertexBuffer_t* boundVertexBuffer;
	IndexBuffer_t* boundIndexBuffer;
	Effect_t* boundEffect;
	
	Matrix4_t worldMatrix;
	Matrix4_t viewMatrix;
	Matrix4_t projectionMatrix;
	
	bool32 effectBegun;
	
};

//NOTE: We will automatically handle looking up "DiffuseTexture" in the boundEffect
void _BindTexture(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, Texture_t* texture)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun == false || texture == renderState->boundTexture);
	
	//Texture is already bound
	if (renderState->effectBegun && texture == renderState->boundTexture) return;
	
	d3dDevice->SetTexture(0, texture->pntr);
	
	if (renderState->boundEffect != nullptr)
	{
		_EffectSetTexture(d3dDevice, renderState->boundEffect, "DiffuseTexture", *texture);
	}
	
	renderState->boundTexture = texture;
}

//NOTE: We will automatically handle looking up "WorldMatrix" in the boundEffect
void _SetWorldMatrix(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	
	d3dDevice->SetTransform(D3DTS_WORLD, (D3DXMATRIX*)&newMatrix);
	
	if (renderState->boundEffect != nullptr)
	{
		_EffectSetMatrix(d3dDevice, renderState->boundEffect, "WorldMatrix", newMatrix);
	}
	
	renderState->worldMatrix = newMatrix;
}
//NOTE: We will automatically handle looking up "ViewMatrix" in the boundEffect
void _SetViewMatrix(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	
	d3dDevice->SetTransform(D3DTS_VIEW, (D3DXMATRIX*)&newMatrix);
	
	if (renderState->boundEffect != nullptr)
	{
		_EffectSetMatrix(d3dDevice, renderState->boundEffect, "ViewMatrix", newMatrix);
	}
	
	renderState->viewMatrix = newMatrix;
}
//NOTE: We will automatically handle looking up "ProjectionMatrix" in the boundEffect
void _SetProjectionMatrix(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, Matrix4_t newMatrix)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	
	d3dDevice->SetTransform(D3DTS_PROJECTION, (D3DXMATRIX*)&newMatrix);
	
	if (renderState->boundEffect != nullptr)
	{
		_EffectSetMatrix(d3dDevice, renderState->boundEffect, "ProjectionMatrix", newMatrix);
	}
	
	renderState->projectionMatrix = newMatrix;
}

void _BindVertexBuffer(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, VertexBuffer_t* buffer)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun == false || buffer == renderState->boundVertexBuffer);
	
	//Buffer is already bound
	if (renderState->effectBegun && buffer == renderState->boundVertexBuffer) return;
	
	d3dDevice->SetStreamSource(0, buffer->pntr, 0, buffer->stride);
	
	renderState->boundVertexBuffer = buffer;
}

void _BindIndexBuffer(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, IndexBuffer_t* buffer)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun == false || buffer == renderState->boundIndexBuffer);
	
	//Buffer is already bound
	if (renderState->effectBegun && buffer == renderState->boundIndexBuffer) return;
	
	//TODO: Implement this
	// d3dDevice->SetStreamSource(0, buffer->pntr, 0, buffer->stride);
	
	renderState->boundIndexBuffer = buffer;
}

void _BindEffect(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, Effect_t* effect)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun == false || effect == renderState->boundEffect);
	
	//Buffer is already bound
	if (renderState->effectBegun && effect == renderState->boundEffect) return;
	
	//NOTE: We don't do anything with the effect until _RenderPassBegin is called
	
	renderState->boundEffect = effect;
}

uint32 _BeginEffect(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun == false);
	
	uint32 numPasses = 1;
	if (renderState->boundEffect != nullptr)
	{
		renderState->boundEffect->pntr->Begin(&numPasses, 0);
	}
	
	renderState->effectBegun = true;
	
	return numPasses;
}

void _RenderPassBegin(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, uint32 passIndex)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun);
	
	if (renderState->boundEffect != nullptr)
	{
		renderState->boundEffect->pntr->BeginPass(passIndex);
	}
	else
	{
		//Don't need to do anything if we aren't using an effect
	}
}

void _RenderBoundBuffer(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState, D3DPRIMITIVETYPE primitiveType, uint32 numPrimitives)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->boundVertexBuffer != nullptr);
	
	d3dDevice->DrawPrimitive(primitiveType, 0, numPrimitives);
}

//TODO: Add _RenderBoundVertexBufferIndexed function

void _RenderPassEnd(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun);
	
	if (renderState->boundEffect != nullptr)
	{
		renderState->boundEffect->pntr->EndPass();
	}
	else
	{
		//Don't need to do anything if we aren't using an effect
	}
}

void _EndEffect(LPDIRECT3DDEVICE9 d3dDevice, RenderState_t* renderState)
{
	Assert(d3dDevice != nullptr);
	Assert(renderState != nullptr);
	Assert(renderState->effectBegun);
	
	if (renderState->boundEffect != nullptr)
	{
		renderState->boundEffect->pntr->End();
	}
	else
	{
		//Don't need to do anything if we aren't using an effect
	}
	
	renderState->effectBegun = false;
}

#endif // _D3D9_RENDERING_H