/*
File:   my_colors.h
Author: Taylor Robbins
Date:   11\03\2017
Description:
	** This file defines a ton of named colors that can be references in several
	** ways. It also defines the Color_t structure. All colors are defined in an
	** bgra memory order (e.g. 0xAARRGGBB)
	
This file is #included by default from mylib.h
*/

//http://www.ryanjuckett.com/programming/rgb-color-space-conversion/
//http://www.brucelindbloom.com/index.html?Math.html
//https://www.easyrgb.com/en/convert.php#inputFORM
//https://www.easyrgb.com/en/math.php
//http://davengrace.com/dave/cspace/
//https://en.wikipedia.org/wiki/Chromaticity
//https://en.wikipedia.org/wiki/HSL_and_HSV
//https://hackaday.com/2018/06/18/buttery-smooth-fades-with-the-power-of-hsv/

//53.232882, 104.575518, 40.000158
//54.232882, 104.575518, 40.000158
//78.232882, 104.575518, 40.000158

#ifndef _COLORS_H
#define _COLORS_H

// +--------------------------------------------------------------+
// |                    Structure Definitions                     |
// +--------------------------------------------------------------+
union Color_t
{
	u32 value;
	struct { u8 b;    u8 g;     u8 r;   u8 a; };
	struct { u8 blue; u8 green; u8 red; u8 alpha; };
};

union Colorf_t
{
	v4 values;
	struct { r32 b;    r32 g;     r32 r;   r32 a; };
	struct { r32 blue; r32 green; r32 red; r32 alpha; };
};

union ColorHSV_t
{
	v4 values;
	struct { r32 h;   r32 s;          r32 v;     r32 a;     };
	struct { r32 hue; r32 saturation; r32 value; r32 alpha; };
};

struct ColorXYZ_t
{
	// v4 values;
	struct { r64 x; r64 y; r64 z; r64 a;     };
	struct { r64 X; r64 Y; r64 Z; r64 alpha; };
};

union Color64_t
{
	r64 values[4];
	struct { r64 x;         r64 y;          r64 z;     r64 a;       };
	struct { r64 X;         r64 Y;          r64 Z;     r64 alpha;   };
	struct { r64 l;         r64 c;          r64 h;     r64 unused1; };
	struct { r64 luminence; r64 chroma;     r64 hue;   r64 unused2; };
	struct { r64 unused3;   r64 s;          r64 v;     r64 unused4; };
	struct { r64 unused5;   r64 saturation; r64 value; r64 unused6; };
	struct { r64 r;         r64 g;          r64 b;     r64 unused7; };
	struct { r64 red;       r64 green;      r64 blue;  r64 unused8; };
	struct { r64 L;         r64 A;          r64 B;     r64 unused9; };
};

// +--------------------------------------------------------------+
// |                        New Functions                         |
// +--------------------------------------------------------------+
inline Color_t NewColor(u32 value)
{
	Color_t result = {};
	result.value = value;
	return result;
}
inline Color_t NewColor(u8 r, u8 g, u8 b)
{
	Color_t result = {};
	result.r = r;
	result.g = g;
	result.b = b;
	result.a = 255;
	return result;
}
inline Color_t NewColor(u8 r, u8 g, u8 b, u8 a)
{
	Color_t result = {};
	result.r = r;
	result.g = g;
	result.b = b;
	result.a = a;
	return result;
}
inline Color_t NewColor(Colorf_t colorf)
{
	Color_t result = {};
	result.r = ClampI32toU8(RoundR32i(colorf.r * 255.0f));
	result.g = ClampI32toU8(RoundR32i(colorf.g * 255.0f));
	result.b = ClampI32toU8(RoundR32i(colorf.b * 255.0f));
	result.a = ClampI32toU8(RoundR32i(colorf.a * 255.0f));
	return result;
}
inline Color_t NewColorFromVec3(v3 vector3)
{
	Color_t result = {};
	result.r = ClampI32toU8(RoundR32i(vector3.x * 255.0f));
	result.g = ClampI32toU8(RoundR32i(vector3.y * 255.0f));
	result.b = ClampI32toU8(RoundR32i(vector3.z * 255.0f));
	result.a = 255;
	return result;
}
inline Color_t NewColorFromVec4(v4 vector4)
{
	Color_t result = {};
	result.r = ClampI32toU8(RoundR32i(vector4.x * 255.0f));
	result.g = ClampI32toU8(RoundR32i(vector4.y * 255.0f));
	result.b = ClampI32toU8(RoundR32i(vector4.z * 255.0f));
	result.a = ClampI32toU8(RoundR32i(vector4.w * 255.0f));
	return result;
}

inline Color_t ColorTransparent(r32 alpha)
{
	return NewColor(255, 255, 255, (u8)(255 * alpha));
}
inline Color_t ColorTransparent(Color_t color, r32 alpha)
{
	return NewColor(color.r, color.g, color.b, (u8)(255 * alpha));
}

inline Colorf_t NewColorf(r32 r, r32 g, r32 b)
{
	Colorf_t result = {};
	result.r = r;
	result.g = g;
	result.b = b;
	result.a = 1.0f;
	return result;
}
inline Colorf_t NewColorf(r32 r, r32 g, r32 b, r32 a)
{
	Colorf_t result = {};
	result.r = r;
	result.g = g;
	result.b = b;
	result.a = a;
	return result;
}
inline Colorf_t NewColorf(Color_t color)
{
	Colorf_t result = {};
	result.r = ((r32)color.r / 255.0f);
	result.g = ((r32)color.g / 255.0f);
	result.b = ((r32)color.b / 255.0f);
	result.a = ((r32)color.a / 255.0f);
	return result;
}
inline Colorf_t NewColorfFromVec3(v3 vector3)
{
	Colorf_t result = {};
	result.r = vector3.r;
	result.g = vector3.g;
	result.b = vector3.b;
	result.a = 1.0f;
	return result;
}
inline Colorf_t NewColorfFromVec4(v4 vector4)
{
	Colorf_t result = {};
	result.r = vector4.r;
	result.g = vector4.g;
	result.b = vector4.b;
	result.a = vector4.a;
	return result;
}

inline ColorHSV_t NewColorHsv(r32 hue, r32 saturation, r32 value)
{
	ColorHSV_t result = {};
	result.hue = hue;
	result.saturation = saturation;
	result.value = value;
	result.alpha = 1.0f;
	return result;
}
inline ColorHSV_t NewColorHsv(r32 hue, r32 saturation, r32 value, r32 alpha)
{
	ColorHSV_t result = {};
	result.hue = hue;
	result.saturation = saturation;
	result.value = value;
	result.alpha = alpha;
	return result;
}
inline ColorHSV_t NewColorHsvFromVec3(v3 vector3)
{
	ColorHSV_t result = {};
	result.hue = vector3.x;
	result.saturation = vector3.y;
	result.value = vector3.z;
	result.alpha = 1.0f;
	return result;
}
inline ColorHSV_t NewColorHsvFromVec4(v4 vector4)
{
	ColorHSV_t result = {};
	result.hue = vector4.x;
	result.saturation = vector4.y;
	result.value = vector4.z;
	result.alpha = vector4.w;
	return result;
}

// +--------------------------------------------------------------+
// |                      Helpful Functions                       |
// +--------------------------------------------------------------+
Color_t ColorLerp(Color_t color1, Color_t color2, r32 amount)
{
	Color_t result = {};
	result.r = (u8)LerpR32((r32)color1.r, (r32)color2.r, amount);
	result.g = (u8)LerpR32((r32)color1.g, (r32)color2.g, amount);
	result.b = (u8)LerpR32((r32)color1.b, (r32)color2.b, amount);
	result.a = (u8)LerpR32((r32)color1.a, (r32)color2.a, amount);
	return result;
}
Colorf_t ColorfLerp(Colorf_t color1, Colorf_t color2, r32 amount)
{
	Colorf_t result = {};
	result.r = LerpR32(color1.r, color2.r, amount);
	result.g = LerpR32(color1.g, color2.g, amount);
	result.b = LerpR32(color1.b, color2.b, amount);
	result.a = LerpR32(color1.a, color2.a, amount);
	return result;
}

Color_t ColorMultiply(Color_t color1, Color_t color2)
{
	Color_t result = {};
	
	result.r = (u8)(((r32)color1.r / 255.0f) * ((r32)color2.r / 255.0f) * 255.0f);
	result.g = (u8)(((r32)color1.g / 255.0f) * ((r32)color2.g / 255.0f) * 255.0f);
	result.b = (u8)(((r32)color1.b / 255.0f) * ((r32)color2.b / 255.0f) * 255.0f);
	result.a = (u8)(((r32)color1.a / 255.0f) * ((r32)color2.a / 255.0f) * 255.0f);
	
	return result;
}
Colorf_t ColorfMultiply(Colorf_t left, Colorf_t right)
{
	Colorf_t result = {};
	result.r = left.r * right.r;
	result.g = left.g * right.g;
	result.b = left.b * right.b;
	result.a = left.a * right.a;
	return result;
}

Color_t ColorMultiplyAlpha(Color_t color, r32 alphaMult)
{
	return NewColor(color.r, color.g, color.b, (u8)(ClampR32(color.a/255.0f * alphaMult, 0.0f, 1.0f) * 255));
}

Color_t ColorOpposite(Color_t color)
{
	return NewColor((u8)(255 - color.r), (u8)(255 - color.g), (u8)(255 - color.b), (u8)(color.a));
}
Colorf_t ColorfOpposite(Colorf_t color)
{
	return NewColorf(1.0f - color.r, 1.0f - color.g, 1.0f - color.b, color.a);
}

Color_t ColorComplimentary(Color_t color)
{
	return NewColor((u8)(((u32)color.r + 128) % 256), (u8)(((u32)color.g + 128) % 256), (u8)(((u32)color.b + 128) % 256), color.a);
}

r32 HueToRGB(r32 value1, r32 value2, r32 vH)
{
	if (vH < 0) { vH += 1; }
	if (vH > 1) { vH -= 1; }
	
	if (6*vH < 1)
	{
		return (value1 + (value2-value1) * 6 * vH);
	}
	else if (2*vH < 1)
	{
		return value2;
	}
	else if (3*vH < 2)
	{
		return (value1 + (value2-value1) * ((2.0f/3) - vH) * 6);
	}
	else
	{
		return value1;
	}
}

Color_t ColorRGBFromHSV(ColorHSV_t colorHsv)
{
	r32 redValue = 0;
	r32 greenValue = 0;
	r32 blueValue = 0;
	
	r32 cValue = colorHsv.v * colorHsv.s;
	r32 xValue = cValue * (1 - AbsR32((r32)((RoundR32i(colorHsv.h) / 60)%2) - 1));
	r32 nextX = cValue * (1 - AbsR32((r32)((RoundR32i(colorHsv.h) / 60 + 1)%2) - 1));
	r32 mValue = colorHsv.v - cValue;
	
	u8 hueSwitch = ClampI32toU8(RoundR32i(colorHsv.h) / 60);
	v3 color1 = {};
	v3 color2 = {};
	r32 lerpAmount = (RoundR32i(colorHsv.h)%60) / 60.0f;
	
	switch (hueSwitch)
	{
		case 0: //0-60 degrees
		{
			color1 = NewVec3(cValue, xValue,      0);
			color2 = NewVec3( nextX, cValue,      0);
		} break;
		case 1: //60-120 degrees
		{
			color1 = NewVec3(xValue, cValue,      0);
			color2 = NewVec3(     0, cValue,  nextX);
		} break;
		case 2: //120-180 degrees
		{
			color1 = NewVec3(     0, cValue, xValue);
			color2 = NewVec3(     0,  nextX, cValue);
		} break;
		case 3: //180-240 degrees
		{
			color1 = NewVec3(     0, xValue, cValue);
			color2 = NewVec3( nextX,      0, cValue);
		} break;
		case 4: //240-300 degrees
		{
			color1 = NewVec3(xValue,      0, cValue);
			color2 = NewVec3(cValue,      0,  nextX);
		} break;
		case 5: //300-360 degrees
		case 6:
		{
			color1 = NewVec3(cValue,      0, xValue);
			color2 = NewVec3(cValue,  nextX,      0);
		} break;
		
		default: return NewColor(0, 0, 0, 0);
	};
	
	Color_t result = NewColor(
		(u8)((LerpR32(color1.x, color2.x, lerpAmount) + mValue)*255),
		(u8)((LerpR32(color1.y, color2.y, lerpAmount) + mValue)*255),
		(u8)((LerpR32(color1.z, color2.z, lerpAmount) + mValue)*255), 255);
	
	return result;
}
ColorHSV_t ColorHSVFromRGB(Color_t color)
{
	ColorHSV_t result = {};
	
	r32 red   = ((r32)color.r / 255.0f);
	r32 green = ((r32)color.g / 255.0f);
	r32 blue  = ((r32)color.b / 255.0f);
	r32 minRgb = MinR32(red, green, blue);
	r32 maxRgb = MaxR32(red, green, blue);
	r32 minMaxDelta = maxRgb - minRgb;
	
	result.a = ((r32)color.a / 255.0f);
	result.v = maxRgb;
	if (minMaxDelta == 0)
	{
		result.h = 0;
		result.s = 0.0f;
	}
	else
	{
		result.s = minMaxDelta / maxRgb;
		
		r32 rDelta = (((maxRgb - red)   / 6) + (minMaxDelta / 2)) / minMaxDelta;
		r32 gDelta = (((maxRgb - green) / 6) + (minMaxDelta / 2)) / minMaxDelta;
		r32 bDelta = (((maxRgb - blue)  / 6) + (minMaxDelta / 2)) / minMaxDelta;
		
		r32 hueR32 = 0.0f;
		if (red == maxRgb) { hueR32 = bDelta - gDelta; }
		else if (green == maxRgb) { hueR32 = (1.0f/3.0f) + rDelta - bDelta; }
		else if (blue == maxRgb) { hueR32 = (2.0f/3.0f) + gDelta - rDelta; }
		
		if (hueR32 < 0) { hueR32 += 1; }
		if (hueR32 > 1) { hueR32 -= 1; }
		
		result.hue = (r32)(RoundR32i(hueR32*360) % 360);
	}
	
	return result;
}

inline v3 NewVec3FromColor(Color_t color)
{
	return NewVec3((r32)color.r / 255.0f, (r32)color.g / 255.0f, (r32)color.b / 255.0f);
}
inline v4 NewVec4FromColor(Color_t color)
{
	return NewVec4((r32)color.r / 255.0f, (r32)color.g / 255.0f, (r32)color.b / 255.0f, (r32)color.a / 255.0f);
}

inline v3 NewVec3FromColorf(Colorf_t color)
{
	return NewVec3(color.r, color.g, color.b);
}
inline v4 NewVec4FromColorf(Colorf_t color)
{
	return NewVec4(color.r, color.g, color.b, color.a);
}

Color_t ColorDarken(Color_t color, u8 amount)
{
	Color_t result = color;
	if (result.r > amount) { result.r -= amount; }
	else { result.r = 0; }
	if (result.g > amount) { result.g -= amount; }
	else { result.g = 0; }
	if (result.b > amount) { result.b -= amount; }
	else { result.b = 0; }
	return result;
}
Color_t ColorLighten(Color_t color, u8 amount)
{
	Color_t result = color;
	if (result.r < 255 - amount) { result.r += amount; }
	else { result.r = 255; }
	if (result.g < 255 - amount) { result.g += amount; }
	else { result.g = 255; }
	if (result.b < 255 - amount) { result.b += amount; }
	else { result.b = 255; }
	return result;
}

Color_t ColorDesaturate(Color_t color, r32 saturation)
{
    // Algorithm from Chapter 16 of OpenGL Shading Language
    v3 w = NewVec3(0.2125f, 0.7154f, 0.0721f);
    u8 intensity = (u8)(Vec3Dot(NewVec3FromColor(color), w) * 255);
    return ColorLerp(NewColor(intensity, intensity, intensity), color, saturation);
}

// +--------------------------------------------------------------+
// |                Illuminant/Observer Constants                 |
// +--------------------------------------------------------------+
//NOTE: These values are copied from https://www.easyrgb.com/en/math.php

typedef enum
{
	ColorObserver_TwoDegree = 0x00, //CIE 1931
	ColorObserver_TenDegree,        //CIE 1964
	ColorObserver_NumOptions,
} ColorObserver_t;

typedef enum
{
	ColorIlluminant_A = 0x00, //Incandescent/tungsten
	ColorIlluminant_B,        //Old direct sunlight at noon
	ColorIlluminant_C,        //Old daylight
	ColorIlluminant_D50,      //ICC profile PCS
	ColorIlluminant_D55,      //Mid-morning daylight
	ColorIlluminant_D65,      //Daylight, sRGB, Adobe-RGB
	ColorIlluminant_D75,      //North sky daylight
	ColorIlluminant_E,        //Equal energy
	ColorIlluminant_F1,       //Daylight Fluorescent
	ColorIlluminant_F2,       //Cool fluorescent
	ColorIlluminant_F3,       //White Fluorescent
	ColorIlluminant_F4,       //Warm White Fluorescent
	ColorIlluminant_F5,       //Daylight Fluorescent
	ColorIlluminant_F6,       //Lite White Fluorescent
	ColorIlluminant_F7,       //Daylight fluorescent, D65 simulator
	ColorIlluminant_F8,       //Sylvania F40, D50 simulator
	ColorIlluminant_F9,       //Cool White Fluorescent
	ColorIlluminant_F10,      //Ultralume 50, Philips TL85
	ColorIlluminant_F11,      //Ultralume 40, Philips TL84
	ColorIlluminant_F12,      //Ultralume 30, Philips TL83
	ColorIlluminant_NumOptions,
} ColorIlluminant_t;

const r64 ColorIllumValues[ColorIlluminant_NumOptions][ColorObserver_NumOptions][3] = {
	{ { 109.850, 100.000, 35.585  }, { 111.144, 100.000, 35.200  } },
	{ { 99.0927, 100.000, 85.313  }, { 99.178,  100.000, 84.3493 } },
	{ { 98.074,  100.000, 118.232 }, { 97.285,  100.000, 116.145 } },
	{ { 96.422,  100.000, 82.521  }, { 96.720,  100.000, 81.427  } },
	{ { 95.682,  100.000, 92.149  }, { 95.799,  100.000, 90.926  } },
	{ { 95.047,  100.000, 108.883 }, { 94.811,  100.000, 107.304 } },
	{ { 94.972,  100.000, 122.638 }, { 94.416,  100.000, 120.641 } },
	{ { 100.000, 100.000, 100.000 }, { 100.000, 100.000, 100.000 } },
	{ { 92.834,  100.000, 103.665 }, { 94.791,  100.000, 103.191 } },
	{ { 99.187,  100.000, 67.395  }, { 103.280, 100.000, 69.026  } },
	{ { 103.754, 100.000, 49.861  }, { 108.968, 100.000, 51.965  } },
	{ { 109.147, 100.000, 38.813  }, { 114.961, 100.000, 40.963  } },
	{ { 90.872,  100.000, 98.723  }, { 93.369,  100.000, 98.636  } },
	{ { 97.309,  100.000, 60.191  }, { 102.148, 100.000, 62.074  } },
	{ { 95.044,  100.000, 108.755 }, { 95.792,  100.000, 107.687 } },
	{ { 96.413,  100.000, 82.333  }, { 97.115,  100.000, 81.135  } },
	{ { 100.365, 100.000, 67.868  }, { 102.116, 100.000, 67.826  } },
	{ { 96.174,  100.000, 81.712  }, { 99.001,  100.000, 83.134  } },
	{ { 100.966, 100.000, 64.370  }, { 103.866, 100.000, 65.627  } },
	{ { 108.046, 100.000, 39.228  }, { 111.428, 100.000, 40.353  } }
};

// +--------------------------------------------------------------+
// |               Color Space Conversion Functions               |
// +--------------------------------------------------------------+
//Convert sRGB to linear RGB
r64 ExpandNonLinearSrgb(r64 nonlinearValue)
{
	return (nonlinearValue <= 0.04045) ? (nonlinearValue / 12.92) : (PowR64((nonlinearValue+0.055)/1.055, 2.4));
}
//Convert linear RGB to sRGB
r64 CompressLinearSrgb(r64 linearValue)
{
	return (linearValue <= 0.0031308) ? (linearValue * 12.92) : (1.055 * PowR64(linearValue, 1.0/2.4) - 0.055);
}

Color64_t ColorXyzFromSrgb(Color_t color)
{
	r64 linearRed   = ExpandNonLinearSrgb((r64)color.r / 255.0);
	r64 linearGreen = ExpandNonLinearSrgb((r64)color.g / 255.0);
	r64 linearBlue  = ExpandNonLinearSrgb((r64)color.b / 255.0);
	
	//NOTE: X, Y and Z output refer to a D65/2° standard illuminant
	Color64_t result = {};
	result.X = (linearRed * 0.4124 + linearGreen * 0.3576 + linearBlue * 0.1805) * 100;
	result.Y = (linearRed * 0.2126 + linearGreen * 0.7152 + linearBlue * 0.0722) * 100;
	result.Z = (linearRed * 0.0193 + linearGreen * 0.1192 + linearBlue * 0.9505) * 100;
	result.a = ((r64)color.a / 255.0);
	return result;
}
Color_t ColorSrgbFromXyz(Color64_t colorXyz, bool* isValidColorOut = nullptr)
{
	r64 nonlinearRed   = CompressLinearSrgb((colorXyz.X *  3.2406 + colorXyz.Y * -1.5372 + colorXyz.Z * -0.4986) / 100.0);
	r64 nonlinearGreen = CompressLinearSrgb((colorXyz.X * -0.9689 + colorXyz.Y *  1.8758 + colorXyz.Z *  0.0415) / 100.0);
	r64 nonlinearBlue  = CompressLinearSrgb((colorXyz.X *  0.0557 + colorXyz.Y * -0.2040 + colorXyz.Z *  1.0570) / 100.0);
	
	Color_t result = {};
	result.r = (u8)RoundR64i(nonlinearRed * 255);
	result.g = (u8)RoundR64i(nonlinearGreen * 255);
	result.b = (u8)RoundR64i(nonlinearBlue * 255);
	result.a = (u8)RoundR64i(colorXyz.a * 255);
	if (isValidColorOut != nullptr)
	{
		if      (nonlinearRed   < 0.0f || nonlinearRed   > 1.0f) { *isValidColorOut = false; }
		else if (nonlinearGreen < 0.0f || nonlinearGreen > 1.0f) { *isValidColorOut = false; }
		else if (nonlinearBlue  < 0.0f || nonlinearBlue  > 1.0f) { *isValidColorOut = false; }
		else { *isValidColorOut = true; }
		
	}
	return result;
}

r64 DoSpecialThing(r64 xyzValue) //TODO: Rename me!
{
	return (xyzValue > 0.008856) ? (CbrtR64(xyzValue)) : ((7.787 * xyzValue) + (16.0 / 116.0));
}
r64 UndoSpecialThing(r64 specialValue) //TODO: Rename me!
{
	return ((specialValue*specialValue*specialValue) > 0.008856) ? (Cube(specialValue)) : ((specialValue - (16.0 / 116.0)) / 7.787);
}

Color64_t ColorLabFromXyz(Color64_t colorXyz, ColorObserver_t observer = ColorObserver_TwoDegree, ColorIlluminant_t illuminant = ColorIlluminant_D65)
{
	Assert(observer < ColorObserver_NumOptions);
	Assert(illuminant < ColorIlluminant_NumOptions);
	
	// MyLibPrintLine_D("colorXyz = (%f, %f, %f)", colorXyz.x, colorXyz.y, colorXyz.z);
	// MyLibPrintLine_D("IllumValues[%u][%u] = {%f, %f, %f}", illuminant, observer, ColorIllumValues[illuminant][observer][0], ColorIllumValues[illuminant][observer][1], ColorIllumValues[illuminant][observer][2]);
	r64 xValue = colorXyz.X / ColorIllumValues[illuminant][observer][0];
	r64 yValue = colorXyz.Y / ColorIllumValues[illuminant][observer][1];
	r64 zValue = colorXyz.Z / ColorIllumValues[illuminant][observer][2];
	// MyLibPrintLine_D("zyzValues = (%f, %f, %f)", xValue, yValue, zValue);
	
	xValue = DoSpecialThing(xValue);
	yValue = DoSpecialThing(yValue);
	zValue = DoSpecialThing(zValue);
	// MyLibPrintLine_D("specialValues = (%f, %f, %f)", xValue, yValue, zValue);
	
	Color64_t result = {};
	result.L = (116 * yValue) - 16;
	result.A = 500 * (xValue - yValue);
	result.B = 200 * (yValue - zValue);
	result.alpha = colorXyz.alpha;
	return result;
}
Color64_t ColorXyzFromLab(Color64_t colorLab, ColorObserver_t observer = ColorObserver_TwoDegree, ColorIlluminant_t illuminant = ColorIlluminant_D65)
{
	Assert(observer < ColorObserver_NumOptions);
	Assert(illuminant < ColorIlluminant_NumOptions);
	
	r64 yValue = (colorLab.L + 16) / 116;
	r64 xValue = (colorLab.A / 500) + yValue;
	r64 zValue = yValue - (colorLab.B / 200);
	
	xValue = UndoSpecialThing(xValue);
	yValue = UndoSpecialThing(yValue);
	zValue = UndoSpecialThing(zValue);
	
	Color64_t result = {};
	result.X = xValue * ColorIllumValues[illuminant][observer][0];
	result.Y = yValue * ColorIllumValues[illuminant][observer][1];
	result.Z = zValue * ColorIllumValues[illuminant][observer][2];
	result.alpha = colorLab.alpha;
	return result;
}

Color64_t ColorLchFromLab(Color64_t colorLab)
{
	r64 hValue = AtanR64(colorLab.B, colorLab.A);
	
	if (hValue > 0) { hValue = (hValue/Pi64) * 180; }
	else { hValue = 360 - ((AbsR64(hValue) / Pi64) * 180); }
	
	Color64_t result = {};
	result.l = colorLab.L;
	result.c = SqrtR64(colorLab.A*colorLab.A + colorLab.B*colorLab.B);
	result.h = hValue;
	result.alpha = colorLab.alpha;
	return result;
}
Color64_t ColorLabFromLch(Color64_t colorLch)
{
	Color64_t result = {};
	result.L = colorLch.l;
	result.A = CosR64(ToRadians(colorLch.h)) * colorLch.c;
	result.B = SinR64(ToRadians(colorLch.h)) * colorLch.c;
	result.alpha = colorLch.alpha;
	return result;
}

Color_t ColorLerpLch(Color_t color1, Color_t color2, r32 amount)
{
	Color64_t colorLch1 = ColorLchFromLab(ColorLabFromXyz(ColorXyzFromSrgb(color1)));
	Color64_t colorLch2 = ColorLchFromLab(ColorLabFromXyz(ColorXyzFromSrgb(color2)));
	Color64_t lerpedLch;
	lerpedLch.l     = colorLch1.l; //LerpR64(colorLch1.l,     colorLch2.l,     amount);
	lerpedLch.c     = colorLch1.c; //LerpR64(colorLch1.c,     colorLch2.c,     amount);
	// r32 AngleLerp(r32 angleFrom, r32 angleTo, r32 amount)
	lerpedLch.h     = (r64)ToDegrees(AngleLerp(ToRadians((r32)colorLch1.h), ToRadians((r32)colorLch2.h), amount));
	lerpedLch.alpha = LerpR64(colorLch1.alpha, colorLch2.alpha, amount);
	Color_t result = ColorSrgbFromXyz(ColorXyzFromLab(ColorLabFromLch(lerpedLch)));
	return result;
}

inline Color64_t ColorLchFromSrgb(Color_t color)
{
	return ColorLchFromLab(ColorLabFromXyz(ColorXyzFromSrgb(color)));
}
inline Color_t ColorSrgbFromLch(Color64_t colorLch)
{
	return ColorSrgbFromXyz(ColorXyzFromLab(ColorLabFromLch(colorLch)));
}
bool IsColorLchValidInSrgb(Color64_t colorLch)
{
	bool result = false;
	ColorSrgbFromXyz(ColorXyzFromLab(ColorLabFromLch(colorLch)), &result);
	return result;
}

// +--------------------------------------------------------------+
// |                         Pure Colors                          |
// +--------------------------------------------------------------+
	#define NUM_PREDEF_PURE_COLORS       6
	#define NUM_PREDEF_GREYSCALE_COLORS  16
	
	#define NoColor_Value                 0x00000000
	#define TransparentBlack_Value        0x00000000
	#define Transparent_Value             0x00FFFFFF
	#define TransparentWhite_Value        0x00FFFFFF
	
	#define Black_Value                   0xFF000000
	#define Grey1_Value                   0xFF111111
	#define Grey2_Value                   0xFF222222
	#define Grey3_Value                   0xFF333333
	#define Grey4_Value                   0xFF444444
	#define Grey5_Value                   0xFF555555
	#define Grey6_Value                   0xFF666666
	#define Grey7_Value                   0xFF777777
	#define Grey8_Value                   0xFF888888
	#define Grey9_Value                   0xFF999999
	#define Grey10_Value                  0xFFAAAAAA
	#define Grey11_Value                  0xFFBBBBBB
	#define Grey12_Value                  0xFFCCCCCC
	#define Grey13_Value                  0xFFDDDDDD
	#define Grey14_Value                  0xFFEEEEEE
	#define White_Value                   0xFFFFFFFF
	
	#define PureRed_Value                 0xFFFF0000
	#define PureOrange_Value              0xFFFFA500
	#define PureYellow_Value              0xFFFFFF00
	#define PureGreen_Value               0xFF008000
	#define PureBlue_Value                0xFF0000FF
	#define PurePurple_Value              0xFF800080
	
	#define NoColor                 NewColor(NoColor_Value)
	#define TransparentBlack        NewColor(TransparentBlack_Value)
	#define Transparent             NewColor(Transparent_Value)
	#define TransparentWhite        NewColor(TransparentWhite_Value)
	#define Black                   NewColor(Black_Value)
	#define Grey1                   NewColor(Grey1_Value)
	#define Grey2                   NewColor(Grey2_Value)
	#define Grey3                   NewColor(Grey3_Value)
	#define Grey4                   NewColor(Grey4_Value)
	#define Grey5                   NewColor(Grey5_Value)
	#define Grey6                   NewColor(Grey6_Value)
	#define Grey7                   NewColor(Grey7_Value)
	#define Grey8                   NewColor(Grey8_Value)
	#define Grey9                   NewColor(Grey9_Value)
	#define Grey10                  NewColor(Grey10_Value)
	#define Grey11                  NewColor(Grey11_Value)
	#define Grey12                  NewColor(Grey12_Value)
	#define Grey13                  NewColor(Grey13_Value)
	#define Grey14                  NewColor(Grey14_Value)
	#define White                   NewColor(White_Value)
	#define PureRed                 NewColor(PureRed_Value)
	#define PureOrange              NewColor(PureOrange_Value)
	#define PureYellow              NewColor(PureYellow_Value)
	#define PureGreen               NewColor(PureGreen_Value)
	#define PureBlue                NewColor(PureBlue_Value)
	#define PurePurple              NewColor(PurePurple_Value)
// +--------------------------------------------------------------+

// +--------------------------------------------------------------+
// |                        Palette Colors                        |
// +--------------------------------------------------------------+
	#define NUM_PREDEF_PAL_COLORS       70
	
	#define PalRedDarker_Value            0xFFB91668
	#define PalRedDark_Value              0xFFE1126B
	#define PalRed_Value                  0xFFF92672
	#define PalRedLight_Value             0xFFFB6C7F
	#define PalRedLighter_Value           0xFFFDB5A8
	#define PalPinkDarker_Value           0xFFBF2A83
	#define PalPinkDark_Value             0xFFD9368B
	#define PalPink_Value                 0xFFED4E95
	#define PalPinkLight_Value            0xFFF088B8
	#define PalPinkLighter_Value          0xFFF6ACCD
	#define PalOrangeDarker_Value         0xFFBD7628
	#define PalOrangeDark_Value           0xFFE4861C
	#define PalOrange_Value               0xFFFD971F
	#define PalOrangeLight_Value          0xFFFEB64B
	#define PalOrangeLighter_Value        0xFFFED88F
	#define PalHoneyDarker_Value          0xFFD5A43E
	#define PalHoneyDark_Value            0xFFE9B34A
	#define PalHoney_Value                0xFFFDC459
	#define PalHoneyLight_Value           0xFFFED47A
	#define PalHoneyLighter_Value         0xFFFEE8AC
	#define PalYellowDarker_Value         0xFFB3B947
	#define PalYellowDark_Value           0xFFD5CE62
	#define PalYellow_Value               0xFFE6DB74
	#define PalYellowLight_Value          0xFFEFE48C
	#define PalYellowLighter_Value        0xFFF3ECAC
	#define PalBananaDarker_Value         0xFFB7BD2F
	#define PalBananaDark_Value           0xFFDAD942
	#define PalBanana_Value               0xFFEEE64F
	#define PalBananaLight_Value          0xFFF4EA6F
	#define PalBananaLighter_Value        0xFFF9F1A4
	#define PalGreenDarker_Value          0xFF76B434
	#define PalGreenDark_Value            0xFF8DCE2E
	#define PalGreen_Value                0xFFA6E22E
	#define PalGreenLight_Value           0xFFC3E753
	#define PalGreenLighter_Value         0xFFE1EE92
	#define PalGrassDarker_Value          0xFF608B4D
	#define PalGrassDark_Value            0xFF7DA641
	#define PalGrass_Value                0xFF92BB41
	#define PalGrassLight_Value           0xFFB3D05B
	#define PalGrassLighter_Value         0xFFD7E295
	#define PalBlueDarker_Value           0xFF2471EC
	#define PalBlueDark_Value             0xFF4585EE
	#define PalBlue_Value                 0xFF669BEF
	#define PalBlueLight_Value            0xFF99B7DE
	#define PalBlueLighter_Value          0xFFCDD9DB
	#define PalSkyDarker_Value            0xFF24B1EC
	#define PalSkyDark_Value              0xFF45C9EE
	#define PalSky_Value                  0xFF66D9EF
	#define PalSkyLight_Value             0xFF99E1DE
	#define PalSkyLighter_Value           0xFFCDEDDB
	#define PalPurpleDarker_Value         0xFF7445FF
	#define PalPurpleDark_Value           0xFF9161FF
	#define PalPurple_Value               0xFFAE81FF
	#define PalPurpleLight_Value          0xFFCBAAEA
	#define PalPurpleLighter_Value        0xFFD6BBEE
	#define PalVioletDarker_Value         0xFFC038FF
	#define PalVioletDark_Value           0xFFC95BFF
	#define PalViolet_Value               0xFFDD7BFF
	#define PalVioletLight_Value          0xFFE595FF
	#define PalVioletLighter_Value        0xFFEBAAFF
	#define PalNeutralDarker_Value        0xFF5E5E4A
	#define PalNeutralDark_Value          0xFF75715E
	#define PalNeutral_Value              0xFF908861
	#define PalNeutralLight_Value         0xFFA79C65
	#define PalNeutralLighter_Value       0xFFB9B084
	
	#define PalBackgroundDarker_Value     0xFF131410
	#define PalBackgroundDark_Value       0xFF1F221A
	#define PalBackground_Value           0xFF272822
	#define PalBackgroundLight_Value      0xFF424431
	#define PalBackgroundLighter_Value    0xFF545934
	#define PalGreyDarker_Value           0xFF64635B
	#define PalGreyDark_Value             0xFF707067
	#define PalGrey_Value                 0xFF7D7C72
	#define PalGreyLight_Value            0xFF98978E
	#define PalGreyLighter_Value          0xFFB1B1AA
	#define PalBlackDarker_Value          0xFF121211
	#define PalBlackDark_Value            0xFF1E1E1D
	#define PalBlack_Value                0xFF262624
	#define PalBlackLight_Value           0xFF3C3C39
	#define PalBlackLighter_Value         0xFF53534E
	
	#define PalRedDarker            NewColor(PalRedDarker_Value)
	#define PalRedDark              NewColor(PalRedDark_Value)
	#define PalRed                  NewColor(PalRed_Value)
	#define PalRedLight             NewColor(PalRedLight_Value)
	#define PalRedLighter           NewColor(PalRedLighter_Value)
	#define PalPinkDarker           NewColor(PalPinkDarker_Value)
	#define PalPinkDark             NewColor(PalPinkDark_Value)
	#define PalPink                 NewColor(PalPink_Value)
	#define PalPinkLight            NewColor(PalPinkLight_Value)
	#define PalPinkLighter          NewColor(PalPinkLighter_Value)
	#define PalOrangeDarker         NewColor(PalOrangeDarker_Value)
	#define PalOrangeDark           NewColor(PalOrangeDark_Value)
	#define PalOrange               NewColor(PalOrange_Value)
	#define PalOrangeLight          NewColor(PalOrangeLight_Value)
	#define PalOrangeLighter        NewColor(PalOrangeLighter_Value)
	#define PalHoneyDarker          NewColor(PalHoneyDarker_Value)
	#define PalHoneyDark            NewColor(PalHoneyDark_Value)
	#define PalHoney                NewColor(PalHoney_Value)
	#define PalHoneyLight           NewColor(PalHoneyLight_Value)
	#define PalHoneyLighter         NewColor(PalHoneyLighter_Value)
	#define PalYellowDarker         NewColor(PalYellowDarker_Value)
	#define PalYellowDark           NewColor(PalYellowDark_Value)
	#define PalYellow               NewColor(PalYellow_Value)
	#define PalYellowLight          NewColor(PalYellowLight_Value)
	#define PalYellowLighter        NewColor(PalYellowLighter_Value)
	#define PalBananaDarker         NewColor(PalBananaDarker_Value)
	#define PalBananaDark           NewColor(PalBananaDark_Value)
	#define PalBanana               NewColor(PalBanana_Value)
	#define PalBananaLight          NewColor(PalBananaLight_Value)
	#define PalBananaLighter        NewColor(PalBananaLighter_Value)
	#define PalGreenDarker          NewColor(PalGreenDarker_Value)
	#define PalGreenDark            NewColor(PalGreenDark_Value)
	#define PalGreen                NewColor(PalGreen_Value)
	#define PalGreenLight           NewColor(PalGreenLight_Value)
	#define PalGreenLighter         NewColor(PalGreenLighter_Value)
	#define PalGrassDarker          NewColor(PalGrassDarker_Value)
	#define PalGrassDark            NewColor(PalGrassDark_Value)
	#define PalGrass                NewColor(PalGrass_Value)
	#define PalGrassLight           NewColor(PalGrassLight_Value)
	#define PalGrassLighter         NewColor(PalGrassLighter_Value)
	#define PalBlueDarker           NewColor(PalBlueDarker_Value)
	#define PalBlueDark             NewColor(PalBlueDark_Value)
	#define PalBlue                 NewColor(PalBlue_Value)
	#define PalBlueLight            NewColor(PalBlueLight_Value)
	#define PalBlueLighter          NewColor(PalBlueLighter_Value)
	#define PalSkyDarker            NewColor(PalSkyDarker_Value)
	#define PalSkyDark              NewColor(PalSkyDark_Value)
	#define PalSky                  NewColor(PalSky_Value)
	#define PalSkyLight             NewColor(PalSkyLight_Value)
	#define PalSkyLighter           NewColor(PalSkyLighter_Value)
	#define PalPurpleDarker         NewColor(PalPurpleDarker_Value)
	#define PalPurpleDark           NewColor(PalPurpleDark_Value)
	#define PalPurple               NewColor(PalPurple_Value)
	#define PalPurpleLight          NewColor(PalPurpleLight_Value)
	#define PalPurpleLighter        NewColor(PalPurpleLighter_Value)
	#define PalVioletDarker         NewColor(PalVioletDarker_Value)
	#define PalVioletDark           NewColor(PalVioletDark_Value)
	#define PalViolet               NewColor(PalViolet_Value)
	#define PalVioletLight          NewColor(PalVioletLight_Value)
	#define PalVioletLighter        NewColor(PalVioletLighter_Value)
	#define PalNeutralDarker        NewColor(PalNeutralDarker_Value)
	#define PalNeutralDark          NewColor(PalNeutralDark_Value)
	#define PalNeutral              NewColor(PalNeutral_Value)
	#define PalNeutralLight         NewColor(PalNeutralLight_Value)
	#define PalNeutralLighter       NewColor(PalNeutralLighter_Value)
	
	#define PalBackgroundDarker     NewColor(PalBackgroundDarker_Value)
	#define PalBackgroundDark       NewColor(PalBackgroundDark_Value)
	#define PalBackground           NewColor(PalBackground_Value)
	#define PalBackgroundLight      NewColor(PalBackgroundLight_Value)
	#define PalBackgroundLighter    NewColor(PalBackgroundLighter_Value)
	#define PalGreyDarker           NewColor(PalGreyDarker_Value)
	#define PalGreyDark             NewColor(PalGreyDark_Value)
	#define PalGrey                 NewColor(PalGrey_Value)
	#define PalGreyLight            NewColor(PalGreyLight_Value)
	#define PalGreyLighter          NewColor(PalGreyLighter_Value)
	#define PalBlackDarker          NewColor(PalBlackDarker_Value)
	#define PalBlackDark            NewColor(PalBlackDark_Value)
	#define PalBlack                NewColor(PalBlack_Value)
	#define PalBlackLight           NewColor(PalBlackLight_Value)
	#define PalBlackLighter         NewColor(PalBlackLighter_Value)
// +--------------------------------------------------------------+

// +--------------------------------------------------------------+
// |                         Named Colors                         |
// +--------------------------------------------------------------+
	#define NUM_PREDEF_NAMED_COLORS 161

	#define ColorAliceBlue_Value         0xFFF0F8FF
	#define ColorAntiqueWhite_Value      0xFFFAEBD7
	#define ColorAquamarine_Value        0xFF7FFFD4
	#define ColorAzure_Value             0xFFF0FFFF
	#define ColorBanana_Value            0xFFE3CF57
	#define ColorBeet_Value              0xFF8E388E
	#define ColorBeige_Value             0xFFF5F5DC
	#define ColorBisque_Value            0xFFFFE4C4
	#define ColorBlack_Value             0xFF000000
	#define ColorBlancheDalmond_Value    0xFFFFEBCD
	#define ColorBlue_Value              0xFF0000FF
	#define ColorBlueViolet_Value        0xFF8A2BE2
	#define ColorBrick_Value             0xFF9C661F
	#define ColorBrown_Value             0xFFA52A2A
	#define ColorBurlyWood_Value         0xFFDEB887
	#define ColorBurntSienna_Value       0xFF8A360F
	#define ColorBurnTumber_Value        0xFF8A3324
	#define ColorCadetBlue_Value         0xFF5F9EA0
	#define ColorCadmiumOrange_Value     0xFFFF6103
	#define ColorCadmiumYellow_Value     0xFFFF9912
	#define ColorCarrot_Value            0xFFED9121
	#define ColorChartreuse_Value        0xFF7FFF00
	#define ColorChocolate_Value         0xFFD2691E
	#define ColorCobalt_Value            0xFF3D59AB
	#define ColorCobaltGreen_Value       0xFF3D9140
	#define ColorColdGrey_Value          0xFF808A87
	#define ColorCoral_Value             0xFFFF7F50
	#define ColorCornflowerBlue_Value    0xFF6495ED
	#define ColorCornSilk_Value          0xFFFFF8DC
	#define ColorCrimson_Value           0xFFDC143C
	#define ColorCyan_Value              0xFF00FFFF
	#define ColorDarkCyan_Value          0xFF008B8B
	#define ColorDarkGoldenRod_Value     0xFFB8860B
	#define ColorDarkGrey_Value          0xFFA9A9A9
	#define ColorDarkGreen_Value         0xFF006400
	#define ColorDarkKhaki_Value         0xFFBDB76B
	#define ColorDarkOliveGreen_Value    0xFF556B2F
	#define ColorDarkOrange_Value        0xFFFF8C00
	#define ColorDarkOrchid_Value        0xFF9932CC
	#define ColorDarkRed_Value           0xFF8B0000
	#define ColorDarkSalmon_Value        0xFFE9967A
	#define ColorDarkSeaGreen_Value      0xFF8FBC8F
	#define ColorDarkSlateBlue_Value     0xFF483D8B
	#define ColorDarkSlateGrey_Value     0xFF2F4F4F
	#define ColorDarkTurquoise_Value     0xFF00CED1
	#define ColorDarkViolet_Value        0xFF9400D3
	#define ColorDeepPink_Value          0xFFFF1493
	#define ColorDeepSkyBlue_Value       0xFF00BFFF
	#define ColorDimGrey_Value           0xFF696969
	#define ColorDodgerBlue_Value        0xFF1E90FF
	#define ColorEggShell_Value          0xFFFCE6C9
	#define ColorEmeraldGreen_Value      0xFF00C957
	#define ColorFireBrick_Value         0xFFB22222
	#define ColorFlesh_Value             0xFFFF7D40
	#define ColorFloralWhite_Value       0xFFFFFAF0
	#define ColorForestGreen_Value       0xFF228B22
	#define ColorGainsboro_Value         0xFFDCDCDC
	#define ColorGhostWhite_Value        0xFFF8F8FF
	#define ColorGold_Value              0xFFFFD700
	#define ColorGoldenRod_Value         0xFFDAA520
	#define ColorGrey_Value              0xFF808080
	#define ColorGreen_Value             0xFF008000
	#define ColorGreenYellow_Value       0xFFADFF2F
	#define ColorHoneydew_Value          0xFFF0FFF0
	#define ColorHotPink_Value           0xFFFF69B4
	#define ColorIndianRed_Value         0xFFB0171F
	#define ColorIndigo_Value            0xFF4B0082
	#define ColorIvory_Value             0xFFFFFFF0
	#define ColorIvoryBlack_Value        0xFF292421
	#define ColorKhaki_Value             0xFFF0E68C
	#define ColorLavender_Value          0xFFE6E6FA
	#define ColorLawnGreen_Value         0xFF7CFC00
	#define ColorLemonChiffon_Value      0xFFFFFACD
	#define ColorLightBlue_Value         0xFFADD8E6
	#define ColorLightCoral_Value        0xFFF08080
	#define ColorLightCyan_Value         0xFFE0FFFF
	#define ColorLightGoldenRod_Value    0xFFFFEC8B
	#define ColorLightGreen_Value        0xFF90EE90
	#define ColorLightGrey_Value         0xFFD3D3D3
	#define ColorLightPink_Value         0xFFFFB6C1
	#define ColorLightSalmon_Value       0xFFFFA07A
	#define ColorLightSeaGreen_Value     0xFF20B2AA
	#define ColorLightSkyBlue_Value      0xFF87CEFA
	#define ColorLightSlateBlue_Value    0xFF8470FF
	#define ColorLightSlateGrey_Value    0xFF778899
	#define ColorLightSteelBlue_Value    0xFFB0C4DE
	#define ColorLightYellow_Value       0xFFFFFFE0
	#define ColorLime_Value              0xFF00FF00
	#define ColorLimeGreen_Value         0xFF32CD32
	#define ColorLinen_Value             0xFFFAF0E6
	#define ColorMagenta_Value           0xFFFF00FF
	#define ColorManganeseBlue_Value     0xFF03A89E
	#define ColorMaroon_Value            0xFFFF34B3
	#define ColorMediumOrchid_Value      0xFFBA55D3
	#define ColorMediumPurple_Value      0xFF9370DB
	#define ColorMediumSeaGreen_Value    0xFF3CB371
	#define ColorMediumSlateBlue_Value   0xFF7B68EE
	#define ColorMediumSpringGreen_Value 0xFF00FA9A
	#define ColorMediumTurquoise_Value   0xFF48D1CC
	#define ColorMediumVioletRed_Value   0xFFC71585
	#define ColorMelon_Value             0xFFE3A869
	#define ColorMidnightBlue_Value      0xFF191970
	#define ColorMint_Value              0xFFBDFCC9
	#define ColorMintCream_Value         0xFFF5FFFA
	#define ColorMistyRose_Value         0xFFFFE4E1
	#define ColorMoccasin_Value          0xFFFFE4B5
	#define ColorNavajoWhite_Value       0xFFFFDEAD
	#define ColorNavy_Value              0xFF000080
	#define ColorOldLace_Value           0xFFFDF5E6
	#define ColorOlive_Value             0xFF808000
	#define ColorOliveDrab_Value         0xFF6B8E23
	#define ColorOrange_Value            0xFFFFA500
	#define ColorOrangeRed_Value         0xFFFF4500
	#define ColorOrchid_Value            0xFFDA70D6
	#define ColorPaleGoldenRod_Value     0xFFEEE8AA
	#define ColorPaleGreen_Value         0xFF98FB98
	#define ColorPaleTurquoise_Value     0xFFAEEEEE
	#define ColorPaleVioletRed_Value     0xFFDB7093
	#define ColorPapayaWhip_Value        0xFFFFEFD5
	#define ColorPeachPuff_Value         0xFFFFDAB9
	#define ColorPeacock_Value           0xFF33A1C9
	#define ColorPeru_Value              0xFFCD853F
	#define ColorPink_Value              0xFFFFC0CB
	#define ColorPlum_Value              0xFFDDA0DD
	#define ColorPowderBlue_Value        0xFFB0E0E6
	#define ColorPurple_Value            0xFF800080
	#define ColorRaspberry_Value         0xFF872657
	#define ColorRawSienna_Value         0xFFC76114
	#define ColorRed_Value               0xFFFF0000
	#define ColorRosyBrown_Value         0xFFBC8F8F
	#define ColorRoyalBlue_Value         0xFF4169E1
	#define ColorSaddleBrown_Value       0xFF8B4513
	#define ColorSalmon_Value            0xFFFA8072
	#define ColorSandyBrown_Value        0xFFF4A460
	#define ColorSapGreen_Value          0xFF308014
	#define ColorSeaGreen_Value          0xFF2E8B57
	#define ColorSeashell_Value          0xFFFFF5EE
	#define ColorSepia_Value             0xFF5E2612
	#define ColorSienna_Value            0xFFA0522D
	#define ColorSilver_Value            0xFFC0C0C0
	#define ColorSkyBlue_Value           0xFF87CEEB
	#define ColorSlateBlue_Value         0xFF6A5ACD
	#define ColorSlateGrey_Value         0xFF708090
	#define ColorSnow_Value              0xFFFFFAFA
	#define ColorSpringGreen_Value       0xFF00FF7F
	#define ColorSteelBlue_Value         0xFF4682B4
	#define ColorSunlight_Value          0xFFEEDCA5
	#define ColorTan_Value               0xFFD2B48C
	#define ColorTeal_Value              0xFF008080
	#define ColorThistle_Value           0xFFD8BFD8
	#define ColorTomato_Value            0xFFFF6347
	#define ColorTurquoise_Value         0xFF40E0D0
	#define ColorTurquoiseBlue_Value     0xFF00C78C
	#define ColorViolet_Value            0xFFEE82EE
	#define ColorVioletRed_Value         0xFFD02090
	#define ColorWarmGrey_Value          0xFF808069
	#define ColorWheat_Value             0xFFF5DEB3
	#define ColorWhite_Value             0xFFFFFFFF
	#define ColorWhiteSmoke_Value        0xFFF5F5F5
	#define ColorYellow_Value            0xFFFFFF00
	#define ColorYellowGreen_Value       0xFF9ACD32
	
	#define ColorAliceBlue               NewColor(ColorAliceBlue_Value)
	#define ColorAntiqueWhite            NewColor(ColorAntiqueWhite_Value)
	#define ColorAquamarine              NewColor(ColorAquamarine_Value)
	#define ColorAzure                   NewColor(ColorAzure_Value)
	#define ColorBanana                  NewColor(ColorBanana_Value)
	#define ColorBeet                    NewColor(ColorBeet_Value)
	#define ColorBeige                   NewColor(ColorBeige_Value)
	#define ColorBisque                  NewColor(ColorBisque_Value)
	#define ColorBlack                   NewColor(ColorBlack_Value)
	#define ColorBlancheDalmond          NewColor(ColorBlancheDalmond_Value)
	#define ColorBlue                    NewColor(ColorBlue_Value)
	#define ColorBlueViolet              NewColor(ColorBlueViolet_Value)
	#define ColorBrick                   NewColor(ColorBrick_Value)
	#define ColorBrown                   NewColor(ColorBrown_Value)
	#define ColorBurlyWood               NewColor(ColorBurlyWood_Value)
	#define ColorBurntSienna             NewColor(ColorBurntSienna_Value)
	#define ColorBurnTumber              NewColor(ColorBurnTumber_Value)
	#define ColorCadetBlue               NewColor(ColorCadetBlue_Value)
	#define ColorCadmiumOrange           NewColor(ColorCadmiumOrange_Value)
	#define ColorCadmiumYellow           NewColor(ColorCadmiumYellow_Value)
	#define ColorCarrot                  NewColor(ColorCarrot_Value)
	#define ColorChartreuse              NewColor(ColorChartreuse_Value)
	#define ColorChocolate               NewColor(ColorChocolate_Value)
	#define ColorCobalt                  NewColor(ColorCobalt_Value)
	#define ColorCobaltGreen             NewColor(ColorCobaltGreen_Value)
	#define ColorColdGrey                NewColor(ColorColdGrey_Value)
	#define ColorCoral                   NewColor(ColorCoral_Value)
	#define ColorCornflowerBlue          NewColor(ColorCornflowerBlue_Value)
	#define ColorCornSilk                NewColor(ColorCornSilk_Value)
	#define ColorCrimson                 NewColor(ColorCrimson_Value)
	#define ColorCyan                    NewColor(ColorCyan_Value)
	#define ColorDarkCyan                NewColor(ColorDarkCyan_Value)
	#define ColorDarkGoldenRod           NewColor(ColorDarkGoldenRod_Value)
	#define ColorDarkGrey                NewColor(ColorDarkGrey_Value)
	#define ColorDarkGreen               NewColor(ColorDarkGreen_Value)
	#define ColorDarkKhaki               NewColor(ColorDarkKhaki_Value)
	#define ColorDarkOliveGreen          NewColor(ColorDarkOliveGreen_Value)
	#define ColorDarkOrange              NewColor(ColorDarkOrange_Value)
	#define ColorDarkOrchid              NewColor(ColorDarkOrchid_Value)
	#define ColorDarkRed                 NewColor(ColorDarkRed_Value)
	#define ColorDarkSalmon              NewColor(ColorDarkSalmon_Value)
	#define ColorDarkSeaGreen            NewColor(ColorDarkSeaGreen_Value)
	#define ColorDarkSlateBlue           NewColor(ColorDarkSlateBlue_Value)
	#define ColorDarkSlateGrey           NewColor(ColorDarkSlateGrey_Value)
	#define ColorDarkTurquoise           NewColor(ColorDarkTurquoise_Value)
	#define ColorDarkViolet              NewColor(ColorDarkViolet_Value)
	#define ColorDeepPink                NewColor(ColorDeepPink_Value)
	#define ColorDeepSkyBlue             NewColor(ColorDeepSkyBlue_Value)
	#define ColorDimGrey                 NewColor(ColorDimGrey_Value)
	#define ColorDodgerBlue              NewColor(ColorDodgerBlue_Value)
	#define ColorEggShell                NewColor(ColorEggShell_Value)
	#define ColorEmeraldGreen            NewColor(ColorEmeraldGreen_Value)
	#define ColorFireBrick               NewColor(ColorFireBrick_Value)
	#define ColorFlesh                   NewColor(ColorFlesh_Value)
	#define ColorFloralWhite             NewColor(ColorFloralWhite_Value)
	#define ColorForestGreen             NewColor(ColorForestGreen_Value)
	#define ColorGainsboro               NewColor(ColorGainsboro_Value)
	#define ColorGhostWhite              NewColor(ColorGhostWhite_Value)
	#define ColorGold                    NewColor(ColorGold_Value)
	#define ColorGoldenRod               NewColor(ColorGoldenRod_Value)
	#define ColorGrey                    NewColor(ColorGrey_Value)
	#define ColorGreen                   NewColor(ColorGreen_Value)
	#define ColorGreenYellow             NewColor(ColorGreenYellow_Value)
	#define ColorHoneydew                NewColor(ColorHoneydew_Value)
	#define ColorHotPink                 NewColor(ColorHotPink_Value)
	#define ColorIndianRed               NewColor(ColorIndianRed_Value)
	#define ColorIndigo                  NewColor(ColorIndigo_Value)
	#define ColorIvory                   NewColor(ColorIvory_Value)
	#define ColorIvoryBlack              NewColor(ColorIvoryBlack_Value)
	#define ColorKhaki                   NewColor(ColorKhaki_Value)
	#define ColorLavender                NewColor(ColorLavender_Value)
	#define ColorLawnGreen               NewColor(ColorLawnGreen_Value)
	#define ColorLemonChiffon            NewColor(ColorLemonChiffon_Value)
	#define ColorLightBlue               NewColor(ColorLightBlue_Value)
	#define ColorLightCoral              NewColor(ColorLightCoral_Value)
	#define ColorLightCyan               NewColor(ColorLightCyan_Value)
	#define ColorLightGoldenRod          NewColor(ColorLightGoldenRod_Value)
	#define ColorLightGreen              NewColor(ColorLightGreen_Value)
	#define ColorLightGrey               NewColor(ColorLightGrey_Value)
	#define ColorLightPink               NewColor(ColorLightPink_Value)
	#define ColorLightSalmon             NewColor(ColorLightSalmon_Value)
	#define ColorLightSeaGreen           NewColor(ColorLightSeaGreen_Value)
	#define ColorLightSkyBlue            NewColor(ColorLightSkyBlue_Value)
	#define ColorLightSlateBlue          NewColor(ColorLightSlateBlue_Value)
	#define ColorLightSlateGrey          NewColor(ColorLightSlateGrey_Value)
	#define ColorLightSteelBlue          NewColor(ColorLightSteelBlue_Value)
	#define ColorLightYellow             NewColor(ColorLightYellow_Value)
	#define ColorLime                    NewColor(ColorLime_Value)
	#define ColorLimeGreen               NewColor(ColorLimeGreen_Value)
	#define ColorLinen                   NewColor(ColorLinen_Value)
	#define ColorMagenta                 NewColor(ColorMagenta_Value)
	#define ColorManganeseBlue           NewColor(ColorManganeseBlue_Value)
	#define ColorMaroon                  NewColor(ColorMaroon_Value)
	#define ColorMediumOrchid            NewColor(ColorMediumOrchid_Value)
	#define ColorMediumPurple            NewColor(ColorMediumPurple_Value)
	#define ColorMediumSeaGreen          NewColor(ColorMediumSeaGreen_Value)
	#define ColorMediumSlateBlue         NewColor(ColorMediumSlateBlue_Value)
	#define ColorMediumSpringGreen       NewColor(ColorMediumSpringGreen_Value)
	#define ColorMediumTurquoise         NewColor(ColorMediumTurquoise_Value)
	#define ColorMediumVioletRed         NewColor(ColorMediumVioletRed_Value)
	#define ColorMelon                   NewColor(ColorMelon_Value)
	#define ColorMidnightBlue            NewColor(ColorMidnightBlue_Value)
	#define ColorMint                    NewColor(ColorMint_Value)
	#define ColorMintCream               NewColor(ColorMintCream_Value)
	#define ColorMistyRose               NewColor(ColorMistyRose_Value)
	#define ColorMoccasin                NewColor(ColorMoccasin_Value)
	#define ColorNavajoWhite             NewColor(ColorNavajoWhite_Value)
	#define ColorNavy                    NewColor(ColorNavy_Value)
	#define ColorOldLace                 NewColor(ColorOldLace_Value)
	#define ColorOlive                   NewColor(ColorOlive_Value)
	#define ColorOliveDrab               NewColor(ColorOliveDrab_Value)
	#define ColorOrange                  NewColor(ColorOrange_Value)
	#define ColorOrangeRed               NewColor(ColorOrangeRed_Value)
	#define ColorOrchid                  NewColor(ColorOrchid_Value)
	#define ColorPaleGoldenRod           NewColor(ColorPaleGoldenRod_Value)
	#define ColorPaleGreen               NewColor(ColorPaleGreen_Value)
	#define ColorPaleTurquoise           NewColor(ColorPaleTurquoise_Value)
	#define ColorPaleVioletRed           NewColor(ColorPaleVioletRed_Value)
	#define ColorPapayaWhip              NewColor(ColorPapayaWhip_Value)
	#define ColorPeachPuff               NewColor(ColorPeachPuff_Value)
	#define ColorPeacock                 NewColor(ColorPeacock_Value)
	#define ColorPeru                    NewColor(ColorPeru_Value)
	#define ColorPink                    NewColor(ColorPink_Value)
	#define ColorPlum                    NewColor(ColorPlum_Value)
	#define ColorPowderBlue              NewColor(ColorPowderBlue_Value)
	#define ColorPurple                  NewColor(ColorPurple_Value)
	#define ColorRaspberry               NewColor(ColorRaspberry_Value)
	#define ColorRawSienna               NewColor(ColorRawSienna_Value)
	#define ColorRed                     NewColor(ColorRed_Value)
	#define ColorRosyBrown               NewColor(ColorRosyBrown_Value)
	#define ColorRoyalBlue               NewColor(ColorRoyalBlue_Value)
	#define ColorSaddleBrown             NewColor(ColorSaddleBrown_Value)
	#define ColorSalmon                  NewColor(ColorSalmon_Value)
	#define ColorSandyBrown              NewColor(ColorSandyBrown_Value)
	#define ColorSapGreen                NewColor(ColorSapGreen_Value)
	#define ColorSeaGreen                NewColor(ColorSeaGreen_Value)
	#define ColorSeashell                NewColor(ColorSeashell_Value)
	#define ColorSepia                   NewColor(ColorSepia_Value)
	#define ColorSienna                  NewColor(ColorSienna_Value)
	#define ColorSilver                  NewColor(ColorSilver_Value)
	#define ColorSkyBlue                 NewColor(ColorSkyBlue_Value)
	#define ColorSlateBlue               NewColor(ColorSlateBlue_Value)
	#define ColorSlateGrey               NewColor(ColorSlateGrey_Value)
	#define ColorSnow                    NewColor(ColorSnow_Value)
	#define ColorSpringGreen             NewColor(ColorSpringGreen_Value)
	#define ColorSteelBlue               NewColor(ColorSteelBlue_Value)
	#define ColorSunlight                NewColor(ColorSunlight_Value)
	#define ColorTan                     NewColor(ColorTan_Value)
	#define ColorTeal                    NewColor(ColorTeal_Value)
	#define ColorThistle                 NewColor(ColorThistle_Value)
	#define ColorTomato                  NewColor(ColorTomato_Value)
	#define ColorTurquoise               NewColor(ColorTurquoise_Value)
	#define ColorTurquoiseBlue           NewColor(ColorTurquoiseBlue_Value)
	#define ColorViolet                  NewColor(ColorViolet_Value)
	#define ColorVioletRed               NewColor(ColorVioletRed_Value)
	#define ColorWarmGrey                NewColor(ColorWarmGrey_Value)
	#define ColorWheat                   NewColor(ColorWheat_Value)
	#define ColorWhite                   NewColor(ColorWhite_Value)
	#define ColorWhiteSmoke              NewColor(ColorWhiteSmoke_Value)
	#define ColorYellow                  NewColor(ColorYellow_Value)
	#define ColorYellowGreen             NewColor(ColorYellowGreen_Value)
// +--------------------------------------------------------------+

const char* GetColorName(u32 colorValue)
{
	switch (colorValue)
	{
		case PalRedDarker_Value:         return "Pal Red Darker";
		case PalRedDark_Value:           return "Pal Red Dark";
		case PalRed_Value:               return "Pal Red";
		case PalRedLight_Value:          return "Pal Red Light";
		case PalRedLighter_Value:        return "Pal Red Lighter";
		case PalPinkDarker_Value:        return "Pal Pink Darker";
		case PalPinkDark_Value:          return "Pal Pink Dark";
		case PalPink_Value:              return "Pal Pink";
		case PalPinkLight_Value:         return "Pal Pink Light";
		case PalPinkLighter_Value:       return "Pal Pink Lighter";
		case PalOrangeDarker_Value:      return "Pal Orange Darker";
		case PalOrangeDark_Value:        return "Pal Orange Dark";
		case PalOrange_Value:            return "Pal Orange";
		case PalOrangeLight_Value:       return "Pal Orange Light";
		case PalOrangeLighter_Value:     return "Pal Orange Lighter";
		case PalHoneyDarker_Value:       return "Pal Honey Darker";
		case PalHoneyDark_Value:         return "Pal Honey Dark";
		case PalHoney_Value:             return "Pal Honey";
		case PalHoneyLight_Value:        return "Pal Honey Light";
		case PalHoneyLighter_Value:      return "Pal Honey Lighter";
		case PalYellowDarker_Value:      return "Pal Yellow Darker";
		case PalYellowDark_Value:        return "Pal Yellow Dark";
		case PalYellow_Value:            return "Pal Yellow";
		case PalYellowLight_Value:       return "Pal Yellow Light";
		case PalYellowLighter_Value:     return "Pal Yellow Lighter";
		case PalBananaDarker_Value:      return "Pal Banana Darker";
		case PalBananaDark_Value:        return "Pal Banana Dark";
		case PalBanana_Value:            return "Pal Banana";
		case PalBananaLight_Value:       return "Pal Banana Light";
		case PalBananaLighter_Value:     return "Pal Banana Lighter";
		case PalGreenDarker_Value:       return "Pal Green Darker";
		case PalGreenDark_Value:         return "Pal Green Dark";
		case PalGreen_Value:             return "Pal Green";
		case PalGreenLight_Value:        return "Pal Green Light";
		case PalGreenLighter_Value:      return "Pal Green Lighter";
		case PalGrassDarker_Value:       return "Pal Grass Darker";
		case PalGrassDark_Value:         return "Pal Grass Dark";
		case PalGrass_Value:             return "Pal Grass";
		case PalGrassLight_Value:        return "Pal Grass Light";
		case PalGrassLighter_Value:      return "Pal Grass Lighter";
		case PalBlueDarker_Value:        return "Pal Blue Darker";
		case PalBlueDark_Value:          return "Pal Blue Dark";
		case PalBlue_Value:              return "Pal Blue";
		case PalBlueLight_Value:         return "Pal Blue Light";
		case PalBlueLighter_Value:       return "Pal Blue Lighter";
		case PalSkyDarker_Value:         return "Pal Sky Darker";
		case PalSkyDark_Value:           return "Pal Sky Dark";
		case PalSky_Value:               return "Pal Sky";
		case PalSkyLight_Value:          return "Pal Sky Light";
		case PalSkyLighter_Value:        return "Pal Sky Lighter";
		case PalPurpleDarker_Value:      return "Pal Purple Darker";
		case PalPurpleDark_Value:        return "Pal Purple Dark";
		case PalPurple_Value:            return "Pal Purple";
		case PalPurpleLight_Value:       return "Pal Purple Light";
		case PalPurpleLighter_Value:     return "Pal Purple Lighter";
		case PalVioletDarker_Value:      return "Pal Violet Darker";
		case PalVioletDark_Value:        return "Pal Violet Dark";
		case PalViolet_Value:            return "Pal Violet";
		case PalVioletLight_Value:       return "Pal Violet Light";
		case PalVioletLighter_Value:     return "Pal Violet Lighter";
		case PalNeutralDarker_Value:     return "Pal Neutral Darker";
		case PalNeutralDark_Value:       return "Pal Neutral Dark";
		case PalNeutral_Value:           return "Pal Neutral";
		case PalNeutralLight_Value:      return "Pal Neutral Light";
		case PalNeutralLighter_Value:    return "Pal Neutral Lighter";
		case PalBackgroundDarker_Value:  return "Pal Background Darker";
		case PalBackgroundDark_Value:    return "Pal Background Dark";
		case PalBackground_Value:        return "Pal Background";
		case PalBackgroundLight_Value:   return "Pal Background Light";
		case PalBackgroundLighter_Value: return "Pal Background Lighter";
		case PalGreyDarker_Value:        return "Pal Grey Darker";
		case PalGreyDark_Value:          return "Pal Grey Dark";
		case PalGrey_Value:              return "Pal Grey";
		case PalGreyLight_Value:         return "Pal Grey Light";
		case PalGreyLighter_Value:       return "Pal Grey Lighter";
		case PalBlackDarker_Value:       return "Pal Black Darker";
		case PalBlackDark_Value:         return "Pal Black Dark";
		case PalBlack_Value:             return "Pal Black";
		case PalBlackLight_Value:        return "Pal Black Light";
		case PalBlackLighter_Value:      return "Pal Black Lighter";
		
		case NoColor_Value:              return "No Color";
		case Transparent_Value:          return "Transparent";
		case Black_Value:                return "Black";
		case Grey1_Value:                return "Grey 1";
		case Grey2_Value:                return "Grey 2";
		case Grey3_Value:                return "Grey 3";
		case Grey4_Value:                return "Grey 4";
		case Grey5_Value:                return "Grey 5";
		case Grey6_Value:                return "Grey 6";
		case Grey7_Value:                return "Grey 7";
		case Grey8_Value:                return "Grey 8";
		case Grey9_Value:                return "Grey 9";
		case Grey10_Value:               return "Grey 10";
		case Grey11_Value:               return "Grey 11";
		case Grey12_Value:               return "Grey 12";
		case Grey13_Value:               return "Grey 13";
		case Grey14_Value:               return "Grey 14";
		case White_Value:                return "White";
		case PureRed_Value:              return "Pure Red";
		case PureOrange_Value:           return "Pure Orange";
		case PureYellow_Value:           return "Pure Yellow";
		case PureGreen_Value:            return "Pure Green";
		case PureBlue_Value:             return "Pure Blue";
		case PurePurple_Value:           return "Pure Purple";
		
		case ColorAliceBlue_Value:        return "Alice Blue";
		case ColorAntiqueWhite_Value:     return "Antique White";
		case ColorAquamarine_Value:       return "Aquamarine";
		case ColorAzure_Value:            return "Azure";
		case ColorBanana_Value:           return "Banana";
		case ColorBeet_Value:             return "Beet";
		case ColorBeige_Value:            return "Beige";
		case ColorBisque_Value:           return "Bisque";
		// case ColorBlack_Value:            return "Black";
		case ColorBlancheDalmond_Value:   return "Blanche Dalmond";
		// case ColorBlue_Value:             return "Blue";
		case ColorBlueViolet_Value:       return "Blue Violet";
		case ColorBrick_Value:            return "Brick";
		case ColorBrown_Value:            return "Brown";
		case ColorBurlyWood_Value:        return "Burly Wood";
		case ColorBurntSienna_Value:      return "Burnt Sienna";
		case ColorBurnTumber_Value:       return "Burn Tumber";
		case ColorCadetBlue_Value:        return "Cadet Blue";
		case ColorCadmiumOrange_Value:    return "Cadmium Orange";
		case ColorCadmiumYellow_Value:    return "Cadmium Yellow";
		case ColorCarrot_Value:           return "Carrot";
		case ColorChartreuse_Value:       return "Chartreuse";
		case ColorChocolate_Value:        return "Chocolate";
		case ColorCobalt_Value:           return "Cobalt";
		case ColorCobaltGreen_Value:      return "Cobalt Green";
		case ColorColdGrey_Value:         return "Cold Grey";
		case ColorCoral_Value:            return "Coral";
		case ColorCornflowerBlue_Value:   return "Cornflower Blue";
		case ColorCornSilk_Value:         return "Corn Silk";
		case ColorCrimson_Value:          return "Crimson";
		case ColorCyan_Value:             return "Cyan";
		case ColorDarkCyan_Value:         return "Dark Cyan";
		case ColorDarkGoldenRod_Value:    return "Dark Golden Rod";
		case ColorDarkGrey_Value:         return "Dark Grey";
		case ColorDarkGreen_Value:        return "Dark Green";
		case ColorDarkKhaki_Value:        return "Dark Khaki";
		case ColorDarkOliveGreen_Value:   return "Dark Olive Green";
		case ColorDarkOrange_Value:       return "Dark Orange";
		case ColorDarkOrchid_Value:       return "Dark Orchid";
		case ColorDarkRed_Value:          return "Dark Red";
		case ColorDarkSalmon_Value:       return "Dark Salmon";
		case ColorDarkSeaGreen_Value:     return "Dark Sea Green";
		case ColorDarkSlateBlue_Value:    return "Dark Slate Blue";
		case ColorDarkSlateGrey_Value:    return "Dark Slate Grey";
		case ColorDarkTurquoise_Value:    return "Dark Turquoise";
		case ColorDarkViolet_Value:       return "Dark Violet";
		case ColorDeepPink_Value:         return "Deep Pink";
		case ColorDeepSkyBlue_Value:      return "Deep Sky Blue";
		case ColorDimGrey_Value:          return "Dim Grey";
		case ColorDodgerBlue_Value:       return "Dodger Blue";
		case ColorEggShell_Value:         return "Egg Shell";
		case ColorEmeraldGreen_Value:     return "Emerald Green";
		case ColorFireBrick_Value:        return "Fire Brick";
		case ColorFlesh_Value:            return "Flesh";
		case ColorFloralWhite_Value:      return "Floral White";
		case ColorForestGreen_Value:      return "Forest Green";
		case ColorGainsboro_Value:        return "Gainsboro";
		case ColorGhostWhite_Value:       return "Ghost White";
		case ColorGold_Value:             return "Gold";
		case ColorGoldenRod_Value:        return "Golden Rod";
		case ColorGrey_Value:             return "Grey";
		// case ColorGreen_Value:            return "Green";
		case ColorGreenYellow_Value:      return "Green Yellow";
		case ColorHoneydew_Value:         return "Honeydew";
		case ColorHotPink_Value:          return "Hot Pink";
		case ColorIndianRed_Value:        return "Indian Red";
		case ColorIndigo_Value:           return "Indigo";
		case ColorIvory_Value:            return "Ivory";
		case ColorIvoryBlack_Value:       return "Ivory Black";
		case ColorKhaki_Value:            return "Khaki";
		case ColorLavender_Value:         return "Lavender";
		case ColorLawnGreen_Value:        return "Lawn Green";
		case ColorLemonChiffon_Value:     return "Lemon Chiffon";
		case ColorLightBlue_Value:        return "Light Blue";
		case ColorLightCoral_Value:       return "Light Coral";
		case ColorLightCyan_Value:        return "Light Cyan";
		case ColorLightGoldenRod_Value:   return "Light Golden Rod";
		case ColorLightGreen_Value:       return "Light Green";
		case ColorLightGrey_Value:        return "Light Grey";
		case ColorLightPink_Value:        return "Light Pink";
		case ColorLightSalmon_Value:      return "Light Salmon";
		case ColorLightSeaGreen_Value:    return "Light Sea Green";
		case ColorLightSkyBlue_Value:     return "Light Sky Blue";
		case ColorLightSlateBlue_Value:   return "Light Slate Blue";
		case ColorLightSlateGrey_Value:   return "Light Slate Grey";
		case ColorLightSteelBlue_Value:   return "Light Steel Blue";
		case ColorLightYellow_Value:      return "Light Yellow";
		case ColorLime_Value:             return "Lime";
		case ColorLimeGreen_Value:        return "Lime Green";
		case ColorLinen_Value:            return "Linen";
		case ColorMagenta_Value:          return "Magenta";
		case ColorManganeseBlue_Value:    return "Manganese Blue";
		case ColorMaroon_Value:           return "Maroon";
		case ColorMediumOrchid_Value:     return "Medium Orchid";
		case ColorMediumPurple_Value:     return "Medium Purple";
		case ColorMediumSeaGreen_Value:   return "Medium Sea Green";
		case ColorMediumSlateBlue_Value:  return "Medium Slate Blue";
		case ColorMediumSpringGreen_Value:return "Medium Spring Green";
		case ColorMediumTurquoise_Value:  return "Medium Turquoise";
		case ColorMediumVioletRed_Value:  return "Medium Violet Red";
		case ColorMelon_Value:            return "Melon";
		case ColorMidnightBlue_Value:     return "Midnight Blue";
		case ColorMint_Value:             return "Mint";
		case ColorMintCream_Value:        return "Mint Cream";
		case ColorMistyRose_Value:        return "Misty Rose";
		case ColorMoccasin_Value:         return "Moccasin";
		case ColorNavajoWhite_Value:      return "Navajo White";
		case ColorNavy_Value:             return "Navy";
		case ColorOldLace_Value:          return "Old Lace";
		case ColorOlive_Value:            return "Olive";
		case ColorOliveDrab_Value:        return "Olive Drab";
		// case ColorOrange_Value:           return "Orange";
		case ColorOrangeRed_Value:        return "Orange Red";
		case ColorOrchid_Value:           return "Orchid";
		case ColorPaleGoldenRod_Value:    return "Pale Golden Rod";
		case ColorPaleGreen_Value:        return "Pale Green";
		case ColorPaleTurquoise_Value:    return "Pale Turquoise";
		case ColorPaleVioletRed_Value:    return "Pale Violet Red";
		case ColorPapayaWhip_Value:       return "Papaya Whip";
		case ColorPeachPuff_Value:        return "Peach Puff";
		case ColorPeacock_Value:          return "Peacock";
		case ColorPeru_Value:             return "Peru";
		case ColorPink_Value:             return "Pink";
		case ColorPlum_Value:             return "Plum";
		case ColorPowderBlue_Value:       return "Powder Blue";
		// case ColorPurple_Value:           return "Purple";
		case ColorRaspberry_Value:        return "Raspberry";
		case ColorRawSienna_Value:        return "Raw Sienna";
		// case ColorRed_Value:              return "Red";
		case ColorRosyBrown_Value:        return "Rosy Brown";
		case ColorRoyalBlue_Value:        return "Royal Blue";
		case ColorSaddleBrown_Value:      return "Saddle Brown";
		case ColorSalmon_Value:           return "Salmon";
		case ColorSandyBrown_Value:       return "Sandy Brown";
		case ColorSapGreen_Value:         return "Sap Green";
		case ColorSeaGreen_Value:         return "Sea Green";
		case ColorSeashell_Value:         return "Seashell";
		case ColorSepia_Value:            return "Sepia";
		case ColorSienna_Value:           return "Sienna";
		case ColorSilver_Value:           return "Silver";
		case ColorSkyBlue_Value:          return "Sky Blue";
		case ColorSlateBlue_Value:        return "Slate Blue";
		case ColorSlateGrey_Value:        return "Slate Grey";
		case ColorSnow_Value:             return "Snow";
		case ColorSpringGreen_Value:      return "Spring Green";
		case ColorSteelBlue_Value:        return "Steel Blue";
		case ColorSunlight_Value:         return "Sunlight";
		case ColorTan_Value:              return "Tan";
		case ColorTeal_Value:             return "Teal";
		case ColorThistle_Value:          return "Thistle";
		case ColorTomato_Value:           return "Tomato";
		case ColorTurquoise_Value:        return "Turquoise";
		case ColorTurquoiseBlue_Value:    return "Turquoise Blue";
		case ColorViolet_Value:           return "Violet";
		case ColorVioletRed_Value:        return "Violet Red";
		case ColorWarmGrey_Value:         return "Warm Grey";
		case ColorWheat_Value:            return "Wheat";
		// case ColorWhite_Value:            return "White";
		case ColorWhiteSmoke_Value:       return "White Smoke";
		// case ColorYellow_Value:           return "Yellow";
		case ColorYellowGreen_Value:      return "Yellow Green";
		
		default: 						return "Unknown";
	}
}
const char* GetColorName(Color_t color)
{
	return GetColorName(color.value);
}

Color_t GetNamedColorByIndex(u32 index)
{
	switch (index)
	{
		case 0:   return ColorAliceBlue;
		case 1:   return ColorAntiqueWhite;
		case 2:   return ColorAquamarine;
		case 3:   return ColorAzure;
		case 4:   return ColorBanana;
		case 5:   return ColorBeet;
		case 6:   return ColorBeige;
		case 7:   return ColorBisque;
		case 8:   return ColorBlack;
		case 9:   return ColorBlancheDalmond;
		case 10:  return ColorBlue;
		case 11:  return ColorBlueViolet;
		case 12:  return ColorBrick;
		case 13:  return ColorBrown;
		case 14:  return ColorBurlyWood;
		case 15:  return ColorBurntSienna;
		case 16:  return ColorBurnTumber;
		case 17:  return ColorCadetBlue;
		case 18:  return ColorCadmiumOrange;
		case 19:  return ColorCadmiumYellow;
		case 20:  return ColorCarrot;
		case 21:  return ColorChartreuse;
		case 22:  return ColorChocolate;
		case 23:  return ColorCobalt;
		case 24:  return ColorCobaltGreen;
		case 25:  return ColorColdGrey;
		case 26:  return ColorCoral;
		case 27:  return ColorCornflowerBlue;
		case 28:  return ColorCornSilk;
		case 29:  return ColorCrimson;
		case 30:  return ColorCyan;
		case 31:  return ColorDarkCyan;
		case 32:  return ColorDarkGoldenRod;
		case 33:  return ColorDarkGrey;
		case 34:  return ColorDarkGreen;
		case 35:  return ColorDarkKhaki;
		case 36:  return ColorDarkOliveGreen;
		case 37:  return ColorDarkOrange;
		case 38:  return ColorDarkOrchid;
		case 39:  return ColorDarkRed;
		case 40:  return ColorDarkSalmon;
		case 41:  return ColorDarkSeaGreen;
		case 42:  return ColorDarkSlateBlue;
		case 43:  return ColorDarkSlateGrey;
		case 44:  return ColorDarkTurquoise;
		case 45:  return ColorDarkViolet;
		case 46:  return ColorDeepPink;
		case 47:  return ColorDeepSkyBlue;
		case 48:  return ColorDimGrey;
		case 49:  return ColorDodgerBlue;
		case 50:  return ColorEggShell;
		case 51:  return ColorEmeraldGreen;
		case 52:  return ColorFireBrick;
		case 53:  return ColorFlesh;
		case 54:  return ColorFloralWhite;
		case 55:  return ColorForestGreen;
		case 56:  return ColorGainsboro;
		case 57:  return ColorGhostWhite;
		case 58:  return ColorGold;
		case 59:  return ColorGoldenRod;
		case 60:  return ColorGrey;
		case 61:  return ColorGreen;
		case 62:  return ColorGreenYellow;
		case 63:  return ColorHoneydew;
		case 64:  return ColorHotPink;
		case 65:  return ColorIndianRed;
		case 66:  return ColorIndigo;
		case 67:  return ColorIvory;
		case 68:  return ColorIvoryBlack;
		case 69:  return ColorKhaki;
		case 70:  return ColorLavender;
		case 71:  return ColorLawnGreen;
		case 72:  return ColorLemonChiffon;
		case 73:  return ColorLightBlue;
		case 74:  return ColorLightCoral;
		case 75:  return ColorLightCyan;
		case 76:  return ColorLightGoldenRod;
		case 77:  return ColorLightGreen;
		case 78:  return ColorLightGrey;
		case 79:  return ColorLightPink;
		case 80:  return ColorLightSalmon;
		case 81:  return ColorLightSeaGreen;
		case 82:  return ColorLightSkyBlue;
		case 83:  return ColorLightSlateBlue;
		case 84:  return ColorLightSlateGrey;
		case 85:  return ColorLightSteelBlue;
		case 86:  return ColorLightYellow;
		case 87:  return ColorLime;
		case 88:  return ColorLimeGreen;
		case 89:  return ColorLinen;
		case 90:  return ColorMagenta;
		case 91:  return ColorManganeseBlue;
		case 92:  return ColorMaroon;
		case 93:  return ColorMediumOrchid;
		case 94:  return ColorMediumPurple;
		case 95:  return ColorMediumSeaGreen;
		case 96:  return ColorMediumSlateBlue;
		case 97:  return ColorMediumSpringGreen;
		case 98:  return ColorMediumTurquoise;
		case 99:  return ColorMediumVioletRed;
		case 100: return ColorMelon;
		case 101: return ColorMidnightBlue;
		case 102: return ColorMint;
		case 103: return ColorMintCream;
		case 104: return ColorMistyRose;
		case 105: return ColorMoccasin;
		case 106: return ColorNavajoWhite;
		case 107: return ColorNavy;
		case 108: return ColorOldLace;
		case 109: return ColorOlive;
		case 110: return ColorOliveDrab;
		case 111: return ColorOrange;
		case 112: return ColorOrangeRed;
		case 113: return ColorOrchid;
		case 114: return ColorPaleGoldenRod;
		case 115: return ColorPaleGreen;
		case 116: return ColorPaleTurquoise;
		case 117: return ColorPaleVioletRed;
		case 118: return ColorPapayaWhip;
		case 119: return ColorPeachPuff;
		case 120: return ColorPeacock;
		case 121: return ColorPeru;
		case 122: return ColorPink;
		case 123: return ColorPlum;
		case 124: return ColorPowderBlue;
		case 125: return ColorPurple;
		case 126: return ColorRaspberry;
		case 127: return ColorRawSienna;
		case 128: return ColorRed;
		case 129: return ColorRosyBrown;
		case 130: return ColorRoyalBlue;
		case 131: return ColorSaddleBrown;
		case 132: return ColorSalmon;
		case 133: return ColorSandyBrown;
		case 134: return ColorSapGreen;
		case 135: return ColorSeaGreen;
		case 136: return ColorSeashell;
		case 137: return ColorSepia;
		case 138: return ColorSienna;
		case 139: return ColorSilver;
		case 140: return ColorSkyBlue;
		case 141: return ColorSlateBlue;
		case 142: return ColorSlateGrey;
		case 143: return ColorSnow;
		case 144: return ColorSpringGreen;
		case 145: return ColorSteelBlue;
		case 146: return ColorSunlight;
		case 147: return ColorTan;
		case 148: return ColorTeal;
		case 149: return ColorThistle;
		case 150: return ColorTomato;
		case 151: return ColorTurquoise;
		case 152: return ColorTurquoiseBlue;
		case 153: return ColorViolet;
		case 154: return ColorVioletRed;
		case 155: return ColorWarmGrey;
		case 156: return ColorWheat;
		case 157: return ColorWhite;
		case 158: return ColorWhiteSmoke;
		case 159: return ColorYellow;
		case 160: return ColorYellowGreen;
		default: return Black;
	}
}
Color_t GetPredefPalColorByIndex(u32 index)
{
	switch (index % NUM_PREDEF_PAL_COLORS)
	{
		case 0:  return PalRed;
		case 1:  return PalOrange;
		case 2:  return PalYellow;
		case 3:  return PalGreen;
		case 4:  return PalBlue;
		case 5:  return PalPurple;
		case 6:  return PalNeutral;
		
		case 7:  return PalRedLight;
		case 8:  return PalOrangeLight;
		case 9:  return PalYellowLight;
		case 10: return PalGreenLight;
		case 11: return PalBlueLight;
		case 12: return PalPurpleLight;
		case 13: return PalNeutralLight;
		
		case 14: return PalRedDark;
		case 15: return PalOrangeDark;
		case 16: return PalYellowDark;
		case 17: return PalGreenDark;
		case 18: return PalBlueDark;
		case 19: return PalPurpleDark;
		case 20: return PalNeutralDark;
		
		case 21: return PalPink;
		case 22: return PalHoney;
		case 23: return PalBanana;
		case 24: return PalGrass;
		case 25: return PalSky;
		case 26: return PalViolet;
		case 27: return PalGrey;
		
		case 28: return PalPinkLight;
		case 29: return PalHoneyLight;
		case 30: return PalBananaLight;
		case 31: return PalGrassLight;
		case 32: return PalSkyLight;
		case 33: return PalVioletLight;
		case 34: return PalGreyLight;
		
		case 35: return PalPinkDark;
		case 36: return PalHoneyDark;
		case 37: return PalBananaDark;
		case 38: return PalGrassDark;
		case 39: return PalSkyDark;
		case 40: return PalVioletDark;
		case 41: return PalGreyDark;
		
		case 42: return PalRedLighter;
		case 43: return PalOrangeLighter;
		case 44: return PalYellowLighter;
		case 45: return PalGreenLighter;
		case 46: return PalBlueLighter;
		case 47: return PalPurpleLighter;
		case 48: return PalNeutralLighter;
		
		case 49: return PalPinkLighter;
		case 50: return PalHoneyLighter;
		case 51: return PalBananaLighter;
		case 52: return PalGrassLighter;
		case 53: return PalSkyLighter;
		case 54: return PalVioletLighter;
		case 55: return PalGreyLighter;
		
		case 56: return PalRedDarker;
		case 57: return PalOrangeDarker;
		case 58: return PalYellowDarker;
		case 59: return PalGreenDarker;
		case 60: return PalBlueDarker;
		case 61: return PalPurpleDarker;
		case 62: return PalNeutralDarker;
		
		case 63: return PalPinkDarker;
		case 64: return PalHoneyDarker;
		case 65: return PalBananaDarker;
		case 66: return PalGrassDarker;
		case 67: return PalSkyDarker;
		case 68: return PalVioletDarker;
		case 69: return PalGreyDarker;
		
		default: return Black;
	}
}
Color_t GetGreyscaleColorByIndex(u32 index)
{
	switch (index % NUM_PREDEF_GREYSCALE_COLORS)
	{
		case 0:  return Black;
		case 1:  return Grey1;
		case 2:  return Grey2;
		case 3:  return Grey3;
		case 4:  return Grey4;
		case 5:  return Grey5;
		case 6:  return Grey6;
		case 7:  return Grey7;
		case 8:  return Grey8;
		case 9:  return Grey9;
		case 10: return Grey10;
		case 11: return Grey11;
		case 12: return Grey12;
		case 13: return Grey13;
		case 14: return Grey14;
		case 15: return White;
		default: return Black;
	}
}

u32 GetNamedColorIndex(u32 colorValue)
{
	switch (colorValue)
	{
		case ColorAliceBlue_Value:         return 0;
		case ColorAntiqueWhite_Value:      return 1;
		case ColorAquamarine_Value:        return 2;
		case ColorAzure_Value:             return 3;
		case ColorBanana_Value:            return 4;
		case ColorBeet_Value:              return 5;
		case ColorBeige_Value:             return 6;
		case ColorBisque_Value:            return 7;
		case ColorBlack_Value:             return 8;
		case ColorBlancheDalmond_Value:    return 9;
		case ColorBlue_Value:              return 10;
		case ColorBlueViolet_Value:        return 11;
		case ColorBrick_Value:             return 12;
		case ColorBrown_Value:             return 13;
		case ColorBurlyWood_Value:         return 14;
		case ColorBurntSienna_Value:       return 15;
		case ColorBurnTumber_Value:        return 16;
		case ColorCadetBlue_Value:         return 17;
		case ColorCadmiumOrange_Value:     return 18;
		case ColorCadmiumYellow_Value:     return 19;
		case ColorCarrot_Value:            return 20;
		case ColorChartreuse_Value:        return 21;
		case ColorChocolate_Value:         return 22;
		case ColorCobalt_Value:            return 23;
		case ColorCobaltGreen_Value:       return 24;
		case ColorColdGrey_Value:          return 25;
		case ColorCoral_Value:             return 26;
		case ColorCornflowerBlue_Value:    return 27;
		case ColorCornSilk_Value:          return 28;
		case ColorCrimson_Value:           return 29;
		case ColorCyan_Value:              return 30;
		case ColorDarkCyan_Value:          return 31;
		case ColorDarkGoldenRod_Value:     return 32;
		case ColorDarkGrey_Value:          return 33;
		case ColorDarkGreen_Value:         return 34;
		case ColorDarkKhaki_Value:         return 35;
		case ColorDarkOliveGreen_Value:    return 36;
		case ColorDarkOrange_Value:        return 37;
		case ColorDarkOrchid_Value:        return 38;
		case ColorDarkRed_Value:           return 39;
		case ColorDarkSalmon_Value:        return 40;
		case ColorDarkSeaGreen_Value:      return 41;
		case ColorDarkSlateBlue_Value:     return 42;
		case ColorDarkSlateGrey_Value:     return 43;
		case ColorDarkTurquoise_Value:     return 44;
		case ColorDarkViolet_Value:        return 45;
		case ColorDeepPink_Value:          return 46;
		case ColorDeepSkyBlue_Value:       return 47;
		case ColorDimGrey_Value:           return 48;
		case ColorDodgerBlue_Value:        return 49;
		case ColorEggShell_Value:          return 50;
		case ColorEmeraldGreen_Value:      return 51;
		case ColorFireBrick_Value:         return 52;
		case ColorFlesh_Value:             return 53;
		case ColorFloralWhite_Value:       return 54;
		case ColorForestGreen_Value:       return 55;
		case ColorGainsboro_Value:         return 56;
		case ColorGhostWhite_Value:        return 57;
		case ColorGold_Value:              return 58;
		case ColorGoldenRod_Value:         return 59;
		case ColorGrey_Value:              return 60;
		case ColorGreen_Value:             return 61;
		case ColorGreenYellow_Value:       return 62;
		case ColorHoneydew_Value:          return 63;
		case ColorHotPink_Value:           return 64;
		case ColorIndianRed_Value:         return 65;
		case ColorIndigo_Value:            return 66;
		case ColorIvory_Value:             return 67;
		case ColorIvoryBlack_Value:        return 68;
		case ColorKhaki_Value:             return 69;
		case ColorLavender_Value:          return 70;
		case ColorLawnGreen_Value:         return 71;
		case ColorLemonChiffon_Value:      return 72;
		case ColorLightBlue_Value:         return 73;
		case ColorLightCoral_Value:        return 74;
		case ColorLightCyan_Value:         return 75;
		case ColorLightGoldenRod_Value:    return 76;
		case ColorLightGreen_Value:        return 77;
		case ColorLightGrey_Value:         return 78;
		case ColorLightPink_Value:         return 79;
		case ColorLightSalmon_Value:       return 80;
		case ColorLightSeaGreen_Value:     return 81;
		case ColorLightSkyBlue_Value:      return 82;
		case ColorLightSlateBlue_Value:    return 83;
		case ColorLightSlateGrey_Value:    return 84;
		case ColorLightSteelBlue_Value:    return 85;
		case ColorLightYellow_Value:       return 86;
		case ColorLime_Value:              return 87;
		case ColorLimeGreen_Value:         return 88;
		case ColorLinen_Value:             return 89;
		case ColorMagenta_Value:           return 90;
		case ColorManganeseBlue_Value:     return 91;
		case ColorMaroon_Value:            return 92;
		case ColorMediumOrchid_Value:      return 93;
		case ColorMediumPurple_Value:      return 94;
		case ColorMediumSeaGreen_Value:    return 95;
		case ColorMediumSlateBlue_Value:   return 96;
		case ColorMediumSpringGreen_Value: return 97;
		case ColorMediumTurquoise_Value:   return 98;
		case ColorMediumVioletRed_Value:   return 99;
		case ColorMelon_Value:             return 100;
		case ColorMidnightBlue_Value:      return 101;
		case ColorMint_Value:              return 102;
		case ColorMintCream_Value:         return 103;
		case ColorMistyRose_Value:         return 104;
		case ColorMoccasin_Value:          return 105;
		case ColorNavajoWhite_Value:       return 106;
		case ColorNavy_Value:              return 107;
		case ColorOldLace_Value:           return 108;
		case ColorOlive_Value:             return 109;
		case ColorOliveDrab_Value:         return 110;
		case ColorOrange_Value:            return 111;
		case ColorOrangeRed_Value:         return 112;
		case ColorOrchid_Value:            return 113;
		case ColorPaleGoldenRod_Value:     return 114;
		case ColorPaleGreen_Value:         return 115;
		case ColorPaleTurquoise_Value:     return 116;
		case ColorPaleVioletRed_Value:     return 117;
		case ColorPapayaWhip_Value:        return 118;
		case ColorPeachPuff_Value:         return 119;
		case ColorPeacock_Value:           return 120;
		case ColorPeru_Value:              return 121;
		case ColorPink_Value:              return 122;
		case ColorPlum_Value:              return 123;
		case ColorPowderBlue_Value:        return 124;
		case ColorPurple_Value:            return 125;
		case ColorRaspberry_Value:         return 126;
		case ColorRawSienna_Value:         return 127;
		case ColorRed_Value:               return 128;
		case ColorRosyBrown_Value:         return 129;
		case ColorRoyalBlue_Value:         return 130;
		case ColorSaddleBrown_Value:       return 131;
		case ColorSalmon_Value:            return 132;
		case ColorSandyBrown_Value:        return 133;
		case ColorSapGreen_Value:          return 134;
		case ColorSeaGreen_Value:          return 135;
		case ColorSeashell_Value:          return 136;
		case ColorSepia_Value:             return 137;
		case ColorSienna_Value:            return 138;
		case ColorSilver_Value:            return 139;
		case ColorSkyBlue_Value:           return 140;
		case ColorSlateBlue_Value:         return 141;
		case ColorSlateGrey_Value:         return 142;
		case ColorSnow_Value:              return 143;
		case ColorSpringGreen_Value:       return 144;
		case ColorSteelBlue_Value:         return 145;
		case ColorSunlight_Value:          return 146;
		case ColorTan_Value:               return 147;
		case ColorTeal_Value:              return 148;
		case ColorThistle_Value:           return 149;
		case ColorTomato_Value:            return 150;
		case ColorTurquoise_Value:         return 151;
		case ColorTurquoiseBlue_Value:     return 152;
		case ColorViolet_Value:            return 153;
		case ColorVioletRed_Value:         return 154;
		case ColorWarmGrey_Value:          return 155;
		case ColorWheat_Value:             return 156;
		case ColorWhite_Value:             return 157;
		case ColorWhiteSmoke_Value:        return 158;
		case ColorYellow_Value:            return 159;
		case ColorYellowGreen_Value:       return 160;
		default: return NUM_PREDEF_NAMED_COLORS;
	}
}
u32 GetNamedColorIndex(Color_t color)
{
	return GetNamedColorIndex(color.value);
}

u32 GetPredefPalColorIndex(u32 colorValue)
{
	switch (colorValue)
	{
		case PalRed_Value:            return 0;
		case PalOrange_Value:         return 1;
		case PalYellow_Value:         return 2;
		case PalGreen_Value:          return 3;
		case PalBlue_Value:           return 4;
		case PalPurple_Value:         return 5;
		case PalNeutral_Value:        return 6;
		case PalRedLight_Value:       return 7;
		case PalOrangeLight_Value:    return 8;
		case PalYellowLight_Value:    return 9;
		case PalGreenLight_Value:     return 10;
		case PalBlueLight_Value:      return 11;
		case PalPurpleLight_Value:    return 12;
		case PalNeutralLight_Value:   return 13;
		case PalRedDark_Value:        return 14;
		case PalOrangeDark_Value:     return 15;
		case PalYellowDark_Value:     return 16;
		case PalGreenDark_Value:      return 17;
		case PalBlueDark_Value:       return 18;
		case PalPurpleDark_Value:     return 19;
		case PalNeutralDark_Value:    return 20;
		case PalPink_Value:           return 21;
		case PalHoney_Value:          return 22;
		case PalBanana_Value:         return 23;
		case PalGrass_Value:          return 24;
		case PalSky_Value:            return 25;
		case PalViolet_Value:         return 26;
		case PalGrey_Value:           return 27;
		case PalPinkLight_Value:      return 28;
		case PalHoneyLight_Value:     return 29;
		case PalBananaLight_Value:    return 30;
		case PalGrassLight_Value:     return 31;
		case PalSkyLight_Value:       return 32;
		case PalVioletLight_Value:    return 33;
		case PalGreyLight_Value:      return 34;
		case PalPinkDark_Value:       return 35;
		case PalHoneyDark_Value:      return 36;
		case PalBananaDark_Value:     return 37;
		case PalGrassDark_Value:      return 38;
		case PalSkyDark_Value:        return 39;
		case PalVioletDark_Value:     return 40;
		case PalGreyDark_Value:       return 41;
		case PalRedLighter_Value:     return 42;
		case PalOrangeLighter_Value:  return 43;
		case PalYellowLighter_Value:  return 44;
		case PalGreenLighter_Value:   return 45;
		case PalBlueLighter_Value:    return 46;
		case PalPurpleLighter_Value:  return 47;
		case PalNeutralLighter_Value: return 48;
		case PalPinkLighter_Value:    return 49;
		case PalHoneyLighter_Value:   return 50;
		case PalBananaLighter_Value:  return 51;
		case PalGrassLighter_Value:   return 52;
		case PalSkyLighter_Value:     return 53;
		case PalVioletLighter_Value:  return 54;
		case PalGreyLighter_Value:    return 55;
		case PalRedDarker_Value:      return 56;
		case PalOrangeDarker_Value:   return 57;
		case PalYellowDarker_Value:   return 58;
		case PalGreenDarker_Value:    return 59;
		case PalBlueDarker_Value:     return 60;
		case PalPurpleDarker_Value:   return 61;
		case PalNeutralDarker_Value:  return 62;
		case PalPinkDarker_Value:     return 63;
		case PalHoneyDarker_Value:    return 64;
		case PalBananaDarker_Value:   return 65;
		case PalGrassDarker_Value:    return 66;
		case PalSkyDarker_Value:      return 67;
		case PalVioletDarker_Value:   return 68;
		case PalGreyDarker_Value:     return 69;
		default: return NUM_PREDEF_PAL_COLORS;
	}
}
u32 GetPredefPalColorIndex(Color_t color)
{
	return GetPredefPalColorIndex(color.value);
}

u32 GetGreyscaleColorIndex(u32 colorValue)
{
	switch (colorValue)
	{
		case Black_Value:  return 0;
		case Grey1_Value:  return 1;
		case Grey2_Value:  return 2;
		case Grey3_Value:  return 3;
		case Grey4_Value:  return 4;
		case Grey5_Value:  return 5;
		case Grey6_Value:  return 6;
		case Grey7_Value:  return 7;
		case Grey8_Value:  return 8;
		case Grey9_Value:  return 9;
		case Grey10_Value: return 10;
		case Grey11_Value: return 11;
		case Grey12_Value: return 12;
		case Grey13_Value: return 13;
		case Grey14_Value: return 14;
		case White_Value:  return 15;
		default: return NUM_PREDEF_GREYSCALE_COLORS;
	}
}
u32 GetGreyscaleColorIndex(Color_t color)
{
	return GetGreyscaleColorIndex(color.value);
}

#endif //_COLORS_H