/*
File:   my_dynamicArray.h
Author: Taylor Robbins
Date:   03\21\2019
Description:
	** Holds a data structure and related functions to allow you
	** to easily create a dynamically growing array on any memory arena
*/

#ifndef _MY_DYNAMIC_ARRAY_H
#define _MY_DYNAMIC_ARRAY_H

struct DynArray_t
{
	MemoryArena_t* allocArena;
	u32 itemSize;
	u32 allocChunkSize;
	
	u32 length;
	u32 allocLength;
	void* items;
};

void DestroyDynamicArray(DynArray_t* array)
{
	Assert(array != nullptr);
	if (array->items != nullptr)
	{
		Assert(ArenaCanPop(array->allocArena, array->items));
		ArenaPop(array->allocArena, array->items);
	}
	ClearPointer(array);
}

void CreateDynamicArray(DynArray_t* array, MemoryArena_t* memArena, u32 itemSize, u32 allocChunkSize = 16, u32 initialSizeRequirement = 0)
{
	Assert(array != nullptr);
	Assert(memArena != nullptr);
	Assert(itemSize > 0);
	Assert(allocChunkSize > 0);
	
	ClearPointer(array);
	array->allocArena = memArena;
	array->itemSize = itemSize;
	array->allocChunkSize = allocChunkSize;
	
	array->length = 0;
	array->allocLength = array->allocChunkSize;
	while (array->allocLength < initialSizeRequirement) { array->allocLength += array->allocChunkSize; }
	array->items = ArenaPush(array->allocArena, array->allocLength * array->itemSize);
	Assert(array->items != nullptr);
}

void DynArrayExpand(DynArray_t* array, u32 lengthRequired)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	if (array->allocLength >= lengthRequired) { return; }
	
	u32 newSpaceLength = array->allocLength;
	while (newSpaceLength < lengthRequired) { newSpaceLength += array->allocChunkSize; }
	//TODO: Make this a ArenaRealloc call?
	void* newSpace = ArenaPush(array->allocArena, array->itemSize * newSpaceLength);
	Assert(newSpace != nullptr);
	if (array->items != nullptr)
	{
		if (array->length > 0)
		{
			MyMemCopy(newSpace, array->items, array->itemSize * array->length);
		}
		if (ArenaCanPop(array->allocArena, array->items)) { ArenaPop(array->allocArena, array->items); }
	}
	array->items = newSpace;
	array->allocLength = newSpaceLength;
}

void* DynArrayAdd_(DynArray_t* array)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	
	DynArrayExpand(array, array->length + 1);
	Assert(array->length < array->allocLength);
	Assert(array->items != nullptr);
	
	void* result = (void*)((u8*)array->items + (array->itemSize * array->length));
	array->length++;
	
	return result;
}
#define DynArrayAdd(arrayPntr, type) ((type*)DynArrayAdd_(arrayPntr))

void* DynArrayAddRange_(DynArray_t* array, u32 numItems)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	
	DynArrayExpand(array, array->length + numItems);
	Assert(array->length + numItems <= array->allocLength);
	Assert(array->items != nullptr);
	
	void* result = (void*)((u8*)array->items + (array->itemSize * array->length));
	array->length += numItems;
	
	return result;
}
#define DynArrayAddRange(arrayPntr, type, numItems) ((type*)DynArrayAddRange_(arrayPntr, numItems))

void* DynArrayGet_(DynArray_t* array, u32 index)
{
	Assert(array != nullptr);
	if (array->items == nullptr) { return nullptr; }
	if (index >= array->length) { return nullptr; }
	
	return (void*)((u8*)array->items + (array->itemSize * index));
}
const void* DynArrayGet_(const DynArray_t* array, u32 index) //const variant
{
	Assert(array != nullptr);
	if (array->items == nullptr) { return nullptr; }
	if (index >= array->length) { return nullptr; }
	
	return (const void*)((const u8*)array->items + (array->itemSize * index));
}
#define DynArrayGet(arrayPntr, type, index) ((type*)DynArrayGet_(arrayPntr, index))

void* DynArrayInsert_(DynArray_t* array, u32 index)
{
	Assert(array != nullptr);
	Assert(array->allocArena != nullptr);
	Assert(index <= array->length);
	if (index == array->length) { return DynArrayAdd_(array); } //degenerative case
	
	DynArrayExpand(array, array->length + 1);
	Assert(array->length < array->allocLength);
	Assert(array->items != nullptr);
	
	array->length++;
	for (u32 iIndex = array->length-1; iIndex > index; iIndex--)
	{
		MyMemCopy(DynArrayGet_(array, iIndex), DynArrayGet_(array, iIndex-1), array->itemSize);
	}
	
	void* result = (void*)((u8*)array->items + (array->itemSize * index));
	return result;
}
#define DynArrayInsert(arrayPntr, type, index) ((type*)DynArrayInsert_(arrayPntr, index))

bool DynArrayRemove(DynArray_t* array, u32 index)
{
	Assert(array != nullptr);
	if (array->items == nullptr) { return false; }
	if (index >= array->length) { return false; }
	
	u8* bytePntr = (u8*)array->items;
	for (u32 iIndex = index; iIndex+1 < array->length; iIndex++)
	{
		MyMemCopy(&bytePntr[iIndex * array->itemSize], &bytePntr[(iIndex+1) * array->itemSize], array->itemSize);
	}
	array->length--;
	
	return true;
}

bool DynArrayRemoveRegion(DynArray_t* array, u32 startIndex, u32 numItems)
{
	Assert(array != nullptr);
	if (numItems == 0) { return true; }
	if (array->items == nullptr) { return false; }
	if (startIndex + numItems > array->length) { return false; }
	
	u8* bytePntr = (u8*)array->items;
	for (u32 iIndex = startIndex; iIndex+numItems < array->length; iIndex++)
	{
		MyMemCopy(&bytePntr[iIndex * array->itemSize], &bytePntr[(iIndex+numItems) * array->itemSize], array->itemSize);
	}
	array->length -= numItems;
	
	return true;
}
void DynArrayClear(DynArray_t* array)
{
	Assert(array != nullptr);
	if (array->length == 0) { return; }
	Assert(array->items != nullptr);
	DynArrayRemoveRegion(array, 0, array->length);
}


bool IsPntrInDynArray(const DynArray_t* array, const void* pointer)
{
	Assert(array != nullptr);
	if (pointer == nullptr) { return false; }
	if (array->items == nullptr) { return false; }
	if (array->length == 0) { return false; }
	u8* basePntr = (u8*)array->items;
	u8* bytePntr = (u8*)pointer;
	return (bytePntr >= basePntr && bytePntr < basePntr + (array->length*array->itemSize));
}

u32 GetDynArrayItemIndex(const DynArray_t* array, const void* pointer)
{
	Assert(IsPntrInDynArray(array, pointer));
	u8* basePntr = (u8*)array->items;
	u8* bytePntr = (u8*)pointer;
	u32 offset = (u32)(bytePntr - basePntr);
	Assert((offset % array->itemSize) == 0);
	Assert(offset / array->itemSize < array->length);
	return (offset / array->itemSize);
}

#ifdef _MY_SORTING_H 
void DynArraySort(DynArray_t* array, CompareFunc_f* compareFunc, void* contextPntr = nullptr)
{
	Assert(array != nullptr);
	Assert(compareFunc != nullptr);
	void* workingSpace = alloca(array->itemSize*2);
	Assert(workingSpace != nullptr);
	QuickSort(array->items, array->length, array->itemSize, workingSpace, compareFunc, contextPntr);
}
#endif

void DynArrayCopy(DynArray_t* dest, const DynArray_t* src, MemoryArena_t* memArena)
{
	NotNull(dest);
	NotNull(src);
	NotNull(memArena);
	CreateDynamicArray(dest, memArena, src->itemSize, src->allocChunkSize, src->length);
	if (src->length > 0)
	{
		void* newSpace = DynArrayAddRange_(dest, src->length);
		NotNull(newSpace);
		MyMemCopy(newSpace, src->items, src->itemSize * src->length);
	}
}

bool _DynArrayContains(const DynArray_t* array, u32 compareSize, const void* comparePntr)
{
	NotNull(array);
	NotNull(comparePntr);
	Assert(array->itemSize == compareSize);
	for (u32 iIndex = 0; iIndex < array->length; iIndex++)
	{
		const void* itemPntr = DynArrayGet_(array, iIndex);
		NotNull(itemPntr);
		if (MyMemCompare(itemPntr, comparePntr, compareSize) == 0) { return true; }
	}
	return false;
}
#define DynArrayContains(array, type, comparePntr) _DynArrayContains((array), sizeof(type), (comparePntr))

#endif //  _MY_DYNAMIC_ARRAY_H
