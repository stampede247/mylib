/*
File:   my_debug.h
Author: Taylor Robbins
Date:   04\02\2020
*/

#ifndef _MY_DEBUG_H
#define _MY_DEBUG_H

typedef enum
{
	DbgLevel_None = 0x00,
	DbgLevel_Debug,
	DbgLevel_Regular,
	DbgLevel_Info,
	DbgLevel_Notify,
	DbgLevel_Other,
	DbgLevel_Warning,
	DbgLevel_Error,
} DbgLevel_t;

typedef enum
{
	DbgFlags_None         = 0x00,
	DbgFlag_Inverted      = 0x01,
	DbgFlag_MyLib         = 0x02,
	DbgFlag_ExternalLib   = 0x04,
	// DbgFlag_Unused     = 0x08,
	// DbgFlag_App1       = 0x10,
	// DbgFlag_App2       = 0x20,
	// DbgFlag_App3       = 0x40,
	// DbgFlag_App4       = 0x80,
} DbgFlags_t;

typedef void MyLibDebugOutput_f(u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool newLine, const char* message);
typedef void MyLibDebugPrint_f(u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...);

static MyLibDebugOutput_f* MyLibDebugOutputFunc = nullptr;
static MyLibDebugPrint_f*  MyLibDebugPrintFunc  = nullptr;

const char* GetDbgLevelStr(DbgLevel_t dbgLevel)
{
	switch (dbgLevel)
	{
		case DbgLevel_None:    return "None";
		case DbgLevel_Debug:   return "Debug";
		case DbgLevel_Regular: return "Regular";
		case DbgLevel_Info:    return "Info";
		case DbgLevel_Notify:  return "Notify";
		case DbgLevel_Other:   return "Other";
		case DbgLevel_Warning: return "Warning";
		case DbgLevel_Error:   return "Error";
		default: return "Unknown";
	};
}

#define MyLibWriteAt(dbgLevel, message)                       MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, dbgLevel, false, message)
#define MyLibWriteLineAt(dbgLevel, message)                   MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, dbgLevel, true,  message)
#define MyLibPrintAt(dbgLevel, formatString, ...)             MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, dbgLevel, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLineAt(dbgLevel, formatString, ...)         MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, dbgLevel, true,  formatString, ##__VA_ARGS__)
#define MyLibWriteAtx(flags, dbgLevel, message)               MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, dbgLevel, false, message)
#define MyLibWriteLineAtx(flags, dbgLevel, message)           MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, dbgLevel, true,  message)
#define MyLibPrintAtx(flags, dbgLevel, formatString, ...)     MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, dbgLevel, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLineAtx(flags, dbgLevel, formatString, ...) MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, dbgLevel, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_D(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, message)
#define MyLibWriteLine_D(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  message)
#define MyLibPrint_D(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_D(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Dx(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Debug, false, message)
#define MyLibWriteLine_Dx(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  message)
#define MyLibPrint_Dx(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Dx(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_R(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, message)
#define MyLibWriteLine_R(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  message)
#define MyLibPrint_R(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_R(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Rx(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Regular, false, message)
#define MyLibWriteLine_Rx(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  message)
#define MyLibPrint_Rx(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Rx(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_I(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Info, false, message)
#define MyLibWriteLine_I(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  message)
#define MyLibPrint_I(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_I(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Ix(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Info, false, message)
#define MyLibWriteLine_Ix(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Info, true,  message)
#define MyLibPrint_Ix(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Ix(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_N(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, message)
#define MyLibWriteLine_N(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  message)
#define MyLibPrint_N(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_N(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Nx(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Notify, false, message)
#define MyLibWriteLine_Nx(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  message)
#define MyLibPrint_Nx(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Nx(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_O(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Other, false, message)
#define MyLibWriteLine_O(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  message)
#define MyLibPrint_O(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_O(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Ox(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Other, false, message)
#define MyLibWriteLine_Ox(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Other, true,  message)
#define MyLibPrint_Ox(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Ox(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_W(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, message)
#define MyLibWriteLine_W(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  message)
#define MyLibPrint_W(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_W(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Wx(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Warning, false, message)
#define MyLibWriteLine_Wx(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  message)
#define MyLibPrint_Wx(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Wx(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)

#define MyLibWrite_E(message)                         MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Error, false, message)
#define MyLibWriteLine_E(message)                     MyLibDebugOutputFunc(DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  message)
#define MyLibPrint_E(formatString, ...)               MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_E(formatString, ...)           MyLibDebugPrintFunc (DbgFlag_MyLib, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)
#define MyLibWrite_Ex(flags, message)                 MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Error, false, message)
#define MyLibWriteLine_Ex(flags, message)             MyLibDebugOutputFunc(((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Error, true,  message)
#define MyLibPrint_Ex(flags, formatString, ...)       MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define MyLibPrintLine_Ex(flags, formatString, ...)   MyLibDebugPrintFunc (((flags)|DbgFlag_MyLib), __FILE__, __LINE__, __func__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)

#endif //  _MY_DEBUG_H
